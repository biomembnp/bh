/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Definition of abstact class BHMinimizationAlgorithm

   This abstract lass acts as a wrapper for the minimization algorithms.

   @file BHminimizationAlgorithm .hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 12/11/2019
   @version 1.0

   base class is complete
*/

#include "BHminimizationAlgorithm.hpp"
namespace BH {
  BHMinimizationAlgorithm::BHMinimizationAlgorithm () = default;
  BHMinimizationAlgorithm::~BHMinimizationAlgorithm () = default;
  BHdouble BHMinimizationAlgorithm::MinimizeCluster (
    BHClusterAtoms &cluster, BHEnergyCalculator *calculator) {
    unsigned nat = cluster.getNofAtoms ();
    if (calculator->nAtoms () != nat) {
      throw "BHEnergyCalculator cannot minimize a cluster with different "
            "dimension from the one that initialized it";
    }
    BHdouble *x = new BHdouble[3 * nat];
    cluster.AssignToVector (x);
    BHdouble energy = Minimization (x, nat, calculator);
    cluster.AssignFromVector (x);
    delete[] x;
    return energy;
  }
  BHdouble BHMinimizationAlgorithm::MinimizeCluster_tol (
    BHClusterAtoms &cluster, BHEnergyCalculator *calculator) {
    unsigned nat = cluster.getNofAtoms ();
    if (calculator->nAtoms () != cluster.getNofAtoms ()) {
      throw "BHEnergyCalculator cannot minimize a cluster with different "
            "dimension from the one that initialized it";
    }
    BHdouble *x = new BHdouble[3 * nat];
    cluster.AssignToVector (x);
    BHdouble energy = Minimization_tol (x, nat, calculator);
    cluster.AssignFromVector (x);
    delete[] x;
    return energy;
  }

} // namespace BH
