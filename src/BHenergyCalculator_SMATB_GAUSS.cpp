/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Definition of class BHEnergyCalculator_SMATB_GAUSS
   @file BHenergyCalculator_SMATB_GAUSS.cpp
   @author Daniele Rapetti (iximiel@gmail.com)


   @date 10/07/20018
   @version 1.0

   adding gaussian to hte energy term
*/
#include <cmath>
#include <cstring>
#include <iomanip> // std::setprecision
#include <iostream>
#include <map>

#include "BHenergyCalculator_SMATB_GAUSS.hpp"
#include "BHmetalParameters.hpp"

// debugging:
using std::cout;
using std::endl;
namespace BH {
  BHEnergyCalculator_SMATB_GAUSS::BHEnergyCalculator_SMATB_GAUSS (
    const BHCluster &theCluster)
    : BHEnergyCalculator (theCluster),
      // gaussFact_(new BHdouble [nInteractions_]),
      sigma2_ (new BHdouble[nInteractions_]),
      twosigma2_ (
        new BHdouble[nInteractions_]), ///@todo: trasform this in 1/2sigma
      useGauss_ (new bool[nInteractions_]),
      den_ (new BHdouble[nAtoms_]),
      Fb_ (new BHdouble *[nAtoms_]),
      G_ (new BHdouble[nInteractions_]),
      gauss_dist_ (new BHdouble[nInteractions_]),
      p_ (new BHdouble[nInteractions_]),
      q_ (new BHdouble[nInteractions_]),
      a_ (new BHdouble[nInteractions_]),
      xi_ (new BHdouble[nInteractions_]),
      NNd_ (new BHdouble[nInteractions_]),
      cs_ (new BHdouble[nInteractions_]),
      ce_ (new BHdouble[nInteractions_]),
      ce2_ (new BHdouble[nInteractions_]),
      P5_ (new BHdouble[nInteractions_]),
      P4_ (new BHdouble[nInteractions_]),
      P3_ (new BHdouble[nInteractions_]),
      Q5_ (new BHdouble[nInteractions_]),
      Q4_ (new BHdouble[nInteractions_]),
      Q3_ (new BHdouble[nInteractions_]) {
    for (unsigned int j = 0; j < nAtoms_; ++j) {
      Fb_[j] = new BHdouble[nAtoms_];
    }

    for (auto corrispondence : INT_MP2ID) {
      int MPindex = corrispondence.first;
      int i = corrispondence.second;
      // std::cout << MPindex << " -> " << i << std::endl;

      if (
        BHMetalParameters::getBHMP ().getPairName (MPindex) == "AgPt" ||
        BHMetalParameters::getBHMP ().getPairName (MPindex) == "PtAg") {
        BHdouble sigma = 0.035 * 4.16; // 2.941564;
        sigma2_[i] = sigma * sigma;
        twosigma2_[i] = 2 * sigma2_[i];
        gauss_dist_[i] = 4.16; //+3.98)*0.5;
#ifdef _OPENMP
        G_[i] = 0.044;

#else
        G_[i] = 2 * 0.044;

#endif //_OPENMP

        useGauss_[i] = true;
        std::cout << "For "
                  << BHMetalParameters::getBHMP ().getPairName (MPindex)
                  << std::endl;
        std::cout << "sigma^2:\t" << sigma2_[i] << std::endl
                  << "gauss_dist:\t" << gauss_dist_[i] << std::endl
#ifdef _OPENMP
                  << "Gauss_fact:\t" << G_[i] << std::endl
#else
                  << "Gauss_fact:\t" << G_[i] / 2 << std::endl

#endif //_OPENMP
                  << ((useGauss_[i]) ? "true" : "false") << std::endl;

      } else {
        sigma2_[i] = 0;
        twosigma2_[i] = 2 * sigma2_[i];
        useGauss_[i] = false;
        gauss_dist_[i] = 0;
        G_[i] = 0;
      }

      BHMetalParameters::getBHMP ().giveMetalPars (
        MPindex, p_[i], q_[i], a_[i], xi_[i], NNd_[i], cs_[i], ce_[i]);
      ce2_[i] = ce_[i] * ce_[i];
      IntNeighDistTol2_[i] = (ce_[i] + 1.0) * (ce_[i] + 1.0);
      /*cout << '\n';
        std::cout << p_[i] << " " << q_[i] << " " << a_[i]
        << " " << xi_[i] << " " << NNd_[i]
        << " " << cs_[i] << " " << ce_[i] <<std::endl
        <<  ce2_[i] << " " << IntNeighDistTol2_[i] << std::endl;*/
      BHMetalParameters::getBHMP ().givePolyPars (
        MPindex, P5_[i], P4_[i], P3_[i], Q5_[i], Q4_[i], Q3_[i]);
    }
    std::cerr << "\033[0;35mWARNING: \033[0m\033[1mcurrently this build works "
                 "ONLY for AgPt; if compiled with OMP will fail the tol "
                 "calculations\033[0m"
              << std::endl;
  }

  BHEnergyCalculator_SMATB_GAUSS::~BHEnergyCalculator_SMATB_GAUSS () {
    delete[] sigma2_;
    delete[] twosigma2_;
    delete[] useGauss_;
    delete[] den_;

    for (unsigned int j = 0; j < nAtoms_; ++j) {
      delete[] Fb_[j];
    }

    delete[] Fb_;

    delete[] G_;
    delete[] gauss_dist_;
    delete[] p_;
    delete[] q_;
    delete[] a_;
    delete[] xi_;
    delete[] NNd_;
    delete[] cs_;
    delete[] ce_;
    delete[] ce2_;
    delete[] P5_;
    delete[] P4_;
    delete[] P3_;
    delete[] Q5_;
    delete[] Q4_;
    delete[] Q3_;
  }

  BHdouble BHEnergyCalculator_SMATB_GAUSS::Energy (BHdouble *x) {
    // std::vector<BHdouble> den(nAtoms_,0);
    memset (
      den_, 0,
      nAtoms_ * sizeof (den_)); // should empty the array efficiently
    BHdouble /*ebi,*/ eri;
    BHdouble xik, yik, zik, dik, dik2, x_i, y_i, z_i;
    BHdouble /*dik0,*/ espo, qsiexpq, aexpp, dikm, /*dikm2,*/ dikm3, dikm4,
      dikm5;
    BHdouble energy (0), gauss_mixed (0);
    unsigned int INTID, i, k, mytype, IDi, IDk;
    for (i = 0; i < nAtoms_; ++i) {
      eri = 0;
      // gauss_mixed=0;
      IDi = 3 * i;
      // BHdouble
      x_i = x[IDi];
      y_i = x[IDi + 1];
      z_i = x[IDi + 2];
      mytype = nTypes_ * typeList_[i];
      for (k = i + 1; k < nAtoms_; ++k) {
        // INTID=intID_[TAI(i,k)];
        INTID = intType_[mytype + typeList_[k]];
        // std :: cout <<mytype + typeList_[k]<<" "<< INTID << std::endl;
        // BHdouble
        IDk = 3 * k;
        xik = x[IDk] - x_i;
        yik = x[IDk + 1] - y_i;
        zik = x[IDk + 2] - z_i;
        // BHdouble
        dik2 = xik * xik + yik * yik + zik * zik;
        if (dik2 < ce2_[INTID]) {
          // BHdouble
          dik = sqrt (dik2);
          if (dik < cs_[INTID]) {
            // dik0=NNd_[INTID];
            espo = 1.0 - dik / NNd_[INTID]; // dik0;
            aexpp = exp (p_[INTID] * espo) * a_[INTID];
            eri += 2.0 * aexpp;
            qsiexpq = (xi_[INTID] * xi_[INTID]) * exp (2.0 * q_[INTID] * espo);
          } else {
            dikm = dik - ce_[INTID];
            // dikm2=dikm*dikm;
            dikm3 = dikm * dikm * dikm;
            dikm4 = dikm3 * dikm;
            dikm5 = dikm4 * dikm;
            qsiexpq =
              Q5_[INTID] * dikm5 + Q4_[INTID] * dikm4 + Q3_[INTID] * dikm3;
            eri += 2.0 * (P5_[INTID] * dikm5 + P4_[INTID] * dikm4 +
                          P3_[INTID] * dikm3);
            qsiexpq = qsiexpq * qsiexpq;
          }
          den_[i] += qsiexpq;
          den_[k] += qsiexpq;
          if (useGauss_[INTID]) {
            // std ::cout << gauss_mixed << " " << dik <<std::endl;
            espo = dik - gauss_dist_[INTID];
            gauss_mixed += G_[INTID] * exp (-espo * espo / (twosigma2_[INTID]));
          }
        }
      }

      // ebi=sqrt(den[i]);
      // den[i]=1.0/ebi;
      // energy=energy+(eri-ebi);
      energy += (eri - sqrt (den_[i])); // - gauss_mixed);
    }
    // std::cout <<std::setprecision(12)<<"*"<< energy << " - " << gauss_mixed;
    energy -= gauss_mixed;
    // std::cout << " = " << energy << std::endl;
    return energy;
  }

  BHdouble BHEnergyCalculator_SMATB_GAUSS::Gradient (BHdouble *x, BHdouble *g) {
    // std::vector<BHdouble> den(nAtoms_,0);
    memset (
      den_, 0,
      nAtoms_ * sizeof (den_[0])); // should empty the array efficiently
    memset (g, 0, n3Atoms_ * sizeof (g[0]));
    memset (nNeigh_, 0, nAtoms_ * sizeof (nNeigh_[0]));
    // cout << sizeof(nNeigh_) <<" " << sizeof(g) <<endl;
    BHdouble ebi, eri;
    BHdouble xik, yik, zik, dik, dik2, x_i, y_i, z_i,
      /*dik0,*/ espo, qsiexpq, aexpp, dikm, dikm2, dikm3, dikm4, dikm5;
    BHdouble energy (0), F_on_dik, gauss_mixed (0);
    BHdouble gaussianValue;
    BHdouble Fr;
    unsigned int INTID, i, j, k, mytype, IDi, IDk;
#pragma omp parallel for default(shared)				\
  private(i,k,INTID,x_i,y_i,z_i,mytype,IDi,IDk,				\
	  xik,yik,zik,dik2,dik,espo,aexpp,qsiexpq,Fr, gaussianValue,	\
	  ebi,eri,dikm,dikm2,dikm3,dikm4,dikm5,F_on_dik,gauss_mixed)	\
  reduction(+:energy)
    for (i = 0; i < nAtoms_; ++i) {
      // nNeigh_[i]=0;
      eri = 0;
      // gauss_mixed=0;
      IDi = i * 3;
      x_i = x[IDi];
      y_i = x[IDi + 1];
      z_i = x[IDi + 2];
      mytype = nTypes_ * typeList_[i];
      for (
#ifdef _OPENMP
        k = 0;
#else
        k = i + 1;
#endif //_OPENMP
        k < nAtoms_; ++k) {
#ifdef _OPENMP
        if (k == i)
          continue;
#endif //_OPENMP
       // INTID=intID_[TAI(i,k)];
        IDk = 3 * k;
        INTID = intType_[mytype + typeList_[k]];
        // std :: cout <<mytype + typeList_[k]<<" "<< INTID << std::endl;
        xik = x[IDk] - x_i;
        yik = x[IDk + 1] - y_i;
        zik = x[IDk + 2] - z_i;

        dik2 = xik * xik + yik * yik + zik * zik;
        if (dik2 < ce2_[INTID]) {
          dik = sqrt (dik2);
          NeighList_[i][nNeigh_[i]] = k;
          if (dik < cs_[INTID]) {
            // dik0=NNd_[INTID];
            espo = 1.0 - dik / NNd_[INTID]; // dik0;
            aexpp = exp (p_[INTID] * espo) * a_[INTID];
            Fr = (2.0 * aexpp) * (p_[INTID] / NNd_[INTID]);
#ifdef _OPENMP
            eri += aexpp;
#else
            eri += 2.0 * aexpp;
#endif //_OPENMP
            qsiexpq = (xi_[INTID] * xi_[INTID]) * exp (2.0 * q_[INTID] * espo);
            Fb_[i][nNeigh_[i]] = qsiexpq * q_[INTID] / NNd_[INTID];
          } else {
            dikm = dik - ce_[INTID];
            dikm2 = dikm * dikm;
            dikm3 = dikm2 * dikm;
            dikm4 = dikm3 * dikm;
            dikm5 = dikm4 * dikm;
            qsiexpq =
              Q5_[INTID] * dikm5 + Q4_[INTID] * dikm4 + Q3_[INTID] * dikm3;
            Fr = -2.0 * (5.0 * P5_[INTID] * dikm4 + 4.0 * P4_[INTID] * dikm3 +
                         3.0 * P3_[INTID] * dikm2);
#ifdef _OPENMP
            eri +=
              (P5_[INTID] * dikm5 + P4_[INTID] * dikm4 + P3_[INTID] * dikm3);
#else
            eri += 2.0 * (P5_[INTID] * dikm5 + P4_[INTID] * dikm4 +
                          P3_[INTID] * dikm3);
#endif //_OPENMP
            Fb_[i][nNeigh_[i]] =
              -(
                (5.0 * Q5_[INTID] * dikm4 + 4.0 * Q4_[INTID] * dikm3 +
                 3.0 * Q3_[INTID] * dikm2)) *
              qsiexpq;
            qsiexpq = qsiexpq * qsiexpq;
          }
          ++nNeigh_[i];
          den_[i] += qsiexpq;

          if (useGauss_[INTID]) {
            espo = dik - gauss_dist_[INTID];
            gaussianValue =
              G_[INTID] * exp (-(espo * espo) / twosigma2_[INTID]);
            gauss_mixed += gaussianValue;
#ifdef _OPENMP
            Fr += espo / sigma2_[INTID] * gaussianValue;
#else
            Fr += (espo / sigma2_[INTID]) *
                  (gaussianValue * 0.5); // G_ is multiplyed by 2
#endif
            //	    std::cout << "*";
          }

          F_on_dik = Fr / dik;
          g[IDi] += F_on_dik * xik;
          g[IDi + 1] += F_on_dik * yik;
          g[IDi + 2] += F_on_dik * zik;
#ifndef _OPENMP
          den_[k] += qsiexpq;
          g[IDk] -= F_on_dik * xik;
          g[IDk + 1] -= F_on_dik * yik;
          g[IDk + 2] -= F_on_dik * zik;
#endif
        }
      }

      ebi = sqrt (den_[i]);

      den_[i] = 1.0 / ebi;
      energy = energy + (eri - ebi); // - gauss_mixed);
    }
    // std::cout << energy << " - " << gauss_mixed;
    energy -= gauss_mixed;
    // std::cout << " = " << energy << std::endl;
    /*double sum=0;
      for ( i=0; i< nAtoms_; ++i) {
      IDi = 3*i;
      std:: cout << i <<"->"<< g[IDi+0]<<" "<< g[IDi+1]<<" "<< g[IDi+2]
      <<"\t\t" << nNeigh_[i] <<'\n';
      for (j=0;j<3;j++) {
      sum+=g[IDi+j]*g[IDi+j];
      }
      }
      std:: cout << sum <<std::endl;
      std:: cout << g[0]<<" "<< g[1]<<" "<< g[2]<<std::endl;
      std::    cin.get();*/

    // shared(x,g,den_,Fb_NeighList_,)
#pragma omp parallel for default(shared) private(                              \
  i, j, k, x_i, y_i, z_i, IDi, IDk, xik, yik, zik, dik, F_on_dik)
    for (i = 0; i < nAtoms_; ++i) {
      IDi = 3 * i;
      x_i = x[IDi];
      y_i = x[IDi + 1];
      z_i = x[IDi + 2];
      // mytype = nTypes_*typeList_[i];
      for (j = 0; j < nNeigh_[i]; ++j) {
        k = NeighList_[i][j]; /*
                                #ifdef _OPENMP
                                if (k<=i) continue;
                                #endif //_OPENMP   */
        IDk = 3 * k;
        // INTID = intType_[mytype + typeList_[k]];
        // std :: cout <<mytype + typeList_[k]<<" "<< INTID << std::endl;
        xik = x[IDk] - x_i;
        yik = x[IDk + 1] - y_i;
        zik = x[IDk + 2] - z_i;

        dik = sqrt (xik * xik + yik * yik + zik * zik);
        // fb(j,i) so this won't run on a "distant" address
        // F_on_dik used for bounding forces
        F_on_dik = Fb_[i][j] * (den_[i] + den_[k]) / dik;

        g[IDi] -= F_on_dik * xik;
        g[IDi + 1] -= F_on_dik * yik;
        g[IDi + 2] -= F_on_dik * zik;
#ifndef _OPENMP
        g[IDk] += F_on_dik * xik;
        g[IDk + 1] += F_on_dik * yik;
        g[IDk + 2] += F_on_dik * zik;
#endif
      }
    }

    /*      double sum=0;
            for ( i=0; i< nAtoms_; ++i) {
            IDi = 3*i;
            for (j=0;j<3;j++) {
            sum+=g[IDi+j]*g[IDi+j];
            }
            }
            std:: cout << sum <<std::endl;
            std:: cout << g[0]<<" "<< g[1]<<" "<< g[2]<<std::endl;
            std::    cin.get();*/
    return energy;
  }

  BHdouble BHEnergyCalculator_SMATB_GAUSS::Energy_tol (BHdouble *x) {
    // std::vector<BHdouble> den(nAtoms_,0);
    memset (
      den_, 0,
      nAtoms_ * sizeof (den_)); // should empty the array efficiently
    BHdouble eri;
    BHdouble xik, yik, zik, dik, dik2, x_i, y_i, z_i,
      /*dik0,*/ espo, qsiexpq, aexpp, dikm, dikm2, dikm3, dikm4, dikm5;
    BHdouble energy (0), gauss_mixed (0);
    unsigned int INTID, i, j, k, mytype, IDi, IDk;

    for (i = 0; i < nAtoms_; ++i) {
      eri = 0;
      gauss_mixed = 0;
      IDi = 3 * i;
      x_i = x[IDi];
      y_i = x[IDi + 1];
      z_i = x[IDi + 2];
      mytype = nTypes_ * typeList_[i];
      for (j = 0; j < nNeigh_[i]; ++j) {
        k = NeighList_[i][j];
        INTID = intType_[mytype + typeList_[k]];
        IDk = 3 * k;
        xik = x[IDk] - x_i;
        yik = x[IDk + 1] - y_i;
        zik = x[IDk + 2] - z_i;

        dik2 = xik * xik + yik * yik + zik * zik;
        if (dik2 < ce2_[INTID]) {
          dik = sqrt (dik2);
          if (dik < cs_[INTID]) {
            // dik0=NNd_[INTID];
            espo = 1.0 - dik / NNd_[INTID]; // dik0;
            aexpp = exp (p_[INTID] * espo) * a_[INTID];
            eri += 2.0 * aexpp;
            // add xi2_ the squared xi
            qsiexpq = (xi_[INTID] * xi_[INTID]) * exp (2.0 * q_[INTID] * espo);
          } else {
            dikm = dik - ce_[INTID];
            dikm2 = dikm * dikm;
            dikm3 = dikm2 * dikm;
            dikm4 = dikm3 * dikm;
            dikm5 = dikm4 * dikm;
            qsiexpq =
              Q5_[INTID] * dikm5 + Q4_[INTID] * dikm4 + Q3_[INTID] * dikm3;
            eri += 2.0 * (P5_[INTID] * dikm5 + P4_[INTID] * dikm4 +
                          P3_[INTID] * dikm3);
            qsiexpq = qsiexpq * qsiexpq;
          }
          den_[i] += qsiexpq;
          den_[k] += qsiexpq;

          if (useGauss_[INTID]) {
            espo = dik - gauss_dist_[INTID];
            gauss_mixed += G_[INTID] * exp (-espo * espo / (twosigma2_[INTID]));
          }
        }
      }
      energy = energy + (eri - sqrt (den_[i]) - gauss_mixed);
    }

    return energy;
  }

  BHdouble
  BHEnergyCalculator_SMATB_GAUSS::Gradient_tol (BHdouble *x, BHdouble *g) {
    // std::vector<BHdouble> den(nAtoms_,0);
    memset (
      den_, 0,
      nAtoms_ * sizeof (den_)); // should empty the array efficiently
    memset (g, 0, n3Atoms_ * sizeof (g));

    BHdouble ebi, eri;
    BHdouble xik, yik, zik, dik, dik2, x_i, y_i, z_i,
      /*dik0,*/ espo, qsiexpq, aexpp, dikm, dikm2, dikm3, dikm4, dikm5;
    BHdouble energy (0), F_on_dik, gauss_mixed (0);
    BHdouble gaussianValue;
    BHdouble Fr;
    unsigned int INTID, i, j, k, mytype, IDi, IDk;
    for (i = 0; i < nAtoms_; ++i) {
      eri = 0;
      gauss_mixed = 0;
      IDi = 3 * i;
      x_i = x[IDi];
      y_i = x[IDi + 1];
      z_i = x[IDi + 2];
      mytype = nTypes_ * typeList_[i];
      for (j = 0; j < nNeigh_[i]; ++j) {
        k = NeighList_[i][j];
        INTID = intType_[mytype + typeList_[k]];
        IDk = 3 * k;
        xik = x[IDk] - x_i;
        yik = x[IDk + 1] - y_i;
        zik = x[IDk + 2] - z_i;

        dik2 = xik * xik + yik * yik + zik * zik;
        if (dik2 < ce2_[INTID]) {
          dik = sqrt (dik2);
          if (dik < cs_[INTID]) {
            // dik0=NNd_[INTID];
            espo = 1.0 - dik / NNd_[INTID]; // dik0;
            aexpp = exp (p_[INTID] * espo) * a_[INTID];
            Fr = (2.0 * p_[INTID] / NNd_[INTID]) * aexpp;
            eri += 2.0 * aexpp;
            // add xi2_ the squared xi
            qsiexpq = (xi_[INTID] * xi_[INTID]) * exp (2.0 * q_[INTID] * espo);
            Fb_[i][j] = qsiexpq * (q_[INTID] / NNd_[INTID]);
          } else {
            dikm = dik - ce_[INTID];
            dikm2 = dikm * dikm;
            dikm3 = dikm2 * dikm;
            dikm4 = dikm3 * dikm;
            dikm5 = dikm4 * dikm;
            qsiexpq =
              Q5_[INTID] * dikm5 + Q4_[INTID] * dikm4 + Q3_[INTID] * dikm3;
            Fr = -2.0 * (5.0 * P5_[INTID] * dikm4 + 4.0 * P4_[INTID] * dikm3 +
                         3.0 * P3_[INTID] * dikm2);
            eri += 2.0 * (P5_[INTID] * dikm5 + P4_[INTID] * dikm4 +
                          P3_[INTID] * dikm3);
            Fb_[i][j] = -(
                          (5.0 * Q5_[INTID] * dikm4 + 4.0 * Q4_[INTID] * dikm3 +
                           3.0 * Q3_[INTID] * dikm2)) *
                        qsiexpq;
            qsiexpq = qsiexpq * qsiexpq;
          }
          den_[i] += qsiexpq;
          den_[k] += qsiexpq;

          if (useGauss_[INTID]) {
            espo = dik - gauss_dist_[INTID];
            gaussianValue =
              G_[INTID] * exp (-espo * espo / (twosigma2_[INTID]));
            gauss_mixed += gaussianValue;
            Fr += (espo / sigma2_[INTID]) * (gaussianValue * 0.5);
          }

          F_on_dik = Fr / dik;

          g[IDi] += F_on_dik * xik;
          g[IDi + 1] += F_on_dik * yik;
          g[IDi + 2] += F_on_dik * zik;
          g[IDk] -= F_on_dik * xik;
          g[IDk + 1] -= F_on_dik * yik;
          g[IDk + 2] -= F_on_dik * zik;
        }
      }

      ebi = sqrt (den_[i]);

      den_[i] = 1.0 / ebi;
      energy = energy + (eri - ebi - gauss_mixed);
    }

    for (i = 0; i < nAtoms_; ++i) {
      IDi = 3 * i;
      x_i = x[IDi];
      y_i = x[IDi + 1];
      z_i = x[IDi + 2];
      mytype = nTypes_ * typeList_[i];
      for (j = 0; j < nNeigh_[i]; ++j) {
        k = NeighList_[i][j];
        INTID = intType_[mytype + typeList_[k]];
        // std :: cout <<mytype + typeList_[k]<<" "<< INTID << std::endl;
        IDk = 3 * k;
        xik = x[IDk] - x_i;
        yik = x[IDk + 1] - y_i;
        zik = x[IDk + 2] - z_i;

        dik2 = xik * xik + yik * yik + zik * zik;
        if (dik2 < ce2_[INTID]) {
          dik = sqrt (dik2);
          // F_on_dik used for bounding forces
          F_on_dik = Fb_[i][j] * (den_[i] + den_[k]) / dik;

          g[IDi] -= F_on_dik * xik;
          g[IDi + 1] -= F_on_dik * yik;
          g[IDi + 2] -= F_on_dik * zik;
          g[IDk] += F_on_dik * xik;
          g[IDk + 1] += F_on_dik * yik;
          g[IDk + 2] += F_on_dik * zik;
        }
      }
    }
    return energy;
  }
} // namespace BH
