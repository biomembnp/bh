!>This sub routine sets the number of atoms, initialize itype, nvois, hasvois and ivois, then copy the content of types in itype
SUBROUTINE BHInterface_SetCluster(N,types)BIND(C, name= "BHInterface_SetCluster")
  use iso_c_binding!in realta` e` uno statement per fortran 2003 ma se compila non mi lamento
  !  USE PARAMETERS, Only: maxpar
  USE BHmodule, Only: nat3D, itype, nvois, ivois!, hasvois
  implicit none
  ! Local variables
  Integer(C_INT),intent(in)::N,types(N)
  Integer(C_INT) :: init!, i
  nat3d = N
  if(allocated(itype))then!i expect all are allocated
     deallocate(itype)
     deallocate(nvois)
     !deallocate(hasvois)
     deallocate(ivois)
  end if
  allocate(itype(nat3d))
  init = nat3d-1
  !allocate(hasvois(init))!the last atom won't have neighbours
  allocate(nvois(nat3d))
  allocate(ivois(init,init))!nat3d-1 because atom i cannot be a neighboir of himself and last atom won't have atoms with index greater than its own
  !copio i valori negli array
  itype = types
!!$  do i=0, Nat3d
!!$     write(*,*) i, itype(i)
!!$  enddo
END SUBROUTINE BHInterface_SetCluster

SUBROUTINE BHInterface_SetPrivatePars(Nt, Coh, Radius, myMass,intT)BIND(C,name="BHInterface_SetPrivatePars")
  use iso_c_binding!in realta` e` uno statement per fortran 2003 ma se compila non mi lamento
  USE BHmodule, Only: Ntypes, ecoh, rat, mass, intType
  Integer(C_INT), intent(in) :: Nt, intT(Nt*Nt)
  Real(C_DOUBLE), intent(in) :: Coh(Nt), Radius(Nt), myMass(Nt)
  Ntypes = Nt
  if(allocated(ecoh))then!i expect all are allocated
     deallocate(ecoh)
     deallocate(rat)
     deallocate(mass)
     deallocate(intType)
  end if
  allocate(ecoh(Ntypes))
  allocate(rat(Ntypes))
  allocate(mass(Ntypes))
  allocate(intType(Ntypes*Ntypes))
  ecoh =  Coh
  rat = Radius
  mass = myMass
  intType = intT+1
!!$  write(*,*) Ntypes,":"
!!$  write(*,*) 1,2
!!$  write(*,*) ecoh
!!$  write(*,*) rat
!!$  write(*,*) mass
END SUBROUTINE BHInterface_SetPrivatePars

SUBROUTINE BHInterface_SETpars(Ninteraction,myp,myq, mya, myqsi, &
     mya5, mya4, mya3, myx5, myx4 , myx3,&
     mydist, mycutoff_start, mycutoff_end)BIND(C,name="BHInterface_SetCouplePars")
  use iso_c_binding!in realta` e` uno statement per fortran 2003 ma se compila non mi lamento
  USE BHmodule, Only: Nint, p, q, a, qsi, &
       a5, a4, a3, x5, x4 , x3,&
       dist, cutoff_start, cutoff_end, cutoff_end2, cutoff_end_tol2
  Integer(C_INT), intent(in) :: Ninteraction
  Real(C_DOUBLE), intent(in) :: myp(Ninteraction), myq(Ninteraction), mya(Ninteraction), myqsi(Ninteraction)
  Real(C_DOUBLE), intent(in) :: mya5(Ninteraction), mya4(Ninteraction), mya3(Ninteraction)
  Real(C_DOUBLE), intent(in) :: myx5(Ninteraction), myx4 (Ninteraction), myx3(Ninteraction)
  Real(C_DOUBLE), intent(in) :: mydist(Ninteraction), mycutoff_start(Ninteraction), mycutoff_end(Ninteraction)
  !!need to put Nt and Nint to work in the module
  Nint = Ninteraction
  if(allocated(p))then!i expect all are allocated
     deallocate(p)
     deallocate(q)
     deallocate(a)
     deallocate(qsi)
     deallocate(a5)
     deallocate(a4)
     deallocate(a3)
     deallocate(x5)
     deallocate(x4 )
     deallocate(x3)
     deallocate(dist)
     deallocate(cutoff_start)
     deallocate(cutoff_end)
     deallocate(cutoff_end2)
     deallocate(cutoff_end_tol2)
  end if
  allocate(p(Nint))
  allocate(q(Nint))
  allocate(a(Nint))
  allocate(qsi(Nint))
  allocate(a5(Nint))
  allocate(a4(Nint))
  allocate(a3(Nint))
  allocate(x5(Nint))
  allocate(x4(Nint))
  allocate(x3(Nint))
  allocate(dist(Nint))
  allocate(cutoff_start(Nint))
  allocate(cutoff_end(Nint))
  allocate(cutoff_end2(Nint))
  allocate(cutoff_end_tol2(Nint))

  p=myp
  q=myq
  a=mya
  qsi=myqsi
  a5=mya5
  a4=mya4
  a3=mya3
  x5=myx5
  x4=myx4
  x3=myx3
  dist=mydist
  cutoff_start=mycutoff_start
  cutoff_end=mycutoff_end
  cutoff_end2 = cutoff_end*cutoff_end
  !with an added angstrom to be used with bigvoi_tol to create an expanded list of neighbours
  cutoff_end_tol2 = (cutoff_end+1.d0)*(cutoff_end+1.d0)

!!$  write(*,*)'cutoff_start ',cutoff_start
!!$  write(*,*)'cutoff_end ',cutoff_end
!!$  write(*,*)'dist',dist
!!$  write(*,*)'p',p
!!$  write(*,*)'q',q
!!$  write(*,*)'a',a
!!$  write(*,*)'qsi',qsi
!!$  write(*,*)'a3:',a3
!!$  write(*,*)'a4:',a4
!!$  write(*,*)'a5:',a5
!!$  write(*,*)'x3:',x3
!!$  write(*,*)'x4:',x4
!!$  write(*,*)'x5:',x5
END SUBROUTINE BHInterface_SETpars

SUBROUTINE CleanMemory()BIND(C,name="BHInterface_CleanMemory")
  use iso_c_binding
  USE BHmodule, Only: itype, nvois, ivois, &!, hasvois, &
       p, q, a, qsi, &
       a5, a4, a3, x5, x4 , x3,&
       dist, cutoff_start, cutoff_end, cutoff_end2, cutoff_end_tol2, &
       ecoh, rat, mass
  if(allocated(p))then!i expect all are allocated
     deallocate(p)
     deallocate(q)
     deallocate(a)
     deallocate(qsi)
     deallocate(a5)
     deallocate(a4)
     deallocate(a3)
     deallocate(x5)
     deallocate(x4 )
     deallocate(x3)
     deallocate(dist)
     deallocate(cutoff_start)
     deallocate(cutoff_end)
     deallocate(cutoff_end2)
     deallocate(cutoff_end_tol2)
  end if

  if(allocated(ecoh))then!i expect all are allocated
     deallocate(ecoh)
     deallocate(rat)
     deallocate(mass)
  end if
  if(allocated(itype))then!i expect all are allocated
     deallocate(itype)
     deallocate(nvois)
     !deallocate(hasvois)
     deallocate(ivois)
  end if
END SUBROUTINE CleanMemory
