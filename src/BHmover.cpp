/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Contains the definition of the BHmover class

@file BHmover.cpp
  @author Daniele Rapetti (iximiel@gmail.com)
    @date 20/12/2017
       @version 1.0

       class definitiom moved from mover.cxx

       @date 24/04/2019
  @version 1.1

  ratianalized some function , in the hope of a easier use
    */
#include <fstream>
#include <iomanip>
#include <iostream>

#include "BHclusterUtilities.hpp"
#include "BHmetalParameters.hpp"
#include "BHminimizationAlgorithmVoid.hpp"
#include "BHmover.hpp"
#include "BHwalkerAlgorithmObstacle.hpp"
#include "BHworkWithFortran.hpp"
namespace BH {
  BHMover::BHMover (
    const BHClusterAtoms &workingConf,
    const BHClusterAtoms &lastAcceptedMove,
    const BHWalkerSettings &walkersettings,
    const BHMetalParameters &bhmp)
    : BHWalker (
        walkersettings,
        workingConf,
        bhmp,
        BHWalkerAlgorithms::BHWalkerAlgorithmObstacle,
        true,
        true) {
    // the BHConfigurationOperator constructor assign the seed to the working
    // configuration
    LastAcceptedConf_ = lastAcceptedMove;
  }

  BHMoverSupport::BHMontecarloStatus
  BHMover::FinishPreviousStep (const BHdouble &MonteCarloT, rndEngine &rng) {
    TAccept_ = MonteCarloT;
    BHdouble deltaE = lastMoveData_.Energy () - lastAcceptedData_.Energy ();
    BHdouble expAcc = exp (-deltaE / (TAccept_ * BHConst::kB));
    bool accepted = verifyStep (rng);
    return {accepted, deltaE, expAcc};
  }
  /// Sets the given energy and calculates the order parameters, then prepares
  /// the cluster to plot
  void BHMover::Energy (BHdouble newEne, BHEnums::energyType units) {
    lastAcceptedData_.setEnergy (convertEnergy2eV (newEne, units));
  }
} // namespace BH

//**********************************
// mover.cxx functions
void Reorder (const std::string &fname, BH::BHMetalParameters &bhmp) {
  BH::BHClusterAtoms mycluster (fname.c_str ());
  BH::BHClusterUtilities::MassesReorder (mycluster, bhmp);
  std::ofstream fileUpdate (fname);
  fileUpdate << std::setprecision (9) << mycluster << std::endl;
  fileUpdate.close ();
}

void EnergyConverter (
  const std::string &fname,
  BH::BHdouble Energy,
  const BH::BHMetalParameters &bhmp,
  BH::BHEnums::energyType units) {
  BH::BHClusterAtoms mycluster (fname.c_str ());
  BH::BHWalkerSettings walkersettings = BH::loadBHWalkerSettings ("mover.in");
  BH::BHMover toUpdate (mycluster, mycluster, walkersettings, bhmp);
  std::vector<BH::BHOrderParameter> OPnames = {
    BH::BHOrderParameter ("mix"),
    BH::BHOrderParameter (bhmp.getAtomName (0) + bhmp.getAtomName (0))};
  BH::BHClusterAnalyser Analyzer (OPnames, bhmp);
  toUpdate.Energy (Energy, units);
  toUpdate.AnalyzeLastAcceptedConf (Analyzer, bhmp);
  std::ofstream fileUpdate (fname);
  fileUpdate << BH::BHClusterUtilities::plotClusterWithData (
                  toUpdate.lastAcceptedConf (),
                  toUpdate.lastAcceptedClusterData (), OPnames)
             << '\n';
  fileUpdate.close ();
}

int MonteCarlo (
  const std::string &last_acp_fname,
  const std::string &new_move_fname,
  const std::string &output_fname,
  BH::BHMetalParameters &bhmp,
  BH::BHdouble latestEnergy,
  BH::BHdouble newEnergy,
  BH::BHdouble TMC,
  BH::BHEnums::energyType units) {
  // in case of refused move will be showed only one time
  BH::BHWalkerSettings walkersettings = BH::loadBHWalkerSettings ("mover.in");
  std::vector<BH::BHOrderParameter> OPnames = {
    BH::BHOrderParameter ("mix"),
    BH::BHOrderParameter (bhmp.getAtomName (0) + bhmp.getAtomName (0))};

  unsigned RNG_seed = static_cast<unsigned> (
    std::chrono::system_clock::now ().time_since_epoch ().count ());
  BH::rndEngine my_rnd_engine (RNG_seed);
  switch (units) {
  case BH::BHEnums::eV:
    break;
  case BH::BHEnums::Ry:
    latestEnergy *= BH::BHConst::Ry;
    newEnergy *= BH::BHConst::Ry;
    break;
  case BH::BHEnums::Ht:
    latestEnergy *= BH::BHConst::Ht;
    newEnergy *= BH::BHConst::Ht;
    break;
  }
  BH::BHMover mover (new_move_fname, last_acp_fname, walkersettings, bhmp);

  BH::BHClusterAnalyser Analyzer (OPnames, bhmp);

  // finishing the set up of the mover
  // reorder things and sets the interaction labels
  mover.reorderAndInitializeLabels (bhmp);

  mover.AnalyzeLastAcceptedConf (Analyzer, bhmp);
  BH::BHWalker::forceLastAcceptedEnergy (mover, latestEnergy);

  mover.ForceAnalyzeWorkingConf (Analyzer, bhmp);
  BH::BHWalker::forceLastMoveEnergy (mover, newEnergy);

  // saving the last step:
  std::ofstream lastStepFile (new_move_fname);

  BH::BHClusterData LastMoveData = mover.lastMoveClusterData ();

  lastStepFile << std::setprecision (9)
               << BH::BHClusterUtilities::plotClusterWithData (
                    mover.lastMoveConf (), LastMoveData, OPnames)
               << '\n';
  lastStepFile.close ();

  BH::BHMoverSupport::BHMontecarloStatus monteCarlo =
    mover.FinishPreviousStep (TMC, my_rnd_engine);
  enum exitValues { EVrejected = 1, EVaccepted = 0 } exitvalue = EVrejected;

  if (monteCarlo.accepted) { // updates the info on the file and append them on
    // ener_best.out
    std::ofstream best (
      "ener_accepted.out",
      std::ofstream::out | std::ofstream::app); // appends info to the file:
    // actual minima [tab] data
    best.setf (std::ios_base::fixed);
    best << std::setprecision (9) << new_move_fname << "\t" << LastMoveData
         << '\n';
    best.close ();
    exitvalue = EVaccepted;
  }

  // appends info to the file:
  std::ofstream all ("ener_all.out", std::ofstream::out | std::ofstream::app);
  all.setf (std::ios_base::fixed);
  all << new_move_fname << "\t" << LastMoveData << "\t" << latestEnergy << "\t"
      << std::scientific << std::setprecision (5) << monteCarlo.deltaE << "\t"
      << monteCarlo.expAcc << '\n';
  all.close ();

  mover.doMove (Analyzer, bhmp, my_rnd_engine);
  std::ofstream fmoves (
    "moves.out",
    std::ofstream::out | std::ofstream::app); // appends info to the file:
  fmoves << new_move_fname << "\t" << mover.lastMoveInfo () << '\n';
  // cout << mover <<std::endl;
  std::ofstream f (output_fname);
  f.setf (std::ios_base::fixed);
  f << std::setprecision (9) << mover.lastMoveConf () << '\n';
  f.close ();
  return exitvalue;
}
