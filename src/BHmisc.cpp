/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Some utilities different

Here you can find function that where previously old main file used for testing
purposes

   @file BHmisc.cpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 17/01/2020
   @version 0.1

@date 29/01/2020
@version 0.2

added centerer and energycalculator
*/
#include "BHmisc.hpp"

#include <iomanip>
#include <iostream>

#include "BHmetalParameters.hpp"

#include "BHanalysisSupport.hpp"
#include "BHbasinHopping.hpp"
#include "BHconstructors.hpp"
#include "BHenergyCalculator_SMATB.hpp"

#include "BHmoves.hpp"

namespace BH {
#define MemorySpacePlotterCommented(Class, comment)                            \
  std::setw (30) << (std::string (#Class) + " [" + #comment + "]") << ":\t"    \
                 << sizeof (Class) / 8. << " B\n"

#define MemorySpacePlotter(Class)                                              \
  std::setw (30) << #Class << ":\t" << sizeof (Class) / 8. << " B\n"

  int printMemory (std::ostream &stream) {
    BasinHopping::run = true;
    stream.setf (std::ios::left);
    stream << std::setw (30) << "Class"
           << ":\tBites" << std::endl;
    stream << MemorySpacePlotter (BHMetalParameters)
           << MemorySpacePlotter (BasinHopping)
           << MemorySpacePlotter (rndEngine) << MemorySpacePlotter (BHSettings)
           << MemorySpacePlotter (BHWalkerSettings)
           << MemorySpacePlotter (BHVector) << MemorySpacePlotter (BHAtom)
           << MemorySpacePlotterCommented (BHClusterAtoms, empty)
           << MemorySpacePlotterCommented (BHCluster, empty)
           << MemorySpacePlotter (BHAnalysisSupport)
           << MemorySpacePlotter (BHOrderParameter)
           << MemorySpacePlotter (BHClusterAnalyser)
#define MOVE_PARSER
#define MoveParser(key, Class) << MemorySpacePlotter (Class)
#include "BHmoves.hpp"
#undef MoveParser
#undef MOVE_PARSER
           << std::endl;
    return 0;
  }

  template <std::size_t N>
  void dataWriter (
    const std::array<const std::string, N> &names,
    const std::array<const std::string, N> &defaults,
    std::ostream &stream) {
    for (unsigned i = 0; i < N; ++i) {
      if (defaults[i] != "NotUsed") {
        stream << names[i] << "\t=\t" << defaults[i] << "\n";
      }
    }
  }

  int plotBHSettingsInputFile (std::ostream &stream, int, char **) {
    BH::BHSettings st;
    stream << BHSettingsUtilities::printInputFileFromData (st) << std::endl;
    return 0;
  }

#define speedDataWriterWalker(name)                                            \
  dataWriter (                                                                 \
    __BHWS_PARSER##name##_NOPT, _BHWSParser_StringsForCard##name,              \
    _BHWSParser_DefaultStringsForCard##name, stream)

  int plotBHWalkerSettingsInputFile (std::ostream &, int argc, char **argv) {
    BHwalkerAlgorithmType chosenAlgorithm = BHwalkerAlgorithmType::stnd;
    if (argc > 1) {
      chosenAlgorithm = walkerAlgorithmFromStr (argv[1]);
    }
    BHWalkerSettings defaultSettings =
      createSettingsWithAllMoves (chosenAlgorithm);

    if (argc > 2) {
      std::ofstream filout (argv[2]);
      std::cout << "Writing input file on file: " << argv[2] << std::endl;
      filout << printInputFileFromWalkerSettings (defaultSettings);
    } else {
      std::cout << printInputFileFromWalkerSettings (defaultSettings);
    }
    return 0;
  }

  int centerCluster (std::ostream &stream, int argc, char **argv) {
    if (argc >= 2) {
      std::ifstream cluster (argv[1]);
      if (!cluster) {
        throw "centerer: cannot open \"" + std::string (argv[1]) + "\"";
      }
      BHVector theCenter (0.0, 0.0, 0.0);
      if (argc > 2) {
        if (argc >= 5) {
          theCenter.setXYZ (
            std::stod (argv[2]), std::stod (argv[3]), std::stod (argv[4]));
        } else {
          throw "centerer: you have to specify three coordinates for the "
                "center";
        }
      }
      BHClusterAtoms mycluster;
      ::operator>> (cluster, mycluster);

      mycluster = BHClusterAtoms::getCenteredCluster (mycluster, theCenter);

      stream << std::setprecision (10);
      ::operator<< (stream, mycluster);
      stream << std::endl;
    }
    return 0;
  }

  int energyCluster (std::ostream &stream, int argc, char **argv) {
    if (argc >= 3) {
      enum { calculateForces = 1, doMinimization = 2, calculatePressure = 4 };
      int flag = 0;
      std::string clusterFileName = argv[1];
      std::string BHMP_filename = "MetalParameters.in";
      for (int i = 2; i < argc; i++) {
        if (std::string (argv[i]) == "-f") {
          flag |= calculateForces;
        } else if (std::string (argv[i]) == "-m") {
          flag |= doMinimization;
        } else if (std::string (argv[i]) == "-p") {
          flag |= calculatePressure;
        } else if (std::string (argv[i]) == "-pot") {
          if (i + 1 < argc) {
            ++i;
            BHMP_filename = argv[i];
          }
        }
      }
      BHMetalParameters bhmp{BHMP_filename, false};
      BHCluster cluster (clusterFileName);
      BHdouble *x = new BH::BHdouble[3 * cluster.getNofAtoms ()];
      // gradient
      BHdouble *g = new BH::BHdouble[3 * cluster.getNofAtoms ()];
      BHdouble *p = new BH::BHdouble[cluster.getNofAtoms ()];
      cluster.AssignToVector (x);
      BHEnergyCalculator *energyCalculator =
        new BHEnergyCalculator_SMATB (cluster, bhmp);
      // std::vector<BHVector> Forces(0);
      // std::vector<BHdouble> Pressure(0);
      BHdouble energy = 0;
      if ((flag & doMinimization) == doMinimization) {
        BHMinimizationAlgorithm_LBFGSB minimizer;
        energy =
          minimizer.Minimization (x, cluster.getNofAtoms (), energyCalculator);
      }
      if ((flag & calculateForces) == calculateForces) {
        energy = energyCalculator->Gradient (x, g);
      }
      if ((flag & calculatePressure) == calculatePressure) {
        energy = energyCalculator->Pressure (x, p);
        std::string baseFCC = cluster.Composition (0);
        BHdouble ret = bhmp.getRadius (baseFCC) * 2.0 * sqrt (2.0);
        BH::BHdouble volume = 160.2 * 4.0 / (3.0 * ret * ret * ret);
        for (size_t i = 0; i < cluster.getNofAtoms (); ++i) {
          p[i] *= volume;
        }
      }

      std::ios state (nullptr);
      state.copyfmt (stream);

      if (
        (flag & (calculateForces | doMinimization | calculatePressure)) == 0) {
        energy = energyCalculator->Energy (x);
        stream << argv[1] << " E = " << std::setprecision (9) << std::setw (14)
               << energy << '\n';
      } else { // printing the needed informtions
        stream << std::setprecision (9);
        stream << cluster.getNofAtoms () << '\n';
        stream << cluster.Composition () << "\tE=" << energy;
        stream << " Properties=species:S:1:pos:R:3";
        if ((flag & calculatePressure) == calculatePressure) {
          stream << ":Pressure:R:1";
        }
        if ((flag & calculateForces) == calculateForces) {
          stream << ":Force:R:3";
        }
        stream << '\n';
        for (unsigned i = 0; i < cluster.getNofAtoms (); ++i) {
          stream << std::setw (5) << std::left << cluster[i].Type () << ' '
                 << std::setw (14) << std::right << x[3u * i] << ' '
                 << std::setw (14) << x[3u * i + 1u] << ' ' << std::setw (14)
                 << x[3u * i + 2u];
          if ((flag & calculatePressure) == calculatePressure) {
            stream << ' ' << std::setw (14) << p[i];
          }
          if ((flag & calculateForces) == calculateForces) {
            stream << ' ' << std::setw (14) << -g[3 * i] << ' '
                   << std::setw (14) << -g[3 * i + 1] << ' ' << std::setw (14)
                   << -g[3 * i + 2];
          }
          stream << '\n';
        }
      }
      stream.copyfmt (state);
      delete[] x;
      delete[] g;
      delete[] p;
      delete energyCalculator;
    }
    return 0;
  }
} // namespace BH
