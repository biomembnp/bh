/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Contains main for speciesRandomizer

   This small utility takes a cluster as input and randomize its atoms on user
   choiche
   @file speciesRandomizer.cxx
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 21/02/2018
   @version 0.1

   first prototype
*/
#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <random>
#include <vector>

#include "BHclusterAtoms.hpp"

using namespace std;
using namespace BH;

struct NameAndNumber {
  std::string Name;
  unsigned int Number;
};

int main (int argc, char **argv) {
  try {
    cout << "                ___  \n"
         << "               (__ \\ \n"
         << "            _ ____) )\n"
         << "           /_|____ ( \n"
         << "          |_ |     | |\n"
         << "          (_/      |_|\n"
         << "\n"
         << "SpeciesRandomizer\n"
         << endl;
    if (argc == 1) {
      cout << "This program need at least one input that is the name of the "
              "base configuration file.\n"
           << "If you specify a second filemane that will be used as the "
              "outputfile"
           << endl;
    } else {
      BHClusterAtoms theCluster (argv[1]);
      cout << "A cluster with " << theCluster.getNofAtoms ()
           << " atoms has been loaded" << endl;

      cout << "How many species do you want in the new cluster? ";
      unsigned int Nspecies;
      cin >> Nspecies;
      if (Nspecies > theCluster.getNofAtoms ()) {
        throw "Number of Species is greater than number of Atoms!";
      }

      unsigned int subtotal (0);

      std::vector<NameAndNumber> NamesAndNumbers (Nspecies);
      for (unsigned int i = 1; i < Nspecies; ++i) {
        cout << "Name for the specie " << i << ": ";
        cin >> NamesAndNumbers[i].Name;
        cout << "Number of " << NamesAndNumbers[i].Name << ": ";
        cin >> NamesAndNumbers[i].Number;
        subtotal += NamesAndNumbers[i].Number;
        if (subtotal > theCluster.getNofAtoms ()) {
          throw "Too many atoms";
        }
      }
      NamesAndNumbers[0].Number = theCluster.getNofAtoms () - subtotal;
      cout << "Input the specie of the remaning " << NamesAndNumbers[0].Number
           << " atom" << ((NamesAndNumbers[0].Number == 1) ? "" : "s") << ": ";

      cin >> NamesAndNumbers[0].Name;
      std::string outfileName ("outfile.xyz");
      if (argc >= 3) {
        outfileName = argv[3];
      }
      cout << "A new file named \"" << outfileName << "\" will be created.\n"
           << "With:\n";
      vector<std::string> Names;
      for (unsigned int i = 0; i < Nspecies; ++i) {
        cout << "  *\t" << NamesAndNumbers[i].Number << "\tatoms of "
             << NamesAndNumbers[i].Name << "\n";
        for (unsigned int j = 0; j < NamesAndNumbers[i].Number; ++j) {
          Names.push_back (NamesAndNumbers[i].Name);
        }
      }
      unsigned seed = static_cast<unsigned> (
        std::chrono::system_clock::now ().time_since_epoch ().count ());
      shuffle (Names.begin (), Names.end (), std::mt19937_64 (seed));
      for (unsigned int i = 0; i < theCluster.getNofAtoms (); ++i) {
        theCluster[i].setType (Names[i]);
      }
      ofstream outfile (outfileName);
      outfile << theCluster << endl;
      outfile.close ();
    }
  } catch (const char *problem) {
    cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m" << endl;
    return -1;
  }
  return 0;
}
