/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#include "BHmoveExchangeInterface.hpp"
#include "BHparsers.hpp"
#include <sstream>

namespace BH {
  BHMoveExchangeInterface::BHMoveExchangeInterface ()
    : BHMoveExchange ("exchangeInterface") {}
  BHMoveExchangeInterface::~BHMoveExchangeInterface () = default;

  std::string BHMoveExchangeInterface::DefaultString () {
    return "prob = 1.0, accTemp = 100, fewNeighbours = 11, NNforHeavy = 1, "
           "NNforLight = 1";
  }

  std::unique_ptr<BHMove> BHMoveExchangeInterface::clone () {
    return std::unique_ptr<BHMove>{new BHMoveExchangeInterface (*this)};
  }

  std::string BHMoveExchangeInterface::printSettingsSpecialized () const {
    std::stringstream ss;
    ss << "Heavy NN > " << NNforHeavy_ << ", Light NN > " << NNforLight_
       << ", Surf if NN < " << fewNeighbours_;
    return ss.str ();
  }

  std::string
  BHMoveExchangeInterface::printSettingsSpecializedForInput () const {
    std::stringstream ss;
#define outputwriter(variable)                                                 \
  ", " << BHParsers::BHMV::EXCHANGEINTERFACE[static_cast<size_t> (             \
            BHParsers::BHMV::EXCHANGEINTERFACEvariable::variable)]             \
       << " = " << variable
    std::boolalpha (ss);
    ss << outputwriter (fewNeighbours_) << outputwriter (NNforHeavy_)
       << outputwriter (NNforLight_);
#undef outputwriter
    return ss.str ();
  }

  bool BHMoveExchangeInterface::selectAtoms (
    BHCluster &out,
    BHClusterAnalyser &Analyzer,
    const BHMetalParameters &bhmp,
    rndEngine &rng) {
    Analyzer.calcNeighbourhood_tol (out, bhmp);
    vectorAtomID interestingAtoms[2]; // list of surface atoms of two species
    // heavy atoms
    unsigned int a = firstAtomperSpecie_[kindToExchange1_],
                 b = lastAtomperSpecie_[kindToExchange1_];
    interestingAtoms[0].clear ();
    for (unsigned int i = a; i < b; ++i) {
      if (out.getNNs_diff (i).size () > NNforHeavy_) {
        interestingAtoms[0].push_back (i);
      }
    }
    a = firstAtomperSpecie_[kindToExchange2_];
    b = lastAtomperSpecie_[kindToExchange2_];
    // light atoms
    interestingAtoms[1].clear ();
    for (unsigned int i = a; i < b; ++i) {
      if (out.getNNs_diff (i).size () > NNforLight_) {
        interestingAtoms[1].push_back (i);
      }
    }

    if (interestingAtoms[1].empty () || interestingAtoms[0].empty ()) {
      return false;
    }
    BHULongRND rnd_first (0, interestingAtoms[0].size () - 1),
      rnd_second (0, interestingAtoms[1].size () - 1);
    chosenAtom1_ = interestingAtoms[0][rnd_first (rng)];
    chosenAtom2_ = interestingAtoms[1][rnd_second (rng)];
    return true;
  }

  bool BHMoveExchangeInterface::parseSpecialized (const std::string &parsed) {
    bool toreturn = false;
    try {
#define inputgetter(variable)                                                  \
  BHParsers::parse (                                                           \
    BHParsers::BHMV::EXCHANGEINTERFACE                                         \
      [static_cast<size_t> (                                                   \
         BHParsers::BHMV::EXCHANGEINTERFACEvariable::variable)]                \
        .c_str (),                                                             \
    parsed, variable)
      toreturn |= inputgetter (fewNeighbours_);
      toreturn |= inputgetter (NNforHeavy_);
      toreturn |= inputgetter (NNforLight_);
#undef inputgetter
    } catch (const std::invalid_argument & /*ia*/) {
      throw typeName () + "::Invalid argument: \"" + parsed + "\"";
    }
    return toreturn;
  }
} // namespace BH
