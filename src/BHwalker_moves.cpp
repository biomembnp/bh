/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Definitions of the move selector and of the various moves of BHWalker

   @file B.oves.cpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 21/3/2017
   @version 0.4.1

   @todo Add support for failed minimum  displacement in ball, shell,
   highenergy, bonds moves
*/
#include "BHmetalParameters.hpp"
#include "BHwalker.hpp"
#include "BHworkWithFortran.hpp"
//#ifdef WALKERDEBUG
#include "BHdebug.hpp"
//#endif //WALKERDEBUG

//
// NB: the definition of surface atoms varies from 9 neighbours to 10 neighbours
//

namespace BH {
  void BHWalker::doMove (rnd_engine &rng, BHClusterAnalyser &Analyzer) {
    ++n_moves;
    start = std::chrono::steady_clock::now ();
    BHClusterAtoms::operator<< (
      LastAcceptedConf_); // so i know that the temp conf is the last accepted
                          // conf
    chosen_move =
      BHWalkerSettings::BHmove (rnd_move (rng)); // explicit conversion from int
    switch (chosen_move) {
    case bonds:
      Analyzer.calcNeighbourhood_tol (
        *this); // first of all i calc the neigh info
      moveBonds (rng);
      break;
    case ball:
      Analyzer.calcNeighbourhood_tol (
        *this); // first of all i calc the neigh info
      moveBall (rng);
      break;
    case shell:
      Analyzer.calcNeighbourhood_tol (
        *this); // first of all i calc the neigh info
      moveShell (rng);
      break;
    case shake:
      moveShake (rng);
      break;
    case highenergyatoms:
      moveHighEnergyAtoms (rng);
      break;
    case exchange:
      moveExchange (rng);
      break;
    case brown:
      moveBrownian (rng);
      break;
    case brownsurf:
      Analyzer.calcNeighbourhood_tol (
        *this); // first of all i calc the neigh info
      moveBrownianSurf (rng);
      break;
    default:
      moveSingle (rng);
    }
    stop = std::chrono::steady_clock::now ();
    ++countMoves_[chosen_move];
    move_time += stop - start;
  }
  /**This move consists in:
   *
   * -# evaluating the minimum number of first neighbours in the cluster
   * (_npvicmin_)
   * -# making a list of atoms with _nvic=npvicmin_ with _numvicmin_ members
   * -# making a list of atoms with _nvic=npvicmin+1_ with _numvicminp1_ members
   * [actually the second least]
   * -# choose an atom randomly in the first list
   * -# then:
   *  - **if _numvicmin_>1** choose the second atom from the first list
   *  - **if _numvicmin_=1** choose the second atom from the second list
   * -# move the first atom in the neighborhood of the second
   */
  void BHWalker::moveBonds (rnd_engine &rng) {
    if (initialized) {
      // vectorint::size_type is for discarding warnings (even if are unsigned
      // long int) initialize the list with the n of neghbours of the first
      // atoms
      vectorint::size_type lessNeig[2] = {20, 21};
      for (unsigned int i = 0; i < NofAtoms_; i++) {
        size_t i_neighs = NNlist[i].size ();
        if (i_neighs < lessNeig[0]) {
          // now the second least neighbours_number is the prevoius least one
          lessNeig[1] = lessNeig[0];
          lessNeig[0] = i_neighs;
        } else if (i_neighs < lessNeig[1] && i_neighs != lessNeig[0]) {
          lessNeig[1] = i_neighs;
        }
      }
      // make a list with the IDs of the atoms for each least n_of_neighbours
      vectorint lonelyAtoms[2];

      for (unsigned int i = 0; i < NofAtoms_; i++) {
        if (lessNeig[0] == NNlist[i].size ()) {
          lonelyAtoms[0].push_back (i);
        } else if (lessNeig[1] == NNlist[i].size ()) {
          lonelyAtoms[1].push_back (i);
        }
      }

      // NB: the distribution runs on the indexes of the atoms IDs stored in
      // lonelyAtoms[*]
      BHIntRND firstAtm (0, lonelyAtoms[0].size () - 1);
      int chosen (lonelyAtoms[0][firstAtm (rng)]), newneigh (lonelyAtoms[1][0]);

      // if the least number of neighbours is 1 or the first list of atoms is
      // made of only one atom
      if (lessNeig[0] == 1 || lonelyAtoms[0].size () == 1) {
        // select an atom from the second list
        BHIntRND secondAtm (0, lonelyAtoms[1].size () - 1);
        newneigh = lonelyAtoms[1][secondAtm (rng)];
      } else { // otherwise select the second atom from the first list
        BHIntRND secondAtm (0, lonelyAtoms[0].size () - 1);
        do {
          newneigh = lonelyAtoms[0][firstAtm (rng)];
        } while (newneigh == chosen);
      }

      Atoms_[chosen] = LastAcceptedConf_[newneigh] +
                       BHVector::RhoThetaPhi (
                         cbrt (SphereSettings_.rho_inf3 + rnd_rho (rng)),
                         acos (rnd_theta (rng)), rnd_phi (rng));

      TAccept = MovesSettings_[bonds].T_accept;
    } else
      throw "walkerNOTinitialized";
  }
  /**This move consists in:
   * -# randomly choosing an atom (iatom) of the cluster
   * -# locating the maximum distance between atoms and cdm (maxdist)
   * -# moving iatom in a randomly chosen position within a sphere centered in
   * cdm and of radius=maxdist
   * -# if iatom has thus been displaced of a quantity bigger then displamin,
   * the program returns. If the displacement is smaller, a move is attempted to
   * another point within the sphere
   */
  void BHWalker::moveBall (rnd_engine &rng) {
    if (initialized) {
      // the centre (of mass):
      BHVector CDM (0, 0, 0);
      // BHdouble tmass=0;
      // calculating the center(of mass) [without using mass]
      for (unsigned int i = 1; i < NofAtoms_; i++) {
        // BHdouble mass = Mass[Slabels[Atoms_[i].Type()]];
        CDM += Atoms_[i]; //*mass;
                          // tmass+=mass
      }
      CDM /= NofAtoms_; // /=tmass;
      BHdouble maxdist2 = Atoms_[0].dist2 (CDM);
      // calculate maxdist from cdm
      for (unsigned int i = 1; i < NofAtoms_; i++) {
        BHdouble d2 = Atoms_[i].dist2 (CDM);
        if (d2 > maxdist2)
          maxdist2 = d2;
      }
      BHdouble MD3 = maxdist2 * sqrt (maxdist2); // maximum sphere radius, cubed
      BHdouble disp2 =
        MinimumMoveDisplacement_ *
        MinimumMoveDisplacement_; // minimum distance of the move, squared
      bool short_distance =
        true; // set to true when the chosen atom is moved near its position
      int chosen = rnd_selector (rng);
      for (int i = 0; i < 10 && short_distance; i++) {
        BHVector newPos = CDM + BHVector::RhoThetaPhi (
                                  cbrt ((MD3)*Metropolis (rng)),
                                  acos (rnd_theta (rng)), rnd_phi (rng));
        if (newPos.dist2 (Atoms_[chosen]) < disp2) {
          Atoms_[chosen] = newPos;
          short_distance = false;
        }
      }
      TAccept = MovesSettings_[ball].T_accept;
    } else
      throw "walkerNOTinitialized";
  }
  /**
   *This move consists in:
   * -# randomly choosing an atom (iatom) of the cluster, among the less
   *coordinated (number of nn < 9)
   * -# locating the maximum distance between atoms and cdm (maxdist)
   * -# moving iatom in a randomly chosen position within a shell of fixed
   *thickness (shell_thickness=1.5 A) centered in cdm and of maximum
   *radius=maxdist
   * -# if iatom has thus been displaced of a quantity bigger then displamin,
   *the program returns.
   * -# If the displacement is smaller, a move is attempted to another point
   *within the shell
   */
  void BHWalker::moveShell (rnd_engine &rng) {
    if (initialized) {
      BHdouble shell_thickness =
        1.5;                 // thickness of the spherical crown, in Angstroms
      vectorint lonelyAtoms; // vector with the atoms with least neighbours
      // the centre (of mass):
      BHVector CDM (0, 0, 0);
      // BHdouble tmass=0;
      // seaching for atoms with a small number of neighbours and calculating
      // the center(of mass)
      for (unsigned int i = 1; i < NofAtoms_; i++) {
        // BHdouble mass = Mass[Slabels[Atoms_[i].Type()]];
        CDM += Atoms_[i]; //*mass;
        // tmass+=mass
        if (NNlist[i].size () < 9) {
          lonelyAtoms.push_back (i);
        }
      }
      CDM /= NofAtoms_; // /=tmass;
      BHdouble maxdist2 = Atoms_[0].dist2 (CDM);
      // calculate maxdist from cdm
      for (unsigned int i = 1; i < NofAtoms_; i++) {
        BHdouble d2 = Atoms_[i].dist2 (CDM);
        if (d2 > maxdist2)
          maxdist2 = d2;
      }
      BHdouble MD3 = maxdist2 * sqrt (maxdist2), // maximum sphere radius, cubed
        md3 = std::max (
          0., sqrt (maxdist2) - shell_thickness); // minimum sphere radius
      md3 = md3 * md3 * md3;
      BHIntRND choseAtom (0, lonelyAtoms.size () - 1);
      BHdouble disp2 =
        MinimumMoveDisplacement_ *
        MinimumMoveDisplacement_; // minimum distance of the move, squared
      bool short_distance =
        true; // set to true when the chosen atom is moved near its position
      int chosen = lonelyAtoms[choseAtom (rng)];
      for (int i = 0; i < 10 && short_distance; i++) {
        BHVector newPos = CDM + BHVector::RhoThetaPhi (
                                  cbrt (md3 - (MD3 - md3) * Metropolis (rng)),
                                  acos (rnd_theta (rng)), rnd_phi (rng));
        if (newPos.dist2 (Atoms_[chosen]) < disp2) {
          Atoms_[chosen] = newPos;
          short_distance = false;
        }
      }
      TAccept = MovesSettings_[shell].T_accept;
    } else
      throw "walkerNOTinitialized";
  }

  /// Shakes all atoms
  /**
   *This move consists in displacing every atom of the cluster
   *within a spherical shell centered in its initial position
   *whose minimum and maximum radii are written in the [input file](@ref
   *parserBHWalkerSettings)
   */
  void BHWalker::moveShake (rnd_engine &rng) {
    if (initialized) {
      int NAt = LastAcceptedConf_.NofAtoms ();
      for (int i = 0; i < NAt; i++) { // uniformly distribuited in a sphere
        Atoms_[i] = LastAcceptedConf_[i] +
                    BHVector::RhoThetaPhi (
                      cbrt (SphereSettings_.rho_inf3 + rnd_rho (rng)),
                      acos (rnd_theta (rng)), rnd_phi (rng));
      }
      TAccept = MovesSettings_[shake].T_accept;
    } else
      throw "walkerNOTinitialized";
  }
  /**
     This move consists in:<ol>
     *<li> evaluating the minimum number of pv in the cluster</li>
     *<li>if this number is .ge. 5, the shake move is applied</li>
     *<li>if this number is .lt. 5, there are two possibilities:</li>
     *<li><ol type="a">
     *<li>npvicmin .le. 3 --> all atoms with npvic=3 are displaced
     *according to the cdm-centered move.</li>
     *<li>npvicmin .eq. 4 --> atoms with npvic=4 are displaced
     *with the probability pvic4 according to the cdm-centered
     move.</li></ol></li></ol>
     */
  void BHWalker::moveHighEnergyAtoms (rnd_engine & /*rng*/) {
    throw "moveHighEnergyAtoms is not implemented, yet";
    if (initialized) {
      BHClusterAtoms::operator<< (
        LastAcceptedConf_); // so i know that the temp conf is the last
                            // accepted conf
      /*    int NAt = LastAcceptedConf_.NofAtoms();
            }
            TAccept=MovesSettings_[bonds].T_accept);*/
    } else
      throw "walkerNOTinitialized";
  }
  /// By "exchange" we mean that the positions of two atoms of different species
  /// are swapped
  /**Several types of exchange move are possibile:<dl>
   *
   *<dt>Exchange move ALL</dt>
   *<dd>Two atoms of different species are randomly picked and their positions
   *are swapped</dd>
   *
   *<dt>Exchange move SUR</dt>
   *<dd>Two surface atoms are of different species are randomly picked and their
   *positions swapped</dd>
   *
   *<dt>Exchange move CSH</dt>
   *<dd>This move favors core-shell ordering. It depends on two parameters:
   *bignn_csh and smallnn_csh. A small surface atom, that have at least
   *bignn_csh neighbors, is swapped with a big volume atom that has at least
   *smallnn_csh small neighbors.</dd>
   *
   *<dt>Exchange move INT</dt>
   *<dd>This move swaps atoms that have more than bignn (for small atoms) or
   *more than smallnn (for big atoms) neighbors. It thus acts on atoms that are
   *isolated within or at the interface with the other species</dd>
   *
   *<dt>Exchange move SEP</dt>
   *<dd>This move works in two opposite ways:<dl>
   *<dt>BHExchangeOp#small_inside = <i>true</i></dt><dd>the move swaps a small
   *atom that is in the volume and has many big neighbors, or is in the surface
   *and has several big neighbors, with a big atom that is in the volume and has
   *many small neighbors</dd> <dt>BHExchangeOp#small_inside =
   *<i>false</i></dt><dd>as before, but the roles of the small and big atoms are
   *swapped. This is useful for binary systems where the small atoms are not the
   *more cohesive, such as Cu-Rh</dd></dl></dd>
   *
   *<dt>Exchange move MIX</dt>
   *<dd>This move swaps atoms of different species that have many atom of their
   *same species</dd></dl>
   */
  void BHWalker::moveExchange (rnd_engine &rng) {
    if (initialized) {
      typeSelected1_ = 0;
      typeSelected2_ = 1;
      if (NofSpecies () > 2) { // if NofSpecies()==2 is already set
        typeSelected1_ = rnd_Type (rng);
        typeSelected2_ = rnd_Type (rng);
        while (typeSelected2_ ==
               typeSelected1_) // try until i selected a second species
          typeSelected2_ = rnd_Type (rng);
        // when needed the lower species-index is expected to be the index of
        // the more massive atoms i set typeSelected1_ to have the lesser index:
        if (typeSelected2_ > typeSelected1_)
          std::swap (typeSelected1_, typeSelected2_);
      }
      BHClusterAtoms::operator<< (
        LastAcceptedConf_); // so i know that the temp conf is the last
                            // accepted conf
      chosen_exch = BHWalkerSettings::BHexchange (rnd_exch (rng));
      // std::cin.get();
      switch (chosen_exch) {
      case All: { // exchange all
        TAccept = ExchangesSettings_[All].T_accept;
        moveExch_All (rng);
        break;
      }
      case Surf: { // exchange surf
        BHMetalParameters::getBHMP ().calcNeighbourhood_tol (
          *this); // first of all i calc the neigh info
        moveExch_onSurface (rng);
        TAccept = ExchangesSettings_[Surf].T_accept;
        break;
      }
      case Bulk: { // exchange bulk
        BHMetalParameters::getBHMP ().calcNeighbourhood_tol (
          *this); // first of all i calc the neigh info
        moveExch_inBulk (rng);
        TAccept = ExchangesSettings_[Bulk].T_accept;
        break;
      }
      case Csh: { // exchange csh(core-shell)
        BHMetalParameters::getBHMP ().calcNeighbourhood_tol (
          *this); // first of all i calc the neigh info
        moveExch_CoreShell (rng);
        TAccept = ExchangesSettings_[Csh].T_accept;
        break;
      }
      case Int: { // exchange int
        BHMetalParameters::getBHMP ().calcNeighbourhood_tol (
          *this); // first of all i calc the neigh info
        moveExch_Interface (rng);
        TAccept = ExchangesSettings_[Int].T_accept;
        break;
      }
      case Sep: { // exchange Sep
        BHMetalParameters::getBHMP ().calcNeighbourhood_tol (
          *this); // first of all i calc the neigh info
        moveExch_Sep (rng);
        TAccept = ExchangesSettings_[Sep].T_accept;
        break;
      }
      case Mix: { // exchange Mix
        BHMetalParameters::getBHMP ().calcNeighbourhood_tol (
          *this); // first of all i calc the neigh info
        moveExch_Mix (rng);
        TAccept = ExchangesSettings_[Mix].T_accept;
        break;
      }
      default: { // in case of errors do an exchange_all
        TAccept = ExchangesSettings_[All].T_accept;
        chosenAtom1_ = rnd_selByType[typeSelected1_]->operator() (rng);
        chosenAtom2_ = rnd_selByType[typeSelected2_]->operator() (rng);
        throw "exch:randomness problems";
      }
      }
      /*
      std::cerr << chosenAtom1_ <<std::endl;
      std::cerr << chosenAtom2_ <<std::endl;*/
      // std::cin.get();
      Atoms_[chosenAtom1_].Pos_Swap (Atoms_[chosenAtom2_]);
      ++countExchanges_[chosen_exch];
    } else
      throw "walkerNOTinitialized";
  }
  /**
   *Evolve the system with a small number of steps by bringing the sistem to an
   *high temperature, then evolving it by simulating a brownian motion of the
   *atoms, influenced by the forces of the cluster
   */
  void BHWalker::moveBrownian (rnd_engine &rng) {
    if (initialized) {
      // distribuition that convert the output of a plain rng in a normal
      // distribuition
      std::normal_distribution<double> gauss (0.0, 1.0);
      static const BHdouble cbltz = 1.38e-23,
                            fric = 1e12, // friction
        tstep = 5e-15,                   // timestep
        mass_bro = 1.67e-25; // a mass smaller than the one of Ag, for all atoms
      /*      factv=dsqrt(cbltz*temp_brow/mass_bro)
              fact_bro_1=dsqrt(2.d0*cbltz*temp_brow*fric*tstep_bro/(mass_bro))*1d10
         !Angs/sec fact_bro_2=(16.d0/arete(1))*tstep_bro/mass_bro*/
      // some constants:
      const BHdouble fact_v =
                       sqrt (cbltz * MovesSettings_[brown].T_move / mass_bro),
                     fact_b1 = sqrt (
                                 2. * (cbltz * MovesSettings_[brown].T_move) *
                                 (fric * tstep) / mass_bro) *
                               1e10, // convert to Angs/sec
        fact_b2 =
          16. * tstep / mass_bro; // in the original BH here is divided by
                                  // arete, but i have discarded that feature
      /*cout << fact_v <<endl
        << fact_b1 <<endl
        << fact_b2 <<endl;*/
      BHVector *v = new BHVector[NofAtoms_];            // velocities
      BHdouble *gradient = new BHdouble[3 * NofAtoms_]; // forces
      for (unsigned int i = 0; i < NofAtoms_;
           i++) { // initial velocities are normally distribuited
        v[i] = fact_v * BHVector (gauss (rng), gauss (rng), gauss (rng));
      }
      for (unsigned int j = 0; j < NofAtoms_; j++) {
        x[3 * j] = Atoms_[j].X ();
        x[3 * j + 1] = Atoms_[j].Y ();
        x[3 * j + 2] = Atoms_[j].Z ();
      }
      // then i proceed with the evolution of the sistem:

      energyCalculator_.Neighbours_for_tol (x);
      for (unsigned int step = 0; step < MovesSettings_[brown].nsteps;
           step++) { // this cannot be a parallel loop
        // BHWWF::BHforces(x,F);
        if (step % 500 == 0) {
          energyCalculator_.Neighbours_for_tol (x);
        }
        energyCalculator_.Gradient_tol (x, gradient);
        // BHMetalParameters::getBHMP().calcNeighINT(*this);
        // BHMetalParameters::getBHMP().calcMMEnergy(*this,F);
        // this is an Euler-Cromer evolution:
        for (unsigned int i = 0; i < NofAtoms_;
             i++) { // this can be parallel but i need a way to protect rng
          v[i] += fact_b1 * BHVector (gauss (rng), gauss (rng), gauss (rng)) -
                  (fric * tstep) * v[i] -
                  fact_b2 * BHVector (
                              gradient[3 * i], gradient[3 * i + 1],
                              gradient[3 * i + 2]);
          x[3 * i] += v[i].X () * tstep;
          x[3 * i + 1] += v[i].Y () * tstep;
          x[3 * i + 2] += v[i].Z () * tstep;
        }
        // think about other types of evolution
        /*/DELETE
          for (unsigned int i=0; i<NofAtoms_; i++) {//this can be parallel but i
          need a way to protect rng Atoms_[i].X(x[3*i]); Atoms_[i].Y(x[3*i+1]);
          Atoms_[i].Z(x[3*i+2]);
          }
          std::ofstream f("ani.xyz",std::ofstream::out|std::ofstream::app);
          f << *this <<'\n';
          f.close();
          //DELETE
          */
      }
      for (unsigned int i = 0; i < NofAtoms_;
           i++) { // this can be parallel but i need a way to protect rng
        Atoms_[i].X (x[3 * i]);
        Atoms_[i].Y (x[3 * i + 1]);
        Atoms_[i].Z (x[3 * i + 2]);
      }
      delete[] v;
      delete[] gradient;
      TAccept = MovesSettings_[brown].T_accept;
    } else
      throw "walkerNOTinitialized";
  }

  /**
   *Evolve the system with a small number of steps by bringing the sistem to an
   *high temperature, then evolving it by simulating a brownian motion of the
   *atoms, influenced by the forces of the cluster but by moving only the atoms
   *with less bonds, which are assumed to be the surface ones
   */
  void BHWalker::moveBrownianSurf (rnd_engine &rng) {
    if (initialized) {
      // distribuition that convert the output of a plain rng in a normal
      // distribuition
      std::normal_distribution<double> gauss (0.0, 1.0);

      static const BHdouble cbltz = 1.38e-23,
                            fric = 1e12, // friction
        tstep = 5e-15,                   // timestep
        mass_bro = 1.67e-25; // a mass smaller than the one of Ag, for all atoms
      // some constants:
      const BHdouble fact_v = sqrt (
                       cbltz * MovesSettings_[brownsurf].T_move / mass_bro),
                     fact_b1 =
                       sqrt (
                         2. * (cbltz * MovesSettings_[brownsurf].T_move) *
                         (fric * tstep) / mass_bro) *
                       1e10, // convert to Angs/sec
        fact_b2 =
          16. * tstep / mass_bro; // in the original BH here is divided by
                                  // arete, but i have discarded that feature
      // I create the list of surface atoms:
      // calculate the the three least number of neighbours
      static const unsigned short few_neigh =
        11; // if 10 or less neighbours the atom is considered to be in
            // surface
      vectorint surfAtoms; // list of surface atoms
      for (unsigned int i = 1; i < NofAtoms_; i++) {
        if (NNlist[i].size () < few_neigh) {
          surfAtoms.push_back (i);
        }
      }
      BHVector *v = new BHVector[NofAtoms_];            // velocities
      BHdouble *gradient = new BHdouble[3 * NofAtoms_]; // forces
      for (const int &i :
           surfAtoms) { // initial velocities are normally distribuited
        v[i] = fact_v * BHVector (gauss (rng), gauss (rng), gauss (rng));
      }
      for (unsigned int j = 0; j < NofAtoms_; j++) {
        x[3 * j] = Atoms_[j].X ();
        x[3 * j + 1] = Atoms_[j].Y ();
        x[3 * j + 2] = Atoms_[j].Z ();
      }
      // then i proceed with the evolution of the sistem:
      for (unsigned int step = 0; step < MovesSettings_[brownsurf].nsteps;
           step++) { // this cannot be a parallel loop
        // BHWWF::BHforces(x,F);
        energyCalculator_.Gradient (x, gradient);
        // BHMetalParameters::getBHMP().calcNeighINT(*this);
        // BHMetalParameters::getBHMP().calcMMEnergy(*this,F);
        // this is an Euler-Cromer evolution:
        for (const int &i : surfAtoms) { // this can be parallel but i need a
                                         // way to protect rng
          v[i] += fact_b1 * BHVector (gauss (rng), gauss (rng), gauss (rng)) -
                  (fric * tstep) * v[i] -
                  fact_b2 * BHVector (
                              gradient[3 * i], gradient[3 * i + 1],
                              gradient[3 * i + 2]);
          x[3 * i] += v[i].X () * tstep;
          x[3 * i + 1] += v[i].Y () * tstep;
          x[3 * i + 2] += v[i].Z () * tstep;
        }
      }
      for (const int &i : surfAtoms) { // this can be parallel
        Atoms_[i].X (x[3 * i]);
        Atoms_[i].Y (x[3 * i + 1]);
        Atoms_[i].Z (x[3 * i + 2]);
      }
      delete[] v;
      delete[] gradient;
      TAccept = MovesSettings_[brownsurf].T_accept;
    } else
      throw "walkerNOTinitialized";
  }

  /// Shakes a single atom
  /**
   *This move consists in displacing one atom (randomly chosen) of the cluster
   *within a spherical shell centered in its initial position
   *whose minimum and maximum radii are written in the input file
   */
  void BHWalker::moveSingle (rnd_engine &rng) {
    if (initialized) {
      int i = rnd_selector (rng);
      Atoms_[i] = LastAcceptedConf_[i] +
                  BHVector::RhoThetaPhi (
                    cbrt (SphereSettings_.rho_inf3 + rnd_rho (rng)),
                    acos (rnd_theta (rng)), rnd_phi (rng));
      TAccept = SingleMoveTemperature_;
    } else {
      throw "walkerNOTinitialized";
    }
  }
} // namespace BH
