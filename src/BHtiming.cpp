/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Contains the definitions for the time analisys
   @file BHtiming.cpp
   @author Daniele Rapetti (iximie@gmail.com)
   @date 21/12/2017
   @version 1.0
*/
#include "BHtiming.hpp"
#include <iostream>

namespace BH {
  void timing (
    std::ostream &stream, const std::chrono::steady_clock::duration &obj) {
    if (obj < std::chrono::duration<int, std::milli> (5000)) {
      stream << std::chrono::duration<double, std::milli> (obj).count ()
             << " ms";
    } else {
      stream << std::chrono::duration<double> (obj).count () << " s";
    }
  }
} // namespace BH
