/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#include "BHwalkerSupport.hpp"
namespace BH {
  BHmoveSelector BHWalkerSupport::MoveDistributionGenerator (
    const std::vector<std::unique_ptr<BHMove>> &moves) {
    std::vector<double> weights (moves.size ());
    for (size_t i = 0; i < moves.size (); ++i) {
      weights[i] = moves[i]->getProbability ();
    }
    return BHmoveSelector (weights.begin (), weights.end ());
  }
} // namespace BH
