/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief a program to select the best minima in a given OP interval, to be used
   after signatureAnalyser

   @file ppanalyser.cxx
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 22/02/2018
   @version 0.10

   Separated analysis function from main program in order to provide them in a
   library

   @date 30/8/2017
   @version 0.1
*/
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>
#include <map>
#include <sstream>
#include <vector>

#include "BHanalysisSupport.hpp"

/*
  std::vector<std::string> split_onspace(const std::string &s);

*/

using namespace BH;

int main (int /*argc*/, char ** /*argv*/) {
  std::string ener_all = "ener_all.out";
  std::string analysis = "analysis.out";
  std::cout << "How to use:\n"
            << "This utility is not much flexible: "
            << "can be used only after a use of signatureAnalyzer "
            << "on a full basin hopping run.\n\n"
            << "In the folder are needed two files:\n"
            << "*" << ener_all << "\toutput from mover\n"
            << "*" << analysis << "\toutput from signatureAnalyzer\n"
            << "this program will output in the file \"param_best.out\" the "
               "list of best minima for a given OP"
            << std::endl;
  try {
    BHAnalysisSupport Analysis;
    Analysis.loadDataFromFiles (ener_all, analysis, true);

    std::vector<BHOrderParameter> myOPs;

    { // this isolates the variables used there
      std::ifstream analysis_out (analysis);
      std::string dummy;
      getline (analysis_out, dummy);
      std::stringstream ss;
      ss.str (dummy);
      ss >> dummy; // this discards the first column
      std::cout << dummy << " ";
      ss >> dummy;
      while (ss) {
        myOPs.emplace_back (dummy);
        std::cout << dummy << " " << myOPs.size ();
        ss >> dummy;
      }
      std::cout << std::endl;
      analysis_out.close ();
    }

    std::cout << "Working modes:\n";
    const int wmodes = 2;
    std::string wmode_names[] = {"1D", "2D"};

    enum workmodes { oneD = 0, twoD = 1 };

    for (int i (0); i < wmodes; ++i) {
      std::cout << "\t" << i << ") " << wmode_names[i] << "\n";
    }
    int selected_workmode = oneD;
    std::cout << "Select working mode: ";
    std::cin >> selected_workmode;

    std::cout << "Order Parameters present in \"" << analysis << "\":\n";
    for (unsigned int i (0); i < myOPs.size (); ++i) {
      std::cout << "\t" << i << ") " << OparName (myOPs[i]) << "\n";
    }
    size_t OPsize = myOPs.size ();
    switch (selected_workmode) {
    case oneD: {
      std::cout << "Select the order parameter that you want to use: ";
      unsigned theOP;
      std::cin >> theOP;
      if (theOP >= OPsize)
        throw "Error in selecting the Order Parameter";

      std::cout << "Will be used: \033[1m" << OparName (myOPs[theOP])
                << "\033[0m, with how many bins? ";
      unsigned nbins;
      std::cin >> nbins;
      Analysis.oneDimensionAnalysis (theOP, nbins);
      Analysis.oneDimensionAnalysis2OutFile ("param_best.out");
      break;
    }
    case twoD: {
      std::cout << "Select the order parameters that you want to use:\n";
      unsigned theOPs[] = {0, 3};
      std::string tmp[] = {"First:\t", "Second:\t"};
      unsigned nbins[] = {12, 50};
      for (int i (0); i < 2; ++i) {
        std::cout << tmp[i];
        // std::cout << theOPs[i] <<std::endl;
        std::cin >> theOPs[i];
        std::cout << "Will be used: \033[1m" << OparName (myOPs[theOPs[i]])
                  << "\033[0m, with how many bins? ";
        std::cin >> nbins[i];
        // std:: cout << nbins[i] <<std::endl;
        if (theOPs[i] >= OPsize)
          throw "Error in selecting the Order Parameter";
      }
      Analysis.twoDimensionAnalysis (theOPs, nbins);
      Analysis.twoDimensionAnalysis2OutFile ("param_histo.out");
      break;
    }
    }
  } catch (const char *problem) {
    std::cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m"
              << std::endl;
  } catch (const std::string &problem) {
    std::cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m"
              << std::endl;
  }

  return 0;
}
/*
  std::vector<std::string> split_onspace(const std::string &s) {
  std::stringstream ss;
  ss.str(s);
  std::vector<std::string> obj;
  while (ss) {
  std::string item;
  ss >> item;
  obj.push_back(item);
  }
  return obj;
  }
*/
