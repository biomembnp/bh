/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#include "BHmoveExchangeMix.hpp"
#include "BHparsers.hpp"
#include <sstream>
namespace BH {

  BHMoveExchangeMix::BHMoveExchangeMix () : BHMoveExchange ("exchangeMix") {}

  BHMoveExchangeMix::~BHMoveExchangeMix () = default;

  std::string BHMoveExchangeMix::DefaultString () {
    return "prob = 1.0, accTemp = 100, fewNeighbours = 11";
  }

  std::unique_ptr<BHMove> BHMoveExchangeMix::clone () {
    return std::unique_ptr<BHMove>{new BHMoveExchangeMix (*this)};
  }

  std::string BHMoveExchangeMix::printSettingsSpecialized () const {
    std::stringstream ss;
    ss << "Weight exponent: " << exponentialWeight_
       << ", Inside: " << (smallInside_ ? "small" : "big") << ", Surf if NN < "
       << fewNeighbours_;
    return ss.str ();
  }

  std::string BHMoveExchangeMix::printSettingsSpecializedForInput () const {
    std::stringstream ss;
#define outputwriter(variable)                                                 \
  ", " << BHParsers::BHMV::EXCHANGEMIX[static_cast<size_t> (                   \
            BHParsers::BHMV::EXCHANGEMIXvariable::variable)]                   \
       << " = " << variable
    std::boolalpha (ss);
    ss << outputwriter (exponentialWeight_) << outputwriter (smallInside_)
       << outputwriter (fewNeighbours_);
#undef outputwriter
    return ss.str ();
  }

  bool BHMoveExchangeMix::selectAtoms (
    BHCluster &out,
    BHClusterAnalyser &Analyzer,
    const BHMetalParameters &bhmp,
    rndEngine &rng) {
    Analyzer.calcNeighbourhood_tol (out, bhmp);
    size_t kTE1 = kindToExchange1_, kTE2 = kindToExchange2_;
    if (smallInside_) {
      kTE1 = kindToExchange2_;
      kTE2 = kindToExchange1_;
    }
    unsigned int fmin, smin;
    // atoms that will go inside
    unsigned int a = firstAtomperSpecie_[kTE2], b = lastAtomperSpecie_[kTE2];
    fmin = a;
    std::vector<BHdouble> firstweights (b - a + 1);
    for (unsigned int i = a; i <= b; ++i) {
      if (out.getNNs (i).size () > fewNeighbours_) {
        firstweights[i - a] =
          std::pow (out.getNNs_same (i).size () + 1, exponentialWeight_);
      } else {
        firstweights[i - a] = std::pow (
          out.getNNs_same (i).size () + 2 + fewNeighbours_ -
            out.getNNs (i).size (),
          exponentialWeight_);
      }
      // cout << i<<" "<<i-a <<" " << firstweights[i-a]<<endl;
    }
    // std::cout << NofAtoms_ <<std::endl;
    a = firstAtomperSpecie_[kTE1];
    b = lastAtomperSpecie_[kTE1];

    smin = a;
    std::vector<BHdouble> secondweights (b - a + 1);
    // atoms that are wanted ouside
    for (unsigned int i = a; i <= b; ++i) {
      secondweights[i - a] =
        std::pow (out.getNNs_same (i).size () + 1, exponentialWeight_);
    }
    // std::cout << a << " "<< b <<" "<<b-a <<" "<<
    // secondweights.size()<<std::endl;
    std::discrete_distribution<unsigned int> frnd (
      firstweights.begin (), firstweights.end ()),
      srnd (secondweights.begin (), secondweights.end ());
    chosenAtom1_ = fmin + frnd (rng);
    chosenAtom2_ = smin + srnd (rng);
    return true;
  }

  bool BHMoveExchangeMix::parseSpecialized (const std::string &parsed) {
    bool toreturn = false;
    try {
#define inputgetter(variable)                                                  \
  BHParsers::parse (                                                           \
    BHParsers::BHMV::EXCHANGEMIX                                               \
      [static_cast<size_t> (BHParsers::BHMV::EXCHANGEMIXvariable::variable)]   \
        .c_str (),                                                             \
    parsed, variable)
      toreturn |= inputgetter (fewNeighbours_);
      toreturn |= inputgetter (exponentialWeight_);
      toreturn |= inputgetter (smallInside_);
#undef inputgetter
    } catch (const std::invalid_argument & /*ia*/) {
      throw typeName () + "::Invalid argument: \"" + parsed + "\"";
    }
    return toreturn;
  }

} // namespace BH
