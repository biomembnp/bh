/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#include "BHenums.hpp"
namespace BH {
  BHminimizationAlgorithmType minAlgorithmFromStr (std::string algoname) {
    // if it does not know the  name assign the stnd algorithm to the walker
    algoname.erase (0, algoname.find_first_not_of ("\t\n\v\f\r "));
    algoname.erase (algoname.find_last_not_of ("\t\n\v\f\r ") + 1);
#define MINALGO_PARSER
#define MinAlgorithmParser(key, Class)                                         \
  else if (algoname == #key) {                                                 \
    return BHminimizationAlgorithmType::key;                                   \
  }

    if (algoname.empty ()) {
      return BHminimizationAlgorithmType::NofKnownAlgoriythms;
    }
#include "BHminimizationAlgorithms.hpp"
    else {
      return BHminimizationAlgorithmType::NofKnownAlgoriythms;
    }

#undef MinAlgorithmParser
#undef MINALGO_PARSER
  }

  std::string
  strFromMinimizationAlgorithm (const BHminimizationAlgorithmType algo) {
#define MINALGO_PARSER
#define MinAlgorithmParser(key, Class)                                         \
  case BHminimizationAlgorithmType::key:                                       \
    return #key;

    switch (algo) {
#include "BHminimizationAlgorithms.hpp"
    default:
      return "undef";
    }

#undef MinAlgorithmParser
#undef MINALGO_PARSER
  }

  BHwalkerAlgorithmType walkerAlgorithmFromStr (std::string algoname) {
    // if it does not know the  name assign the stnd algorithm to the walker
    algoname.erase (0, algoname.find_first_not_of ("\t\n\v\f\r "));
    algoname.erase (algoname.find_last_not_of ("\t\n\v\f\r ") + 1);
#define WALKER_ALGORITHM_PARSER
#define WalkerAlgorithmParser(key, Class, standalone, skipSteps)               \
  else if (algoname == #key) {                                                 \
    return BHwalkerAlgorithmType::key;                                         \
  }

    if (algoname.empty ()) {
      return BHwalkerAlgorithmType::NofKnownAlgoriythms;
    }
#include "BHwalkerAlgorithms.hpp"
    else {
      return BHwalkerAlgorithmType::NofKnownAlgoriythms;
    }

#undef WalkerAlgorithmParser
#undef WALKER_ALGORITHM_PARSER
  }

  std::string strFromWalkerAlgorithm (const BHwalkerAlgorithmType algo) {
#define WALKER_ALGORITHM_PARSER
#define WalkerAlgorithmParser(key, Class, standalone, skipSteps)               \
  case BHwalkerAlgorithmType::key:                                             \
    return #key;

    switch (algo) {
#include "BHwalkerAlgorithms.hpp"
    default:
      return "undef";
    }

#undef WalkerAlgorithmParser
#undef WALKER_ALGORITHM_PARSER
  }

} // namespace BH
