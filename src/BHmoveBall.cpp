/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#include <sstream>

#include "BHmoveBall.hpp"
#include "BHparsers.hpp"

namespace BH {

  BHMoveBall::BHMoveBall ()
    : BHMove ("ball"),
      MinimumMoveDisplacementSQ_ (
        MinimumMoveDisplacement_ * MinimumMoveDisplacement_) {}

  /* in gcc the default copy ctor is good enough untill there are no pointers
  BHMoveBall::BHMoveBall (const BHMoveBall &other)
    : BHMove (other),
      MinimumMoveDisplacement_{other.MinimumMoveDisplacement_},
      MinimumMoveDisplacementSQ_{other.MinimumMoveDisplacementSQ_},
      rndSelector_{other.rndSelector_} {}
*/
  BHMoveBall::~BHMoveBall () = default;

  std::string BHMoveBall::DefaultString () {
    return "prob = 1.0, accTemp = 300, minimumDisplacement =3.0";
  }

  std::unique_ptr<BHMove> BHMoveBall::clone () {
    return std::unique_ptr<BHMove>{new BHMoveBall (*this)};
  }

  std::string BHMoveBall::printSettingsSpecialized () const {
    std::stringstream ss;
    ss << "Minimum displacement: " << MinimumMoveDisplacement_;
    return ss.str ();
  }

  std::string BHMoveBall::printSettingsSpecializedForInput () const {
    std::stringstream ss;
    ss << ", " << BHParsers::BHMV::BALL[0] << " = " << MinimumMoveDisplacement_;
    return ss.str ();
  }

  bool BHMoveBall::doMoveAlgorithm (
    BHCluster &in,
    BHCluster &out,
    BHClusterAnalyser &,
    const BHMetalParameters &,
    rndEngine &rng) {
    out << in;
    size_t NofAtoms = in.getNofAtoms ();
    vectorAtomID lonelyAtoms; // vector with the atoms with least neighbours
    // the centre (of mass):
    BHVector GeometricCenter = out.getGeometricCenter ();

    BHdouble maxdistSQ = out[0].dist2 (GeometricCenter);
    // calculate maxdist from cdm
    for (unsigned i = 1; i < NofAtoms; i++) {
      BHdouble d2 = out[i].dist2 (GeometricCenter);
      if (d2 > maxdistSQ) {
        maxdistSQ = d2;
      }
    }

    BHdouble maxdist = sqrt (maxdistSQ); // maximum sphere radius

    bool short_distance =
      true; // set to true when the chosen atom is moved near its position
    auto chosen = rndSelector_ (rng);
    for (unsigned i = 0; i < 10 && short_distance; i++) {
      BHVector newPos = GeometricCenter + BHVector::RandomVector (rng, maxdist);
      if (newPos.dist2 (out[chosen]) > MinimumMoveDisplacementSQ_) {
        out[chosen] = newPos;
        short_distance = false;
      }
    }
    return !short_distance;
  }

  void BHMoveBall::Specialize (const BHCluster &in, const BHMetalParameters &) {
    rndSelector_.param (BHUIntRND::param_type (0u, in.getNofAtoms () - 1));
  }

  bool BHMoveBall::parseSpecialized (const std::string &parsed) {
    bool toreturn = false;
    try {
      toreturn |= BHParsers::parse (
        BHParsers::BHMV::BALL[0].c_str (), parsed, MinimumMoveDisplacement_);
    } catch (const std::invalid_argument & /*ia*/) {
      throw typeName () + "::Invalid argument: \"" + parsed + "\"";
    }
    return toreturn;
  }

  bool BHMoveBall::parseAfter () {
    MinimumMoveDisplacementSQ_ =
      MinimumMoveDisplacement_ * MinimumMoveDisplacement_;
    return true;
  }
} // namespace BH
