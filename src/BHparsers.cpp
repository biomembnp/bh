/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/** @brief contain the declaration of the parsers

  @file BHparsers.cpp

      @date 3/4/2019
      @version 1.0
    */
#include "BHparsers.hpp"
#include <algorithm>
#include <iostream>
#include <regex>
#include <sstream>
#include <utility>
namespace BH {
  namespace BHParsers {

    template <class T>
    T sto (const std::string &str) {
      return T (str);
    }

    template <>
    double sto<double> (const std::string &str) {
      return stod (str);
    }

    template <>
    int sto<int> (const std::string &str) {
      return stoi (str);
    }

    template <>
    size_t sto<size_t> (const std::string &str) {
      return static_cast<size_t> (stoll (str));
    }

    template <>
    unsigned int sto<unsigned int> (const std::string &str) {
      return static_cast<unsigned> (stoi (str));
    }

    template <>
    bool sto<bool> (const std::string &str) {
      return str.find ("true") != std::string::npos;
    }

    template <>
    std::string sto<std::string> (const std::string &str) {
      return std::regex_replace (str, std::regex ("(^[ \t]+)|([\t ]+$)"), "");
      // return std::regex_replace (str, std::regex ("^ +| +$|( ) +"), "$1");
      /*std::string tmp = std::regex_replace (str, std::regex ("/^ /"), "");
      return std::regex_replace (tmp, std::regex ("/ $/"), "");
*/
    }

    template <class T>
    bool parseTemplate (std::string key, std::string &&where, T &variable) {
      std::string parsed (where);
      std::transform (key.begin (), key.end (), key.begin (), ::tolower);
      std::transform (
        parsed.begin (), parsed.end (), parsed.begin (), ::tolower);

      std::size_t eq_pos = where.find ('=');
      bool parsedCorreclty = false;
      if (eq_pos != std::string::npos) {
        std::stringstream ss (parsed.substr (0, eq_pos));
        std::string token;
        ss >> token;
        /*std::cout <<"\""<< key << "\" \"" << parsed <<"\""<< "\" \"" << token
         <<"\""<<std::endl;*/
        if (token == key) {
          // in this way we go after the equal
          ++eq_pos;
          variable = sto<T> (where.substr (eq_pos));
          parsedCorreclty = true;
        }
      }
      return parsedCorreclty;
    }

    bool parse (std::string tofind, std::string where, BHdouble &variable) {
      return parseTemplate (tofind, std::move (where), variable);
    }

    bool parse (std::string tofind, std::string where, int &variable) {
      return parseTemplate (tofind, std::move (where), variable);
    }

    bool parse (std::string tofind, std::string where, unsigned int &variable) {
      return parseTemplate (tofind, std::move (where), variable);
    }

    bool parse (std::string tofind, std::string where, size_t &variable) {
      return parseTemplate (tofind, std::move (where), variable);
    }

    bool parse (std::string tofind, std::string where, bool &variable) {
      return parseTemplate (tofind, std::move (where), variable);
    }
    bool parse (std::string tofind, std::string where, std::string &variable) {
      return parseTemplate (tofind, std::move (where), variable);
    }
  } // namespace BHParsers
} // namespace BH
