/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#include "BHmoveExchangeOnSurface.hpp"
#include "BHparsers.hpp"
#include <sstream>

namespace BH {
  BHMoveExchangeOnSurface::BHMoveExchangeOnSurface ()
    : BHMoveExchange ("exchangeOnSurface") {}

  BHMoveExchangeOnSurface::~BHMoveExchangeOnSurface () = default;

  std::string BHMoveExchangeOnSurface::DefaultString () {
    return "prob = 1.0, accTemp = 100, fewNeighbours = 11";
  }

  std::unique_ptr<BHMove> BHMoveExchangeOnSurface::clone () {
    return std::unique_ptr<BHMove>{new BHMoveExchangeOnSurface (*this)};
  }

  std::string BHMoveExchangeOnSurface::printSettingsSpecialized () const {
    std::stringstream ss;
    ss << "Surf if NN < " << fewNeighbours_;
    return ss.str ();
  }

  std::string
  BHMoveExchangeOnSurface::printSettingsSpecializedForInput () const {
    std::stringstream ss;
#define outputwriter(variable)                                                 \
  ", " << BHParsers::BHMV::EXCHANGEONSURFACE[static_cast<size_t> (             \
            BHParsers::BHMV::EXCHANGEONSURFACEvariable::variable)]             \
       << " = " << variable
    ss << outputwriter (fewNeighbours_);
#undef outputwriter
    return ss.str ();
  }

  bool BHMoveExchangeOnSurface::selectAtoms (
    BHCluster &out,
    BHClusterAnalyser &Analyzer,
    const BHMetalParameters &bhmp,
    rndEngine &rng) {
    Analyzer.calcNeighbourhood_tol (out, bhmp);
    vectorAtomID firstSurfAtoms,
      secondSurfAtoms; // list of surface atoms of two species
    unsigned a, b;
    bool firstisInWork = true;
    size_t redoNo = 0;
    while (firstisInWork) {
      a = firstAtomperSpecie_[kindToExchange1_];
      b = lastAtomperSpecie_[kindToExchange1_];
      for (unsigned i = a; i <= b; ++i) {
        if (out.getNNs (i).size () >= fewNeighbours_) {
          firstSurfAtoms.push_back (i);
        }
      }
      if (firstSurfAtoms.empty ()) {
        if (redoNo > NofSpecies_) {
          return false;
        }
        ++redoNo;
        if (NofSpecies_ > 2) {
          size_t kindToExchangePrevious = kindToExchange1_;
          // try until i selected a second species
          while (kindToExchange2_ == kindToExchange1_ ||
                 kindToExchange1_ == kindToExchangePrevious) {
            ++kindToExchange1_;
            if (kindToExchange1_ >= NofSpecies_) {
              kindToExchange1_ = 0;
            }
          }
        } else {
          return false;
        }
      } else {
        firstisInWork = false;
      }
    }
    // second bulk atoms
    bool secondisInWork = true;
    redoNo = 0;
    while (secondisInWork) {
      a = firstAtomperSpecie_[kindToExchange2_];
      b = lastAtomperSpecie_[kindToExchange2_];
      for (unsigned i = a; i <= b; ++i) {
        if (out.getNNs (i).size () >= fewNeighbours_) {
          secondSurfAtoms.push_back (i);
        }
      }
      if (secondSurfAtoms.empty ()) {
        if (redoNo > NofSpecies_) {
          return false;
        }
        ++redoNo;
        if (NofSpecies_ > 2) {
          size_t kindToExchangePrevious = kindToExchange2_;
          // try until i selected a second species
          while (kindToExchange1_ == kindToExchange2_ ||
                 kindToExchange2_ == kindToExchangePrevious) {
            ++kindToExchange2_;
            if (kindToExchange2_ >= NofSpecies_) {
              kindToExchange1_ = 0;
            }
          }
        } else {
          return false;
        }
      } else {
        secondisInWork = false;
      }
    }
    BHULongRND frnd (0, firstSurfAtoms.size () - 1);
    BHULongRND srnd (0, secondSurfAtoms.size () - 1);
    chosenAtom1_ = firstSurfAtoms[frnd (rng)];
    chosenAtom2_ = secondSurfAtoms[srnd (rng)];
    return true;
  }
  bool BHMoveExchangeOnSurface::parseSpecialized (const std::string &parsed) {
    bool toreturn = false;
    try {
#define inputgetter(variable)                                                  \
  BHParsers::parse (                                                           \
    BHParsers::BHMV::EXCHANGEONSURFACE                                         \
      [static_cast<size_t> (                                                   \
         BHParsers::BHMV::EXCHANGEONSURFACEvariable::variable)]                \
        .c_str (),                                                             \
    parsed, variable)
      toreturn |= inputgetter (fewNeighbours_);
#undef inputgetter

    } catch (const std::invalid_argument & /*ia*/) {
      throw typeName () + "::Invalid argument: \"" + parsed + "\"";
    }
    return toreturn;
  }
} // namespace BH
