/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#include "BHconstructors.hpp"
#include "BHminimizationAlgorithms.hpp"
#include "BHmoves.hpp"
#include "BHwalkerAlgorithms.hpp"

namespace BH {
  std::unique_ptr<BHMove>
  createMoveFromString (const std::string moveName, const std::string options) {
    /*
    // writing and calling the lambda in the same statement
    std::unique_ptr<BHMove> move (
        [] (const std::string &name) -> BHMove * {
          if (name.empty ()) {
            throw "Cannot initialize a empty-name move";
          }
// I'm copiyng this without any shame from LAMMPS
#define MOVE_PARSER
#define MoveParser(key, Class)                                                 \
  else if (name == #key) {                                                     \
    return new Class ();                                                       \
  }
#include "BHmoves.hpp"
#undef MoveParser
#undef MOVE_PARSER
          else {
            throw "Cannot initialize \"" + name + "\": not a known move";
          }
        }(moveName));
    move->parse (options);
    return move;
    */
    return BHConstructors::getConstructor ().createMoveFromString (
      moveName, options);
  }

  BHConstructors::BHConstructors () {
#define MOVE_PARSER
#define MoveParser(key, Class)                                                 \
  moveCreators_.emplace (#key, createMoveCreator<Class> ());
#include "BHmoves.hpp"
#undef MoveParser
#undef MOVE_PARSER
  }

  std::unique_ptr<BHMove> BHConstructors::createMoveFromString (
    const std::string moveName, const std::string options) {
    if (moveCreators_.count (moveName) == 0)
      throw "Cannot initialize \"" + moveName + "\": not a known move";
    return moveCreators_.at (moveName) (options);
  }

  BHMinimizationAlgorithm *
  BHConstructors::getMinimizationAlgorithm (const BHSettings &settings) {
#define MINALGO_PARSER
#define MinAlgorithmParser(key, Class)                                         \
  case BHminimizationAlgorithmType::key:                                       \
    return new Class ();
    switch (settings.minimizationAlgorithm_) {
#include "BHminimizationAlgorithms.hpp"
    case BHminimizationAlgorithmType::NofKnownAlgoriythms:
      [[fallthrough]];
    default:
      throw "Unknown minimization algorithm";
    }
#undef MinAlgorithmParser
#undef MINALGO_PARSER
  }

  void BHConstructors::addMoveToList (
    std::string moveName, BHConstructors::moveCreator creatorFunction) {
    if (moveCreators_.count (moveName) != 0)
      throw "\"" + moveName +
        "\" is already present in the list of known moves";

    moveCreators_.emplace (moveName, creatorFunction);
  }

  BHWalker BHConstructors::createWalker (
    BHWalkerSettings ws, const BHMetalParameters &bhmp, rndEngine &rng) const {
    return createWalker (ws, bhmp, getSeedFromSettings (ws, rng));
  }

  BHWalker BHConstructors::createWalker (
    const BHWalkerSettings &ws,
    const BHMetalParameters &bhmp,
    BHClusterAtoms seed) const {
    switch (ws.ChosenAlgorithm_) {
#define WALKER_ALGORITHM_PARSER
#define WalkerAlgorithmParser(key, Function, standalone, skipSteps)            \
  case BHwalkerAlgorithmType::key:                                             \
    return BHWalker (                                                          \
      ws, seed, bhmp, BHWalkerAlgorithms::Function, standalone, skipSteps);
#include "BHwalkerAlgorithms.hpp"
#undef WalkerAlgorithmParser
#undef WALKER_ALGORITHM_PARSER
    default:
      return BHWalker (
        ws, seed, bhmp, BHWalkerAlgorithms::BHWalkerAlgorithmStandard, true,
        false);
    }
  }

  BHWalker BHConstructors::createStandardWalker (
    BHWalkerSettings ws, const BHMetalParameters &bhmp, rndEngine &rng) const {
    // ws is copied
    ws.ChosenAlgorithm_ = BHwalkerAlgorithmType::stnd;
    return createWalker (ws, bhmp, rng);
  }

  BHWalker BHConstructors::createStandardWalker (
    BHWalkerSettings ws,
    const BHMetalParameters &bhmp,
    BHClusterAtoms seed) const {
    // ws is copied
    ws.ChosenAlgorithm_ = BHwalkerAlgorithmType::stnd;
    return createWalker (ws, bhmp, seed);
  }

  BHConstructors &BHConstructors::getConstructor () {
    static BHConstructors MC;
    return MC;
  }
} // namespace BH
