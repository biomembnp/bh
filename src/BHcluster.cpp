/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Definition of class BHCluster
   @file BHcluster.cpp
   @author Daniele Rapetti (iximiel@gmail.com)

@version 1.1
    @date 21/3/2017

    Merging from BHNeighboursInfo

    @version 1.0
    @date 21/3/2017
    */
#include "BHcluster.hpp"
#include "BHmetalParameters.hpp"
#include <exception>
//#include <iostream>

namespace BH {
  BHCluster::BHCluster () = default;

#define LazyInitializer                                                        \
  for (unsigned int i = 0; i < NofAtoms_; i++) {                               \
    InteractionLabels[i] = new unsigned[NofAtoms_];                            \
    notOfSameKind[i] = new unsigned[NofAtoms_];                                \
    for (unsigned int j = 0; j < NofAtoms_; j++) {                             \
      InteractionLabels[i][j] = 4294967295;                                    \
      notOfSameKind[i][j] = 4294967295;                                        \
    }                                                                          \
  }

  BHCluster::BHCluster (const BHClusterAtoms &seed)
    : BHClusterAtoms (seed),
      InteractionLabels (new unsigned *[NofAtoms_]),
      notOfSameKind (new unsigned *[NofAtoms_]),
      NNlist (new vectorAtomID[NofAtoms_]),
      NNlist_same (new vectorAtomID[NofAtoms_]),
      NNlist_diff (new vectorAtomID[NofAtoms_]),
      InteractingList (new vectorAtomID[NofAtoms_]){LazyInitializer}

      BHCluster::BHCluster (const BHCluster &seed)
    : BHClusterAtoms (seed),
      InteractionLabels (new unsigned *[NofAtoms_]),
      notOfSameKind (new unsigned *[NofAtoms_]),
      NNlist (new vectorAtomID[NofAtoms_]),
      NNlist_same (new vectorAtomID[NofAtoms_]),
      NNlist_diff (new vectorAtomID[NofAtoms_]),
      InteractingList (new vectorAtomID[NofAtoms_]) {
    for (unsigned int i = 0; i < NofAtoms_; i++) {
      InteractionLabels[i] = new unsigned[NofAtoms_];
      notOfSameKind[i] = new unsigned[NofAtoms_]; // 1 if ij is a couple of
                                                  // different atoms
      for (unsigned int j = 0; j < NofAtoms_; j++) {
        InteractionLabels[i][j] = seed.InteractionLabels[i][j];
        notOfSameKind[i][j] = seed.notOfSameKind[i][j];
      }
    }
  }

  BHCluster::BHCluster (BHCluster &&other) noexcept
    : BHClusterAtoms (std::move (other)),
      InteractionLabels (other.InteractionLabels),
      notOfSameKind (other.notOfSameKind),
      NNlist (other.NNlist),
      NNlist_same (other.NNlist_same),
      NNlist_diff (other.NNlist_diff),
      InteractingList (other.InteractingList) {
    other.InteractionLabels = nullptr;
    other.notOfSameKind = nullptr;
    other.NNlist = nullptr;
    other.NNlist_same = nullptr;
    other.NNlist_diff = nullptr;
    other.InteractingList = nullptr;
  }

  BHCluster::BHCluster (BHClusterAtoms &&seed) noexcept
    : BHClusterAtoms (seed),
      InteractionLabels (new unsigned *[NofAtoms_]),
      notOfSameKind (new unsigned *[NofAtoms_]),
      NNlist (new vectorAtomID[NofAtoms_]),
      NNlist_same (new vectorAtomID[NofAtoms_]),
      NNlist_diff (new vectorAtomID[NofAtoms_]),
      InteractingList (new vectorAtomID[NofAtoms_]){LazyInitializer}

      // initialize an enpty cluster with NAtoms of unknown type
      BHCluster::BHCluster (const unsigned int NAtoms)
    : BHClusterAtoms (NAtoms),
      InteractionLabels (new unsigned *[NofAtoms_]()),
      notOfSameKind (new unsigned *[NofAtoms_]()),
      NNlist (new vectorAtomID[NofAtoms_]),
      NNlist_same (new vectorAtomID[NofAtoms_]),
      NNlist_diff (new vectorAtomID[NofAtoms_]),
      InteractingList (new vectorAtomID[NofAtoms_]) {
    LazyInitializer
  }
#undef LazyInitializer

  BHCluster::~BHCluster () {
    for (unsigned int i = 0; i < NofAtoms_ && InteractionLabels; ++i) {
      delete[] InteractionLabels[i];
    }
    for (unsigned int i = 0; i < NofAtoms_ && notOfSameKind; ++i) {
      delete[] notOfSameKind[i];
    }

    delete[] InteractionLabels;
    delete[] notOfSameKind;
    delete[] NNlist;
    delete[] NNlist_same;
    delete[] NNlist_diff;
    delete[] InteractingList;
  }

  void BHCluster::setNofAtoms (unsigned newN) {
    if (newN != NofAtoms_) {
      BHClusterAtoms::setNofAtoms (newN);
      if (InteractionLabels) {
        for (unsigned int i = 0; i < NofAtoms_; ++i) {
          delete[] InteractionLabels[i];
          delete[] notOfSameKind[i];
        }
        delete[] InteractionLabels;
        delete[] notOfSameKind;
        delete[] NNlist;
        delete[] NNlist_same;
        delete[] NNlist_diff;
        delete[] InteractingList;
      }
      InteractionLabels = new unsigned *[NofAtoms_];
      notOfSameKind = new unsigned *[NofAtoms_];
      NNlist = new vectorAtomID[NofAtoms_];
      NNlist_same = new vectorAtomID[NofAtoms_];
      NNlist_diff = new vectorAtomID[NofAtoms_];
      InteractingList = new vectorAtomID[NofAtoms_];
      for (unsigned int i = 0; i < NofAtoms_; i++) {
        InteractionLabels[i] = new unsigned[NofAtoms_];
        notOfSameKind[i] = new unsigned[NofAtoms_];
      }
    }
  }

  BHCluster &BHCluster::operator= (const BHCluster &other) {
    if (this != &other) {
      bool differentN = (NofAtoms_ == other.NofAtoms_);
      BHClusterAtoms::operator= (other);
      if (!differentN) {
        if (InteractionLabels) {
          for (unsigned int i = 0; i < NofAtoms_; ++i) {
            delete[] InteractionLabels[i];
            delete[] notOfSameKind[i];
          }
          delete[] InteractionLabels;
          delete[] notOfSameKind;
          delete[] NNlist;
          delete[] NNlist_same;
          delete[] NNlist_diff;
          delete[] InteractingList;
        }
        InteractionLabels = new unsigned *[NofAtoms_];
        notOfSameKind = new unsigned *[NofAtoms_];
        NNlist = new vectorAtomID[NofAtoms_];
        NNlist_same = new vectorAtomID[NofAtoms_];
        NNlist_diff = new vectorAtomID[NofAtoms_];
        InteractingList = new vectorAtomID[NofAtoms_];
        for (unsigned int i = 0; i < NofAtoms_; i++) {
          InteractionLabels[i] = new unsigned[NofAtoms_];
          notOfSameKind[i] = new unsigned[NofAtoms_];
        }
      }
    }
    for (unsigned int i = 0; i < NofAtoms_; ++i) {
      std::copy (
        other.InteractionLabels[i], other.InteractionLabels[i] + NofAtoms_,
        InteractionLabels[i]);
      std::copy (
        other.notOfSameKind[i], other.notOfSameKind[i] + NofAtoms_,
        notOfSameKind[i]);
    }
    // no need to copy the working arrays
    return *this;
  }

  BHCluster &BHCluster::operator= (BHCluster &&other) noexcept {
    BHClusterAtoms::operator= (other);

    if (InteractionLabels) {
      for (unsigned int i = 0; i < NofAtoms_; ++i) {
        delete[] InteractionLabels[i];
        delete[] notOfSameKind[i];
      }
      delete[] InteractionLabels;
      delete[] notOfSameKind;
      delete[] NNlist;
      delete[] NNlist_same;
      delete[] NNlist_diff;
      delete[] InteractingList;
    }
    InteractionLabels = other.InteractionLabels;
    other.InteractionLabels = nullptr;
    notOfSameKind = other.notOfSameKind;
    other.notOfSameKind = nullptr;
    NNlist = other.NNlist;
    other.NNlist = nullptr;
    NNlist_same = other.NNlist_same;
    other.NNlist_same = nullptr;
    NNlist_diff = other.NNlist_diff;
    other.NNlist_diff = nullptr;
    InteractingList = other.InteractingList;
    other.InteractingList = nullptr;
    return *this;
  }

  void BHCluster::setINTlab (
    const unsigned i, const unsigned j, const unsigned INT) {
    InteractionLabels[i][j] = INT;
    InteractionLabels[j][i] = INT;
  }

  unsigned BHCluster::INTlab (const unsigned i, const unsigned j) const {
    return InteractionLabels[i][j];
  }

  void BHCluster::setSAMElab (
    const unsigned i, const unsigned j, const unsigned SAME) {
    notOfSameKind[i][j] = SAME;
    notOfSameKind[j][i] = SAME;
  }

  unsigned BHCluster::SAMElab (const unsigned i, const unsigned j) const {
    return notOfSameKind[i][j];
  }

  void BHCluster::copyInteractionLabelsFrom (const BHCluster &other) {
    if (NofAtoms_ != other.NofAtoms_) {
      throw std::logic_error (
        "Tried \"getLabelsFrom\" in clusters with different NofAtoms");
    }
    for (unsigned int i = 0; i < NofAtoms_; i++) {
      for (unsigned int j = 0; j < NofAtoms_; j++) {
        InteractionLabels[i][j] = other.InteractionLabels[i][j];
        notOfSameKind[i][j] = other.notOfSameKind[i][j];
      }
    }
  }

  /// Clears all the NN vectors
  void BHCluster::clear_NN () {
    for (unsigned int i = 0; i < NofAtoms_; i++) {
      NNlist[i].clear ();
      NNlist_same[i].clear ();
      NNlist_diff[i].clear ();
      InteractingList[i].clear ();
    }
  }
  /// apply vector::shrink_to_fit() to all NN vectors
  void BHCluster::shrink_NN_to_fit () {
    for (unsigned int i = 0; i < NofAtoms_; i++) {
      NNlist[i].shrink_to_fit ();
      NNlist_same[i].shrink_to_fit ();
      NNlist_diff[i].shrink_to_fit ();
      InteractingList[i].shrink_to_fit ();
    }
  }
#ifdef COMPILEBHWITHSINGLENNDEFINTIONS
  void BHCluster::addNN (const unsigned atomID, const unsigned neighID) {
    NNlist[atomID].push_back (neighID);
  }

  void BHCluster::addNN_same (const unsigned atomID, const unsigned neighID) {
    NNlist_same[atomID].push_back (neighID);
  }

  void BHCluster::addNN_diff (const unsigned atomID, const unsigned neighID) {
    NNlist_diff[atomID].push_back (neighID);
  }
#endif
  void
  BHCluster::addNNcouple (const unsigned firstID, const unsigned secondID) {
    NNlist[firstID].push_back (secondID);
    NNlist[secondID].push_back (firstID);
  }

  void BHCluster::addNNcouple_same (
    const unsigned firstID, const unsigned secondID) {
    NNlist_same[firstID].push_back (secondID);
    NNlist_same[secondID].push_back (firstID);
  }

  void BHCluster::addNNcouple_diff (
    const unsigned firstID, const unsigned secondID) {
    NNlist_diff[firstID].push_back (secondID);
    NNlist_diff[secondID].push_back (firstID);
  }

  void BHCluster::addINT (const unsigned atomID, const unsigned neighID) {
    InteractingList[atomID].push_back (neighID);
  }

  const vectorAtomID &BHCluster::getNNs (const unsigned i) const {
    return NNlist[i];
  }

  const vectorAtomID &BHCluster::getNNs_same (const unsigned i) const {
    return NNlist_same[i];
  }

  const vectorAtomID &BHCluster::getNNs_diff (const unsigned i) const {
    return NNlist_diff[i];
  }

  const vectorAtomID &BHCluster::getInteracting (const unsigned i) const {
    return InteractingList[i];
  }
  /*
    std::ostream& operator<< (std::ostream& stream, const BHCluster& obj){
      //the precision must be set before calling this operator
      stream << obj.NofAtoms() << std::endl
             << obj.Composition() << std::endl;
      for(unsigned int i =0;i<obj.NofAtoms();i++){
        stream << std::endl << obj[i];
      }
      return stream;
    }*/
} // namespace BH
