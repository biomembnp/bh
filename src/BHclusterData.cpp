/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Definition of class BHClusterData
   @file BHclusterData.cpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @version 0.9.2
   @date 6/3/2018

   trying to add a  rule of 5 to this

   @version 0.9.1
   @date 11/12/2017
   small changes in the structure

   @version 0.9
   @date 4/7/2017

*/

#include <cmath>
#include <iostream>
//#include <fstream>
//#include <sstream>
#include "BHclusterData.hpp"

namespace BH {
  BHClusterData::BHClusterData () = default;

  BHClusterData::BHClusterData (
    const BHdouble Etot, const BHdouble OP0, const BHdouble OP1)
    : myEtot (Etot),
      /* myE_met_ox(0), myE_met_met(0),*/ myOrder_par{OP0, OP1} {}

  BHClusterData::BHClusterData (
    const BHdouble Etot, const std::vector<BHdouble> &OPs)
    : myEtot (Etot),
      myOrder_par (OPs) {}

  BHClusterData::BHClusterData (
    const BHdouble Etot, const std::vector<BHdouble> &&OPs)
    : myEtot (Etot),
      myOrder_par (OPs) {}

  BHClusterData::BHClusterData (const BHClusterData &other)
    : myEtot (other.myEtot),
      /* myE_met_ox(0), myE_met_met(0),*/ myOrder_par (other.myOrder_par) {}

  BHClusterData::BHClusterData (BHClusterData &&other) noexcept
    : myEtot (other.myEtot),
      /* myE_met_ox(0), myE_met_met(0),*/ myOrder_par (other.myOrder_par) {}

  BHClusterData &BHClusterData::operator= (const BHClusterData &other) {
    if (this != &other) { // protect against invalid self-assignment
      myEtot = other.myEtot;
      /*myE_met_ox = other.myE_met_ox;
        myE_met_met = other.myE_met_met;*/
      myOrder_par = other.myOrder_par;
    }
    return *this;
  }

  BHClusterData &BHClusterData::operator= (BHClusterData &&other) noexcept {
    if (this != &other) { // protect against invalid self-assignment
      myEtot = other.myEtot;
      /*myE_met_ox = other.myE_met_ox;
        myE_met_met = other.myE_met_met;*/
      myOrder_par = other.myOrder_par;
    }
    return *this;
  }

  // says if ate least one betrwwn energy and ops is different
  bool BHClusterData::differentFrom (
    const BHClusterData &other,
    const BHdouble energyDiff,
    const BHdouble OPDiff) {
    bool different = std::abs (myEtot - other.myEtot) > energyDiff;
    for (unsigned i = 0; i < myOrder_par.size (); ++i) {
      different |= (std::abs (myOrder_par[i] - other.myOrder_par[i]) > OPDiff);
    }
    return different;
    ;
  }

  // getters
  BHdouble BHClusterData::Energy () const { return myEtot; }
  BHdouble BHClusterData::orderParameter (unsigned opID) const {
    return myOrder_par[opID];
  }

  std::vector<BHdouble> BHClusterData::orderParameters () const {
    return myOrder_par;
  }

  void BHClusterData::setOrderParameters (std::vector<BHdouble> &newOPs) {
    myOrder_par = newOPs;
  }

  void BHClusterData::setOrderParameters (std::vector<BHdouble> &&newOPs) {
    myOrder_par = newOPs;
  }

  size_t BHClusterData::numberOfOP () const { return myOrder_par.size (); }

  // assigns
  void BHClusterData::setEnergy (const BHdouble newE) { myEtot = newE; }
  void
  BHClusterData::setOrderParameter (const unsigned opID, const BHdouble newOP) {
    myOrder_par[opID] = newOP;
  }

  void BHClusterData::SetData (
    const BHdouble &Etot, const BHdouble &OP0, const BHdouble &OP1) {
    /*void BHClusterData::SetData(BHdouble MOE, BHdouble MME, BHdouble OP1,
      BHdouble OP2){ myE_met_ox = MOE; myE_met_met = MME;
      myEtot = myE_met_met + myE_met_ox;*/
    myEtot = Etot;
    myOrder_par[0] = OP0;
    myOrder_par[1] = OP1;
  }

  /*
    BHdouble BHClusterData::M_O_Energy() const{return myE_met_ox;}
    BHdouble BHClusterData::M_M_Energy() const{return myE_met_met;}
    void BHClusterData::M_O_Energy(BHdouble newEMO){
    myE_met_ox = newEMO;
    myEtot = myE_met_met + myE_met_ox;
    }
    void BHClusterData::M_M_Energy(BHdouble newEMM){
    myE_met_met = newEMM;
    myEtot = myE_met_met + myE_met_ox;
    }
  */
  // IO
  /*std::istream& operator>> (std::istream& stream, BH::BHClusterData& obj){
    BH::BHdouble td;
    //get a line of data
    stream  >> td;
    obj.totEnergy(td);
    stream  >> td;
    obj.M_O_Energy(td);
    stream  >> td;
    obj.M_M_Energy(td);
    stream  >> td;
    obj.param_ord(0,td);
    stream  >> td;
    obj.param_ord(1,td);
    return stream;
    }
  */
  std::string
  BHClusterData::ClusterData_list (const std::vector<BHOrderParameter> &OP) {
    return "E\t" + [&OP] () {
      std::string s;
      for (unsigned i = 0; i < OP.size (); ++i)
        s += "\t" + OparName (OP[i]);
      return s;
    }();
  }

  std::string BHClusterData::plotDataForxyz (
    const std::vector<BHOrderParameter> &OP) const {
    // Ei=-0.859468 Ec=0.391648 Lattice="11.796 0 0 0 11.796 0 0 0 30.5061 "
    // Properties=species:S:1:pos:R:3:eta:R:1:sulphur_dist:R:1 R=2.25 H=0.00
    // A=0.00 E=-50470.99693037 comment="Energy in Ry, forces in Ry/au"
    // from="configurations001Ad.xyz"

    return "E=" + std::to_string (myEtot) + [&OP, this] () {
      std::string s;
      for (unsigned i = 0; i < OP.size (); ++i)
        s += " " + OparName (OP[i]) + "=" + std::to_string (myOrder_par[i]);
      return s;
    }();
  }

  std::ostream &
  operator<< (std::ostream &stream, const BH::BHClusterData &obj) {
    // plot a line of data
    stream << obj.Energy ()
           //	 << "\t" << obj.M_O_Energy()commented until mo is used
           //	 << "\t" << obj.M_M_Energy()
           << "\t" << obj.orderParameter (0) << "\t" << obj.orderParameter (1);
    return stream;
  }
} // namespace BH
