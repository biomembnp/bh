/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Contains the definition of the utilities in BHmyMath
   @file BHmyMath.cpp
   @author Daniele Rapetti (iximie@gmail.com)

   @date 8/02/2018
   @version 1.0

   Created file with Factorial and RepetitionComb
*/
#include "BHmyMath.hpp"
namespace BH {
  int Factorial (int x) {
    int toreturn = 1;
    for (int i = x; i > 0; i--)
      toreturn *= i;
    return toreturn;
  }

  int RepetitionComb (int elements, int tuples) {
    return (Factorial (elements + tuples - 1)) /
           (Factorial (elements - 1) * Factorial (tuples));
  }

  unsigned Factorial (unsigned x) {
    unsigned toreturn = 1;
    for (unsigned i = x; i > 0; i--)
      toreturn *= i;
    return toreturn;
  }

  unsigned RepetitionComb (unsigned elements, unsigned tuples) {
    return (Factorial (elements + tuples - 1)) /
           (Factorial (elements - 1) * Factorial (tuples));
  }

  BHdouble convertEnergy2eV (BHdouble newEne, BHEnums::energyType units) {
    switch (units) {
    case BHEnums::Ry:
      return newEne * BHConst::Ry;
    case BHEnums::Ht:
      return newEne * BHConst::Ht;
    case BHEnums::eV: // if it don't recognise the Energy threat it as it is in
                      // eV
      [[fallthrough]];
    default: /*newEne = newEne; */
      return newEne;
    }
  }

  BHdouble getSwitchingPolyA5 (
    const BHdouble cs,
    const BHdouble ce,
    const BHdouble Fval,
    const BHdouble dFval,
    const BHdouble d2Fval) {
    BHdouble cse = cs - ce;
    BHdouble cse2 = cse * cse;
    BHdouble cse3 = cse * cse * cse;
    BHdouble cse4 = cse * cse3;
    BHdouble cse5 = cse * cse4;
    return 0.5 * (d2Fval * cse2 - 6.0 * dFval * cse + 12.0 * Fval) / cse5;
  }

  BHdouble getSwitchingPolyA4 (
    const BHdouble cs,
    const BHdouble ce,
    const BHdouble Fval,
    const BHdouble dFval,
    const BHdouble d2Fval) {
    BHdouble cse = cs - ce;
    BHdouble cse2 = cse * cse;
    BHdouble cse3 = cse * cse * cse;
    BHdouble cse4 = cse * cse3;
    return -(d2Fval * cse2 - 7.0 * dFval * cse + 15.0 * Fval) / cse4;
  }

  BHdouble getSwitchingPolyA3 (
    const BHdouble cs,
    const BHdouble ce,
    const BHdouble Fval,
    const BHdouble dFval,
    const BHdouble d2Fval) {
    BHdouble cse = cs - ce;
    BHdouble cse2 = cse * cse;
    BHdouble cse3 = cse * cse * cse;
    return 0.5 * (d2Fval * cse2 - 8.0 * dFval * cse + 20.0 * Fval) / cse3;
  }
} // namespace BH
