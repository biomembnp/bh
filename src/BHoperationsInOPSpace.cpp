/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Definitions of constructors and some base elements of BHWalker

   @file BHwalker.cpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 26/4/2018
   @version 0.5.3
   added isInForbiddenRegion for rew algorithm

   @date 27/9/2017
   @version 0.5.1

*/

//#include <iostream>
#include <algorithm> //sort
#include <fstream>
#include <sstream>
#include <utility>

#include "BHclusterUtilities.hpp"
#include "BHmetalParameters.hpp"
#include "BHoperationsInOPSpace.hpp"

//#ifdef WALKERDEBUG
#include "BHdebug.hpp"
//#endif //WALKERDEBUG
#include "BHworkWithFortran.hpp" //debugging
// seeded by a file
namespace BH {

  BHOperationsInOPSpace::BHOperationsInOPSpace (
    const BHWalkerSettings &walkersettings)
    : BHWalkerSettings (walkersettings) {}

  BHOperationsInOPSpace::BHOperationsInOPSpace () = default;

  BHOperationsInOPSpace::BHOperationsInOPSpace (
    const BHOperationsInOPSpace &other)
    : BHWalkerSettings (other) {}

  BHOperationsInOPSpace::BHOperationsInOPSpace (
    BHOperationsInOPSpace &&other) noexcept
    : BHWalkerSettings (std::move (other)) {}

  bool BHOperationsInOPSpace::isNeighbour (
    const std::vector<BHdouble> &OPs,
    const std::vector<BHdouble> &otherOPs) const {
    bool toRet = true;
    for (unsigned i = 0; i < OPs.size () && toRet; ++i) {
      toRet &=
        std::abs (OPs[i] - otherOPs[i]) < OPspaceSettings_[i].WalkerWidth_;
    }
    return toRet;
  }

  bool BHOperationsInOPSpace::isInForbiddenRegion (
    const std::vector<BHdouble> &OPs) const {
    bool toRet = true;
    for (unsigned i = 0; i < OPs.size () && toRet; ++i) {
      toRet &= (OPspaceSettings_[i].RepulsionZone_.low < OPs[i]) &&
               (OPs[i] < OPspaceSettings_[i].RepulsionZone_.high);
    }
    return toRet;
  }
} // namespace BH
