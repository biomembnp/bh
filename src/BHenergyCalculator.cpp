/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Definition of class BHEnergyCalculator
   @file BHenergyCalculator.cpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 05/06/2017
   @version 1.2.1

   Interacting neighbours moved here from child class

   @date 04/06/2017
   @version 1.2

   FIRE minimization algorithm works, but needs tweaks

   @date 29/05/2017
   @version 1.2

   Adding FIRE algorithm

   @date 23/10/2017
   @version 1.1

   revamped L-BFGS-B, now works correclty (logical is long int and not bool)
   look like that it works correctly, really fast with tollerance, quite slow
   with the standard routine (referrign tho teh SMATB)

   @date 23/10/2017
   @version 1.0

   L-BFGS-B seems to work correclty
   @date 19/10/2017
   @version 0.4.1

   Cannot make L-BFGS-B work properly
   @date 19/10/2017
   @version 0.4

   Gradient defined and working, divided base class from SMATB

   @date 17/10/2017
   @version 0.3

   energy defined and added a possible inheritance for different potential

   @date 16/10/2017
   @version 0.1

   constructor
*/
#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>

#include <cmath>
#include <cstring>

#include "BHenergyCalculator.hpp"
#include "BHmetalParameters.hpp"

/***timer/
    #include <chrono>
    typedef std::chrono::steady_clock::duration ms;
    typedef std::chrono::steady_clock::time_point tp;
    void tplot (std::ostream& stream, const std::chrono::steady_clock::duration&
obj){ if(obj <  std::chrono::duration<int,std::milli>(5000)) stream <<
std::chrono::duration <double, std::milli> (obj).count() <<" ms"; else stream <<
std::chrono::duration <double> (obj).count() <<" s";
    }
**/

// debugging:
/*
using std::cout;
using std::endl;
using std::cin;*/
namespace BH {

  BHEnergyCalculator::BHEnergyCalculator (
    const BHCluster &theCluster, const BHMetalParameters &bhmp)
    : nAtoms_ (theCluster.getNofAtoms ()),
      n3Atoms_ (3 * nAtoms_),
      nTypes_ (theCluster.NofSpecies ()),
      nInteractions_ (RepetitionComb (nTypes_, unsigned (2))), // in BHmyMath
      typeList_ (new unsigned short[nAtoms_]),
      intType_ (new unsigned short[nTypes_ * nTypes_]),
      nNeigh_ (new unsigned short[nAtoms_]),
      NeighList_ (new unsigned int *[nAtoms_]),
      IntNeighDistTol2_ (new BHdouble[nInteractions_]) { // triangular matrix
    // BHMetalParameters could have defined more atomtypes than needed
    // interaction ID
    unsigned short intID (0);
    // creating a list of interaction:
    for (unsigned int i = 0; i < nTypes_; ++i) {
      std::string atomType = theCluster.Composition (i);
      Name2ID[atomType] = static_cast<unsigned short> (i);
      // updating the intType_
      intType_[i * nTypes_ + i] = intID;
      // Needed for Backward compatibility
      INT_MP2ID[bhmp.getPlabels (atomType + atomType)] =
        static_cast<int> (intID);
      intName2ID_[atomType + atomType] = intType_[i * nTypes_ + i];
      ++intID;
      for (unsigned int j = i + 1; j < nTypes_; ++j) {
        std::string atomType2 = theCluster.Composition (j);
        // updating the intType_
        intType_[i * nTypes_ + j] = intID;
        intType_[j * nTypes_ + i] = intID;
        intName2ID_[atomType + atomType2] = intType_[i * nTypes_ + j];
        intName2ID_[atomType2 + atomType] = intType_[j * nTypes_ + i];
        // Needed for Backward compatibility
        INT_MP2ID[bhmp.getPlabels (atomType + atomType2)] =
          static_cast<int> (intID);
        ++intID;
      }
    }

    for (unsigned int i = 0; i < nAtoms_; ++i) {
      typeList_[i] = Name2ID[theCluster[i].Type ()];
      NeighList_[i] = new unsigned[nAtoms_ - 1];
    }
  }

  BHEnergyCalculator::BHEnergyCalculator (const BHEnergyCalculator &other)
    : nAtoms_ (other.nAtoms_),
      n3Atoms_ (other.n3Atoms_),
      nTypes_ (other.nTypes_),
      nInteractions_ (other.nInteractions_),
      typeList_ (new unsigned short[nAtoms_]),
      intType_ (new unsigned short[nTypes_ * nTypes_]),
      nNeigh_ (new unsigned short[nAtoms_]),
      NeighList_ (new unsigned int *[nAtoms_]),
      IntNeighDistTol2_ (new BHdouble[nInteractions_]),
      Name2ID (other.Name2ID),
      intName2ID_ (other.intName2ID_),
      INT_MP2ID (other.INT_MP2ID) {
    for (unsigned i = 0; i < nAtoms_; ++i) {
      typeList_[i] = other.typeList_[i];
      // no need to copy nNeigh_[i], it is a working array
      NeighList_[i] = new unsigned[nAtoms_ - 1];
    }
    for (unsigned i = 0; i < nTypes_ * nTypes_; ++i) {
      intType_[i] = other.intType_[i];
    }
    for (unsigned i = 0; i < nInteractions_; ++i) {
      IntNeighDistTol2_[i] = other.IntNeighDistTol2_[i];
    }
  }

  BHEnergyCalculator::BHEnergyCalculator (BHEnergyCalculator &&other) noexcept
    : nAtoms_ (other.nAtoms_),
      n3Atoms_ (other.n3Atoms_),
      nTypes_ (other.nTypes_),
      nInteractions_ (other.nInteractions_),
      typeList_ (other.typeList_),
      intType_ (other.intType_),
      nNeigh_ (other.nNeigh_),
      NeighList_ (other.NeighList_),
      IntNeighDistTol2_ (std::move (other.IntNeighDistTol2_)),
      Name2ID (std::move (other.Name2ID)),
      intName2ID_ (std::move (other.intName2ID_)),
      INT_MP2ID (std::move (other.INT_MP2ID)) {
    other.typeList_ = nullptr;
    other.intType_ = nullptr;
    other.nNeigh_ = nullptr;
    other.NeighList_ = nullptr;
    other.IntNeighDistTol2_ = nullptr;
  }

  BHEnergyCalculator::~BHEnergyCalculator () {
    // std::cout << "~BHEnergyCalculator()" <<std::endl;
    delete[] typeList_;
    delete[] intType_;
    delete[] nNeigh_;
    // delete [] intID_;
    if (NeighList_) {
      for (unsigned int i = 0; i < nAtoms_; ++i) {
        delete[] NeighList_[i];
      }
    }
    delete[] NeighList_;
    delete[] IntNeighDistTol2_;
    /*
        delete[] l_;
        delete[] u_;
        delete[] nbd_;
        delete[] g_;
        // delete [] wa;
        delete[] iwa_;
        delete[] V_;*/
  }

  BHdouble BHEnergyCalculator::Energy (const BHClusterAtoms &cluster) {
    if (cluster.getNofAtoms () != nAtoms_) {
      ///@todo better error message
      throw "Energy calculator is not set up for this cluster";
    }
    BHdouble *x = new BHdouble[n3Atoms_];
    cluster.AssignToVector (x);
    BHdouble energy = Energy (x);
    delete[] x;
    return energy;
  }

  void BHEnergyCalculator::Neighbours_for_tol (BHdouble *x) {
    memset (nNeigh_, 0, nAtoms_ * sizeof (nNeigh_[0]));
    unsigned IDi, IDk;
    for (unsigned int i = 0; i < nAtoms_; ++i) {
      IDi = 3 * i;
      BHdouble x_i = x[IDi], y_i = x[IDi + 1], z_i = x[IDi + 2];
      unsigned int mytype = nTypes_ * typeList_[i];
      for (unsigned int k = i + 1; k < nAtoms_; ++k) {
        IDk = 3 * k;
        // INTID=intID_[TAI(i,k)];
        unsigned int INTID = intType_[mytype + typeList_[k]];
        // std :: cout <<mytype + typeList_[k]<<" "<< INTID << std::endl;
        BHdouble xik = x[IDk] - x_i, yik = x[IDk + 1] - y_i,
                 zik = x[IDk + 2] - z_i;
        BHdouble dik2 = xik * xik + yik * yik + zik * zik;
        if (dik2 < IntNeighDistTol2_[INTID]) {
          NeighList_[i][nNeigh_[i]] = k;
          ++nNeigh_[i];
        }
      }
    }
    /*
      for (unsigned int i=0; i< nAtoms_; ++i) {
      cout << "Atom " << i << ": " << nNeigh_[i] <<endl;
      for ( int j=0; j < nNeigh_[i]; ++j) {e
      cout <<  NeighList_[i][j] <<", ";
      }
      cout <<endl;
      }*/
  }

  BHdouble BHEnergyCalculator::Pressure (BHdouble *, BHdouble *) {
    throw "BHEnergyCalculator::Pressure calculation are not defined with this "
          "potential";
    // return 0.0;
  }

  unsigned BHEnergyCalculator::nAtoms () const { return nAtoms_; }
} // namespace BH
