/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/** @brief contain the implementation of the non pure virutal methods of the
 abstract class BHMove
If the doMove winction return a negative number the basin hopping will interpet
it as the move has  failed so the setep will be automatically refused!

 @file BHmove.cpp

   @date 3/4/2019
   @version 0.1

 */

#include "BHmove.hpp"
#include "BHdebug.hpp"
#include "BHparsers.hpp"

#include "BHconstructors.hpp"

#include <algorithm>
#include <iomanip>
#include <sstream>
#include <utility>

namespace BH {
  BHMove::BHMove (std::string name, const bool isExchange)
    : name_ (std::move (name)),
      isExchange_{isExchange} {}

  BHMove::BHMove (const BHMove &other)
    : name_{other.name_},
      rawOptions_{other.rawOptions_},
      descriptor_{other.descriptor_},
      probability_{other.probability_},
      acceptTemperature_{other.acceptTemperature_},
      isExchange_{other.isExchange_} {
    // move count and accepted should not be copied
  }

  BHMove::~BHMove () = default;

  BHdouble BHMove::getAcceptanceTemperature () { return acceptTemperature_; }

  BHdouble BHMove::getProbability () { return probability_; }
  unsigned int BHMove::getMoveCount () { return count_; }
  unsigned int BHMove::getAcceptedCount () { return accepted_; }
  bool BHMove::isExchange () { return isExchange_; }
  void BHMove::hasBeenAccepted () { ++accepted_; }

  std::string BHMove::printSettingslegend () {
    std::stringstream ss;
    ss << std::setw (20) << std::left << "Name" << std::setw (8) << "Weight"
       << std::setw (8) << "Temp"
       << "Specialized infos";
    return ss.str ();
  }
  std::string BHMove::printSettings () const {
    std::stringstream ss;
    ss << std::setw (20) << std::left
       << (typeName () +
           ((descriptor ().empty ()) ? "" : ("(" + descriptor () + ")")))
       << std::setw (8) << probability_ << std::setw (8) << acceptTemperature_
       << printSettingsSpecialized ();
    return ss.str ();
  }
  std::string BHMove::printSettingsSpecialized () const { return ""; }

  std::string BHMove::printSettingsForInput () const {
    std::stringstream ss;
    ss << std::left << typeName ()
       << ((descriptor ().empty ()) ? "" : (" aka=" + descriptor ()) + ",")
       << " p=" << probability_ << ", t=" << acceptTemperature_
       << printSettingsSpecializedForInput ();
    return ss.str ();
  }
  std::string BHMove::printSettingsSpecializedForInput () const { return ""; }

  BHdouble BHMove::doMove (
    BHCluster &in,
    BHCluster &out,
    BHClusterAnalyser &Analyzer,
    const BHMetalParameters &bhmp,
    rndEngine &rng) {
    if (doMoveAlgorithm (in, out, Analyzer, bhmp, rng)) {
      ++count_;
      return acceptTemperature_;
    }
    return -1.0;
  }

  std::string BHMove::DefaultString () { return "prob = 1.0, accTemp = 300"; }

  void BHMove::writeStatistics (std::ostream &stream) const {
    stream << typeName () << ":\tdone: " << count_
           << "\taccepted: " << accepted_ << std::endl;
  }
  std::string BHMove::name () const { return typeName () + descriptor_; }

  const std::string &BHMove::typeName () const { return name_; }
  const std::string &BHMove::descriptor () const { return descriptor_; }

  bool BHMove::parse (const std::string &parsedStr) {
    rawOptions_ = parsedStr;
    bool toreturn = true;
    // std::cout << parsedStr << std::endl;
    std::string token;
    std::istringstream tokenStream (parsedStr);
    while (std::getline (tokenStream, token, ',') && toreturn) {
      // std::cout << token << std::endl;
      toreturn = parseProbAndTAcc (token);
      if (!toreturn) {
        toreturn = parseSpecialized (token);
      }
      if (!toreturn) {
        throw typeName () + std::string ("::Problem with parsing: \"")
                              .append (parsedStr)
                              .append ("\" in \"")
                              .append (token)
                              .append ("\"");
      }
    }
    parseAfter ();
    return toreturn;
  }

  bool BHMove::parseSpecialized (const std::string &) { return true; }
  bool BHMove::parseAfter () { return true; }

  void BHMove::Specialize (const BHCluster &, const BHMetalParameters &) {}

  bool BHMove::parseProbAndTAcc (const std::string &parsed) {
    bool toreturn = false;
    try {
      toreturn |= BHParsers::parse (
        _BHMVParser_StringsForBASE[0].c_str (), parsed, probability_);
      toreturn |= BHParsers::parse (
        _BHMVParser_StringsForBASE[1].c_str (), parsed, probability_);
      toreturn |= BHParsers::parse (
        _BHMVParser_StringsForBASE[2].c_str (), parsed, acceptTemperature_);
      toreturn |= BHParsers::parse (
        _BHMVParser_StringsForBASE[3].c_str (), parsed, acceptTemperature_);
      toreturn |= BHParsers::parse (
        _BHMVParser_StringsForBASE[4].c_str (), parsed, descriptor_);
      toreturn |= BHParsers::parse (
        _BHMVParser_StringsForBASE[5].c_str (), parsed, descriptor_);
    } catch (const std::invalid_argument &ia) {
      throw typeName () + "::Invalid argument: \"" + ia.what () + "\",\"" +
        parsed + "\"";
    }
    return toreturn;
  }
} // namespace BH
