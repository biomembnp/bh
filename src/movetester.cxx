/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief A move tester: get a cluste and make a move on it

@file movetester.cxx
  @author Daniele Rapetti (iximiel@gmail.com)
    @date 28/8/2017
  @version 1.0
  */

#include <chrono>
#include <iostream>
#include <string>

#include "BHconstructors.hpp"
#include "BHdebug.hpp"
#include "BHenergyCalculator_SMATB.hpp"
#include "BHmetalParameters.hpp"
#include "BHmoveTester.hpp"
#include "BHmoves.hpp"

#ifdef __DOMINIMIZATION_WITH_FIRE
#include "BHminimizationAlgorithm_FIRE.hpp"
#else
#include "BHminimizationAlgorithm_LBFGSB.hpp"
#endif //__DOMINIMIZATION_WITH_FIRE

#include "BHminimizationAlgorithm_FIRE.hpp"
using namespace std;

using namespace BH;

int main (int /*argc*/, char ** /*argv*/) {
  try {
    std::cout << "Insert seed: ";
    size_t seed (123456789);
    std::string seedStr;
    getline (std::cin, seedStr);
    try {
      seed = (seedStr.empty ()) ? seed : std::stoull (seedStr);
    } catch (...) {
      std::cout << "I will use default seed\n";
      seed = 123456789;
    }

    std::cout << "Will be used \033[1m" << seed << "\033[0m as seed."
              << std::endl;

    BHMetalParameters bhmp ("MetalParameters.in", true);
    rndEngine my_rnd_engine (seed);
    BHWalkerSettings WalkerCreator =

      createSettingsWithAllMoves (
        BHwalkerAlgorithmType::stnd, "input_movetester.in");
    std::vector<BHOrderParameter> OPs = {"555"};
    BHClusterAnalyser Analyzer (OPs, bhmp);
    moveTester tst (WalkerCreator, {"seed0.in"}, bhmp);
    tst.doMove (Analyzer, bhmp, my_rnd_engine);
    tst.ExportResultAndMinimized (
      new BHMinimizationAlgorithm_LBFGSB (),
      new BHEnergyCalculator_SMATB (tst.lastMoveConf (), bhmp), "LBFGSB");
    tst.ExportResultAndMinimized (
      new BHMinimizationAlgorithm_FIRE (),
      new BHEnergyCalculator_SMATB (tst.lastMoveConf (), bhmp), "FIRE");

  } catch (const char *problem) {
    cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m" << endl;
  } catch (const string &problem) {
    cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m" << endl;
  }
  return 0;
}
