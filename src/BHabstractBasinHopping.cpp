/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Here the definitions the basin hopping algorithm

@file BHabstractBasinHopping.cpp
  @author Daniele Rapetti (iximiel@gmail.com)

  @date 13/6/2019
  @version 1.0

  Now the algorithm is modular, and easier to be read  and easier to be modified

  @date 12/6/2019
  @version 0.1

    */
#include <algorithm>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "BHabstractBasinHopping.hpp"
#include "BHclusterUtilities.hpp"
#include "BHworkWithFortran.hpp"
//#include "BHdebug.hpp"
#include "BHconstructors.hpp"
#include "BHmetalParameters.hpp"
#ifndef USESMATBGAUSS
#include "BHenergyCalculator_SMATB.hpp"

using EnergyCalculator = BH::BHEnergyCalculator_SMATB;
#else
#include "BHenergyCalculator_SMATB_GAUSS.hpp"
using EnergyCalculator = BH::BHEnergyCalculator_SMATB_GAUSS;
#endif

// remember always to use wID when selecting a walker!!!
// it is more stylish and easy to read

/*
instruction for a benchmark:
/usr/bin/time -o tempo -p ../main
*/
constexpr int WIDTH10 = 10;
constexpr int WIDTH8 = 8;
constexpr int MOVELOG = 500;
constexpr int TOTALLOG = 1000;

namespace BH {

  bool BHAbstractBasinHopping::run = true;
  const char *BHAbstractBasinHopping::staticLogo () { return ""; }
  const char *BHAbstractBasinHopping::logo () { return staticLogo (); }
  BHAbstractBasinHopping::BHAbstractBasinHopping (const BHSettings &settings)
    : settings_ (settings),
      metalParameters_ (settings.namePotpar_),
      walkers_ (),
      acceptedMovesInfo_ (),
      minimizator_ (
        BHConstructors::getConstructor ().getMinimizationAlgorithm (settings)),
      energyCalculator_ (nullptr),
      algorithmSupport_{
        {BHSettings::getOPsFromSettings (settings_.OPSettings_),
         metalParameters_},
        {settings.OPSettings_[0].histoPars_, settings.OPSettings_[1].histoPars_,
         settings.renewHisto_}},
      rndEngine_ (settings.RngSeed_) {}

  BHAbstractBasinHopping::~BHAbstractBasinHopping () {
    BHWWF::BHInterface_CleanMemory ();
    delete minimizator_;
    delete energyCalculator_;
  }

  void BHAbstractBasinHopping::Processing () {
    if (!initialized_) {
      // PreOutputInitialization ();
      Initialization ();
      // maybe I should add the histogram initialization to a postInitialization
      // virtual method
      if (settings_.saveHistogramData_) {
        algorithmSupport_.Histogram.initializeHisto ();
      }
      GloInfoGeneral_.energy = walkers_[0].lastAcceptedEnergy ();
      wIDSaved_ = 0;
      OutputInitialization ();
      // needed for not repeat the last row in moves_out
      lastLogOnStep_ = 0;
      GloInfoGeneral_.GloID = 0;
      GloInfoGeneral_.mcStep = 0;

      // runnigWalker_ is used to select the moving walker
      // first walker will be the 0th unless is a obstacle
      runnigWalker_ = 0;
      if (
        walkers_[runnigWalker_].myAlgorithm () ==
        BH::BHwalkerAlgorithmType::obst) {
        ChangeWalker ();
      }
      mcStep_ = 1;
      for (unsigned i = 0; i < settings_.NumberOfOP; ++i) {
        if (settings_.OPSettings_[i].parametersForOutput_.active) {
          minimaListSelection_.push_back (i);
        }
      }
      initialized_ = true;
    }
    std::cout << "**********************************\n"
              << "*Starting the global minimization*\n"
              << " With  " << (settings_.nMC_ - mcStep_ + 1) << " steps\n"
              << "**********************************\n";

    for (; mcStep_ <= settings_.nMC_ && run; ++mcStep_) {
      // std::cout <<mcStep_<<"->";
      BHClusterData PreviouslyAcceptedData =
        walkers_[runnigWalker_].lastAcceptedClusterData ();
      walkers_[runnigWalker_].doMove (
        algorithmSupport_.Analyzer, metalParameters_, rndEngine_);
      { ///@todo rationalize this bracket in a single operation
        BHdouble tmpEnergy = minimizeAndCountTime (runnigWalker_);
        BHWalker::forceLastMoveEnergy (walkers_[runnigWalker_], tmpEnergy);
      }
      bool moveAccepted = walkers_[runnigWalker_].confirmStep (
        walkers_, metalParameters_, algorithmSupport_, rndEngine_);

      if (settings_.analyzeAtEveryStep_) {
        walkers_[runnigWalker_].AnalyzeWorkingConf (
          algorithmSupport_.Analyzer, metalParameters_);
        ///@todo update minima also with analyzeAllMinima, but needs a way to
        /// update the right one
        updateMinima (runnigWalker_);
      }
      if (moveAccepted) {
        if (PreviouslyAcceptedData.differentFrom (
              walkers_[runnigWalker_].lastAcceptedClusterData ())) {
          // a real move is a move that differs in energy or at least in an OP
          // configuration from the previous accepted one we have chose 5e-3 as
          // minimimun difference
          ++acceptedMovesInfo_[runnigWalker_].RealMoves;
          ++totalMoves_.RealMoves;
        }
        updateMinima (runnigWalker_);

        MoveAccepted ();
        walkers_[runnigWalker_].postAcceptedActions_ ();
        MoveAcceptedPostProduction ();
      }
      OutputAfterStep ();
      ChangeWalker ();
      ScreenLogging ();
    }
    --mcStep_; // the for cicle advances the step by 1
    // std::cout<<endl;//delete!
    std::cout << "**********************************\n"
              << "*Finished the global minimization*\n"
              << "**********************************\n"
              << "in " << mcStep_ << " Monte Carlo steps." << std::endl
              << "Found the best energy " << GloInfoGeneral_.energy
              << std::endl;
    writeStatus (std::cout, totalMoves_, mcStep_);
    writeMovesStatusByType (movesStatistics_);
    for (auto &fil : enerAll_) {
      fil.close ();
    }
    for (auto &fil : enerBest_) {
      fil.close ();
    }

    if (mcStep_ != lastLogOnStep_) {
      writeMovesStatus (movesOut_, totalMoves_, mcStep_);
    }

    // add an option to ask if it is wanted
    if (settings_.saveHistogramData_) {
      // BHSettings::PlotHistoMatrix (HistogramOutFile_.c_str
      // (),algorithmSupport_.histo);
      std::ofstream histoOut (settings_.HistogramOutFile_);
      BHSettingsUtilities::PlotHistoCoordinates (
        histoOut, settings_, algorithmSupport_.Histogram);
      histoOut.close ();
    }
  }

  BHdouble BHAbstractBasinHopping::initializeClusters (const unsigned &wID) {
    std::stringstream fname;
    fname << "SeedOut_" << walkers_[wID].identificator << ".xyz";

    walkers_[wID].reorderAndInitializeLabels (metalParameters_);

    std::ofstream seedOut (fname.str ());
    ::operator<< (seedOut, walkers_[wID].lastMoveConf ());
    seedOut << std::endl;
    seedOut.close ();
    BHdouble energy = 0;
    do {
      walkers_[wID].CollapseIslandsOnWorkingConf (metalParameters_, rndEngine_);
      // FIRE gives some problems with big bang initializations
      energy = minimizeAndCountTime (wID);
      // energy = minimizeLBFGSAndCountTime (wID);
    } while (walkers_[wID].CountIslandsOnWorkingConf (metalParameters_) > 1);
    return energy;
  }

  BHdouble BHAbstractBasinHopping::minimizeAndCountTime (const unsigned wID) {
    ++nMinimizations_;
    tp start, stop;
    start = std::chrono::steady_clock::now ();
    BHdouble energy =
      walkers_[wID].minimizeWorkingConf (minimizator_, energyCalculator_);
    stop = std::chrono::steady_clock::now ();
    minimizationTime_ += stop - start;
    return energy;
  }

  BHdouble
  BHAbstractBasinHopping::minimizeLBFGSAndCountTime (const unsigned wID) {
    ++nMinimizations_;
    tp start, stop;
    start = std::chrono::steady_clock::now ();
    BHMinimizationAlgorithm_LBFGSB LBFGS;
    BHdouble energy =
      walkers_[wID].minimizeWorkingConf (&LBFGS, energyCalculator_);
    stop = std::chrono::steady_clock::now ();
    minimizationTime_ += stop - start;
    return energy;
  }

  void BHAbstractBasinHopping::updateMinima (const size_t &wID) {
    for (auto parID : minimaListSelection_) {
      BHdouble OPvalue = walkers_[wID].lastAcceptedOP (parID);
      BHdouble newEne = walkers_[wID].lastAcceptedEnergy ();
      int getPar = -1;
      if (
        (settings_.OPSettings_[parID].parametersForOutput_.paramMin <=
         OPvalue) ||
        (OPvalue <=
         settings_.OPSettings_[parID].parametersForOutput_.paramMax)) {
        int parpos = static_cast<int> (
          OPvalue / settings_.OPSettings_[parID].parametersForOutput_.interval);
        if (minimaList_[parID].count (parpos) == 0) {
          getPar = parpos;
        } else if (newEne < minimaList_[parID][parpos].Energy ()) {
          getPar = parpos;
        }
      }

      if (getPar >= 0) {
        // updating the min****.xyz file and the map
        // at the time of writing, map add a new element if it is not present
        minimaList_[parID][getPar] = walkers_[wID].lastAcceptedClusterData ();
        std::stringstream fname;
        fname << OparName (settings_.OPSettings_[parID].OP_) << "-min"
              << std::setw (4) << std::setfill ('0') << getPar << ".xyz";
        std::ofstream min (fname.str ());
        min.setf (std::ios_base::fixed);
        // here I am using exposeWorkingConfiguration because I don't know if I
        // am her with AnalyzeAll set to true
        min << BHClusterUtilities::plotClusterWithData (
                 walkers_[wID].lastAcceptedConf (),
                 walkers_[wID].lastAcceptedClusterData (),
                 BHSettings::getOPsFromSettings (settings_.OPSettings_))
            << std::endl;
        min.close ();
        // overwriting param_best.out
        std::ofstream minimafile (
          OparName (settings_.OPSettings_[parID].OP_) + "-param_best.out");
        if (minimafile.good ()) {
          minimafile.setf (std::ios_base::fixed);
          minimafile << "minID\tinterval\t"
                     << BHClusterData::ClusterData_list (
                          BHSettings::getOPsFromSettings (
                            settings_.OPSettings_))
                     << '\n';
          // for (auto it = minima_list.begin();it!=minima_list.end();it++) {
          for (const auto &it : minimaList_[parID]) {
            minimafile << it.first << "\t"
                       << (it.first * settings_.OPSettings_[parID]
                                        .parametersForOutput_.interval)
                       << "\t" << it.second << '\n';
          }
          minimafile.close ();

        } else {
          throw "BH:problem opening param_best.out for output";
        }
      }
    }
  }

  void BHAbstractBasinHopping::updateNewGlobalMinimum (
    const unsigned int &wID,
    const unsigned &mcStep,
    const unsigned &gloID,
    std::ofstream &ener_best,
    const std::string &prefix) {
    std::stringstream fname;
    fname << prefix << "glo" << std::setw (4) << std::setfill ('0') << gloID
          << ".xyz";
    // flushed on the go in order to give the user the possibility to see the
    // evolution
    ener_best << mcStep << "\t" << gloID << "\t"
              << walkers_[wID].lastAcceptedClusterData () << std::endl;
    std::ofstream glo (fname.str ());
    glo << BHClusterUtilities::plotClusterWithData (
             walkers_[wID].lastAcceptedConf (),
             walkers_[wID].lastAcceptedClusterData (),
             BHSettings::getOPsFromSettings (settings_.OPSettings_))
        << std::endl;
    glo.close ();
  }

  void BHAbstractBasinHopping::updateNewGlobalMinimum (
    const unsigned int &wID,
    const BHGloInfo &gloInfo,
    std::ofstream &ener_best,
    const std::string &prefix) {
    updateNewGlobalMinimum (
      wID, gloInfo.mcStep, gloInfo.GloID, ener_best, prefix);
  }

  void BHAbstractBasinHopping::writeStatus (
    std::ostream &stream,
    const BHMoveStatistic &totalMoves,
    const size_t &mcStep) {
    algorithmSupport_.Analyzer.times (stream);
    stream << "minimization:\t" << nMinimizations_ << ",\t";
    timing (stream, minimizationTime_);
    stream << std::endl;

    stream << "Accepted moves: " << totalMoves.AcceptedMoves << " ("
           << (100. * totalMoves.AcceptedMoves / static_cast<BHdouble> (mcStep))
           << "%)"
           << "\nTrue moves:     " << totalMoves.RealMoves << " ("
           << (100. * totalMoves.RealMoves / static_cast<BHdouble> (mcStep))
           << "%)" << std::endl;
    for (size_t wID = 0; wID < walkers_.size (); wID++) {
      stream << "Times for walker " << wID << " ("
             << strFromWalkerAlgorithm (walkers_[wID].myAlgorithm ()) << ")\n";
      walkers_[wID].times (stream);
      stream << "Accepted moves: " << acceptedMovesInfo_[wID].AcceptedMoves
             << ", real: " << acceptedMovesInfo_[wID].RealMoves << std::endl;
    }
  }

  void BHAbstractBasinHopping::writeMovesStatusByType (std::ostream &stream) {
    for (size_t wID = 0; wID < walkers_.size (); wID++) {
      stream << "Walker " << wID << '\n';
      walkers_[wID].movesCount (stream);
      stream << std::flush;
    }
  }

  void BHAbstractBasinHopping::writeMovesStatus (
    std::ostream &stream,
    const BHMoveStatistic &totalMoves,
    const size_t &mcStep) {
    stream << std::setw (WIDTH10) << mcStep << std::setprecision (2)
           << std::fixed << std::setw (WIDTH8)
           << (100. * totalMoves.AcceptedMoves / static_cast<BHdouble> (mcStep))
           << std::setw (WIDTH8)
           << (100. * totalMoves.RealMoves / static_cast<BHdouble> (mcStep));
    if (walkers_.size () > 1) {
      for (size_t wIDt = 0; wIDt < walkers_.size (); wIDt++) {
        int movesdone = walkers_[wIDt].getTotalMovesDone ();
        stream << std::setw (WIDTH10) << movesdone << std::setw (WIDTH8)
               << ((100. * acceptedMovesInfo_[wIDt].AcceptedMoves) / movesdone)
               << std::setw (WIDTH8)
               << ((100. * acceptedMovesInfo_[wIDt].RealMoves) / movesdone);
      }
    }
    stream << std::endl;
  }

  void BHAbstractBasinHopping::Initialization () {
    std::cout << "\t#########################\n"
              << "\t#INITIALIZE BASINHOPPING#\n"
              << "\t#########################\n";
    bool allWalkerAreObstacle = true;
    // initializing the walker(s)
    for (unsigned wID = 0; wID < settings_.walkerFilenames_.size (); ++wID) {
      std::cout << "\n###############Creating walker " << wID << std::endl;

      BHWalkerSettings thisWalkerSettings =
        loadBHWalkerSettings (settings_.walkerFilenames_[wID].flyingWalker);
      // creating the walker
      if (
        settings_.walkerFilenames_.size () == 1 &&
        !isStandaloneAlgorithm (thisWalkerSettings.ChosenAlgorithm_)) {
        walkers_.push_back (
          BHConstructors::getConstructor ().createStandardWalker (
            thisWalkerSettings, metalParameters_, rndEngine_));
      } else {
        walkers_.push_back (BHConstructors::getConstructor ().createWalker (
          thisWalkerSettings, metalParameters_, rndEngine_));
      }

      walkers_.back ().printSettings ();
      std::stringstream fname;
      fname << "w" << std::setw (2) << std::setfill ('0') << wID;
      walkers_.back ().identificator = fname.str ();
      if (wID == 0) {
        energyCalculator_ = new EnergyCalculator (
          walkers_[0].lastAcceptedConf (), metalParameters_);
      } else {
        if (
          walkers_[0].lastAcceptedConf ().getNofAtoms () !=
          walkers_[wID].lastAcceptedConf ().getNofAtoms ()) {
          throw "BH: walkers have different number of atoms";
        }
        if (!walkers_[0].lastAcceptedConf ().testSameConfiguration (
              walkers_[wID].lastAcceptedConf ())) {
          throw "BH: walkers have different combination of atoms";
        }
      }
      std::cout << "Initialization of walker " << wID << std::endl;
      // collapse islands and set the energy for the move
      initializeClusters (wID);

      // this should set up also the interaction labels
      walkers_[wID].AnalyzeWorkingConf (
        algorithmSupport_.Analyzer, metalParameters_);
      walkers_[wID].flyingID_ = wID;
      walkers_[wID].landingID_ = wID;
      walkers_[wID].hikingID_ = wID;
      // this updates the last accepted parameters and copies the working conf
      // in the lastaccepted conf
      walkers_[wID].moveAccepted ();
      // and this give some feedback on the success
      std::cout << "Storing initial energy for walker " << wID << ": "
                << walkers_[wID].lastAcceptedEnergy () << std::endl;

      allWalkerAreObstacle &= walkers_[wID].ignoreStep ();
      switch (walkers_[wID].myAlgorithm ()) {
      // if the algorithm is histo the algorithm needs to update and work on the
      // algorithm
      case BHwalkerAlgorithmType::histo: {
        settings_.saveHistogramData_ = true;
        algorithmSupport_.Histogram.initializeHisto ();
      } break;
      default:
        break;
      }
    }
    if (allWalkerAreObstacle) {
      throw "You cannot start a minimization using only fixed obstacle walkers";
    }

    for (unsigned wID = 0; wID < settings_.walkerFilenames_.size (); ++wID) {
      initializeLandingWalkers (wID);
    }

    for (unsigned wID = 0; wID < settings_.walkerFilenames_.size (); ++wID) {
      initializeHikingWalkers (wID);
    }
    initializeBestWalker ();
    acceptedMovesInfo_.resize (walkers_.size ());
  }

  inline void erase_all (std::string &str, char x) {
    str.erase (std::remove (str.begin (), str.end (), x), str.end ());
  }

  void BHAbstractBasinHopping::initializeLandingWalkers (const unsigned wID) {
    if (settings_.walkerFilenames_[wID].landingWalker != "") {
      settings_.separateWalkerOutput_ = true;
      std::cout << "\n###############Creating landing walker for walker " << wID
                << std::endl;
      std::string landingFile = settings_.walkerFilenames_[wID].landingWalker;
      landingFile = landingFile.substr (landingFile.find_last_of (':'));
      // this removes the ':'
      erase_all (landingFile, ':');

      BHWalkerSettings landingSettings = loadBHWalkerSettings (landingFile);
      walkers_.push_back (BHConstructors::getConstructor ().createWalker (
        landingSettings, metalParameters_, rndEngine_));

      walkers_.back ().printSettings ();
      std::stringstream fname;
      fname << "lw" << std::setw (2) << std::setfill ('0') << wID;
      walkers_.back ().identificator = fname.str ();

      unsigned landingID_ = walkers_.size () - 1U;
      // setting up interactions flags
      walkers_[wID].landingID_ = landingID_;

      walkers_[landingID_].flyingID_ = wID;
      walkers_[landingID_].landingID_ = landingID_;
      walkers_[landingID_].hikingID_ = wID;
      EnergyAtDive_.resize (settings_.walkerFilenames_.size ());
      walkers_[wID].postAcceptedActions_ = [this, wID] () {
        BHdouble newEne = walkers_[wID].lastAcceptedEnergy ();
        unsigned landingID = walkers_[wID].landingID_;
        if (
          (GloInfoPerWalker_[wID].mcStep == mcStep_) &&
          (wID < settings_.walkerFilenames_.size ())) {
          BHdouble deltaFlying = EnergyAtDive_[wID] - newEne;
          BHdouble deltaLanding =
            EnergyAtDive_[wID] - walkers_[landingID].lastAcceptedEnergy ();
          BHdouble eps = std::pow (
            std::abs (deltaFlying / deltaLanding), settings_.landingAlpha_);
          if (LandingRestart_ (rndEngine_) < eps) {
            EnergyAtDive_[wID] = newEne;
            walkers_[landingID].copyAcceptedConfiguration (walkers_[wID]);
          }
        }
      };
    }
  }

  void BHAbstractBasinHopping::initializeHikingWalkers (const unsigned wID) {
    if (
      settings_.walkerFilenames_[wID].hikingWalker != "" &&
      walkers_[wID].landingID_ != walkers_[wID].flyingID_) {
      std::cout << "\n###############Creating hiking walker for walker " << wID
                << std::endl;
      std::string hikingFile = settings_.walkerFilenames_[wID].hikingWalker;
      std::string hikingSetting =
        hikingFile.substr (0, hikingFile.find_last_of (':'));
      hikingFile = hikingFile.substr (hikingFile.find_last_of (':'));
      erase_all (hikingFile, ':');
      BHWalkerSettings hikingSettings = loadBHWalkerSettings (hikingFile);
      walkers_.push_back (BHConstructors::getConstructor ().createWalker (
        hikingSettings, metalParameters_, rndEngine_));

      walkers_.back ().printSettings ();
      std::stringstream fname;
      fname << "hw" << std::setw (2) << std::setfill ('0') << wID;
      walkers_.back ().identificator = fname.str ();

      unsigned hikingID = walkers_.size () - 1U;
      // setting up interactions flags
      walkers_[wID].hikingID_ = hikingID;
      walkers_[walkers_[wID].landingID_].hikingID_ = hikingID;

      walkers_[hikingID].flyingID_ = wID;
      walkers_[hikingID].landingID_ = walkers_[wID].landingID_;
      walkers_[hikingID].hikingID_ = hikingID;

      if (hikingSetting == "hikingR") {
        walkers_[walkers_[wID].landingID_].postAcceptedActions_ =
          [this, flyingID = wID, landingID = walkers_[wID].landingID_,
           hikingID] () {
            if (
              walkers_[landingID].lastAcceptedEnergy () <
              GloInfoPerWalker_[hikingID].energy) {
              walkers_[hikingID].copyAcceptedConfiguration (
                walkers_[landingID]);
              walkers_[landingID].copyAcceptedConfiguration (
                walkers_[flyingID]);
            }
          };
      } else { // default is not restarting
        walkers_[walkers_[wID].landingID_].postAcceptedActions_ =
          [this, landingID = walkers_[wID].landingID_, hikingID] () {
            if (
              walkers_[landingID].lastAcceptedEnergy () <
              GloInfoPerWalker_[hikingID].energy) {
              walkers_[hikingID].copyAcceptedConfiguration (
                walkers_[landingID]);
            }
          };
      }
    }
  }

  void BHAbstractBasinHopping::initializeBestWalker () {
    ///@todo add the restarting version
    if (settings_.uniqueBestWalker_ != "") {
      // adds a best walker to all the landing walkers that do not have a hiking
      bool bestWalkerNeeded = false;

      unsigned bestWalkerID = walkers_.size ();

      for (unsigned wID = 0; wID < settings_.walkerFilenames_.size (); ++wID) {
        if (
          settings_.walkerFilenames_[wID].hikingWalker != "" &&
          walkers_[wID].landingID_ != walkers_[wID].flyingID_ &&
          walkers_[wID].hikingID_ == wID) {
          bestWalkerNeeded = true;
          // setting up interactions flags
          walkers_[wID].hikingID_ = bestWalkerID;
          walkers_[walkers_[wID].landingID_].hikingID_ = bestWalkerID;
          if (settings_.BWRestartsLanding_) {
            walkers_[walkers_[wID].landingID_].postAcceptedActions_ =
              [this, flyingID = wID, landingID = walkers_[wID].landingID_,
               bestWalkerID] () {
                if (
                  walkers_[landingID].lastAcceptedEnergy () <
                  GloInfoPerWalker_[bestWalkerID].energy) {
                  walkers_[bestWalkerID].copyAcceptedConfiguration (
                    walkers_[landingID]);
                  walkers_[landingID].copyAcceptedConfiguration (
                    walkers_[flyingID]);
                }
              };
          } else {
            walkers_[walkers_[wID].landingID_].postAcceptedActions_ =
              [this, landingID = walkers_[wID].landingID_, bestWalkerID] () {
                if (
                  walkers_[landingID].lastAcceptedEnergy () <
                  GloInfoPerWalker_[bestWalkerID].energy) {
                  walkers_[bestWalkerID].copyAcceptedConfiguration (
                    walkers_[landingID]);
                }
              };
          }
        }
      }
      // if at least one of the landing walker needs the best walker create it
      if (bestWalkerNeeded) {
        std::cout << "\n###############Creating the best walker" << std::endl;
        std::string bestWalkerFile = settings_.uniqueBestWalker_;
        bestWalkerFile = bestWalkerFile.substr (bestWalkerFile.find (':'));
        erase_all (bestWalkerFile, ':');
        BHWalkerSettings hikingSettings = loadBHWalkerSettings (bestWalkerFile);
        walkers_.push_back (BHConstructors::getConstructor ().createWalker (
          hikingSettings, metalParameters_, rndEngine_));

        walkers_.back ().printSettings ();
        std::stringstream fname;
        fname << "bw" << std::setw (2) << std::setfill ('0') << 0;
        walkers_.back ().identificator = fname.str ();

        walkers_[bestWalkerID].flyingID_ = bestWalkerID;
        walkers_[bestWalkerID].landingID_ = bestWalkerID;
        walkers_[bestWalkerID].hikingID_ = bestWalkerID;
      }
    }
  }

  void BHAbstractBasinHopping::PreOutputInitialization () {}

  void BHAbstractBasinHopping::OutputInitialization () {
    if (settings_.separateWalkerOutput_) {
      GloInfoPerWalker_.resize (walkers_.size (), {0.0, 0, 0});
      for (size_t wID = 0; wID < walkers_.size (); ++wID) {
        enerAll_.emplace_back (walkers_[wID].identificator + "ener_all.out");
        enerBest_.emplace_back (walkers_[wID].identificator + "ener_best.out");
      }
    }
    // this streams must have the biggest id within the std::vector
    enerAll_.emplace_back ("ener_all.out");
    enerBest_.emplace_back ("ener_best.out");
    // header for reconstructing the simulation
    enerAll_.back () << "#Seed: " << settings_.RngSeed_;
    if (!settings_.analyzeAtEveryStep_) {
      enerAll_.back () << ", " << OparName (settings_.OPSettings_[0].OP_)
                       << " and " << OparName (settings_.OPSettings_[1].OP_)
                       << " are referred at the last accepted move, see manual";
    }
    enerAll_.back () << '\n';
    for (size_t wID = 0; wID < enerAll_.size (); ++wID) {
      enerAll_[wID].setf (std::ios_base::fixed);
      enerAll_[wID] << "mcStep_\t"
                    << BHClusterData::ClusterData_list (
                         BHSettings::getOPsFromSettings (settings_.OPSettings_))
                    << "\tlast_acp_E\tT_move\tmoveName" << '\n';
      enerBest_[wID].setf (std::ios_base::fixed);
      enerBest_[wID] << "mcStep_\tglo\t"
                     << BHClusterData::ClusterData_list (
                          BHSettings::getOPsFromSettings (
                            settings_.OPSettings_))
                     << '\n';
      if (wID < walkers_.size () && settings_.separateWalkerOutput_) {
        GloInfoPerWalker_[wID].GloID = 0;
        GloInfoPerWalker_[wID].mcStep = 0;
        GloInfoPerWalker_[wID].energy = walkers_[wID].lastAcceptedEnergy ();
        enerAll_[wID] << "0\t" << walkers_[wID].lastAcceptedClusterData ()
                      << "\t" << walkers_[wID].lastAcceptedEnergy () << "\t"
                      << walkers_[wID].lastMoveInfo () << std::endl;
        enerBest_[wID] << "0\t0\t" << walkers_[wID].lastAcceptedClusterData ()
                       << std::endl;
      }
    }
    enerAll_.back () << "0\t" << walkers_[wIDSaved_].lastAcceptedClusterData ()
                     << "\t" << walkers_[wIDSaved_].lastAcceptedEnergy ()
                     << "\t" << walkers_[wIDSaved_].lastMoveInfo ()
                     << std::endl;
    enerBest_.back () << "0\t0\t"
                      << walkers_[wIDSaved_].lastAcceptedClusterData ()
                      << std::endl;

    for (size_t wID = 0; wID < walkers_.size (); ++wID) {
      updateMinima (wID);
      BHdouble thisEne = walkers_[wID].lastAcceptedClusterData ().Energy ();
      if (thisEne < GloInfoGeneral_.energy) {
        GloInfoGeneral_.energy = thisEne;
        wIDSaved_ = wID;
      }
      if (settings_.separateWalkerOutput_) {
        std::ofstream wIDglo0000 (walkers_[wID].identificator + "glo0000.xyz");
        GloInfoPerWalker_[wID].energy = thisEne;
        wIDglo0000 << BHClusterUtilities::plotClusterWithData (
                        walkers_[wID].lastAcceptedConf (),
                        walkers_[wID].lastAcceptedClusterData (),
                        BHSettings::getOPsFromSettings (settings_.OPSettings_))
                   << std::endl;
        wIDglo0000.close ();
      }
    }
    // glo0000 is the best between the starting clusters
    std::ofstream glo0000 ("glo0000.xyz");
    glo0000 << BHClusterUtilities::plotClusterWithData (
                 walkers_[wIDSaved_].lastAcceptedConf (),
                 walkers_[wIDSaved_].lastAcceptedClusterData (),
                 BHSettings::getOPsFromSettings (settings_.OPSettings_))
            << std::endl;
    glo0000.close ();

    // two files for understand how the simulation is going
    // a more verbose output
    std::ofstream movesStatistics ("movesStatistics.out");
    // a tabular output with % of accepted and true moves
    std::ofstream moves_out ("moves.out");

    moves_out << std::setw (WIDTH10) << "mcStep" << std::setw (WIDTH8)
              << "totacc" << std::setw (WIDTH8) << "tottrue";
    if (walkers_.size () > 1) {
      for (size_t wID = 0; wID < walkers_.size (); ++wID) {
        std::stringstream fname;
        fname << std::setw (2) << std::setfill ('0') << wID;
        moves_out << std::setw (WIDTH10) << ("Nmovesw" + fname.str ())
                  << std::setw (WIDTH8) << ("accw" + fname.str ())
                  << std::setw (WIDTH8) << ("truew" + fname.str ());
      }
    }
    moves_out << std::endl;
  }

  void BHAbstractBasinHopping::MoveAccepted () {
    ++acceptedMovesInfo_[runnigWalker_].AcceptedMoves;
    ++totalMoves_.AcceptedMoves;
    // saves the runnigWalker_ of the last accepted minimum in order to use it
    // in ener_all
    wIDSaved_ = runnigWalker_;
    BHdouble newEne = walkers_[runnigWalker_].lastAcceptedEnergy ();

    if (settings_.separateWalkerOutput_) {
      if (
        (GloInfoPerWalker_[runnigWalker_].energy - newEne) >
        MINIMUN_ENERGY_DIFFERENCE) {
        GloInfoPerWalker_[runnigWalker_].recodNewMinimum (newEne, mcStep_);
        updateNewGlobalMinimum (
          runnigWalker_, GloInfoPerWalker_[runnigWalker_],
          enerBest_[runnigWalker_], walkers_[runnigWalker_].identificator);
      }
    } // if (separateWalkerOutput)

    if ((GloInfoGeneral_.energy - newEne) > MINIMUN_ENERGY_DIFFERENCE) {
      GloInfoGeneral_.recodNewMinimum (newEne, mcStep_);
      updateNewGlobalMinimum (
        runnigWalker_, GloInfoGeneral_, enerBest_.back ());
    }
  }

  void BHAbstractBasinHopping::MoveAcceptedPostProduction () {}

  void BHAbstractBasinHopping::OutputAfterStep () {
    enerAll_.back () << mcStep_ << "\t"
                     << walkers_[runnigWalker_].lastMoveEnergy ();
    if (settings_.analyzeAtEveryStep_) {
      enerAll_.back ()
        << "\t"
        << walkers_[runnigWalker_].lastMoveClusterData ().orderParameter (0)
        << "\t"
        << walkers_[runnigWalker_].lastMoveClusterData ().orderParameter (1);
    } else {
      enerAll_.back ()
        << "\t"
        << walkers_[wIDSaved_].lastAcceptedClusterData ().orderParameter (0)
        << "\t"
        << walkers_[wIDSaved_].lastAcceptedClusterData ().orderParameter (1);
    }

    enerAll_.back () << "\t"
                     << walkers_[wIDSaved_].lastAcceptedClusterData ().Energy ()
                     << "\t" << walkers_[runnigWalker_].lastMoveInfo ()
                     << std::endl;
    if (settings_.separateWalkerOutput_ && walkers_.size () > 1) {
      enerAll_[runnigWalker_]
        << mcStep_ << "\t" << walkers_[runnigWalker_].lastMoveEnergy () << "\t"
        << walkers_[runnigWalker_].lastAcceptedClusterData ().orderParameter (0)
        << "\t"
        << walkers_[runnigWalker_].lastAcceptedClusterData ().orderParameter (1)
        << "\t" << walkers_[runnigWalker_].lastAcceptedClusterData ().Energy ()
        << "\t" << walkers_[runnigWalker_].lastMoveInfo () << std::endl;
    }
    if (settings_.saveHistogramData_) {
      algorithmSupport_.Histogram.IncrementBin (
        walkers_[runnigWalker_].lastAcceptedOP (0),
        walkers_[runnigWalker_].lastAcceptedOP (1));
    }
  }

  void BHAbstractBasinHopping::ChangeWalker () {
    do {
      // as now this is the only way of moving walkers
      runnigWalker_ =
        (runnigWalker_ >= walkers_.size () - 1) ? 0 : runnigWalker_ + 1;
      // std::cout << mcStep_ << " " << runnigWalker_ <<std::endl;
    } while (walkers_[runnigWalker_].ignoreStep ());
  }

  void BHAbstractBasinHopping::ScreenLogging () {
    // std::cout<< "\r"<<mcStep_ << " " << runnigWalker_; //delete!
    if ((mcStep_ % TOTALLOG) == 0) {
      // std::cout<< "\r";//delete
      std::cout << "**At mcStep: " << mcStep_
                << "\n!Energy: " << GloInfoGeneral_.energy << std::endl;
      writeStatus (std::cout, totalMoves_, mcStep_);
      writeMovesStatusByType (movesStatistics_);
      // std::cout.flush();//delete!
    }
    // info on moves
    if ((mcStep_ % MOVELOG) == 0) {
      lastLogOnStep_ = mcStep_;
      writeMovesStatus (movesOut_, totalMoves_, mcStep_);
    }
  }
  void BHAbstractBasinHopping::BHGloInfo::recodNewMinimum (
    const BHdouble &newEne, const unsigned &newStep) {
    ++GloID;
    energy = newEne;
    mcStep = newStep;
  }
} // namespace BH
