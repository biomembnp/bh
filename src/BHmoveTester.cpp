/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Contains the definitions of moveTester
   @file BHmoveTester.cpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 27/8/2017
   @version 1.1
*/
#include "BHmoveTester.hpp"

#include "BHdebug.hpp"
#include "BHmetalParameters.hpp"
#include "BHwalkerAlgorithmObstacle.hpp"
#include "BHworkWithFortran.hpp"
#include <fstream>
#include <iostream>
using std::cin;

namespace BH {
  moveTester::moveTester (
    const BHWalkerSettings &walkersettings,
    const BHClusterAtoms &seed,
    const BHMetalParameters &bhmp)
    : BHWalker (
        walkersettings,
        seed,
        bhmp,
        BHWalkerAlgorithms::BHWalkerAlgorithmObstacle,
        true,
        true) {}

  BHdouble moveTester::doMove (
    BHClusterAnalyser &Analyzer,
    const BHMetalParameters &bhmp,
    rndEngine &rng) {
    bhmp.setInteractionLabels (WorkingConf_);
    bhmp.setInteractionLabels (LastAcceptedConf_);
    unsigned in;
    cout << "Doable moves: " << endl;
    for (unsigned int i = 0; i < 9; ++i) {
      std::cout << i << "\t" << moves_[i]->name () << std::endl;
    }
    cout << "Select the move that you want to execute: ";
    cin >> in;
    if (in > moves_.size ()) {
      in = static_cast<typeof (in)> (moves_.size ());
    }
    ++n_moves;
    start = std::chrono::steady_clock::now ();
    // BHClusterAtoms::operator<<(LastAcceptedConf_);//so i know that the temp
    // conf is the last accepted conf
    chosenMove_ = in;

    TAccept_ = moves_[chosenMove_]->doMove (
      LastAcceptedConf_, WorkingConf_, Analyzer, bhmp, rng);
    if (TAccept_ < 0) {
      std::cout << "I failed to made the move " << moves_[chosenMove_]->name ()
                << " correctly\n";
    }
    stop = std::chrono::steady_clock::now ();
    move_time += stop - start;
    return TAccept_;
  }

  void moveTester::ExportResultAndMinimized (
    BHMinimizationAlgorithm *minimizator,
    BHEnergyCalculator *ec,
    std::string optional) {
    cout << "Move executed," << endl;
    std::string fname = moves_[chosenMove_]->name () + optional;
    std::string moved_unminimized = fname + "_unminimized.xyz",
                moved_minimized = fname + "_minimized.xyz";
    std::ofstream file (moved_unminimized);
    ::operator<< (file, WorkingConf_);
    file << endl;
    file.close ();

    minimizator->MinimizeCluster (WorkingConf_, ec);

    file.open (moved_minimized);
    ::operator<< (file, WorkingConf_);
    file << endl;
    cout << "I have minimezed the configuration and saved two new files:\n"
         << "the minimized  configuration in \"" << moved_minimized << "\"\n"
         << "the not minimized  configuration in \"" << moved_unminimized
         << "\"\n"
         << "\n\n now I'm falling back to the starting configuration" << endl;
    WorkingConf_ = (LastAcceptedConf_);
  }
} // namespace BH
