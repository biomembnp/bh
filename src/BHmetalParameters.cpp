/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @file BHmetalParameters.cpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @brief Definition of BHMetalParameters

@version 1.2
  @date 8/2/2017

      main feature: the user can add a custom NNdist as last field in the
                       interaction section in the MetalParameters.in file

                         now calculates interaction only one time and
                           plots them also only one time

                     @version 1.1.1
                     @date 7/2/2017

  Lot of modifies have not been tracked

  @version 1.1
  @date 20/4/2017
  */
#include "BHmetalParameters.hpp"

#include <cmath>
#include <fstream>
#include <sstream>

//#ifdef METALPARDEBUG
#include <algorithm>
#include <iomanip>
#include <iostream>

using std::cout;
using std::endl;
using std::setw;

// using std::cerr;
// using std::cin;
//#endif//METALPARDEBUG

namespace BH {
  constexpr BHdouble sqrt8 = 2.0 * M_SQRT2;
  constexpr BHdouble densq2 = 1. / M_SQRT2;

  std::istream &operator>> (std::istream &stream, BH::BHSMATBInteraction &t) {
    stream >> t.p >> t.q >> t.a >> t.qsi >> t.cutOff_start >> t.cutOff_end;
    return stream;
  }

  std::istream &operator>> (std::istream &stream, BH::BHLJInteraction &t) {
    stream >> t.epsilon >> t.sigma >> t.cutOff_start >> t.cutOff_end;
    return stream;
  }

  /*
          !Unit conversions
          //rat = Radius
          arete(1)=rat(1)*dsqrt(8.d0)
          arete(2)=rat(2)*dsqrt(8.d0)
          arete(3)=(arete(1)+arete(2))/2.0d0
        */
  /// Gets the arete for a single metal
  inline BHdouble getArete (BHdouble radius) { return radius * sqrt8; }
  /// Get the arete for a mixed metal
  inline BHdouble getArete (BHdouble radius1, BHdouble radius2) {
    return (radius1 + radius2) * M_SQRT2;
  }

  BHMetalParameters::BHMetalParameters () = default;

  BHMetalParameters::BHMetalParameters (std::string filename, bool verbose) {
    if (filename != "") {
      loadFromFile (filename);
      ready = true;
    }
    if (verbose) {
      printData (std::cout);
    }
  }

  BHMetalParameters::BHMetalParameters (const BHMetalParameters &other) =
    default;
  /*: Nmetals{other.Nmetals},
    Ninteractions{other.Ninteractions},
    Slabels{other.Slabels},
    Plabels{other.Plabels},
    Sname{other.Sname},
    Pname{other.Pname},
    InteractionParameters_{other.InteractionParameters_},
    AtomParameters_{other.AtomParameters_},
    ready{other.ready} {}*/

  BHMetalParameters::BHMetalParameters (BHMetalParameters &&other) noexcept =
    default;
  /*: Nmetals{other.Nmetals},
    Ninteractions{other.Ninteractions},
    Slabels{std::move (other.Slabels)},
    Plabels{std::move (other.Plabels)},
    Sname{std::move (other.Sname)},
    Pname{std::move (other.Pname)},
    InteractionParameters_{std::move (other.InteractionParameters_)},
    AtomParameters_{std::move (other.AtomParameters_)},
    ready{other.ready} {}*/

  BHMetalParameters::Interactionkind
  BHMetalParameters::intKind (std::string input) {
    auto toret = Interactionkind::DEFAULTSMATB;
    std::transform (
      input.begin (), input.end (), input.begin (),
      [] (unsigned char c) { return std::tolower (c); });
    if (input == "smatb") {
      toret = Interactionkind::SMATB;
    } else if (input == "lj") {
      toret = Interactionkind::LJ;
    }
    return toret;
  }

  void BHMetalParameters::loadFromFile (std::string filename) {
    // these brackets strip the file from the comments and prepare a sstream
    // with all the data
    std::ifstream file (filename);
    if (!file.good ()) {
      throw "BHMETALPARAMETERS: cannot open file " + filename;
    }
    loadFromStream (file);
  }

  void BHMetalParameters::loadFromStream (std::istream &file) {
    std::stringstream f;
    {
      // stripping file of comments
      bool workNotDone = true;
      while (workNotDone) {
        std::string t;
        if (file.peek () == '#') {
          file >> t;
          if (t == "#comments")
            workNotDone = false;
          file.ignore (std::numeric_limits<std::streamsize>::max (), '\n');
        } else {
          std::getline (file, t);
          f << t << '\n';
        }
      }
    }

    unsigned NmetalsInFile;
    f >> NmetalsInFile;
    unsigned NinteractionsInFile = RepetitionComb (NmetalsInFile, 2U);

    // loading atom parameters
    for (unsigned i = 0u; i < NmetalsInFile; i++) { // load "private" parameters
      std::string met;
      BHAtomParameters atomsPar;
      f >> met;
      f >> atomsPar.Coh >> atomsPar.Radius >> atomsPar.Mass;
      if (Slabels.find (met) == Slabels.end ()) {
        Sname[Nmetals] = met;
        Slabels[met] = Nmetals;
        AtomParameters_[met] = atomsPar;
        ++Nmetals;
      }
    }

    // passing to line to line reading
    f.ignore (std::numeric_limits<std::streamsize>::max (), '\n');
    std::string str_dataLine;
    // loading interaction parameters
    std::string met1, met2;
    // this function will be used only within this function
    auto finalizeInitialization = [this] (
                                    BHInteraction &t, std::string metalName1,
                                    std::string metalName2) {
      t.cutOff_endSQ = t.cutOff_end * t.cutOff_end;
      if (t.NNdist < 0.0) {
        if (metalName1 == metalName2) {
          updateNNdistSAME (t, AtomParameters_[metalName1].Radius);
        } else {
          updateNNdistNOTSAME (
            t, AtomParameters_[metalName1].Radius,
            AtomParameters_[metalName2].Radius);
        }
      }
    };
    for (unsigned i = 0; i < NinteractionsInFile; i++) {
      getline (f, str_dataLine);
      std::stringstream dataLine (str_dataLine);

      dataLine >> met1;
      switch (intKind (met1)) {
      case Interactionkind::LJ: {
        dataLine >> met1;
        dataLine >> met2;
        BHLJInteraction t;
        dataLine >> t;

        if (!dataLine.eof ()) {
          dataLine >> t.NNdist;
        } /*else {
//not necessary due to default initialization
          t.NNdist = -1.0;
      }*/
        finalizeInitialization (t, met1, met2);
        finalizeInitialization (t, met1, met2);
        std::string tmet2met1 = (met2 + met1);
        if (Plabels.find (tmet2met1) == Plabels.end ()) {
          Pname[Ninteractions] = (met1 + met2);
          Plabels[(met1 + met2)] = Ninteractions;
          InteractionParameters_[met1 + met2] = t;
          if (met1 != met2) {
            Plabels[(met2 + met1)] = Ninteractions;
            InteractionParameters_[met2 + met1] = t;
          }
          ++Ninteractions;
        }
      } break;
      case Interactionkind::SMATB: {
        dataLine >> met1;
      }
        [[fallthrough]];
      case Interactionkind::DEFAULTSMATB: {
        dataLine >> met2;
        BHSMATBInteraction t;
        dataLine >> t;

        if (!dataLine.eof ()) {
          dataLine >> t.NNdist;
        } /*else {
//not necessary due to default initialization
          t.NNdist = -1.0;
      }*/
        finalizeInitialization (t, met1, met2);
        std::string tmet2met1 = (met2 + met1);
        if (Plabels.find (tmet2met1) == Plabels.end ()) {
          calcPar (t);
          Pname[Ninteractions] = (met1 + met2);
          Plabels[(met1 + met2)] = Ninteractions;
          InteractionParameters_[met1 + met2] = t;
          if (met1 != met2) {
            Plabels[(met2 + met1)] = Ninteractions;
            InteractionParameters_[met2 + met1] = t;
          }
          ++Ninteractions;
        }
      }
      }
    }
//#define CUTOFFDEBUG
#ifdef CUTOFFDEBUG
    {
      BHdouble minimum = 1e99, maximum = 0;
      for (auto &Plabel : Plabels) {
        if (SMATBinteractions_.at (Plabel.first).cutOff_start < minimum)
          minimum = SMATBinteractions_.at (Plabel.first).cutOff_start;
        if (SMATBinteractions_.at (Plabel.first).cutOff_end > maximum)
          maximum = SMATBinteractions_.at (Plabel.first).cutOff_end;
      }
      BHdouble del = maximum - minimum;
      minimum -= del / 10.;
      del = maximum - minimum;
      std::ofstream qfile ("qcut.out"), pfile ("pcut.out");
      qfile << "x\t";
      pfile << "x\t";
      std::vector<std::string> OrderedLabels;
      for (auto &Plabel : Plabels) {
        qfile << Plabel.first << "\t";
        pfile << Plabel.first << "\t";
        OrderedLabels.emplace_back (Plabel.first);
      }
      qfile << std::endl;
      pfile << std::endl;
      for (unsigned int i = 0; i < 1000; i++) {
        BHdouble myx = minimum + del * (i / 1000.);
        qfile << myx << "\t";
        pfile << myx << "\t";
        for (std::string &Plabel : OrderedLabels) {
          if (myx < SMATBinteractions_.at (Plabel).cutOff_start) {
            qfile << SMATBinteractions_.at (Plabel).Qexp (myx) << "\t";
            pfile << SMATBinteractions_.at (Plabel).Pexp (myx) << "\t";
          } else if (myx < SMATBinteractions_.at (Plabel).cutOff_end) {
            qfile << SMATBinteractions_.at (Plabel).Qpoly (myx) << "\t";
            pfile << SMATBinteractions_.at (Plabel).Ppoly (myx) << "\t";
          } else {
            qfile << 0 << "\t";
            pfile << 0 << "\t";
          }
        }
        qfile << std::endl;
        pfile << std::endl;
      }
    }
#endif // CUTOFFDEBUG
  }

  BHMetalParameters::~BHMetalParameters () = default;

  /*
void BHMetalParameters::plotData () {
    cout << std::setprecision (5) << std::fixed;
    cout << "\t#########################\n"
         << "\t###POTENTIAL  SETTINGS###\n"
         << "\t#########################\n"
         << "Atoms Known:\n"
         << "met" << setw (10) << "Coh" << setw (10) << "Radius" << setw (10)
         << "Mass" << endl;
    for (const auto &it : Slabels) {
      cout << it.first << setw (10) << AtomParameters_.at (it.first).Coh
           << setw (10) << AtomParameters_.at (it.first).Radius << setw (10)
           << AtomParameters_.at (it.first).Mass << endl;
    }
    if (SMATBinteractions_.size () > 0) {

  // this is made up in a way that interaction are not repeated
  //(like AuPt and then PtAu)

  std::vector<std::string> interactions;
  {
    std::string str_index, index;
    for (auto it = Slabels.begin (); it != Slabels.end (); ++it) {
      str_index = it->first;
      for (auto it2 = it; it2 != Slabels.end (); ++it2) {
        //    for (auto &Plabel : Plabels) {
        index = str_index + it2->first;
        if (SMATBinteractions_.find (index) != SMATBinteractions_.end ()) {
          interactions.emplace_back (index);
        }
      }
    }
  }
  cout << "##SMATB interactions:##\n"
       << "m1m2" << setw (10) << "p" << setw (10) << "q" << setw (10) << "a"
       << setw (10) << "qsi" << setw (13) << "cutOff_start" << setw (13)
       << "cutOff_end" << endl;
  for (const std::string &index : interactions) {
    cout << index << setw (10) << SMATBinteractions_.at (index).p
         << setw (10) << SMATBinteractions_.at (index).q << setw (10)
         << SMATBinteractions_.at (index).a << setw (10)
         << SMATBinteractions_.at (index).qsi << setw (13)
         << SMATBinteractions_.at (index).cutOff_start << setw (13)
         << SMATBinteractions_.at (index).cutOff_end << endl;
  }

  stream << "FCC values:\n";
  stream << "m1m2" << setw (10) << "NN" << endl;
  for (const std::string &index : interactions) {
    stream << index << setw (10) << setw (10)
           << SMATBinteractions_.at (index).NNdist << endl;
  }
  cout << std::setprecision (12) << std::fixed;
  cout << "Polynomial switching parameters: "
          "[a5(r0-r)^5+a4(r0-r)^4+a3(r0-r)^3]\n";
  cout << "m1m2" << setw (18) << "a5" << setw (18) << "a4" << setw (18)
       << "a3" << endl;
  for (const std::string &index : interactions) {
    stream << index << setw (18) << SMATBinteractions_.at (index).P_par5
           << setw (18) << SMATBinteractions_.at (index).P_par4 << setw (18)
           << SMATBinteractions_.at (index).P_par3 << endl;
  }
  cout << "Polynomial switching parameters: "
          "[x5(r0-r)^5+x4(r0-r)^4+x3(r0-r)^3]\n";
  cout << "m1m2" << setw (18) << "x5" << setw (18) << "x4" << setw (18)
       << "x3" << endl;
  for (const std::string &index : interactions) {
    stream << index << setw (18) << SMATBinteractions_.at (index).Q_par5
           << setw (18) << SMATBinteractions_.at (index).Q_par4 << setw (18)
           << SMATBinteractions_.at (index).Q_par3 << endl;
  }
}
if (LJ12_6interactions_.size () > 0) {
  // this is made up in a way that interaction are not repeated
  //(like AuPt and then PtAu)
  std::vector<std::string> interactions;
  {
    std::string str_index, index;
    for (auto it = Slabels.begin (); it != Slabels.end (); ++it) {
      str_index = it->first;
      for (auto it2 = it; it2 != Slabels.end (); ++it2) {
        //    for (auto &Plabel : Plabels) {
        index = str_index + it2->first;
        if (LJ12_6interactions_.find (index) !=
            LJ12_6interactions_.end ()) {
          interactions.emplace_back (index);
        }
      }
    }
  }
  cout << "##LJ12_6 interactions:##\n"
       << "m1m2" << setw (10) << "epsilon" << setw (10) << "sigma" << endl;
  for (const std::string &index : interactions) {
    cout << index << setw (10) << LJ12_6interactions_.at (index).epsilon
         << setw (10) << LJ12_6interactions_.at (index).sigma << endl;
  }
}
cout << std::resetiosflags (std::ios::floatfield);
}
*/

  void BHMetalParameters::printData (std::ostream &stream) {
    stream << std::setprecision (5) << std::fixed;
    stream << "\t#########################\n"
           << "\t###POTENTIAL  SETTINGS###\n"
           << "\t#########################\n";
    stream << "met" << setw (10) << "Coh" << setw (10) << "Radius" << setw (10)
           << "Mass" << endl;
    for (const auto &it : Slabels) {
      stream << it.first << setw (10) << AtomParameters_.at (it.first).Coh
             << setw (10) << AtomParameters_.at (it.first).Radius << setw (10)
             << AtomParameters_.at (it.first).Mass << endl;
    }
    // stream<<"m1m2\tp\tq\ta\tqsi\tcutOff_start\tcutOff_end"<<endl;
    stream << "m1m2" << setw (10) << "p" << setw (10) << "q" << setw (10) << "a"
           << setw (10) << "qsi" << setw (13) << "cutOff_start" << setw (13)
           << "cutOff_end" << endl;
    // for( auto it = Plabels.begin(); it != Plabels.end(); ++it ){
    // for(int intID = 0; intID < Ninteractions ; ++intID) {
    // then the mixed values:
    std::vector<std::string> interactions;
    {
      std::string str_index, index;
      for (auto it = Slabels.begin (); it != Slabels.end (); ++it) {
        str_index = it->first;
        for (auto it2 = it; it2 != Slabels.end (); ++it2) {
          //    for (auto &Plabel : Plabels) {
          index = str_index + it2->first;
          if (
            InteractionParameters_.find (index) !=
            InteractionParameters_.end ()) {
            interactions.emplace_back (index);
          }
        }
      }
    }

    for (const std::string &index : interactions) {
      stream << index << setw (10)
             << InteractionParameters_.at (index).getSMATBData ().p << setw (10)
             << InteractionParameters_.at (index).getSMATBData ().q << setw (10)
             << InteractionParameters_.at (index).getSMATBData ().a << setw (10)
             << InteractionParameters_.at (index).getSMATBData ().qsi
             << setw (13)
             << InteractionParameters_.at (index).getIntData ().cutOff_start
             << setw (13)
             << InteractionParameters_.at (index).getIntData ().cutOff_end
             << endl;
    }

    stream << "FCC values:\n";
    stream << "m1m2" << setw (10) << "NN" << endl;
    for (const std::string &index : interactions) {
      stream << index << setw (10) << setw (10)
             << InteractionParameters_.at (index).getIntData ().NNdist << endl;
    }
    stream << std::setprecision (12) << std::fixed;
    stream << "Polynomial elements:\n";
    stream << "m1m2" << setw (18) << "a5" << setw (18) << "a4" << setw (18)

           << "a3" << endl;
    for (const std::string &index : interactions) {
      stream << index << setw (18)
             << InteractionParameters_.at (index).getSMATBData ().P_par5
             << setw (18)
             << InteractionParameters_.at (index).getSMATBData ().P_par4
             << setw (18)
             << InteractionParameters_.at (index).getSMATBData ().P_par3
             << endl;
    }

    stream << "m1m2" << setw (18) << "x5" << setw (18) << "x4" << setw (18)
           << "x3" << endl;
    for (const std::string &index : interactions) {
      stream << index << setw (18)
             << InteractionParameters_.at (index).getSMATBData ().Q_par5
             << setw (18)
             << InteractionParameters_.at (index).getSMATBData ().Q_par4
             << setw (18)
             << InteractionParameters_.at (index).getSMATBData ().Q_par3
             << endl;
    }

    stream << std::resetiosflags (std::ios::floatfield);
  }

  BHdouble Pexp (const BHSMATBInteraction &pars, const BHdouble x) {
    return pars.a * exp (pars.p * (1. - x / pars.NNdist));
  }

  BHdouble Qexp (const BHSMATBInteraction &pars, const BHdouble x) {
    return pars.qsi * exp (pars.q * (1. - x / pars.NNdist));
  }

  BHdouble Qpoly (const BHSMATBInteraction &pars, const BHdouble x) {
    BHdouble Tmyx = x - pars.cutOff_end;
    BHdouble myx2 = Tmyx * Tmyx, myx3 = myx2 * Tmyx;
    return myx3 * (pars.Q_par3 + Tmyx * (pars.Q_par4 + Tmyx * pars.Q_par5));
  }

  BHdouble Ppoly (const BHSMATBInteraction &pars, const BHdouble x) {
    BHdouble Tmyx = x - pars.cutOff_end;
    BHdouble myx2 = Tmyx * Tmyx, myx3 = myx2 * Tmyx;
    return myx3 * (pars.P_par3 + Tmyx * (pars.P_par4 + Tmyx * pars.P_par5));
  }

  void calcPar (BHSMATBInteraction &pars) {
    // 20% tolerance for moves and analysis
    pars.NNdist_tolSQ = pars.NNdist * pars.NNdist * 1.44;

    // then create/update the variables for the polynoms
    BHdouble expp = Pexp (pars, pars.cutOff_start);
    BHdouble dexpp = -pars.p * expp / pars.NNdist;
    BHdouble d2expp = -pars.p * dexpp / pars.NNdist;
    pars.P_par5 = getSwitchingPolyA5 (
      pars.cutOff_start, pars.cutOff_end, expp, dexpp, d2expp);
    pars.P_par4 = getSwitchingPolyA4 (
      pars.cutOff_start, pars.cutOff_end, expp, dexpp, d2expp);
    pars.P_par3 = getSwitchingPolyA3 (
      pars.cutOff_start, pars.cutOff_end, expp, dexpp, d2expp);

    BHdouble expq = Qexp (pars, pars.cutOff_start);
    BHdouble dexpq = -pars.q * expq / pars.NNdist;
    BHdouble d2expq = -pars.q * dexpq / pars.NNdist;

    pars.Q_par5 = getSwitchingPolyA5 (
      pars.cutOff_start, pars.cutOff_end, expq, dexpq, d2expq);
    pars.Q_par4 = getSwitchingPolyA4 (
      pars.cutOff_start, pars.cutOff_end, expq, dexpq, d2expq);
    pars.Q_par3 = getSwitchingPolyA3 (
      pars.cutOff_start, pars.cutOff_end, expq, dexpq, d2expq);
  }

  /*
    BHSMATBInteraction::BHSMATBInteraction () = default;
    BHSMATBInteraction::BHSMATBInteraction (const BHSMATBInteraction &x) =
        default;
    BHSMATBInteraction::BHSMATBInteraction (BHSMATBInteraction &&x) noexcept =
        default;

    BHSMATBInteraction &BHSMATBInteraction::operator= (BHSMATBInteraction x) {
      // this is the big4 and half trick: the input is either move or copy
      // constructed and then is swapped correctly, for this trivial struct is
    an
      // overkill, but i am learning
      if (this != &x) {
        swap (x);
      }
      return *this;
    }

    void BHSMATBInteraction::swap (BHSMATBInteraction &x) {
      std::swap (p, x.p);
      std::swap (q, x.q);
      std::swap (a, x.a);
      std::swap (qsi, x.qsi);
      std::swap (NNdist, x.NNdist);
      std::swap (NNdist_tolSQ, x.NNdist_tolSQ);
      std::swap (cutOff_start, x.cutOff_start);
      std::swap (cutOff_end, x.cutOff_end);
      std::swap (cutOff_endSQ, x.cutOff_endSQ);
      std::swap (P_par5, x.P_par5);
      std::swap (P_par4, x.P_par4);
      std::swap (P_par3, x.P_par3);
      std::swap (Q_par5, x.Q_par5);
      std::swap (Q_par4, x.Q_par4);
      std::swap (Q_par3, x.Q_par3);
    }
  */
  /*
  BHdouble BHSMATBInteraction::Pexp (const BHdouble x) {
    return a * exp (p * (1. - x / NNdist));
  }

  BHdouble BHSMATBInteraction::Qexp (const BHdouble x) {
    return qsi * exp (q * (1. - x / NNdist));
  }

  BHdouble BHSMATBInteraction::Qpoly (const BHdouble x) {
    BHdouble Tmyx = x - cutOff_end;
    BHdouble myx2 = Tmyx * Tmyx, myx3 = myx2 * Tmyx;
    return myx3 * (Q_par3 + Tmyx * (Q_par4 + Tmyx * Q_par5));
  }

  BHdouble BHSMATBInteraction::Ppoly (const BHdouble x) {
    BHdouble Tmyx = x - cutOff_end;
    BHdouble myx2 = Tmyx * Tmyx, myx3 = myx2 * Tmyx;
    return myx3 * (P_par3 + Tmyx * (P_par4 + Tmyx * P_par5));
  }

  void BHSMATBInteraction::calcPar () {
    // 20% tolerance for moves and analysis
    NNdist_tolSQ = NNdist * NNdist * 1.44;

    // then create/update the variables for the polynoms
    BHdouble expp = Pexp (cutOff_start), dexpp = -p * expp / NNdist,
             d2expp = -p * dexpp / NNdist;
    // a5 in BH
    P_par5 = getSwitchingPolyA5 (cutOff_start, cutOff_end, expp, dexpp, d2expp);
    // a4 in BH
    P_par4 = getSwitchingPolyA4 (cutOff_start, cutOff_end, expp, dexpp, d2expp);
    // a3 in BH
    P_par3 = getSwitchingPolyA3 (cutOff_start, cutOff_end, expp, dexpp, d2expp);
    BHdouble expq = Qexp (cutOff_start), dexpq = -q * expq / NNdist,
             d2expq = -q * dexpq / NNdist;

    // x5 in BH
    Q_par5 = getSwitchingPolyA5 (cutOff_start, cutOff_end, expq, dexpq, d2expq);
    // x4 in BH
    Q_par4 = getSwitchingPolyA4 (cutOff_start, cutOff_end, expq, dexpq, d2expq);
    // x3 in BH
    Q_par3 = getSwitchingPolyA3 (cutOff_start, cutOff_end, expq, dexpq, d2expq);
  }
*/
  /*
  void BHInteraction::updateNNdistSAME (BHdouble radius) {
    NNdist = getArete (radius) * densq2;
  }

  void BHInteraction::updateNNdistNOTSAME (BHdouble radius1, BHdouble radius2) {
    NNdist = getArete (radius1, radius2) * densq2;
  }
*/
  void updateNNdistSAME (BHInteraction &interaction, BHdouble radius) {
    interaction.NNdist = getArete (radius) * densq2;
  }

  void updateNNdistNOTSAME (
    BHInteraction &interaction, BHdouble radius1, BHdouble radius2) {
    interaction.NNdist = getArete (radius1, radius2) * densq2;
  }

  void BHMetalParameters::setInteractionLabels (BHCluster &cluster) const {
    const unsigned int NofAtoms = cluster.getNofAtoms ();
    for (unsigned int i = 0; i < NofAtoms; i++) {
      for (unsigned int j = i + 1; j < NofAtoms; j++) {
        cluster.setINTlab (
          i, j, Plabels.at (cluster[i].Type () + cluster[j].Type ()));
        cluster.setSAMElab (
          i, j, (cluster[i].Type () == cluster[j].Type ()) ? 0 : 1);
      }
    }
  }
  /*
    BHLJInteraction::BHLJInteraction () = default;
    BHLJInteraction::BHLJInteraction (const BHLJInteraction &x) = default;
    BHLJInteraction::BHLJInteraction (BHLJInteraction &&x) noexcept = default;
    BHLJInteraction &BHLJInteraction::operator= (BHLJInteraction x) {
      if (this != &x) {
        swap (x);
      }
      return *this;
    }

    void BHLJInteraction::swap (BHLJInteraction &x) {
      std::swap (epsilon, x.epsilon);
      std::swap (sigma, x.sigma);
      std::swap (NNdist, x.NNdist);
      std::swap (NNdist_tolSQ, x.NNdist_tolSQ);
      std::swap (cutOff_start, x.cutOff_start);
      std::swap (cutOff_end, x.cutOff_end);
      std::swap (cutOff_endSQ, x.cutOff_endSQ);
    }
  */
  /// no tolerance is used here for NN
  void BHMetalParameters::calcNeighbourhood (BHCluster &cluster) const {
    ///@todo move this in another class, in a more time efficient fashion
    cluster.clear_NN ();
    for (unsigned int i = 0; i < cluster.getNofAtoms (); i++) {
      for (unsigned int j = i + 1; j < cluster.getNofAtoms (); j++) {
        std::string index = cluster[i].Type () + cluster[j].Type ();
        BHdouble d = cluster[i].dist2 (cluster[j]);
        if (d < InteractionParameters_.at (index).getIntData ().cutOff_endSQ) {
          cluster.addINT (i, j);
          cluster.addINT (j, i);
          if (
            d < InteractionParameters_.at (index).getIntData ().NNdist *
                  InteractionParameters_.at (index).getIntData ().NNdist) {
            cluster.addNNcouple (j, i);
            if (cluster[i].Type () == cluster[j].Type ()) {
              cluster.addNNcouple_same (j, i);
            } else {
              cluster.addNNcouple_diff (j, i);
            }
          }
        }
      }
    }
    cluster.shrink_NN_to_fit (); // this is for allocating the right amount of
                                 // memory
    /*
            #ifdef METALPARDEBUG
            for(int i = 0;i<cluster.NofAtoms();i++){
            cout << "Atom " << i;
            cout << endl << "NN:\t\t";
            for(const int& k:neighbourhood.getNNs(i))
            cout <<" "<< k;
            cout << endl << "NN_same:\t";
            for(const int& k:neighbourhood.getNNs_same(i))
            cout <<" "<< k;
            cout << endl << "NN_diff: \t";
            for(const int& k:neighbourhood.getNNs_diff(i))
            cout <<" "<< k;
            cout << endl << "INT:\t\t";
            for(const int& k:neighbourhood.getINT(i))
            cout <<" "<< k;
            cout << endl;
            }
            #endif//METALPARDEBUG
          */
  }

  /// computes only the neighbours
  void BHMetalParameters::calcNeighINT (BHCluster &cluster) const {
    ///@todo move this in another class, in a more time efficient fashion
    cluster.clear_NN ();
    for (unsigned int i = 0; i < cluster.getNofAtoms (); i++) {
      for (unsigned int j = i + 1; j < cluster.getNofAtoms (); j++) {
        std::string index = cluster[i].Type () + cluster[j].Type ();
        BHdouble d = cluster[i].dist2 (cluster[j]);
        // cout << i << " " <<cluster[i] <<" "<< j << " "<<cluster[i]<<" " << d
        // <<endl;
        if (d < InteractionParameters_.at (index).getIntData ().cutOff_endSQ) {
          cluster.addINT (i, j);
          cluster.addINT (j, i);
        }
      }
    }
    cluster.shrink_NN_to_fit (); // this is for allocating the right amount of
                                 // memory
  }

  void BHMetalParameters::calcNeighbourhood_tol (BHCluster &cluster) const {
    ///@todo move this in another class, in a more time efficient fashion
    cluster.clear_NN ();
    for (unsigned int i = 0; i < cluster.getNofAtoms (); i++) {
      for (unsigned int j = i + 1; j < cluster.getNofAtoms (); j++) {
        std::string index = cluster[i].Type () + cluster[j].Type ();
        BHdouble d = cluster[i].dist2 (cluster[j]);
        if (d < InteractionParameters_.at (index).getIntData ().NNdist_tolSQ) {
          cluster.addNNcouple (j, i);
          if (cluster[i].Type () == cluster[j].Type ()) {
            cluster.addNNcouple_same (j, i);
          } else {
            cluster.addNNcouple_diff (j, i);
          }
        }
      }
    }
    // cluster.shrink_NN_to_fit();//this is for allocating the right amount of
    // memory
    /*
            #ifdef METALPARDEBUG
            for(int i = 0;i<NofAtoms_;i++){
            cout << "Atom " << i+1;
            cout << endl << "NN:"<<NNlist[i].size()<<"\t\t";
            for(const int& k:NNlist[i])
            cout <<" "<< k+1;
            cout << endl << "NN_same:"<<NNlist_same[i].size()<<"\t";
            for(const int& k:NNlist_same[i])
            cout <<" "<< k+1;
            cout << endl << "NN_diff:"<<NNlist_diff[i].size()<<"\t";
            for(const int& k:NNlist_diff[i])
            cout <<" "<< k+1;
            cout << endl << "INT:"<<INTlist[i].size()<<"\t\t";
            for(const int& k:INTlist[i])
            cout <<" "<< k+1;
            cout << endl;
            }
            #endif//METALPARDEBUG
          */
  }
  unsigned BHMetalParameters::getNofMetals () const {
    // return Nmetals;
    return static_cast<unsigned> (AtomParameters_.size ());
  }
  unsigned BHMetalParameters::getNofInteractions () const {
    // return Ninteractions;
    return static_cast<unsigned> (Pname.size ());
    // return SMATBinteractions_.size () + LJ12_6interactions_.size ();
  }
  /// look if an atom is i
  bool BHMetalParameters::IknowAbout (const std::string &Atom_name) const {
    return Slabels.find (Atom_name) != Slabels.end ();
  }
  std::string BHMetalParameters::getAtomName (const unsigned id) const {
    return Sname.at (id);
  }
  std::string BHMetalParameters::getPairName (const unsigned id) const {
    return Pname.at (id);
  }
  unsigned BHMetalParameters::getPlabels (const std::string &Atoms_pair) const {
    return Plabels.at (Atoms_pair);
  }
  unsigned
  BHMetalParameters::getPlabels (const unsigned i, const unsigned j) const {
    return Plabels.at (Sname.at (i) + Sname.at (j));
  }
  unsigned BHMetalParameters::getSlabels (const std::string &Atom) const {
    return Slabels.at (Atom);
  }
  unsigned BHMetalParameters::getSlabels (const unsigned i) const {
    return Slabels.at (Sname.at (i));
  }
  BHdouble BHMetalParameters::getNNdist (const unsigned codePair) const {
    return getNNdist (Pname.at (codePair));
  }
  BHdouble BHMetalParameters::getNNdist (const std::string &Atoms_pair) const {
    return InteractionParameters_.at (Atoms_pair).getIntData ().NNdist;
  }
  BHdouble BHMetalParameters::getD2tol (const unsigned codePair) const {
    return getD2tol (Pname.at (codePair));
  }
  BHdouble BHMetalParameters::getD2tol (const std::string &Atoms_pair) const {
    return InteractionParameters_.at (Atoms_pair).getIntData ().NNdist_tolSQ;
  }
  BHdouble BHMetalParameters::getCutOffEnd (const unsigned codePair) const {
    return getCutOffEnd (Pname.at (codePair));
  }
  BHdouble
  BHMetalParameters::getCutOffEnd (const std::string &Atoms_pair) const {
    return InteractionParameters_.at (Atoms_pair).getIntData ().cutOff_end;
  }
  BHdouble BHMetalParameters::getMass (const std::string &Atom_name) const {
    return AtomParameters_.at (Atom_name).Mass;
  }
  BHdouble BHMetalParameters::getRadius (const std::string &Atom_name) const {
    return AtomParameters_.at (Atom_name).Radius;
  }

  void BHMetalParameters::giveCohRadiusMass (
    const unsigned index,
    BHdouble &gCoh,
    BHdouble &gRadius,
    BHdouble &gMass) const {
    giveCohRadiusMass (Sname.at (index), gCoh, gRadius, gMass);
  }

  void BHMetalParameters::giveMetalPars (
    const unsigned index,
    BHdouble &gp,
    BHdouble &gq,
    BHdouble &ga,
    BHdouble &gqsi,
    BHdouble &gNN,
    BHdouble &gcs,
    BHdouble &gce) const {
    giveMetalPars (Pname.at (index), gp, gq, ga, gqsi, gNN, gcs, gce);
  }

  void BHMetalParameters::givePolyPars (
    const unsigned index,
    BHdouble &gP5,
    BHdouble &gP4,
    BHdouble &gP3,
    BHdouble &gQ5,
    BHdouble &gQ4,
    BHdouble &gQ3) const {
    givePolyPars (Pname.at (index), gP5, gP4, gP3, gQ5, gQ4, gQ3);
  }

  void BHMetalParameters::giveCohRadiusMass (
    const std::string &index,
    BHdouble &gCoh,
    BHdouble &gRadius,
    BHdouble &gMass) const {
    try {
      gCoh = AtomParameters_.at (index).Coh;
      gRadius = AtomParameters_.at (index).Radius;
      gMass = AtomParameters_.at (index).Mass;
    } catch (std::out_of_range & /**/) {
      throw "BHMP::Cannot find atom parameters for \"" + index + "\"";
    }
  }

  void BHMetalParameters::giveMetalPars (
    const std::string &index,
    BHdouble &gp,
    BHdouble &gq,
    BHdouble &ga,
    BHdouble &gqsi,
    BHdouble &gNN,
    BHdouble &gcs,
    BHdouble &gce) const {
    try {
      gp = InteractionParameters_.at (index).getSMATBData ().p;
      gq = InteractionParameters_.at (index).getSMATBData ().q;
      ga = InteractionParameters_.at (index).getSMATBData ().a;
      gqsi = InteractionParameters_.at (index).getSMATBData ().qsi;
      gNN = InteractionParameters_.at (index).getSMATBData ().NNdist;
      gcs = InteractionParameters_.at (index).getSMATBData ().cutOff_start;
      gce = InteractionParameters_.at (index).getSMATBData ().cutOff_end;
    } catch (std::out_of_range & /**/) {
      throw "BHMP::Cannot find SMATB interaction parameters for \"" + index +
        "\"";
    }
  }

  void BHMetalParameters::givePolyPars (
    const std::string &index,
    BHdouble &gP5,
    BHdouble &gP4,
    BHdouble &gP3,
    BHdouble &gQ5,
    BHdouble &gQ4,
    BHdouble &gQ3) const {
    try {
      gP5 = InteractionParameters_.at (index).getSMATBData ().P_par5;
      gP4 = InteractionParameters_.at (index).getSMATBData ().P_par4;
      gP3 = InteractionParameters_.at (index).getSMATBData ().P_par3;
      gQ5 = InteractionParameters_.at (index).getSMATBData ().Q_par5;
      gQ4 = InteractionParameters_.at (index).getSMATBData ().Q_par4;
      gQ3 = InteractionParameters_.at (index).getSMATBData ().Q_par3;
    } catch (std::out_of_range & /**/) {
      throw "BHMP::Cannot find SMATB interaction parameters for \"" + index +
        "\"";
    }
  }

  const BHSMATBInteraction &BHMetalParameters::giveSMATBInteractionParameters (
    const unsigned index) const {
    return giveSMATBInteractionParameters (Sname.at (index));
  }

  const BHSMATBInteraction &BHMetalParameters::giveSMATBInteractionParameters (
    const std::string &index) const {
    try {
      return InteractionParameters_.at (index).getSMATBData ();
    } catch (std::out_of_range & /**/) {
      throw "BHMP::Cannot find SMATB interaction parameters for \"" + index +
        "\"";
    }
  }

  const BHLJInteraction &
  BHMetalParameters::giveLJInteractionParameters (const unsigned index) const {
    return giveLJInteractionParameters (Sname.at (index));
  }

  const BHLJInteraction &BHMetalParameters::giveLJInteractionParameters (
    const std::string &index) const {
    try {
      return InteractionParameters_.at (index).getLJData ();
    } catch (std::out_of_range & /**/) {
      throw "BHMP::Cannot find LJ interaction parameters for \"" + index + "\"";
    }
  }

  BHSMATBInteraction &
  BHMetalParameters::giveSMATBInteractionParameters (const unsigned index) {
    return giveSMATBInteractionParameters (Sname.at (index));
  }

  BHSMATBInteraction &
  BHMetalParameters::giveSMATBInteractionParameters (const std::string &index) {
    try {
      return InteractionParameters_.at (index).getSMATBData ();
    } catch (std::out_of_range & /**/) {
      throw "BHMP::Cannot find SMATB interaction parameters for \"" + index +
        "\"";
    }
  }

  BHLJInteraction &
  BHMetalParameters::giveLJInteractionParameters (const unsigned index) {
    return giveLJInteractionParameters (Sname.at (index));
  }

  BHLJInteraction &
  BHMetalParameters::giveLJInteractionParameters (const std::string &index) {
    try {
      return InteractionParameters_.at (index).getLJData ();
    } catch (std::out_of_range & /**/) {
      throw "BHMP::Cannot find LJ interaction parameters for \"" + index + "\"";
    }
  }

  BHMetalParameters::BHInteractionStorer::BHInteractionStorer () = default;

  BHMetalParameters::BHInteractionStorer::BHInteractionStorer (
    const BHLJInteraction &ljINT)
    : kind (Interactionkind::LJ),
      intData_ (new BHLJInteraction (ljINT)) {}

  BHMetalParameters::BHInteractionStorer::BHInteractionStorer (
    const BHSMATBInteraction &smatbINT)
    : kind (Interactionkind::SMATB),
      intData_ (new BHSMATBInteraction (smatbINT)) {}

  BHMetalParameters::BHInteractionStorer::BHInteractionStorer (
    const BHInteractionStorer &other)
    : kind{other.kind},
      intData_{[&other] () -> BHInteraction * {
        BHInteraction *ptr = nullptr;
        switch (other.kind) {
        case Interactionkind::LJ:
          ptr = new BHLJInteraction (other.getLJData ());
          break;
        case Interactionkind::SMATB:
          [[fallthrough]];
        case Interactionkind::DEFAULTSMATB:
          ptr = new BHSMATBInteraction (other.getSMATBData ());
        }

        return ptr;
      }()} {}

  BHMetalParameters::BHInteractionStorer::BHInteractionStorer (
    BHInteractionStorer &&other) noexcept
    : kind (other.kind),
      intData_ (std::move (other.intData_)) {}

  BHMetalParameters::BHInteractionStorer &
  BHMetalParameters::BHInteractionStorer::operator= (
    BHInteractionStorer other) {
    swap (other);
    return *this;
  }

  void
  BHMetalParameters::BHInteractionStorer::swap (BHInteractionStorer &other) {
    std::swap (kind, other.kind);
    intData_.swap (other.intData_);
  }

  BHInteraction &BHMetalParameters::BHInteractionStorer::getIntData () {
    return *intData_;
  }

  BHSMATBInteraction &BHMetalParameters::BHInteractionStorer::getSMATBData () {
    if (kind != Interactionkind::SMATB)
      throw "Requested data is not of the SMATB kind";
    return *static_cast<BHSMATBInteraction *> (intData_.get ());
  }

  BHLJInteraction &BHMetalParameters::BHInteractionStorer::getLJData () {
    if (kind != Interactionkind::LJ)
      throw "Requested data is not of the LJ kind";
    return *static_cast<BHLJInteraction *> (intData_.get ());
  }

  const BHInteraction &
  BHMetalParameters::BHInteractionStorer::getIntData () const {
    return *intData_;
  }

  const BHSMATBInteraction &
  BHMetalParameters::BHInteractionStorer::getSMATBData () const {
    if (kind != Interactionkind::SMATB)
      throw "Requested data is not of the SMATB kind";
    return *static_cast<BHSMATBInteraction *> (intData_.get ());
  }

  const BHLJInteraction &
  BHMetalParameters::BHInteractionStorer::getLJData () const {
    if (kind != Interactionkind::LJ)
      throw "Requested data is not of the LJ kind";
    return *static_cast<BHLJInteraction *> (intData_.get ());
  }
} // namespace BH
