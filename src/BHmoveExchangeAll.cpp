/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#include "BHmoveExchangeAll.hpp"
namespace BH {

  BHMoveExchangeAll::BHMoveExchangeAll ()
    : BHMoveExchange ("exchangeAll"),
      AtomSelectors_ () {}

  BHMoveExchangeAll::~BHMoveExchangeAll () = default;

  std::string BHMoveExchangeAll::DefaultString () {
    return "prob = 1.0, accTemp = 100";
  }

  std::unique_ptr<BHMove> BHMoveExchangeAll::clone () {
    return std::unique_ptr<BHMove>{new BHMoveExchangeAll (*this)};
  }

  bool BHMoveExchangeAll::selectAtoms (
    BHCluster &,
    BHClusterAnalyser &,
    const BHMetalParameters &,
    rndEngine &rng) {
    chosenAtom1_ =
      static_cast<unsigned> (AtomSelectors_[kindToExchange1_](rng));
    chosenAtom2_ =
      static_cast<unsigned> (AtomSelectors_[kindToExchange2_](rng));
    return true;
  }

  void BHMoveExchangeAll::SpecializeExchange (const BHCluster &) {
    for (size_t i = 0; i < NofSpecies_; ++i) {
      AtomSelectors_.emplace_back (
        firstAtomperSpecie_[i], lastAtomperSpecie_[i]);
    }
    for (size_t i = 0; i < NofSpecies_; ++i) {
      if (!(unsigned (AtomSelectors_[i].a ()) == firstAtomperSpecie_[i] &&
            unsigned (AtomSelectors_[i].b ()) == lastAtomperSpecie_[i]))
        throw std::string ("Daniele has got something wrong in these lists: ")
          .append (std::string (__FILE__))
          .append (" ")
          .append (std::to_string (__LINE__));
    }
  }
} // namespace BH
