/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Contains main ad the definitio of BHmover

   This program is made to act as an interface to <a
   href="http://www.quantum-espresso.org/">QUANTUM ESPRESSO</a>
   @file qeInterface.cxx
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 21/3/2017
   @version 1.0
   @todo add a way to understand if the actual step has altready been explored
*/
#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>

#include "BHconstructors.hpp"
#include "BHmetalParameters.hpp"
#include "BHworkWithFortran.hpp"

#include "BHmover.hpp"

using namespace std;
using namespace BH;

void onlymove (char **argv, const BHMetalParameters &bhmp);
void Headers (const BHMetalParameters &bhmp);

int main (int argc, char **argv) {
  BH::BHMetalParameters bhmp ("MetalParameters.in", true);
  try {
    // for(int i=0;i<argc;++i) {cerr << i << "\t" << argv[i] <<endl;};
    switch (argc) {
    case 3: {
      if (string (argv[1]) == "--start") {
        Reorder (argv[2], bhmp);
        Headers (bhmp);
        return 0;
      }
      onlymove (argv, bhmp);
      // BHWWF::BHInterface_CleanMemory();
      return 0;
    }
    case 7:
      [[fallthrough]];
    case 8: {
      BHdouble latestEnergy = stod (argv[4]); // stringtodouble
      BHdouble newEnergy = stod (argv[5]);    // stringtodouble
      BHdouble TAccept = stod (argv[6]);      // stringtodouble
      BHEnums::energyType Etype = BHEnums::eV;
      if (string (argv[7]) == "Ry") {
        Etype = BHEnums::Ry;
      } else if (string (argv[7]) == "Ht") {
        Etype = BHEnums::Ht;
      }
      return MonteCarlo (
        argv[1], argv[2], argv[3], bhmp, latestEnergy, newEnergy, TAccept,
        Etype);
    } /*
     case 7: { // legacy version for compatibility with old scripts
       BHdouble latestEnergy = stod (argv[4]); // stringtodouble
       BHdouble newEnergy = stod (argv[5]);    // stringtodouble
       BHdouble TAccept = stod (argv[6]);      // stringtodouble
       return MonteCarlo (argv[1], argv[2], argv[3], latestEnergy, newEnergy,
                          TAccept, BHEnums::Ry);
     }*/
    case 2:
      if (string (argv[1]) == "--start") {
        Headers (bhmp);
        return 0;
      }
      [[fallthrough]];
    case 4: {
      if (string (argv[1]) == "--Ry") {
        EnergyConverter (argv[2], stod (argv[3]), bhmp, BHEnums::Ry);
        return 0;
      }
      if (string (argv[1]) == "--Ht") {
        cerr << "Ht" << endl;
        EnergyConverter (argv[2], stod (argv[3]), bhmp, BHEnums::Ht);
        return 0;
      }
      if (string (argv[1]) == "--eV") {
        cerr << "eV" << endl;
        EnergyConverter (argv[2], stod (argv[3]), bhmp, BHEnums::eV);
        return 0;
      }
    }
      [[fallthrough]];
    default:
      cout << " ______  _     _                             \n"
           << "(____  \\| |   | |                            \n"
           << " ____)  ) |__ | |____   ___ _   _ ____  ____ \n"
           << "|  __  (|  __)| |    \\ / _ \\ | | / _  )/ ___)\n"
           << "| |__)  ) |   | | | | | |_| \\ V ( (/ /| |    \n"
           << "|______/|_|   |_|_|_|_|\\___/ \\_/ \\____)_|    \n"
           << "                                             \n";
      std::cerr << "Libraries: " << BHPP_LIBRARIES_MAJOR << "."
                << BHPP_LIBRARIES_MINOR << "." << BHPP_LIBRARIES_PATCHLEVEL
                << std::endl;
      cerr
        << "You need to have in the work directory two files: 'mover.in'\n"
           "with the walker settings and 'MetalParameters.in' with the data\n"
           "of the metal you are using.\n"
        << "Usage: two modes that switches by the number of arguments:\n\n"
        << "*Use as mover: input_file.xyz output_file.xyz\n"
        << "\tperforms a move by the configuration present in\n"
           "\tinput_file.xyz and outputs it in output_file.xyz\n\n"
        << "*Use as an updater:\n"
        << "\"--Ry input_file.xyz EnergyInRy\"\n"
        << "\t*--Ry:conversion to Ev from Ry\n"
        << "\t*--Ht:conversion to Ev from Ht\n"
        << "\t*input_file.xyz: data file: caluclates OP and updates the "
           "header\n"
        << "\t*EnergyInRy: the energy to update\n\n"
        << "*Use as BasinHopping engine:\n"
           "\"last_accepted.xyz new_move.xyz output.xyz latestEnergy "
           "newEnergy TMC Ht/Ry\"\n"
        << "\t*last_accepted.xyz\tfile with the last accepted move, uses "
           "this as a\n\t\tbase for the next move if last move is rejeted\n"
        << "\t*new_move.xyz\t\tfile with the last minimized move, uses this "
           "as\n\t\ta base for the next move if last move is accepted\n"
        << "\t*output.xyz\t\tfile in which the configuration of next move "
           "\n\t\twill be stored\n"
        << "\t*latestEnergy\t\tthe last accepted energy\n"
        << "\t*newEnergy\t\tthe energy of the new move\n"
        << "\t*TMC\t\t\tthe Monte Carlo's temperature\n"
        << "\t*Ht/Ry\t\t\twrite \"Ht\" or \"Ry\" to chose in which units\n"
           "\t\tyou are giving the energy to the routine,\n\t\tleave blank "
           "if you "
           "are using eV"
        << endl
        << "*Kickstart the files for the Basin Hopping:\n"
        << "\"--start\"\n"
        << "\t\tThe program will overwrite the the files "
           "ener_accepted.out,\n\t\tener_all.out and moves.out\n\t\tand "
           "replace them with the headers for the colums"
        << endl;
      //<< "You used " << argc << " arguments." << endl;
      return 10;
    }
  } catch (const char *problem) {
    cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m" << endl;
    // BHWWF::BHInterface_CleanMemory();
    return -1;
  } catch (const string &problem) {
    cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m" << endl;
    return -1;
  }
  // BHWWF::BHInterface_CleanMemory();
  return 0;
}

void Headers (const BH::BHMetalParameters &bhmp) {
  ofstream all ("ener_all.out");
  std::vector<BH::BHOrderParameter> OPnames = {
    BH::BHOrderParameter ("mix"),
    BH::BHOrderParameter (bhmp.getAtomName (0) + bhmp.getAtomName (0))};
  all << "file_name\t" << BHClusterData::ClusterData_list (OPnames)
      << "\tlast_acp_E\tdeltaE\t\tMC_expo" << endl;
  all.close ();

  ofstream best ("ener_accepted.out");
  best << "file_name\t" << BHClusterData::ClusterData_list (OPnames) << endl;
  best.close ();

  ofstream fmoves ("moves.out");
  fmoves << "file_name\t" << BHWalker::lastMoveInfo_legenda () << endl;
  fmoves.close ();
}

void onlymove (char **argv, const BH::BHMetalParameters &bhmp) {
  BHWalkerSettings walkersettings = loadBHWalkerSettings ("mover.in");
  BH::BHOrderParameter OPnames[2] = {
    BH::BHOrderParameter ("mix"),
    BH::BHOrderParameter (bhmp.getAtomName (0) + bhmp.getAtomName (0))};
  ///@todo plot with analysis
  unsigned RNG_seed = static_cast<unsigned> (
    std::chrono::system_clock::now ().time_since_epoch ().count ());
  rndEngine my_rnd_engine (RNG_seed);
  // cout << "Da " <<argv[1] << " a " <<argv[2]<<endl;
  BHMover do_move ({argv[1]}, {argv[1]}, walkersettings, bhmp);
  // cout << do_move <<endl;
  // cout << "Mossa"<<endl;
  // the most generic ops
  std::vector<BHOrderParameter> OPs = {"421", "555"};
  BHClusterAnalyser Analyser (OPs, bhmp);
  do_move.doMove (Analyser, bhmp, my_rnd_engine);
  // cout << do_move <<endl;
  ofstream f (argv[2]);
  f << do_move.lastMoveConf () << endl;
  f.close ();
}
