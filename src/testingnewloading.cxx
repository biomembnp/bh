/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#include <iostream>

#include "BHclusterAtoms.hpp"
#include "BHtiming.hpp"
using namespace BH;
int main (int, char **) {
  std::cout << "Let's test how fast is my new method for assigning data "
               "from/to the cluster\n";
  BHClusterAtoms test ("../startingpoints/ag1289to.xyz");
  BHdouble *x = new BHdouble[3 * test.getNofAtoms ()];
  test.AssignToVector (x);
  ms timeOld = std::chrono::steady_clock::duration::zero ();
  ms timeMid = std::chrono::steady_clock::duration::zero ();
  ms timeMid2 = std::chrono::steady_clock::duration::zero ();
  ms timeNew = std::chrono::steady_clock::duration::zero ();
  ms timeCopyOld = std::chrono::steady_clock::duration::zero ();
  // ms timeMid=std::chrono::steady_clock::duration::zero();
  // ms timeMid2=std::chrono::steady_clock::duration::zero();
  ms timeCopyNew = std::chrono::steady_clock::duration::zero ();
  tp start, stop;
  for (int var = 0; var < 1000000; ++var) {
    start = std::chrono::steady_clock::now ();
    for (unsigned int i = 0; i < test.getNofAtoms (); ++i) {
      x[i * 3] = test[i].X ();
      x[i * 3 + 1] = test[i].Y ();
      x[i * 3 + 2] = test[i].Z ();
    }
    stop = std::chrono::steady_clock::now ();
    timeCopyOld += stop - start;

    start = std::chrono::steady_clock::now ();
    test.AssignToVector (x);
    stop = std::chrono::steady_clock::now ();
    timeCopyNew += stop - start;

    start = std::chrono::steady_clock::now ();
    for (unsigned int i = 0; i < test.getNofAtoms (); ++i) {
      test[i].X (x[i * 3]);
      test[i].Y (x[i * 3 + 1]);
      test[i].Z (x[i * 3 + 2]);
    }
    stop = std::chrono::steady_clock::now ();
    timeOld += stop - start;

    start = std::chrono::steady_clock::now ();
    for (unsigned int i = 0; i < test.getNofAtoms (); ++i) {
      test[i].setXYZ (x[i * 3], x[i * 3 + 1], x[i * 3 + 2]);
    }
    stop = std::chrono::steady_clock::now ();
    timeMid += stop - start;

    start = std::chrono::steady_clock::now ();
    for (unsigned int i = 0; i < test.getNofAtoms (); ++i) {
      test[i].setXYZ (x + i * 3);
    }
    stop = std::chrono::steady_clock::now ();
    timeMid2 += stop - start;

    start = std::chrono::steady_clock::now ();
    test.AssignFromVector (x);
    stop = std::chrono::steady_clock::now ();
    timeNew += stop - start;
  }
  std::cout << "Copying to a vector:";
  std::cout << "\nNew time: ";
  timing (std::cout, timeCopyNew);
  std::cout << "\nOld time: ";
  timing (std::cout, timeCopyOld);
  std::cout << "\n\n\nCopying from a vector:";
  std::cout << "\nNew time: ";
  timing (std::cout, timeNew);
  std::cout << "\nMid time: ";
  timing (std::cout, timeMid);
  std::cout << "\nMid2 time: ";
  timing (std::cout, timeMid2);
  std::cout << "\nOld time: ";
  timing (std::cout, timeOld);
  std::cout << "\n";
  delete[] x;
  return 0;
}
