/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#include "BHwalker.hpp"
#include "BHclusterUtilities.hpp"
#include "BHmetalParameters.hpp"

namespace BH {
  // everithing-ish is defined as standard values in the hpp file
  BHWalker::BHWalker () = default;

  BHWalker::BHWalker (
    const BHWalkerSettings &walkersettings,
    const BHClusterAtoms &seed,
    const BHMetalParameters &bhmp,
    BHWalkerAlgorithm algorithm,
    const bool standaloneAlgorithm,
    const bool skipStep)
    : WorkingConf_{seed},
      LastAcceptedConf_{seed},
      walkerSettings_{walkersettings},
      Algorithm_{algorithm},
      Metropolis_{0., 1.},
      moves_{createListOfSpecializedMoves (
        walkersettings.MovesStrings_, seed, bhmp)},
      rnd_move (BHWalkerSupport::MoveDistributionGenerator (moves_).param ()),
      ignoreSteps_{skipStep},
      isStandalone_{standaloneAlgorithm} {
    ///@todo rememer to add this somewhere!
    /*
    // look if there are any atoms that are not stored in BHMetalParameters
    for (unsigned int i = 0; i < WorkingConf_.getNofAtoms (); i++) {
      if (!bhmp.IknowAbout (WorkingConf_[i].Type ()))
        throw "BHwalker: cluster atoms type are not mapped in the settings";
    }*/
    {
      unsigned int ind = 0;
      std::string st = WorkingConf_[0].Type ();
      // this funtion determines the first atom of each type, and if they are
      // ordered
      for (unsigned int i = 1; i < WorkingConf_.getNofAtoms (); i++) {
        if (WorkingConf_[i].Type () != st) {
          if (ind >= WorkingConf_.NofSpecies ()) {
            throw "Atoms are not ordered";
          }
          st = WorkingConf_[i].Type ();
        }
      }
      ind = 0;
      st = LastAcceptedConf_[0].Type ();
      // this funtion determines the first atom of each type, and if they are
      // ordered
      for (unsigned int i = 1; i < WorkingConf_.getNofAtoms (); i++) {
        if (LastAcceptedConf_[i].Type () != st) {
          if (ind >= LastAcceptedConf_.NofSpecies ()) {
            throw "Atoms are not ordered";
          }
          st = LastAcceptedConf_[i].Type ();
        }
      }
    }
    reorderAndInitializeLabels (bhmp);
  }

  BHWalker::BHWalker (const BHWalker &other)
    : WorkingConf_ (other.WorkingConf_),
      LastAcceptedConf_ (other.LastAcceptedConf_),
      lastAcceptedData_ (other.lastAcceptedData_),
      lastMoveData_ (other.lastMoveData_),
      walkerSettings_ (other.walkerSettings_),
      Algorithm_ (other.Algorithm_),
      Metropolis_ (0., 1.),
      TAccept_ (other.TAccept_),
      rnd_move (other.rnd_move),
      chosenMove_ (other.chosenMove_),
      ignoreSteps_ (other.ignoreSteps_),
      isStandalone_ (other.isStandalone_),
      isReorderedAndInitialized_ (other.isReorderedAndInitialized_) {
    copyMovesFrom (other);
  }

  BHWalker::BHWalker (BHWalker &&other) noexcept
    : WorkingConf_ (std::move (other.WorkingConf_)),
      LastAcceptedConf_ (other.LastAcceptedConf_),
      lastAcceptedData_ (other.lastAcceptedData_),
      lastMoveData_ (other.lastMoveData_),
      walkerSettings_ (std::move (other.walkerSettings_)),
      Algorithm_ (other.Algorithm_),
      Metropolis_ (0., 1.),
      moves_ (std::move (other.moves_)),
      TAccept_ (other.TAccept_),
      rnd_move (other.rnd_move),
      chosenMove_ (other.chosenMove_),
      ignoreSteps_ (other.ignoreSteps_),
      isStandalone_ (other.isStandalone_),
      isReorderedAndInitialized_ (other.isReorderedAndInitialized_) {}
  /*
    BHWalker &BHWalker::operator= (const BHWalker &other) {
      if (this != &other) {
        WorkingConf_ = other.WorkingConf_;
        LastAcceptedConf_ = other.LastAcceptedConf_;
        lastAcceptedData_ = other.lastAcceptedData_;
        lastMoveData_ = other.lastMoveData_;
        walkerSettings_ = other.walkerSettings_;
        Algorithm_ = other.Algorithm_;
        TAccept_ = other.TAccept_;
        rnd_move.param (other.rnd_move.param ());
        chosenMove_ = other.chosenMove_;
        isStandalone_ = other.isStandalone_;
        ignoreSteps_ = other.ignoreSteps_;
        isReorderedAndInitialized_ = other.isReorderedAndInitialized_;
        copyMovesFrom (other);
      }
      return *this;
    }

    BHWalker &BHWalker::operator= (BHWalker &&other) noexcept {
      if (this != &other) {
        WorkingConf_ = std::move (other.WorkingConf_);
        LastAcceptedConf_ = std::move (other.LastAcceptedConf_);
        lastAcceptedData_ = other.lastAcceptedData_;
        lastMoveData_ = other.lastMoveData_;
        walkerSettings_ = std::move (other.walkerSettings_);
        Algorithm_ = other.Algorithm_;
        moves_ = std::move (other.moves_);
        TAccept_ = other.TAccept_;
        rnd_move.param (other.rnd_move.param ());
        chosenMove_ = other.chosenMove_;
        isStandalone_ = other.isStandalone_;
        ignoreSteps_ = other.ignoreSteps_;
        isReorderedAndInitialized_ = other.isReorderedAndInitialized_;
      }
      return *this;
    }
  */

  BHWalker::~BHWalker () = default;

  void BHWalker::reorderAndInitializeLabels (const BHMetalParameters &bhmp) {
    if (!isReorderedAndInitialized_) {
      workingConfAnalysed_ = false;
      BHClusterUtilities::MassesReorder (WorkingConf_, bhmp);
      WorkingConf_.updateComposition ();
      bhmp.setInteractionLabels (WorkingConf_);
      // In this way all of the Interaction labels are moved in the accepted
      // configuration with the correct ordering required by the echange moves
      LastAcceptedConf_ = WorkingConf_;
    }
  }

  /// return a string with the Temperature and the name of the move
  std::string BHWalker::lastMoveInfo () const {
    return std::to_string (TAccept_) + "\t" + moves_[chosenMove_]->name ();
  }

  bool BHWalker::IsLastMoveEschange () const {
    return moves_[chosenMove_]->isExchange ();
  }

  std::string BHWalker::lastMoveInfo_legenda () { return "TAccept\tmove_name"; }

  void BHWalker::movesCount (std::ostream &stream) const {
    stream << "Total moves: " << n_moves << std::endl;
    stream << "name\t\tnUsed\tNaccepted" << std::endl;
    for (const auto &move : moves_) {
      stream << move->name () << ":\t" << move->getMoveCount () << "\t"
             << move->getAcceptedCount () << std::endl;
    }
  }

  BHdouble BHWalker::doMove (
    BHClusterAnalyser &Analyzer,
    const BHMetalParameters &bhmp,
    rndEngine &rng) {
    ++n_moves;
    start = std::chrono::steady_clock::now ();
    // BHClusterAtoms::operator<<(LastAcceptedConf_);//so i know that the temp
    // conf is the last accepted conf
    // int numberoftry = 0;
    // do {
    chosenMove_ = rnd_move (rng); // explicit conversion from int
    TAccept_ = moves_[chosenMove_]->doMove (
      LastAcceptedConf_, WorkingConf_, Analyzer, bhmp, rng);
    //++numberoftry;
    //} while (TAccept < 0 && numberoftry < 11);
    // std::cout << moves_[chosenMove_]->name()<<std::endl;
    /*
    std::ofstream of("aftermove"+std::to_string(n_moves)+".xyz");
    of << *(this) << std::endl;
    of << BHClusterAtoms(LastAcceptedConf_) <<std::endl;*/
    stop = std::chrono::steady_clock::now ();
    move_time += stop - start;
    workingConfAnalysed_ = false;
    return TAccept_;
  }

  bool BHWalker::ignoreStep () const { return ignoreSteps_; }

  bool BHWalker::hasStandaloneAlgorithm () const { return isStandalone_; }

  BHdouble BHWalker::Algorithm (
    std::vector<BHWalker> &walkers,
    const BHMetalParameters &bhmp,
    BHWalkerSupport::BHWalkerAlgorithmSupport &support,
    rndEngine &rng) {
    return Algorithm_ (*this, walkers, bhmp, support, rng);
  }

  /// NB::EnergyModifier is Added to this step energy before verifying if it is
  /// advantageous
  bool BHWalker::verifyStep (rndEngine &rng, BHdouble EnergyModifier) {
    // The code commented below can be used to create a small animation of the
    // Basin Hopping
    /*
     td::ofstream traj ("tray.xyz", std::ofstream::out | std::ofstream::app);
    ::operator<<(traj,WorkingConf_);
    traj <<std::endl;
    std::ofstream lapc ("lapc.xyz",
                       std::ofstream::out |
                           std::ofstream::app);
    ::operator<<(lapc,LastAcceptedConf_);
    lapc <<std::endl;
*/
    BHdouble deltaE =
      (lastMoveData_.Energy () + EnergyModifier) - lastAcceptedData_.Energy ();
    bool accepted = false;
    // Metropolis
    if (deltaE < 0) { // the exponential woul be always >0
      accepted = true;
      // std::cout << myene << ">" <<newene<<std::endl;
    } else {
      BHdouble expAcc = exp (-deltaE / (TAccept_ * BHConst::kB)); //<1
      BHdouble z = Metropolis_ (rng); // casual number between 0 and 1;
      if (z <= expAcc) {
        // std::cout << myene << "<" <<newene<<std::endl;
        accepted = true;
      }
    }
    return accepted;
  }

  bool BHWalker::confirmStep (
    std::vector<BHWalker> &walkers,
    const BHMetalParameters &bhmp,
    BHWalkerSupport::BHWalkerAlgorithmSupport &support,
    rndEngine &rng) {
    BHdouble EnergyModifier = Algorithm (walkers, bhmp, support, rng);
    bool accepted = verifyStep (rng, EnergyModifier);
    if (accepted) {
      // this updates and analyze the working configuration
      AnalyzeWorkingConf (support.Analyzer, bhmp);
      // data will be updated here
      moveAccepted ();
    }
    return accepted;
  }

  int BHWalker::getTotalMovesDone () const { return n_moves; }

  void
  BHWalker::forceLastMoveOPs (BHWalker &walker, std::vector<BHdouble> &newOPs) {
    walker.lastMoveData_.setOrderParameters (newOPs);
  }

  void BHWalker::forceLastMoveOPs (
    BHWalker &walker, BHdouble newOP0, BHdouble newOP1) {
    walker.lastMoveData_.setOrderParameter (0, newOP0);
    walker.lastMoveData_.setOrderParameter (1, newOP1);
  }

  void BHWalker::forceLastMoveEnergy (BHWalker &walker, BHdouble newEnergy) {
    walker.lastMoveData_.setEnergy (newEnergy);
  }

  void BHWalker::forceLastAcceptedOPs (
    BHWalker &walker, std::vector<BHdouble> &newOPs) {
    walker.lastAcceptedData_.setOrderParameters (newOPs);
  }

  void BHWalker::forceLastAcceptedOPs (
    BHWalker &walker, BHdouble newOP0, BHdouble newOP1) {
    walker.lastAcceptedData_.setOrderParameter (0, newOP0);
    walker.lastAcceptedData_.setOrderParameter (1, newOP1);
  }

  void
  BHWalker::forceLastAcceptedEnergy (BHWalker &walker, BHdouble newEnergy) {
    walker.lastAcceptedData_.setEnergy (newEnergy);
  }

  void BHWalker::CollapseIslandsOnWorkingConf (
    const BHMetalParameters &bhmp, rndEngine &rng) {
    tp Island_start, Island_stop;
    Island_start = std::chrono::steady_clock::now ();
    BHClusterUtilities::IslandsCollapser (WorkingConf_, bhmp, rng);
    Island_stop = std::chrono::steady_clock::now ();
    ms neighs_time = Island_stop - Island_start;
    std::cout
      << "Island collapsed in "
      << std::chrono::duration<double, std::milli> (neighs_time).count ()
      << " ms" << std::endl;
  }

  unsigned BHWalker::CountIslandsOnWorkingConf (const BHMetalParameters &bhmp) {
    return BHClusterUtilities::CountIslands (WorkingConf_, bhmp);
  }

  BHdouble BHWalker::lastAcceptedEnergy () const {
    return lastAcceptedData_.Energy ();
  }

  BHdouble BHWalker::lastMoveEnergy () const { return lastMoveData_.Energy (); }

  BHdouble BHWalker::lastAcceptedOP (const unsigned &i) const {
    return lastAcceptedData_.orderParameters ()[i];
  }

  BHdouble BHWalker::lastMoveOP (const unsigned &i) const {
    return lastMoveData_.orderParameters ()[i];
    ;
  }

  BHClusterData BHWalker::lastMoveClusterData () const { return lastMoveData_; }

  const BHCluster &BHWalker::lastMoveConf () const { return WorkingConf_; }

  BHdouble BHWalker::minimizeWorkingConf (
    BHMinimizationAlgorithm *minimizer, BHEnergyCalculator *ec) {
    workingConfAnalysed_ = false;
    lastMoveData_.setEnergy (minimizer->MinimizeCluster (WorkingConf_, ec));
    return lastMoveData_.Energy ();
  }

  BHdouble BHWalker::minimizeTolWorkingConf (
    BHMinimizationAlgorithm *minimizer, BHEnergyCalculator *ec) {
    workingConfAnalysed_ = false;
    lastMoveData_.setEnergy (minimizer->MinimizeCluster_tol (WorkingConf_, ec));
    return lastMoveData_.Energy ();
  }

  std::vector<BHdouble> BHWalker::AnalyzeWorkingConf (
    BHClusterAnalyser &analysis, const BHMetalParameters &bhmp) {
    if (!workingConfAnalysed_) {
      workingConfAnalysed_ = true;
      lastMoveData_.setOrderParameters (analysis (WorkingConf_, bhmp));
    }
    return lastMoveData_.orderParameters ();
  }

  std::vector<BHdouble> BHWalker::ForceAnalyzeWorkingConf (
    BHClusterAnalyser &analysis, const BHMetalParameters &bhmp) {
    workingConfAnalysed_ = false;
    return AnalyzeWorkingConf (analysis, bhmp);
  }

  std::vector<BHdouble> BHWalker::AnalyzeLastAcceptedConf (
    BHClusterAnalyser &analysis, const BHMetalParameters &bhmp) {
    lastAcceptedData_.setOrderParameters (analysis (LastAcceptedConf_, bhmp));
    return lastAcceptedData_.orderParameters ();
  }

  BHClusterData BHWalker::lastAcceptedClusterData () const {
    return lastAcceptedData_;
  }

  const BHCluster &BHWalker::lastAcceptedConf () const {
    return LastAcceptedConf_;
  }

  bool BHWalker::isNeighbour (const BHWalker &other) const {
    return walkerSettings_.isNeighbour (
      lastAcceptedData_.orderParameters (),
      other.lastAcceptedData_.orderParameters ());
  }

  bool BHWalker::isInForbiddenRegion () const {
    return walkerSettings_.isInForbiddenRegion (
      lastAcceptedData_.orderParameters ());
  }
  BHdouble BHWalker::getWalkerRepulsion () const {
    return walkerSettings_.WalkerRepulsion_;
  }
  const std::vector<unsigned> &BHWalker::interactWith () const {
    return walkerSettings_.interactWith_;
  }

  // return const_cast<char &>(static_cast<const C &>(*this).get());

  void BHWalker::times (std::ostream &stream) {
    // moves
    stream << "moves:\t\t" << n_moves << ",\t";
    timing (stream, move_time);
    stream << std::endl;
  }

  bool BHWalker::isWorkingConfAnalysed () const { return workingConfAnalysed_; }

  void BHWalker::moveAccepted () {
    moves_[chosenMove_]->hasBeenAccepted ();
    lastAcceptedData_ = lastMoveData_;
    LastAcceptedConf_ << WorkingConf_;
  }

  void BHWalker::copyAllConfigurations (const BHWalker &other) {
    lastMoveData_ = other.lastMoveData_;
    workingConfAnalysed_ = other.workingConfAnalysed_;
    WorkingConf_ = other.WorkingConf_;

    lastAcceptedData_ = other.lastAcceptedData_;
    LastAcceptedConf_ = other.LastAcceptedConf_;
  }

  void BHWalker::copyAcceptedConfiguration (const BHWalker &other) {
    lastAcceptedData_ = other.lastAcceptedData_;
    LastAcceptedConf_ = other.LastAcceptedConf_;
  }

  BHwalkerAlgorithmType BHWalker::myAlgorithm () const {
    return walkerSettings_.ChosenAlgorithm_;
  }

  void BHWalker::printSettings () {
    printWalkerSettings (walkerSettings_);
    std::cout << BHMove::printSettingslegend () << std::endl;

    for (auto &move : moves_) {
      std::cout << move->printSettings () << std::endl;
    }
  }

  void BHWalker::setDepth (unsigned newDepth) { depth_ = newDepth; }

  void BHWalker::copyMovesFrom (const BHWalker &other) {
    for (auto &move : other.moves_) {
      moves_.emplace_back (move->clone ());

      // moves_.back ()->Specialize (WorkingConf_);
      if (moves_.back ()->isExchange () && WorkingConf_.NofSpecies () == 1) {
        throw "you cannot use exchanges if there is only one kind of atom";
      }
    }
  }
} // namespace BH
