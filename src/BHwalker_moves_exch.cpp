/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Definitions of the various exchange moves of BHWalker

   @file B.oves_exch.cpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 11/12/2017
   @version 0.5

   Added BulkMove and compacted few_neigh/lot_neigh

   @date 16/11/2017
   @version 0.4.4
   Adding documentation to the code

   @date 29/3/2017
   @version 0.4.3

   @date 31/1/2017
   @todo
   need to protect special moves from errors (like surf for a perfect coreshell)
*/
#include "BHdebug.hpp"
#include "BHwalker.hpp"

#define few_neigh 10
#define lot_neigh 11

namespace BH {
  //***********************CORREGGERE IL MINIMO DI ESTRAZIONE
  /**This function selects two random atoms of different kind.*/
  void BHWalker::moveExch_All (rnd_engine &rng) {
    chosenAtom1_ = rnd_selByType[typeSelected1_]->operator() (rng);
    chosenAtom2_ = rnd_selByType[typeSelected2_]->operator() (rng);
  }
  /**This move works like moveExch_All(), but selects only atoms that are on the
   * surface*/
  void BHWalker::moveExch_onSurface (rnd_engine &rng) {
    // This creates the list of surface atoms:
    // static const unsigned short few_neigh = 10;//if 9 or less neighbours the
    // atom is considered to be in surface
    vectorint firstSurfAtoms,
      secondSurfAtoms; // list of surface atoms of two species
    int a = rnd_selByType[typeSelected1_]->min (),
        b = rnd_selByType[typeSelected1_]->max ();
    for (int i = a; i <= b; i++) {
      if (NNlist[i].size () < few_neigh) {
        firstSurfAtoms.push_back (i);
      }
    }
    a = rnd_selByType[typeSelected2_]->min ();
    b = rnd_selByType[typeSelected2_]->max ();
    for (int i = a; i <= b; i++) {
      if (NNlist[i].size () < few_neigh) {
        secondSurfAtoms.push_back (i);
      }
    }
    // if no atoms of one species are on the surface simply do an all
    if (firstSurfAtoms.size () == 0 || secondSurfAtoms.size () == 0) {
      chosen_exch = BHWalkerSettings::All;
      TAccept = ExchangesSettings_[All].T_accept;
      moveExch_All (rng);
    } else {
      BHIntRND frnd (0, firstSurfAtoms.size () - 1),
        srnd (0, secondSurfAtoms.size () - 1);
      chosenAtom1_ = firstSurfAtoms[frnd (rng)];
      chosenAtom2_ = secondSurfAtoms[srnd (rng)];
    }
  }

  /**This move works like moveExch_All(), but selects only atoms that are in the
   * bulk*/
  void BHWalker::moveExch_inBulk (rnd_engine &rng) {
    // This creates the list of surface atoms:
    // static const unsigned short few_neigh = 10;//if 9 or less neighbours the
    // atom is considered to be in surface
    vectorint firstBulkAtoms,
      secondBulkAtoms; // list of surface atoms of two species
    int a = rnd_selByType[typeSelected1_]->min (),
        b = rnd_selByType[typeSelected1_]->max ();
    for (int i = a; i <= b; i++) {
      if (NNlist[i].size () >= few_neigh) {
        firstBulkAtoms.push_back (i);
      }
    }
    a = rnd_selByType[typeSelected2_]->min ();
    b = rnd_selByType[typeSelected2_]->max ();
    for (int i = a; i <= b; i++) {
      if (NNlist[i].size () >= few_neigh) {
        secondBulkAtoms.push_back (i);
      }
    }
    // if no atoms of one species are on the surface simply do an all
    if (firstBulkAtoms.size () == 0 || secondBulkAtoms.size () == 0) {
      chosen_exch = BHWalkerSettings::All;
      TAccept = ExchangesSettings_[All].T_accept;
      moveExch_All (rng);
    } else {
      BHIntRND frnd (0, firstBulkAtoms.size () - 1),
        srnd (0, secondBulkAtoms.size () - 1);
      chosenAtom1_ = firstBulkAtoms[frnd (rng)];
      chosenAtom2_ = secondBulkAtoms[srnd (rng)];
    }
  }

  /**This function makes a list of light atoms that are on the surface and a
     list of heavy atoms that are in the core of the system.

     Then it updates #chosenAtom1_ and #chosenAtom2_ with random atoms each
     selected from the two lists.
  */
  void BHWalker::moveExch_CoreShell (rnd_engine &rng) {
    // static const unsigned short few_neigh = 10;//if 9 or less neighbours the
    // light atom is considered to be in surface static const unsigned short
    // lot_neigh = 11;//if 12 or more neighbours the heavy atom is considered to
    // be in the bulk
    //**********************when starting to work with 3 or more types need to
    // control the atoms involved here

    vectorint interestingAtoms[2]; // list of surface atoms of two species
    // heavy atoms
    int a = rnd_selByType[typeSelected1_]->min (),
        b = rnd_selByType[typeSelected1_]->max ();
    for (int i = a; i <= b; i++) {
      if (
        (NNlist[i].size () > lot_neigh) &&
        (NNlist_diff[i].size () > ExchangesSettings_[Csh].bigNN)) {
        interestingAtoms[0].push_back (i);
      }
    }
    a = rnd_selByType[typeSelected2_]->min ();
    b = rnd_selByType[typeSelected2_]->max ();
    // light atoms
    for (int i = a; i <= b; i++) {
      if (
        (NNlist[i].size () < few_neigh) &&
        (NNlist_diff[i].size () > ExchangesSettings_[Csh].smallNN)) {
        interestingAtoms[1].push_back (i);
      }
    }
    if (interestingAtoms[1].size () == 0 || interestingAtoms[0].size () == 0) {
      // backup with a simple all if can't do the move
      chosen_exch = BHWalkerSettings::All;
      TAccept = ExchangesSettings_[All].T_accept;
      moveExch_All (rng);
    } else {
      BHIntRND frnd (0, interestingAtoms[0].size () - 1),
        srnd (0, interestingAtoms[1].size () - 1);
      chosenAtom1_ = interestingAtoms[0][frnd (rng)];
      chosenAtom2_ = interestingAtoms[1][srnd (rng)];
    }
  }

  /**
     This functions makes two lists of atoms:

     - one for the heavy atoms with more than BHExchangeOpt#smallNN atoms of
     different kind.
     - the chosenAtom2_ for light atoms with more than BHExchangeOpt#smallNN
     atoms of different kind.

     Then it updates #chosenAtom1_ and #chosenAtom2_ with random atoms each
     selected from the two lists.
  */
  void BHWalker::moveExch_Interface (rnd_engine &rng) {
    //**********************when starting to work with 3 or more types need to
    // control the atoms involved here
    vectorint interestingAtoms[2]; // list of surface atoms of two species
    // heavy atoms
    unsigned int lowestID = rnd_selByType[typeSelected1_]->min (),
                 highestID = rnd_selByType[typeSelected1_]->max ();
    interestingAtoms[0].clear ();
    for (unsigned int i = lowestID; i < highestID; i++) {
      if (NNlist_diff[i].size () > ExchangesSettings_[Int].smallNN) {
        interestingAtoms[0].push_back (i);
      }
    }
    lowestID = rnd_selByType[typeSelected2_]->min ();
    highestID = rnd_selByType[typeSelected2_]->max ();
    // light atoms
    interestingAtoms[1].clear ();
    for (unsigned int i = lowestID; i < highestID; i++) {
      if (NNlist_diff[i].size () > ExchangesSettings_[Int].bigNN) {
        interestingAtoms[1].push_back (i);
      }
    }

    if (interestingAtoms[1].size () == 0 || interestingAtoms[0].size () == 0) {
      // backup with a simple all if can't do the move
      chosen_exch = BHWalkerSettings::All;
      TAccept = ExchangesSettings_[All].T_accept;
      moveExch_All (rng);
    } else {
      BHIntRND rnd_first (0, interestingAtoms[0].size () - 1),
        rnd_second (0, interestingAtoms[1].size () - 1);
      chosenAtom1_ = interestingAtoms[0][rnd_first (rng)];
      chosenAtom2_ = interestingAtoms[1][rnd_second (rng)];
    }
  }

  /**
      This functions makes two lists of weights, aimed to separate the two kind
     of atoms:

      - one for atoms that should stay inside of the cluster. The more
     neighbours they have the lighter is their weight in the extraction list
      - the second for atoms that we desire to be on the ouside of the cluster.

      Then it updates #chosenAtom1_ and #chosenAtom2_ with random atoms each
     selected from the two lists.

      The weight are calculated with \f$(n_{NdiffNeigh}+1)^{esp}\f$, where _esp_
     is #exchSep.exp, or in the case of the first list when an atom is on the
     surface: \f$(n_{NdiffNeigh}+1+n_{NNlimSup}-n_{NNeigh})^{esp}\f$ where
     \f$n_{NNlimSup}\f$ is the number of neigbour minimum for an atom to be
     considered in the bulk

      If #small_inside is _true_ the first list will be composed of the light
     atoms.
  */
  void BHWalker::moveExch_Sep (rnd_engine &rng) {
    // static const unsigned short lot_neigh = 12;//if 12 or more neighbours the
    // heavy atom is considered to be in the bulk
    //**********************when starting to work with 3 or more types need to
    // control the atoms involved here
    if (ExchangesSettings_[Sep]
          .smallInside) { // if is true i want to move light atoms from
                          // surfate to bulk
      std::swap (typeSelected1_, typeSelected2_);
    }
    int fmin, smin;
    // atoms that will go inside
    unsigned int lowestID = rnd_selByType[typeSelected1_]->min (),
                 highestID = rnd_selByType[typeSelected1_]->max ();
    fmin = lowestID;
    std::vector<BHdouble> firstweights (highestID - lowestID + 1);
    for (unsigned int i = lowestID; i <= highestID; i++) {
      if (NNlist[i].size () > lot_neigh) {
        firstweights[i - lowestID] =
          std::pow (NNlist_diff[i].size () + 1, ExchangesSettings_[Sep].exp);
      } else {
        firstweights[i - lowestID] = std::pow (
          NNlist_diff[i].size () + 2 + lot_neigh - NNlist[i].size (),
          ExchangesSettings_[Sep].exp);
      }
      // cout << i<<" "<<i-lowestID <<" " << firstweights[i-lowestID]<<endl;
    }
    // std::cout << NofAtoms_ <<std::endl;
    lowestID = rnd_selByType[typeSelected2_]->min ();
    highestID = rnd_selByType[typeSelected2_]->max ();

    smin = lowestID;
    std::vector<BHdouble> secondweights (highestID - lowestID + 1);
    // atoms that are wanted ouside
    for (unsigned int i = lowestID; i <= highestID; i++) {
      secondweights[i - lowestID] =
        std::pow (NNlist_diff[i].size () + 1, ExchangesSettings_[Sep].exp);
    }
    // std::cout << lowestID << " "<< highestID <<" "<<highestID-lowestID <<"
    // "<< secondweights.size()<<std::endl;
    std::discrete_distribution<int> frnd (
      firstweights.begin (), firstweights.end ()),
      srnd (secondweights.begin (), secondweights.end ());
    chosenAtom1_ = fmin + frnd (rng);
    chosenAtom2_ = smin + srnd (rng);
    // cout << first <<" "<< chosenAtom2_ <<endl;
    // std::cin.get();
  }

  /**
      This functions makes two lists of weights, aimed to mix the two kind of
     atoms:

      -for lighter atoms gives the more weight to atoms that have more
     neighbours of the same kind, and if an atom is on the surface tries to
     bring it inside, by agumenting its weight. -for heavier atoms gives the
     more weight to atoms that have more neighbours of the same kind Then it
     updates #chosenAtom1_ and #chosenAtom2_ with random atoms each selected
     from the two lists.
  */
  void BHWalker::moveExch_Mix (rnd_engine &rng) {
    // static const unsigned short lot_neigh = 12;//if 12 or more neighbours the
    // heavy atom is considered to be in the bulk
    //**********************when starting to work with 3 or more types need to
    // control the atoms involved here ligth atoms
    unsigned int fmin, smin;
    unsigned int lowestID = rnd_selByType[typeSelected2_]->min (),
                 highestID = rnd_selByType[typeSelected2_]->max ();
    fmin = lowestID;
    std::vector<BHdouble> firstweights (highestID - lowestID + 1);
    for (unsigned int i = lowestID; i <= highestID; i++) {
      if (NNlist[i].size () >= lot_neigh) {
        firstweights[i - lowestID] =
          std::pow (NNlist_same[i].size () + 1, ExchangesSettings_[Mix].mixPar);
      } else {
        firstweights[i - lowestID] = std::pow (
          NNlist_same[i].size () + 2 + lot_neigh - NNlist[i].size (),
          ExchangesSettings_[Mix].mixPar);
      }
    }
    lowestID = rnd_selByType[typeSelected1_]->min ();
    highestID = rnd_selByType[typeSelected1_]->max ();
    smin = lowestID;
    std::vector<BHdouble> secondweights (highestID - lowestID + 1);
    // heavy atoms
    for (unsigned int i = lowestID; i <= highestID; i++) {
      secondweights[i - lowestID] =
        std::pow (NNlist_same[i].size () + 1, ExchangesSettings_[Mix].mixPar);
    }

    std::discrete_distribution<int> frnd (
      firstweights.begin (), firstweights.end ()),
      srnd (secondweights.begin (), secondweights.end ());
    chosenAtom1_ = fmin + frnd (rng);
    chosenAtom2_ = smin + srnd (rng);
  }

} // namespace BH
