/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief This program simply minimize a configuration

   @file minimizer.cxx
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 18/4/18
   @version 0.1

   writing down the first idea
*/

#include <iostream>
//#include <sstream>
#include <iomanip>
//#include <fstream>
#include <chrono>
#include <csignal>

#include "BHbasinHopping.hpp"
#include "BHmetalParameters.hpp"

#ifndef USESMATBGAUSS
#include "BHenergyCalculator_SMATB.hpp"
using EnergyCalculator = BH::BHEnergyCalculator_SMATB;
#else
#include "BHenergyCalculator_SMATB_GAUSS.hpp"
using EnergyCalculator = BH::BHEnergyCalculator_SMATB_GAUSS;
#endif

#include "BHdebug.hpp"
#ifdef _OPENMP
#include <omp.h>
#endif

using namespace std;
using namespace BH;

// test for basin hopping
int main (int argc, char **argv) {
  int retval = 0;
  try {
#ifdef _OPENMP
    cout << "***********************WARNING********************************\n"
         << "Compiled with openMP for minimization speedup\n"
         << "Paralelization add some variations in the order of the sums,\n"
         << "so two consecutive runs could give slightly different results.\n"
         << "In fact results are not reproducible\n"
         //<< "Number of threads: " << omp_get_num_threads() <<'\n'
         << "Max number of threads: " << omp_get_max_threads () << '\n'
         << "**************************************************************"
         << endl;
#endif
    if (argc < 1) {
      throw "No argument: you must specify at least the starting configuration";
    }
    BHMetalParameters::BHMP_filename = "MetalParameters.in";

    BHCluster cluster (argv[1]);

    cluster.MassesReorder ();

    BHMetalParameters::getBHMP (false).setInteractionLabels (cluster);
    EnergyCalculator myenergy (cluster);
    unsigned int NofAtoms = cluster.getNofAtoms ();
    BHdouble *x = new BHdouble[NofAtoms * 3];
    for (unsigned int j = 0; j < NofAtoms; j++) {
      x[3 * j] = cluster[j].X ();
      x[3 * j + 1] = cluster[j].Y ();
      x[3 * j + 2] = cluster[j].Z ();
    }
    /*
    myenergy.Neighbours_for_tol(x);
    BHdouble OldEnergy = myenergy.Energy_tol (x);
    BHdouble Energy = myenergy.Minimization_LBFGSB_tol (x);
    */
    BHdouble OldEnergy = myenergy.Energy (x);
    BHdouble Energy = myenergy.Minimization_LBFGSB (x);
    std::string outfileName = "minimized_configuration.out";
    std::ofstream fout (outfileName);
    fout << NofAtoms << "\n" << Energy << '\n';
    std::cout << "Original Energy = " << OldEnergy << '\n';
    std::cout << "Minimized with energy = " << std::setprecision (9) << Energy
              << '\n'
              << "Configuration stored in the file " << outfileName << '\n';
    for (unsigned int j = 0; j < NofAtoms; j++) {
      fout << cluster[j].Type () << '\t' << std::setprecision (9) << x[3 * j]
           << '\t' << x[3 * j + 1] << '\t' << x[3 * j + 2] << '\n';
    }
    fout.close ();
    delete[] x;
  } catch (const char *problem) {
    cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m" << endl;
    retval = 1;
  } catch (const string problem) {
    cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m" << endl;
    retval = 1;
  }
  return retval;
}
