/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief The main program thwt will start the BH algorithm

@file main.cxx
  @author Daniele Rapetti (iximiel@gmail.com)
    @date 21/3/2017
  @version 1.0
  */

#include <iostream>

#include "BHbasinHopping.hpp"
#include "BHbasinHoppingFlyingLanding.hpp"
#include "BHdebug.hpp"
#include "BHmainTemplate.hpp"
#include "BHmisc.hpp"
#include "BHwalkerSettings.hpp"
#ifdef _OPENMP
#include <omp.h>
#endif

using namespace std;

int main (int argc, char **argv) {
  int retval = 0;
  try {
    if (argc > 1) {
      if (std::string (argv[1]) == "memory") {
        return BH::printMemory (std::cout);
      } else if (std::string (argv[1]) == "settingsInput") {
        ///@todo add a better and simpler option name
        return BH::plotBHSettingsInputFile (std::cout, argc - 1, argv + 1);
      } else if (std::string (argv[1]) == "walkerInput") {
        ///@todo add a better and simpler option name
        return BH::plotBHWalkerSettingsInputFile (
          std::cout, argc - 1, argv + 1);
      } else if (std::string (argv[1]) == "center") {
        ///@todo add a better and simpler option name
        return BH::centerCluster (std::cout, argc - 1, argv + 1);
      } else if (std::string (argv[1]) == "energy") {
        ///@todo add a better and simpler option name
        return BH::energyCluster (std::cout, argc - 1, argv + 1);
      } else if (std::string (argv[1]) == "help") {
        std::cout << "The help menu is under work\n";
        std::cout << "Options are:\n"
                  << "memory\n"
                  << "settingsInput\n"
                  << "walkerInput\n"
                  << "center\n"
                  << "energy\n"
                  << "help\n"
                  << "FL\n"
                  << "BH" << std::endl;
        return 0;
      } else if (std::string (argv[1]) == "FL") {
        return BH::BHexecutor<BH::BHBasinHoppingFlyingLanding> (
          argc - 1, argv + 1);
      } else if (std::string (argv[1]) == "BH") {
        return BH::BHexecutor<BH::BasinHopping> (argc - 1, argv + 1);
      } else {
        throw "Unknown option: \"" + std::string (argv[1]) + "\"";
      }
    }
#ifdef _OPENMP
    std::cerr
      << "***********************WARNING********************************\n"
      << "Compiled with openMP for minimization speedup\n"
      << "Paralelization add some variations in the order of the sums,\n"
      << "so two consecutive runs could give slightly different results.\n"
      << "In fact results are not reproducible\n"
      //<< "Number of threads: " << omp_get_num_threads() <<'\n'
      << "Max number of threads: " << omp_get_max_threads () << '\n'
      << "**************************************************************"
      << endl;
#endif
    /*const char BHpplogo[] = " ______  _     _             \n"
                            "(____  \\| |   | |  _     _   \n"
                            " ____)  ) |__ | |_| |_ _| |_ \n"
                            "|  __  (|  __)| (_   _|_   _)\n"
                            "| |__)  ) |   | | |_|   |_|  \n"
                            "|______/|_|   |_|            \n";
    std::cout << BHpplogo << std::endl;*/
    // select between Doom:
    //        http://patorjk.com/software/taag/#p=display&f=Doom&t=BH%2B%2B
    // and   Stop
    //        http://patorjk.com/software/taag/#p=display&f=Stop&t=BH%2B%2B
    // if 0 args do cthe classical execution
    retval = BH::BHexecutor<BH::BasinHopping> (argc, argv);

  } catch (const char *problem) {
    cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m" << endl;
    retval = 1;
  } catch (const string &problem) {
    cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m" << endl;
    retval = 1;
  }
  return retval;
}
