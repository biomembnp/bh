/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief A small test program for BHmetalParameters

   @file MetalParametersTest.cxx
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 28/8/2017
   @version 1.0
*/

#include <iostream>
#include <string>

#include "BHmetalParameters.hpp"

using namespace std;

using namespace BH;

int main (int /*argc*/, char ** /*argv*/) {
  try {
    BHMetalParameters bhmp ("MetalParameters.in", true);
  } catch (const char *problem) {
    cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m" << endl;
  } catch (const string &problem) {
    cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m" << endl;
  }
  return 0;
}
