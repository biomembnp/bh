/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @file BHworkWithFortran.cpp

   WWF stands for WorkWithFortran
   @author Daniele Rapetti (iximiel@gmail.com)
   @version 1.1
   @date 15/11/2017

   Separated from BHmetalParameters, works in a similar manner of
   BHEnergyCalculator_SMATB
   @version 1.0
   @date 21/3/2017
   @brief Definition of interfaces to Fortran for BHMetalParameters

*/
#include "BHworkWithFortran.hpp"
#include "BHmyMath.hpp"

#include <iostream>
#include <map>

namespace BH {
  namespace BHWWF {
    void sendClusterToFortran (
      BHClusterAtoms &cluster, const BHMetalParameters &bhmp) {
      cluster.updateComposition ();
      unsigned nAtoms = /*static_cast<int>*/ (cluster.getNofAtoms ());
      unsigned nTypes = /*static_cast<int>*/ (cluster.NofSpecies ());
      unsigned nInteractions = RepetitionComb (nTypes, 2u);

      int *typeList (new int[nAtoms]), *intType (new int[nTypes * nTypes]);
      // preparing parameters to be sent to fortran
      int intID (0);
      std::map<std::string, int> Name2ID; // converts the atom name to his ID
      std::map<unsigned, int> INT_MP2ID;  // converts MP IDs to this class IDs
      BHdouble *Coh (new BHdouble[nTypes]), *Radius (new BHdouble[nTypes]),
        *Mass (new BHdouble[nTypes]);
      for (unsigned i = 0; i < nTypes; ++i) {
        std::string atomType = cluster.Composition (i);
        Name2ID[atomType] = static_cast<integer> (i);
        // updating the intType
        intType[i * nTypes + i] = intID;
        INT_MP2ID[bhmp.getPlabels (atomType + atomType)] = intID++;
        for (unsigned j = i + 1; j < nTypes; ++j) {
          // updating the intType
          intType[i * nTypes + j] = intID;
          intType[j * nTypes + i] = intID;

          std::string atomType2 = cluster.Composition (j);
          INT_MP2ID[bhmp.getPlabels (atomType + atomType2)] = intID++;
        }
        bhmp.giveCohRadiusMass (
          bhmp.getSlabels (atomType), Coh[i], Radius[i], Mass[i]);
      }

      for (unsigned i = 0; i < nAtoms; ++i) {
        typeList[i] = 1 + Name2ID[cluster[i].Type ()];
      }

      integer nTypes_int = static_cast<integer> (nTypes);
      BHInterface_SetPrivatePars (nTypes_int, Coh, Radius, Mass, intType);
      BHdouble *p (new BHdouble[nInteractions]);
      BHdouble *q (new BHdouble[nInteractions]);
      BHdouble *a (new BHdouble[nInteractions]);
      BHdouble *xi (new BHdouble[nInteractions]);
      BHdouble *NNd (new BHdouble[nInteractions]);
      BHdouble *cs (new BHdouble[nInteractions]);
      BHdouble *ce (new BHdouble[nInteractions]);
      BHdouble *P5 (new BHdouble[nInteractions]);
      BHdouble *P4 (new BHdouble[nInteractions]);
      BHdouble *P3 (new BHdouble[nInteractions]);
      BHdouble *Q5 (new BHdouble[nInteractions]);
      BHdouble *Q4 (new BHdouble[nInteractions]);
      BHdouble *Q3 (new BHdouble[nInteractions]);

      for (auto corrispondence : INT_MP2ID) {
        unsigned MPindex = corrispondence.first;
        int i = corrispondence.second;

        bhmp.giveMetalPars (
          MPindex, p[i], q[i], a[i], xi[i], NNd[i], cs[i], ce[i]);
        bhmp.givePolyPars (MPindex, P5[i], P4[i], P3[i], Q5[i], Q4[i], Q3[i]);
      }
      integer nInteractions_int = static_cast<integer> (nInteractions);
      BHInterface_SetCouplePars (
        nInteractions_int, p, q, a, xi, P5, P4, P3, Q5, Q4, Q3, NNd, cs, ce);
      integer nAtoms_int = static_cast<integer> (nAtoms);
      BHInterface_SetCluster (nAtoms_int, typeList);
      delete[] typeList; // it has been copied in the fortran module
      delete[] intType;
      delete[] Coh;
      delete[] Radius;
      delete[] Mass;
      delete[] p;
      delete[] q;
      delete[] a;
      delete[] xi;
      delete[] NNd;
      delete[] cs;
      delete[] ce;
      delete[] P5;
      delete[] P4;
      delete[] P3;
      delete[] Q5;
      delete[] Q4;
      delete[] Q3;
    }
    BHLBFGSBInterface::BHLBFGSBInterface (unsigned N, unsigned M)
      : n (N),
        m (M),
        lowerBounds_ (new BHWWF::real8[N]),
        upperBounds_ (new BHWWF::real8[N]),
        boundsInfo_ (new BHWWF::integer[N]),
        intWorkArray_ (new BHWWF::integer[3 * N]),
        workArray_ (new BHWWF::real8[(2 * M + 4) * N + 12 * M * (M + 1)])
    // modifications for 3.0:
    // workArray_ (new BHWWF::real8[11*m*m+8*m+(2*m+5)*n])
    {
      setTaskToSTART ();
      std::fill_n (csave_, 60, ' ');
      std::fill_n (boundsInfo_, n, 0);
    }

    BHLBFGSBInterface::~BHLBFGSBInterface () {
      delete[] lowerBounds_;
      delete[] upperBounds_;
      delete[] boundsInfo_;
      delete[] intWorkArray_;
      delete[] workArray_;
      delete[] task_;
      delete[] csave_;
      delete[] lsave_;
      delete[] integerInfos_;
      delete[] doubleInfos_;
    }

    void BHLBFGSBInterface::step (
      BHWWF::real8 inputEnergy, BHWWF::real8 *x, BHWWF::real8 *g) {
      integer n_ = static_cast<integer> (n);
      integer m_ = static_cast<integer> (m);
      BHWWF::setulb_ (
        &n_, &m_, x, lowerBounds_, upperBounds_, boundsInfo_, &inputEnergy, g,
        &factr_, &pgtol_, workArray_, intWorkArray_, task_, &verbosityFlag_,
        csave_, lsave_, integerInfos_, doubleInfos_);
    }

    void BHLBFGSBInterface::setTaskToSTART () {
      std::fill (&task_[5], task_ + 60, ' ');
      task_[0] = 'S';
      task_[1] = 'T';
      task_[2] = 'A';
      task_[3] = 'R';
      task_[4] = 'T';
    }

    void BHLBFGSBInterface::setTaskToSTOP () {
      std::fill (&task_[4], task_ + 60, ' ');
      task_[0] = 'S';
      task_[1] = 'T';
      task_[2] = 'O';
      task_[3] = 'P';
    }

    void BHLBFGSBInterface::setVerbosity (int v) { verbosityFlag_ = v; }

    void BHLBFGSBInterface::setFunctionTolerance (BHdouble ftol) {
      factr_ = ftol;
    }

    void BHLBFGSBInterface::setGradientTolerance (BHdouble pgtol) {
      pgtol_ = pgtol;
    }

    void BHLBFGSBInterface::setLowerBoundFor (unsigned i, BHdouble Lower) {
      // nbd(i)=0 if x(i) is unbounded,
      //  1 if x(i) has only a lower bound,
      //  2 if x(i) has both lower and upper bounds, and
      //  3 if x(i) has only an upper bound.
      boundsInfo_[i] = 1;
      lowerBounds_[i] = Lower;
    }

    void BHLBFGSBInterface::setUpperAndLowerBoundsFor (
      unsigned i, BHdouble Upper, BHdouble Lower) {
      // nbd(i)=0 if x(i) is unbounded,
      //  1 if x(i) has only a lower bound,
      //  2 if x(i) has both lower and upper bounds, and
      //  3 if x(i) has only an upper bound.
      boundsInfo_[i] = 2;
      lowerBounds_[i] = Lower;
      upperBounds_[i] = Upper;
    }

    void BHLBFGSBInterface::setUpperBoundFor (unsigned i, BHdouble Upper) {
      // nbd(i)=0 if x(i) is unbounded,
      //  1 if x(i) has only a lower bound,
      //  2 if x(i) has both lower and upper bounds, and
      //  3 if x(i) has only an upper bound.
      boundsInfo_[i] = 3;
      upperBounds_[i] = Upper;
    }

    void BHLBFGSBInterface::setNoBoundsFor (unsigned i) {
      // nbd(i)=0 if x(i) is unbounded,
      //  1 if x(i) has only a lower bound,
      //  2 if x(i) has both lower and upper bounds, and
      //  3 if x(i) has only an upper bound.
      boundsInfo_[i] = 0;
    }

    int BHLBFGSBInterface::nstep () const { return integerInfos_[29]; }

    int BHLBFGSBInterface::nFunctionEvaluationTotal () const {
      return integerInfos_[33];
    }

    int BHLBFGSBInterface::nFunctionIterationCurrentStep () const {
      return integerInfos_[35];
    }

    BHdouble BHLBFGSBInterface::projectedGradientNorm () const {
      return doubleInfos_[12];
    }

    BHdouble BHLBFGSBInterface::fValuePreviousStep () const {
      return doubleInfos_[1];
    }

    BHLBFGSBInterface::taskResult BHLBFGSBInterface::giveCurrentTask () const {
      switch (task_[0]) {
      case 'F':
        return taskResult::FG;
      case 'N':
        return taskResult::NEW_X;
      case 'E':
        return taskResult::ERROR;
      case 'A':
        return taskResult::ABNO;
      case 'W':
        return taskResult::WARN;
      case 'C':
        return taskResult::CONV;
      case 'S':
        return taskResult::STOP;
      }
      return taskResult::unknown;
    }

    const char *BHLBFGSBInterface::getTask () const { return task_; }

    const BHWWF::logical *BHLBFGSBInterface::getLsave () const {
      return lsave_;
    }

    const BHWWF::integer *BHLBFGSBInterface::getIsave () const {
      return integerInfos_;
    }

    const BHWWF::real8 *BHLBFGSBInterface::getDsave () const {
      return doubleInfos_;
    }
  } // namespace BHWWF
} // namespace BH
