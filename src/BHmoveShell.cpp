/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#include "BHmoveShell.hpp"
#include "BHparsers.hpp"

#include <iomanip>
#include <sstream>

namespace BH {
  BHMoveShell::BHMoveShell ()
    : BHMove ("shell"),
      MinimumMoveDisplacementSQ_ (
        MinimumMoveDisplacement_ * MinimumMoveDisplacement_) {}
  BHMoveShell::~BHMoveShell () = default;

  std::string BHMoveShell::DefaultString () {
    return "prob = 1.0, accTemp = 300, minimumDisplacement =3.0, "
           "shellThickness = 1.5";
  }

  std::unique_ptr<BHMove> BHMoveShell::clone () {
    return std::unique_ptr<BHMove>{new BHMoveShell (*this)};
  }

  std::string BHMoveShell::printSettingsSpecialized () const {
    std::stringstream ss;
    ss << "Shell: " << crown_
       << ", Minimum displacement: " << MinimumMoveDisplacement_;
    return ss.str ();
  }

  std::string BHMoveShell::printSettingsSpecializedForInput () const {
    std::stringstream ss;
#define outputwriter(variable)                                                 \
  ", " << BHParsers::BHMV::SHELL[static_cast<size_t> (                         \
            BHParsers::BHMV::SHELLvariable::variable)]                         \
       << " = " << variable

    ss << outputwriter (crown_) << outputwriter (MinimumMoveDisplacement_);
#undef outputwriter
    return ss.str ();
  }

  bool BHMoveShell::doMoveAlgorithm (
    BHCluster &in,
    BHCluster &out,
    BHClusterAnalyser &Analyzer,
    const BHMetalParameters &bhmp,
    rndEngine &rng) {
    out << in;
    Analyzer.calcNeighbourhood_tol (out, bhmp);
    auto NofAtoms = out.getNofAtoms ();
    vectorAtomID lonelyAtoms; // vector with the atoms with least neighbours
    // the centre (of mass):
    BHVector GeometricCenter = out.getGeometricCenter ();
    for (unsigned i = 1; i < NofAtoms; i++) {
      if (out.getNNs (i).size () < 9) {
        lonelyAtoms.push_back (i);
      }
    }

    /*
    //BHdouble tmass=0;
    //seaching for atoms with a small number of neighbours and calculating the
    center(of mass) for (unsigned int i=1; i < NofAtoms ;i++) { BHdouble mass =
    Mass[Slabels[Atoms_[i].Type()]]; CDM+=out[i];*mass; tmass+=mass
          if(NNlist[i].size() < 9){
          lonelyAtoms.push_back(i);
    }
    }
    CDM/=tmass;*/
    BHdouble maxdistSQ = out[0].dist2 (GeometricCenter);
    // calculate maxdist from cdm
    for (unsigned int i = 1; i < NofAtoms; i++) {
      BHdouble d2 = out[i].dist2 (GeometricCenter);
      if (d2 > maxdistSQ)
        maxdistSQ = d2;
    }
    BHdouble maxdist = sqrt (maxdistSQ),          // maximum sphere radius
      mindist = std::max (0.0, maxdist - crown_); // minimum sphere radius
    BHUIntRND choseAtom (0, unsigned (lonelyAtoms.size () - 1));
    bool short_distance =
      true; // set to true when the chosen atom is moved near its position
    unsigned chosen = lonelyAtoms[choseAtom (rng)];
    bool toreturn = false;
    for (int i = 0; i < 10 && short_distance; i++) {
      BHVector newPos =
        GeometricCenter + BHVector::RandomVector (rng, maxdist, mindist);
      if (newPos.dist2 (out[chosen]) < MinimumMoveDisplacementSQ_) {
        out[chosen] = newPos;
        short_distance = false;
        toreturn = true;
      }
    }
    return toreturn;
  }

  bool BHMoveShell::parseAfter () {
    MinimumMoveDisplacementSQ_ =
      MinimumMoveDisplacement_ * MinimumMoveDisplacement_;
    return true;
  }

  bool BHMoveShell::parseSpecialized (const std::string &parsed) {
    bool toreturn = false;
    try {
#define inputgetter(variable)                                                  \
  BHParsers::parse (                                                           \
    BHParsers::BHMV::SHELL[static_cast<size_t> (                               \
                             BHParsers::BHMV::SHELLvariable::variable)]        \
      .c_str (),                                                               \
    parsed, variable)
      toreturn |= inputgetter (crown_);
      toreturn |= inputgetter (MinimumMoveDisplacement_);
#undef inputgetter
    } catch (const std::invalid_argument & /*ia*/) {
      throw typeName () + "::Invalid argument: \"" + parsed + "\"";
    }
    return toreturn;
  }
} // namespace BH
