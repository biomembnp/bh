/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#include "BHmoveExchange.hpp"

#include <utility>
namespace BH {
  BHMoveExchange::BHMoveExchange (std::string name)
    : BHMove (std::move (name), true) {}

  BHMoveExchange::~BHMoveExchange () = default;

  void
  BHMoveExchange::Specialize (const BHCluster &in, const BHMetalParameters &) {
    NofSpecies_ = in.NofSpecies ();
    if (NofSpecies_ < 2) {
      throw "You cannot ask for exchange moves if your cluster is made of only "
            "one type of atom";
    }
    rndSpecies_.param (BHULongRND::param_type (0ul, NofSpecies_ - 1));

    firstAtomperSpecie_.resize (NofSpecies_);
    lastAtomperSpecie_.resize (NofSpecies_);

    unsigned int ind = 0;
    std::string st = in[0].Type ();

    firstAtomperSpecie_[ind] = 0;
    lastAtomperSpecie_[NofSpecies_ - 1] = (in.getNofAtoms () - 1);
    // this funtion determines the first atom of each type, and if they are
    // ordered
    for (unsigned int i = 1; i < in.getNofAtoms (); ++i) {
      if (in[i].Type () != st) {
        if (ind >= NofSpecies_) {
          throw "Atoms are not ordered";
        }
        st = in[i].Type ();
        lastAtomperSpecie_[ind] = i - 1;
        ++ind;
        firstAtomperSpecie_[ind] = i;
      }
    }
    SpecializeExchange (in);
  }

  void BHMoveExchange::SpecializeExchange (const BHCluster &) {}

  bool BHMoveExchange::doMoveAlgorithm (
    BHCluster &in,
    BHCluster &out,
    BHClusterAnalyser &Analyzer,
    const BHMetalParameters &bhmp,
    rndEngine &rng) {
    out << in;
    // size_t kindToExchange1_=0, kindToExchange2_=1;
    // select the type of atoms on which act
    if (NofSpecies_ > 2) { // if NofSpecies()==2 is already set
      kindToExchange1_ = rndSpecies_ (rng);
      kindToExchange2_ = rndSpecies_ (rng);
      while (kindToExchange2_ ==
             kindToExchange1_) { // try until i selected a second species
        kindToExchange2_ = rndSpecies_ (rng);
      }
      // the lower species-index is expected to be the index of the more massive
      // atom: this is needed for some kind of exchanges
      if (kindToExchange2_ > kindToExchange1_) {
        std::swap (kindToExchange1_, kindToExchange2_);
      }
    }
    if (selectAtoms (out, Analyzer, bhmp, rng)) {
      out[chosenAtom1_].positionSwap (out[chosenAtom2_]);
      return true;
    }
    return false;
  }

} // namespace BH
