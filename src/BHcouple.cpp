/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declaration of BHCouple

   @file BHcouple.hpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 20/4/2017
   @version 1.0
*/
#include "BHcouple.hpp"
namespace BH {
#ifndef __MINGW32__
  BHCouple::BHCouple (int i, int j)
    : first (unsigned (i)),
      second (unsigned (j)),
      r (0),
      s (0),
      t (0) {}
  BHCouple::BHCouple (unsigned i, unsigned j)
    : first (i),
      second (j),
      r (0),
      s (0),
      t (0) {}
#endif //__MINGW32__
  BHCouple::BHCouple (size_t i, size_t j)
    : first (i),
      second (j),
      r (0),
      s (0),
      t (0) {}

  std::ostream &operator<< (std::ostream &stream, const BHCouple &cp) {
    stream << "<" << cp.first + 1 << ">-<" << cp.second + 1 << ">" << std::endl
           << "r:" << int (cp.r) << " s:" << int (cp.s) << " t:" << int (cp.t)
           << std::endl;
    for (auto i : cp.commonNeighs) {
      stream << i + 1 << ", ";
    }
    return stream;
  }
} // namespace BH
