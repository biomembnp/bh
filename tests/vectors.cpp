#include "BHvector.hpp"
#include "testhelper.hpp"
#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>

using BHV = BH::BHVector;
// Catch::literals contains _a thta can be used in place of Catch::Approx
using namespace Catch::literals;

SCENARIO ("BHV algebra", "[BHVector]") {
  GIVEN ("Two BHVectors (1,1,1) and (1,1,0)") {
    BHV v1 (1, 1, 1);
    BHV v2 (1, 1, 0);
    THEN (
      "The abs function shoud be equal to the self sqrt of scalar product") {
      REQUIRE (sqrt (v1 * v1) == Catch::Approx (abs (v1)));
      REQUIRE (sqrt (v1 * v2) == Catch::Approx (abs (v2)));
    }
    AND_THEN ("The distance operator should work") {
      REQUIRE (v1.dist (v2) == 1.0_a);
      REQUIRE (v1.dist2 (2 * v2) == 3.0_a);
    }
    AND_THEN ("Get the inverse") {
      auto t1 = -v1;
      REQUIRE (t1.X () == Catch::Approx (-v1.X ()));
      REQUIRE (t1.Y () == Catch::Approx (-v1.Y ()));
      REQUIRE (t1.Z () == Catch::Approx (-v1.Z ()));
    }

    THEN ("Mathematical expressions should work") {
      REQUIRE (v1 + v2 == BHV (2., 2., 1.));
      REQUIRE (v1 - v2 == BHV (0., 0., 1.));
      REQUIRE (v1 % v2 == BHV (-1., 1., 0.));
      REQUIRE (3 * v1 == BHV (3., 3., 3.));
      REQUIRE (3 * v2 == BHV (3., 3., 0.));
      REQUIRE (v1 / 2.0 == BHV (0.5, 0.5, 0.5));
      REQUIRE (v2 / 2.0 == BHV (0.5, 0.5, 0.0));
    }
  }
  GIVEN ("The static polar generator of BHVector") {
    WHEN ("Generating the zero vector and the axes") {
      BHV zero = BHV::RhoThetaPhi (0, 0, 0);
      BHV x = BHV::RhoThetaPhi (1.0, M_PI_2, 0);
      BHV y = BHV::RhoThetaPhi (1.0, M_PI_2, M_PI_2);
      BHV z = BHV::RhoThetaPhi (1.0, 0, 0);
      THEN ("The number generated shoud be correct") {
        REQUIRE (zero == BHV (0.0, 0.0, 0.0));
        REQUIRE (x == BHV (1.0, 0.0, 0.0));
        REQUIRE (y == BHV (0.0, 1.0, 0.0));
        REQUIRE (z == BHV (0.0, 0.0, 1.0));
      }
    }
    WHEN ("Generating the reverse of the axes") {
      BHV x = BHV::RhoThetaPhi (1.0, M_PI_2, M_PI);
      BHV y = BHV::RhoThetaPhi (1.0, M_PI_2, 1.5 * M_PI);
      BHV z = BHV::RhoThetaPhi (1.0, M_PI, 0);
      THEN ("The number generated shoud be correct") {
        REQUIRE (x == BHV (-1.0, 0.0, 0.0));
        REQUIRE (y == BHV (0.0, -1.0, 0.0));
        REQUIRE (z == BHV (0.0, 0.0, -1.0));
      }
    }
  }
}
