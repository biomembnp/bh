#include "BHconstructors.hpp"
#include "BHmoves.hpp"
#include "testhelper.hpp"
#include <catch2/catch_approx.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include <catch2/catch_test_macros.hpp>
#include <iostream>

// Catch::literals contains _a thta can be used in place of Catch::Approx
using namespace Catch::literals;

class UnitTestMove : public BH::BHMove {
public:
  UnitTestMove () : BHMove ("unitTest", false) {}
  ~UnitTestMove () override = default;
  std::unique_ptr<BHMove> clone () override {
    return std::unique_ptr<BHMove>{new UnitTestMove (*this)};
  }
  static std::string DefaultString () { return "prob = 1.0, accTemp = 300"; }

protected:
  bool doMoveAlgorithm (
    BH::BHCluster &,
    BH::BHCluster &,
    BH::BHClusterAnalyser &,
    const BH::BHMetalParameters &,
    BH::rndEngine &) override {
    return true;
  }
};

SCENARIO ("We want to create some moves", "[utility],[BHMoves]") {
  GIVEN ("A name and the parameter for a move") {
    std::string name = "shake";
    std::string options = "p=0.5, rmin=1.44, rmax=2.0";
    WHEN ("The move factory is called") {
      auto move = BH::createMoveFromString (name, options);
      THEN ("The move should be correclty initialized") {
        REQUIRE (move->name () == name);
        REQUIRE (move->getProbability () == 0.5_a);
      }
    }
    WHEN ("The singleton move factory is called") {
      auto move = BH::BHConstructors::getConstructor ().createMoveFromString (
        name, options);
      THEN ("The move should be correclty initialized") {
        REQUIRE (move->name () == name);
        REQUIRE (move->getProbability () == 0.5_a);
      }
    }
  }
  GIVEN ("The list of the default moves") {
    std::vector<std::pair<std::string, std::string>> movelist;
#define MOVE_PARSER
#define MoveParser(key, Class)                                                 \
  movelist.push_back ({#key, BH::Class::DefaultString ()});
#include "BHmoves.hpp"
#undef MoveParser
#undef MOVE_PARSER
    THEN ("The movelist should not be empty") {
      REQUIRE_FALSE (movelist.empty ());
    }
    THEN ("All the move should be correclty initialized by the factory") {
      for (const auto &p : movelist) {
        std::string name = p.first;
        std::string options = p.second;
        auto move = BH::createMoveFromString (name, options);
        REQUIRE (move->name () == name);
      }
    }
    THEN ("All the move should be correclty initialized by the singleton "
          "factory") {
      for (const auto &p : movelist) {
        std::string name = p.first;
        std::string options = p.second;
        auto move = BH::BHConstructors::getConstructor ().createMoveFromString (
          name, options);
        REQUIRE (move->name () == name);
        REQUIRE (move->getProbability () == 1.0_a);
      }
    }
  }

  GIVEN ("A wrong move name") {
    THEN ("The constructor function should throw") {
      REQUIRE_THROWS (
        BH::BHConstructors::getConstructor ().createMoveFromString (
          "thisMustThrow", ""));
    }
  }
  GIVEN ("A new type of bhmove") {
    std::string name = "unitTest";
    BH::addMoveToConstructorList<UnitTestMove> (name);
    REQUIRE_THROWS (BH::addMoveToConstructorList<UnitTestMove> (name));
    THEN ("It should be recognised") {
      auto move =
        BH::BHConstructors::getConstructor ().createMoveFromString (name, "");
      REQUIRE (move->name () == name);
    }
  }
}

TEMPLATE_TEST_CASE (
  "Copying moves",
  "[moves]",
  BH::BHMoveShake,
  BH::BHMoveBonds,
  BH::BHMoveBall,
  BH::BHMoveShell,
  BH::BHMoveBrownian,
  BH::BHMoveBrownianSurface,
  BH::BHMoveExchangeAll,
  BH::BHMoveExchangeMix,
  BH::BHMoveExchangeInBulk,
  BH::BHMoveExchangeOnSurface,
  BH::BHMoveExchangeSeparate,
  BH::BHMoveExchangeCoreShell,
  BH::BHMoveExchangeInterface) {
  GIVEN ("The classic Ag27Cu7 cluster, a BHMP, two rng engine intializated in "
         "the same way and a move") {
    BH::BHMetalParameters bhmp = getBHMPWithAgCu ();
    BH::BHCluster cl (getAg27Cu7 ());
    BH::BHClusterAnalyser analyser{{}, bhmp};
    constexpr unsigned RNGseed = 123456u;
    BH::rndEngine rng_original (RNGseed);
    BH::rndEngine rng_copy (RNGseed);
    TestType move_original;
    bhmp.setInteractionLabels (cl);
    move_original.parse (TestType::DefaultString ());
    move_original.Specialize (cl, bhmp);
    WHEN ("The move is copied") {
      TestType move_copy (move_original);
      THEN (
        "Both the original move and the new move should give the same result") {
        BH::BHCluster cl_original (cl);
        BH::BHCluster cl_copy (cl);
        move_original.doMove (cl, cl_original, analyser, bhmp, rng_original);
        move_copy.doMove (cl, cl_copy, analyser, bhmp, rng_copy);
        REQUIRE (cl_copy == cl_original);
        // and again
        move_original.doMove (cl, cl_original, analyser, bhmp, rng_original);
        move_copy.doMove (cl, cl_copy, analyser, bhmp, rng_copy);
        REQUIRE (cl_copy == cl_original);
        // and again
        move_original.doMove (cl, cl_original, analyser, bhmp, rng_original);
        move_copy.doMove (cl, cl_copy, analyser, bhmp, rng_copy);
        REQUIRE (cl_copy == cl_original);
        // and again
        move_original.doMove (cl, cl_original, analyser, bhmp, rng_original);
        move_copy.doMove (cl, cl_copy, analyser, bhmp, rng_copy);
        REQUIRE (cl_copy == cl_original);
        // and again
        move_original.doMove (cl, cl_original, analyser, bhmp, rng_original);
        move_copy.doMove (cl, cl_copy, analyser, bhmp, rng_copy);
        REQUIRE (cl_copy == cl_original);
        // Maybe this is a lot too more redundant
      }
    }
  }
}

TEMPLATE_TEST_CASE (
  "Cloning moves",
  "[moves]",
  BH::BHMoveShake,
  BH::BHMoveBonds,
  BH::BHMoveBall,
  BH::BHMoveShell,
  BH::BHMoveBrownian,
  BH::BHMoveBrownianSurface,
  BH::BHMoveExchangeAll,
  BH::BHMoveExchangeMix,
  BH::BHMoveExchangeInBulk,
  BH::BHMoveExchangeOnSurface,
  BH::BHMoveExchangeSeparate,
  BH::BHMoveExchangeCoreShell,
  BH::BHMoveExchangeInterface) {
  GIVEN ("The classic Ag27Cu7 cluster, a BHMP, two rng engine intializated in "
         "the same way and a move") {
    BH::BHMetalParameters bhmp = getBHMPWithAgCu ();
    BH::BHCluster cl (getAg27Cu7 ());
    BH::BHClusterAnalyser analyser{{}, bhmp};
    constexpr unsigned RNGseed = 123456u;
    BH::rndEngine rng_original (RNGseed);
    BH::rndEngine rng_copy (RNGseed);
    std::unique_ptr<BH::BHMove> move_original{new TestType ()};
    bhmp.setInteractionLabels (cl);
    move_original->parse (TestType::DefaultString ());
    move_original->Specialize (cl, bhmp);
    WHEN ("The move is cloned") {
      std::unique_ptr<BH::BHMove> move_copy{move_original->clone ()};

      THEN (
        "Both the original move and the new move should give the same result") {
        BH::BHCluster cl_original (cl);
        BH::BHCluster cl_copy (cl);
        move_original->doMove (cl, cl_original, analyser, bhmp, rng_original);
        move_copy->doMove (cl, cl_copy, analyser, bhmp, rng_copy);
        REQUIRE (cl_copy == cl_original);
        // and again
        move_original->doMove (cl, cl_original, analyser, bhmp, rng_original);
        move_copy->doMove (cl, cl_copy, analyser, bhmp, rng_copy);
        REQUIRE (cl_copy == cl_original);
        // and again
        move_original->doMove (cl, cl_original, analyser, bhmp, rng_original);
        move_copy->doMove (cl, cl_copy, analyser, bhmp, rng_copy);
        REQUIRE (cl_copy == cl_original);
        // and again
        move_original->doMove (cl, cl_original, analyser, bhmp, rng_original);
        move_copy->doMove (cl, cl_copy, analyser, bhmp, rng_copy);
        REQUIRE (cl_copy == cl_original);
        // and again
        move_original->doMove (cl, cl_original, analyser, bhmp, rng_original);
        move_copy->doMove (cl, cl_copy, analyser, bhmp, rng_copy);
        REQUIRE (cl_copy == cl_original);
        // Maybe this is a lot too more redundant
      }
    }
  }
}
