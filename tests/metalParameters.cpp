#include <catch2/catch_approx.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include <catch2/catch_test_macros.hpp>
#include <sstream>

#include "BHmetalParameters.hpp"
#include "testhelper.hpp"

using namespace Catch::literals;

bool operator== (
  const BH::BHSMATBInteraction &x, const BH::BHSMATBInteraction &y) {
  return x.p == Catch::Approx (y.p) && x.q == Catch::Approx (y.q) &&
         x.a == Catch::Approx (y.a) && x.qsi == Catch::Approx (y.qsi) &&
         x.NNdist == Catch::Approx (y.NNdist) &&
         x.NNdist_tolSQ == Catch::Approx (y.NNdist_tolSQ) &&
         x.cutOff_start == Catch::Approx (y.cutOff_start) &&
         x.cutOff_end == Catch::Approx (y.cutOff_end) &&
         x.cutOff_endSQ == Catch::Approx (y.cutOff_endSQ) &&
         // x.Arete == Catch::Approx (y.Arete) &&
         x.P_par5 == Catch::Approx (y.P_par5) &&
         x.P_par4 == Catch::Approx (y.P_par4) &&
         x.P_par3 == Catch::Approx (y.P_par3) &&
         x.Q_par5 == Catch::Approx (y.Q_par5) &&
         x.Q_par4 == Catch::Approx (y.Q_par4) &&
         x.Q_par3 == Catch::Approx (y.Q_par3);
}

TEST_CASE ("Load MetalParameters", "[BHMetalParameters]") {
  GIVEN ("A empty-initializated BHMetalParameter") {
    BH::BHMetalParameters bhmp;
    THEN ("Should be empty") { REQUIRE (bhmp.getNofMetals () == 0u); }
    WHEN ("BHMP is initializated with a file containga AgCu data") {
      std::stringstream ss = AgCuBHMP ();
      bhmp.loadFromStream (ss);
      THEN ("Ag and Cu interaction should have been loaded correctly") {
        REQUIRE (bhmp.IknowAbout ("Ag"));
        REQUIRE (bhmp.IknowAbout ("Cu"));
        REQUIRE (bhmp.getNofMetals () == 2);
        REQUIRE (bhmp.getNofInteractions () == 3);
        auto AgAg = bhmp.giveSMATBInteractionParameters ("AgAg");
        auto AgCu = bhmp.giveSMATBInteractionParameters ("AgCu");
        auto CuCu = bhmp.giveSMATBInteractionParameters ("CuCu");
        REQUIRE (AgAg.p == 10.85_a);
        REQUIRE (AgAg.a == 0.1031_a);

        REQUIRE (AgCu.q == 2.805_a);
        REQUIRE (AgCu.qsi == 1.2275_a);

        REQUIRE (CuCu.cutOff_start == 3.62038672_a);
        REQUIRE (CuCu.cutOff_end == 4.4340500673763259_a);
      }
      AND_WHEN ("We load an additional atom parameter") {
        std::stringstream ss = ArArBHMP ();
        bhmp.loadFromStream (ss);
        THEN ("The older data should be present") {
          REQUIRE (bhmp.IknowAbout ("Ag"));
          REQUIRE (bhmp.IknowAbout ("Cu"));
          auto AgAg = bhmp.giveSMATBInteractionParameters ("AgAg");
          auto AgCu = bhmp.giveSMATBInteractionParameters ("AgCu");
          auto CuCu = bhmp.giveSMATBInteractionParameters ("CuCu");
          REQUIRE (AgAg.p == 10.85_a);
          REQUIRE (AgAg.a == 0.1031_a);

          REQUIRE (AgCu.q == 2.805_a);
          REQUIRE (AgCu.qsi == 1.2275_a);

          REQUIRE (CuCu.cutOff_start == 3.62038672_a);
          REQUIRE (CuCu.cutOff_end == 4.4340500673763259_a);
          AND_THEN ("The LJ data should have been loaded correctly") {
            REQUIRE (bhmp.IknowAbout ("Ar"));
            auto ArAr = bhmp.giveLJInteractionParameters ("ArAr");
            REQUIRE_THROWS (bhmp.giveSMATBInteractionParameters ("ArAr"));
            REQUIRE (ArAr.epsilon == 119.8_a);
            REQUIRE (ArAr.sigma == 3.405_a);
          } // AND_THEN
        }   // THEN
      }     // AND_WHEN - load ArAr
      AND_WHEN ("We test a BHSMATBInteraction") {
        BH::BHSMATBInteraction AgAg =
          bhmp.giveSMATBInteractionParameters ("AgAg");
        THEN ("The assingment should have gone correctly") {
          REQUIRE (AgAg == bhmp.giveSMATBInteractionParameters ("AgAg"));
        }
        AND_THEN ("I can copy correctly the interaction") {
          // to make things more spicier
          AgAg.p += 0.1;
          BH::BHSMATBInteraction AgAg2;
          AgAg2 = AgAg;
          REQUIRE (AgAg == AgAg2);
          REQUIRE_FALSE (AgAg2 == bhmp.giveSMATBInteractionParameters ("AgAg"));
        }
        AND_THEN ("I can copy correctly the interaction even as a move") {
          // to make things more spicier
          AgAg.p += 0.1;
          BH::BHSMATBInteraction AgAg2, AgAg3;
          AgAg2 = AgAg;
          AgAg3 = std::move (AgAg);
          REQUIRE (AgAg3 == AgAg2);
          REQUIRE_FALSE (AgAg3 == bhmp.giveSMATBInteractionParameters ("AgAg"));
        }
      } // AND WHEN - testing testing BHSMATBInteraction
    }   // WHEN
  }     // GIVEN
}

SCENARIO ("Copying correctly BHMP", "[BHMetalPArameters") {
  GIVEN ("An intialized BHMP") {
    BH::BHMetalParameters bhmp;
    std::stringstream ss = AgCuBHMP ();
    bhmp.loadFromStream (ss);
    WHEN ("We copy ctr it in a new variable") {
      BH::BHMetalParameters bhmp_copied (bhmp);
      THEN ("it should be equal") {
        REQUIRE (bhmp.getNofMetals () == bhmp_copied.getNofMetals ());
        REQUIRE (
          bhmp.giveSMATBInteractionParameters ("AgAg") ==
          bhmp_copied.giveSMATBInteractionParameters ("AgAg"));
      }
    }
  }
}
