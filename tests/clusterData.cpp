#include "BHclusterData.hpp"
#include "testhelper.hpp"
#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>

using clusterData = BH::BHClusterData;
// Catch::literals contains _a thta can be used in place of Catch::Approx
using namespace Catch::literals;

namespace Catch {
  template <>
  struct StringMaker<clusterData> {
    static std::string convert (clusterData const &value) {
      std::string toret = "(E=" + std::to_string (value.Energy ()) + " OPs:";
      for (const auto &OP : value.orderParameters ()) {
        toret += " " + std::to_string (OP);
      }
      toret += ")";
      return toret;
    }
  };
} // namespace Catch

SCENARIO ("Testing the BHClusterData container", "[BHClusterData]") {
  GIVEN ("The data from a cluster") {
    // Mocking the results of a calculations
    BH::BHdouble EnergyMock = -10;
    std::vector<BH::BHdouble> OPsMock = {0.1, 0.0, 1.0};
    WHEN ("A BHClusterData is initalied with the data from the cluster") {
      clusterData cd (EnergyMock, OPsMock);
      THEN ("The getters should return the right numbers") {
        REQUIRE (cd.Energy () == Catch::Approx (EnergyMock));
        REQUIRE (cd.numberOfOP () == OPsMock.size ());
        for (unsigned i = 0; i < OPsMock.size (); ++i) {
          REQUIRE (cd.orderParameter (i) == Catch::Approx (OPsMock[i]));
          REQUIRE (cd.orderParameters ()[i] == Catch::Approx (OPsMock[i]));
        }
      }
      AND_WHEN ("Another BHClusterData is initialized from the first") {
        // copy ctor
        clusterData cd2 = cd;
        THEN ("They should be equal") {
          REQUIRE (cd.Energy () == Catch::Approx (cd2.Energy ()));
          REQUIRE (cd.numberOfOP () == cd2.numberOfOP ());
          for (unsigned i = 0; i < OPsMock.size (); ++i) {
            REQUIRE (
              cd.orderParameter (i) == Catch::Approx (cd2.orderParameter (i)));
          }
          // and also lets see if the operator== written for testing works
          REQUIRE (cd2 == cd);
        }
      }
      AND_WHEN ("Another BHClusterData is move-initialized from the first") {
        // move ctor with a lambda to move the result of the copy of cd
        clusterData cd2 =
          std::move ([] (clusterData &cd1) { return clusterData (cd1); }(cd));
        THEN ("They should be equal") {
          REQUIRE (cd.Energy () == Catch::Approx (cd2.Energy ()));
          REQUIRE (cd.numberOfOP () == cd2.numberOfOP ());
          for (unsigned i = 0; i < OPsMock.size (); ++i) {
            REQUIRE (
              cd.orderParameter (i) == Catch::Approx (cd2.orderParameter (i)));
          }
          // and also lets see if the operator== written for testing works
          REQUIRE (cd2 == cd);
        }
      }
      AND_WHEN ("Another BHClusterData copies from the first") {
        BH::BHdouble EnergyMock2 = -13;
        std::vector<BH::BHdouble> OPsMock2 = {0.3, 0.1};
        // this is a different BHClusterData
        clusterData cd2 (EnergyMock2, OPsMock2);
        cd2 = cd;
        THEN ("They should be equal") {
          REQUIRE (cd.Energy () == Catch::Approx (cd2.Energy ()));
          REQUIRE (cd.numberOfOP () == cd2.numberOfOP ());
          for (unsigned i = 0; i < OPsMock.size (); ++i) {
            REQUIRE (
              cd.orderParameter (i) == Catch::Approx (cd2.orderParameter (i)));
          }
          // and also lets see if the operator== written for testing works
          REQUIRE (cd2 == cd);
        }
      }
      AND_WHEN ("Another BHClusterData copies from the first") {
        BH::BHdouble EnergyMock2 = -13;
        std::vector<BH::BHdouble> OPsMock2 = {0.3, 0.1};
        // this is a different BHClusterData
        clusterData cd2 (EnergyMock2, OPsMock2);
        // move ctor with a lambda to move the result of the copy of cd
        cd2 =
          std::move ([] (clusterData &cd1) { return clusterData (cd1); }(cd));
        THEN ("They should be equal") {
          REQUIRE (cd.Energy () == Catch::Approx (cd2.Energy ()));
          REQUIRE (cd.numberOfOP () == cd2.numberOfOP ());
          for (unsigned i = 0; i < OPsMock.size (); ++i) {
            REQUIRE (
              cd.orderParameter (i) == Catch::Approx (cd2.orderParameter (i)));
          }
          // and also lets see if the operator== written for testing works
          REQUIRE (cd2 == cd);
        }
      }
      AND_WHEN ("The energy is changed") {
        BH::BHdouble newEnergyMock = EnergyMock * 0.9;
        cd.setEnergy (newEnergyMock);
        THEN ("The energy should be updated correctly") {
          REQUIRE (cd.Energy () == Catch::Approx (newEnergyMock));
        }
      }

      AND_WHEN ("The OP \"1\" is changed") {
        BH::BHdouble OP1Mock = OPsMock[1] * 0.9;
        cd.setOrderParameter (1, OP1Mock);
        THEN ("The OP[0] should be updated correctly") {
          REQUIRE (cd.orderParameter (1) == Catch::Approx (OP1Mock));
        }
      }
      AND_WHEN ("All the OPs are changed") {
        auto newOPsMock = OPsMock;
        for (auto &i : newOPsMock) {
          i *= 0.9;
        }
        cd.setOrderParameters (newOPsMock);
        THEN ("The OP[0] should be updated correctly") {
          for (unsigned i = 0; i < OPsMock.size (); ++i) {
            REQUIRE (cd.orderParameter (i) == Catch::Approx (newOPsMock[i]));
          }
        }
      }
    }
  }

  GIVEN ("A few BHClusterData objects") {
    BH::BHdouble EnergyMock = -10;
    std::vector<BH::BHdouble> OPsMock = {0.1, 0.0, 1.0};
    clusterData cd (EnergyMock, OPsMock);
    WHEN ("the data is confronted wiht a copy of itself") {
      clusterData cde = cd;
      THEN ("They should be equal") { REQUIRE_FALSE (cd.differentFrom (cde)); }
    }
    AND_WHEN ("A clusterData only different in Energy is given") {
      clusterData cd_E = cd;
      cd_E.setEnergy (cd_E.Energy () + 10.0);
      THEN ("They should be different") { REQUIRE (cd.differentFrom (cd_E)); }
    }
    AND_WHEN ("A clusterData only different in an OP is given") {
      clusterData cd_OP = cd;
      cd_OP.setOrderParameter (1, cd_OP.orderParameter (1) + 10.0);
      THEN ("They should be different") { REQUIRE (cd.differentFrom (cd_OP)); }
    }
  }
}
