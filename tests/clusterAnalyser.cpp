#include "BHclusterAnalyser.hpp"
#include "BHclusterUtilities.hpp"
#include "BHenergyCalculator_SMATB.hpp"
#include "BHmetalParameters.hpp"
#include "testhelper.hpp"
#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <iostream>

// Catch::literals contains _a thta can be used in place of Catch::Approx
using namespace Catch::literals;

std::unordered_map<short, int>
getCNASignatures (BH::BHCluster &cluster, const BH::BHMetalParameters &bhmp) {
  bhmp.calcNeighbourhood_tol (cluster);
  auto couples = BH::BHClusterUtilities::calculateCNAsignatures (cluster);
  auto signatures = BH::BHClusterUtilities::extractCNASignatures (couples);
  return signatures;
}

TEST_CASE ("ClusterInteractions", "[BHClusterAnalyser], [BHCluster]") {
  BH::BHMetalParameters bhmp = getBHMPWithAgCu ();
  BH::BHCluster c1 (getAg27Cu7 ());
  bhmp.setInteractionLabels (c1);
  {
    BH::BHCluster c2 (getAg27Cu7 ());
    // start from 0 : check if initializer read the same file
    REQUIRE (c1.getNofAtoms () == c2.getNofAtoms ());
    for (unsigned i = 0; i < c1.getNofAtoms (); ++i) {
      REQUIRE (c1[i] == c2[i]);
    }
    REQUIRE_FALSE (c1.INTlab (0, 1) == c2.INTlab (0, 1));
    REQUIRE_FALSE (c1.SAMElab (0, 1) == c2.SAMElab (0, 1));
    constexpr unsigned changeID = 6;
    c2[changeID] += BH::BHVector (1, 0, 0);
    // check if every thing not changed is the same
    for (unsigned i = 0; i < c1.getNofAtoms (); ++i) {
      if (i == changeID)
        continue;
      REQUIRE (c1[i] == c2[i]);
    }
    REQUIRE_FALSE (c1[changeID] == c2[changeID]);
    c2.copyInteractionLabelsFrom (c1);
    // check if every thing not changed is the same
    for (unsigned i = 0; i < c1.getNofAtoms (); ++i) {
      if (i == changeID)
        continue;
      REQUIRE (c1[i] == c2[i]);
    }
    REQUIRE_FALSE (c1[changeID] == c2[changeID]);

    REQUIRE (c1[changeID].Y () == Catch::Approx (c2[changeID].Y ()));
    REQUIRE (c1.INTlab (0, 1) == c2.INTlab (0, 1));
    REQUIRE (c1.SAMElab (0, 1) == c2.SAMElab (0, 1));
  }
  {
    BH::BHCluster c3 (c1);
    REQUIRE (c1.INTlab (0, 1) == c3.INTlab (0, 1));
    REQUIRE (c1.SAMElab (0, 1) == c3.SAMElab (0, 1));
    REQUIRE (c1.getNofAtoms () == c3.getNofAtoms ());
    for (unsigned i = 0; i < c1.getNofAtoms (); ++i) {
      REQUIRE (c1[i] == c3[i]);
    }
  }
  {
    BH::BHCluster c4 (c1);
    c4 = c1;
    REQUIRE (c1.INTlab (0, 1) == c4.INTlab (0, 1));
    REQUIRE (c1.SAMElab (0, 1) == c4.SAMElab (0, 1));
    REQUIRE (c1.getNofAtoms () == c4.getNofAtoms ());
    for (unsigned i = 0; i < c1.getNofAtoms (); ++i) {
      REQUIRE (c1[i] == c4[i]);
    }
  }
}

TEST_CASE ("BHClusterAnalyser", "[BHClusterAnalyser]") {
  BH::BHMetalParameters bhmp = getBHMPWithAgCu ();
  BH::BHCluster cluster (getAg27Cu7 ());
  REQUIRE (cluster.getNofAtoms () == 34);
  BH::BHEnergyCalculator_SMATB ec (cluster, bhmp);
  std::vector<BH::BHdouble> x (3 * cluster.getNofAtoms ());
  cluster.AssignToVector (x.data ());
  REQUIRE (ec.Energy (x.data ()) == -90.164962_a);
  std::vector<BH::BHOrderParameter> ops = {"555",     "421",     "422", "AgCu",
                                           "surf Ag", "surf Cu", "q4",  "q6"};
  BH::BHClusterAnalyser analyser (ops, bhmp);
  bhmp.setInteractionLabels (cluster);
  auto OPs = analyser (cluster, bhmp);
  /*
  for(unsigned i =0; i < ops.size() ; ++i) {
    std::cout << BH::OparName(ops[i])<<" " << analyser.OP(i) << std::endl;
  }
*/
  // sign 555
  REQUIRE (OPs[0] == 0.475524_a);
  // sign 421
  REQUIRE (OPs[1] == 0.0_a);
  // sign 422
  REQUIRE (OPs[2] == 0.0_a);
  // bonds AgCu
  REQUIRE (OPs[3] == 0.363636_a);
  // surf Ag
  REQUIRE (OPs[4] == 1.0_a);
  // surf Cu
  REQUIRE (OPs[5] == 0.0_a);
}

TEST_CASE ("Testing correctness of analysis", "[BHClusterAnalyser]") {
  BH::BHMetalParameters bhmp = getBHMPWithAgCu ();
  GIVEN ("A BHClusterAnalyser with signatures 666, 555,  444,  422, 421, and "
         "% of couples AgAg, AgCu, CuCu") {
    std::vector<BH::BHOrderParameter> ops (8);
    enum {
      op666 = 0,
      op555 = 1,
      op444 = 2,
      op422 = 3,
      op421 = 4,
      opAgAg = 5,
      opAgCu = 6,
      opCuCu = 7
    };
    ops[op666] = BH::BHOrderParameter ("666");
    ops[op555] = BH::BHOrderParameter ("555");
    ops[op444] = BH::BHOrderParameter ("444");
    ops[op422] = BH::BHOrderParameter ("422");
    ops[op421] = BH::BHOrderParameter ("421");
    ops[opAgAg] = BH::BHOrderParameter ("AgAg");
    ops[opAgCu] = BH::BHOrderParameter ("AgCu");
    ops[opCuCu] = BH::BHOrderParameter ("CuCu");
    BH::BHClusterAnalyser analyser (ops, bhmp);

    WHEN ("We analyse the cluster representing the minimal 421 signature") {
      BH::BHCluster cluster (getMimimumSignature421 ());
      bhmp.setInteractionLabels (cluster);
      THEN ("The OPs should be correct") {
        auto OPs = analyser (cluster, bhmp);

        REQUIRE (OPs[op666] == 0.0_a);
        REQUIRE (OPs[op555] == 0.0_a);
        REQUIRE (OPs[op444] == 0.0_a);
        REQUIRE (OPs[op422] == 0.0_a);
        REQUIRE (OPs[op421] == 0.0909090909_a);
        REQUIRE (OPs[opAgAg] == 0.1818181818_a);
        REQUIRE (OPs[opAgCu] == 0.7272727273_a);
        REQUIRE (OPs[opCuCu] == 0.0909090909_a);
      }
      AND_THEN (
        "The cluster should cointain exactly one of the chosen signature") {
        auto signatures = getCNASignatures (cluster, bhmp);
        REQUIRE (signatures.at (421) == 1);
      }
    }

    WHEN ("We analyse the cluster representing the minimal 422 signature") {
      BH::BHCluster cluster (getMimimumSignature422 ());
      bhmp.setInteractionLabels (cluster);
      THEN ("The OPs should be correct") {
        auto OPs = analyser (cluster, bhmp);

        REQUIRE (OPs[op666] == 0.0_a);
        REQUIRE (OPs[op555] == 0.0_a);
        REQUIRE (OPs[op444] == 0.0_a);
        REQUIRE (OPs[op422] == 0.0909090909_a);
        REQUIRE (OPs[op421] == 0.0_a);
        REQUIRE (OPs[opAgAg] == 0.1818181818_a);
        REQUIRE (OPs[opAgCu] == 0.7272727273_a);
        REQUIRE (OPs[opCuCu] == 0.0909090909_a);
      }
      AND_THEN (
        "The cluster should cointain exactly one of the chosen signature") {
        auto signatures = getCNASignatures (cluster, bhmp);
        REQUIRE (signatures.at (422) == 1);
      }
    }

    WHEN ("We analyse the cluster representing the minimal 555 signature") {
      BH::BHCluster cluster (getMimimumSignature555 ());
      bhmp.setInteractionLabels (cluster);
      THEN ("The OPs should be correct") {
        auto OPs = analyser (cluster, bhmp);

        REQUIRE (OPs[op666] == 0.0_a);
        REQUIRE (OPs[op555] == 0.0625_a);
        REQUIRE (OPs[op444] == 0.0_a);
        REQUIRE (OPs[op422] == 0.0_a);
        REQUIRE (OPs[op421] == 0.0_a);
        REQUIRE (OPs[opAgAg] == 0.3125_a);
        REQUIRE (OPs[opAgCu] == 0.625_a);
        REQUIRE (OPs[opCuCu] == 0.0625_a);
      }
      AND_THEN (
        "The cluster should cointain exactly one of the chosen signature") {
        auto signatures = getCNASignatures (cluster, bhmp);
        REQUIRE (signatures.at (555) == 1);
      }
    }

    WHEN ("We analyse the cluster representing the minimal 444 signature") {
      BH::BHCluster cluster (getMimimumSignature444 ());
      bhmp.setInteractionLabels (cluster);
      THEN ("The OPs should be correct") {
        auto OPs = analyser (cluster, bhmp);

        REQUIRE (OPs[op666] == 0.0_a);
        REQUIRE (OPs[op555] == 0.0_a);
        REQUIRE (OPs[op444] == 0.0769230769_a);
        REQUIRE (OPs[op422] == 0.0_a);
        REQUIRE (OPs[op421] == 0.0_a);
        REQUIRE (OPs[opAgAg] == 1.0_a);
        REQUIRE (OPs[opAgCu] == 0.0_a);
        REQUIRE (OPs[opCuCu] == 0.0_a);
      }
      AND_THEN (
        "The cluster should cointain exactly one of the chosen signature") {
        auto signatures = getCNASignatures (cluster, bhmp);
        REQUIRE (signatures.at (444) == 1);
      }
    }

    WHEN ("We analyse the cluster representing the minimal 666 signature") {
      BH::BHCluster cluster (getMimimumSignature666 ());
      bhmp.setInteractionLabels (cluster);
      THEN ("The OPs should be correct") {
        auto OPs = analyser (cluster, bhmp);

        REQUIRE (OPs[op666] == 0.0526315789_a);
        REQUIRE (OPs[op555] == 0.0_a);
        REQUIRE (OPs[op444] == 0.0_a);
        REQUIRE (OPs[op422] == 0.0_a);
        REQUIRE (OPs[op421] == 0.0_a);
        REQUIRE (OPs[opAgAg] == 1.0_a);
        REQUIRE (OPs[opAgCu] == 0.0_a);
        REQUIRE (OPs[opCuCu] == 0.0_a);
      }
      AND_THEN (
        "The cluster should cointain exactly one of the chosen signature") {
        auto signatures = getCNASignatures (cluster, bhmp);
        REQUIRE (signatures.at (666) == 1);
      }
    }
  }
}

TEST_CASE ("BHClusterAnalyser benchmark", "[BHClusterAnalyser],[.]") {
  BH::BHMetalParameters bhmp = getBHMPWithAgCu ();
  BH::BHCluster cluster (getAg27Cu7 ());

  std::vector<BH::BHOrderParameter> ops = {"555",     "421",     "422", "AgCu",
                                           "surf Ag", "surf Cu", "q4",  "q6"};
  BH::BHClusterAnalyser analyser (ops, bhmp);
  bhmp.setInteractionLabels (cluster);

  BENCHMARK ("Analyser speed") {
    analyser (cluster, bhmp);
    return;
  };
}
