#include "BHenergyCalculator_SMATB.hpp"
#include "BHminimizationAlgorithm_FIRE.hpp"
#include "BHminimizationAlgorithm_LBFGSB.hpp"

#include "testhelper.hpp"
#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/catch_approx.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include <catch2/catch_test_macros.hpp>
using namespace Catch::literals;

using BH::BHdouble;

SCENARIO ("Energy calculator can be copyed", "[energy]") {
  GIVEN ("Metal parameters and an energy calculator") {
    BH::BHMetalParameters bhmp{};
    std::stringstream ss = AgCuBHMP ();
    bhmp.loadFromStream (ss);
    BH::BHClusterAtoms cl{getAg27Cu7 ()};
    BH::BHEnergyCalculator *ec = new BH::BHEnergyCalculator_SMATB (cl, bhmp);
    BHdouble e = ec->Energy (cl);
    WHEN ("The energy calculator is copied") {
      BH::BHEnergyCalculator *ec2{ec->clone ()};
      THEN ("It should return the very same energy of the original") {
        BHdouble e2 = ec2->Energy (cl);
        REQUIRE (e2 == Catch::Approx (e));
      }
    }
  }
}

SCENARIO ("Minimization with LBFGS", "[BHLBFGSInterface]") {
  GIVEN ("A BHLBFGSInterface for a N dimensioned function") {
    unsigned N = 3;
    BH::BHWWF::BHLBFGSBInterface lbfgsInterface (N);
    WHEN ("A function is given") {
      auto f = [] (double *x, double *g) {
        g[0] = 2.0 * x[0];
        g[1] = 2.0 * x[1];
        g[2] = 2.0 * x[2];
        return x[0] * x[0] + x[1] * x[1] + x[2] * x[2];
      };
      THEN ("It should work correclty") {
        double x[3] = {5.0, -5.0, 1.0}, g[3];
        auto energy = f (x, g);
        REQUIRE (energy == 51.0_a);
        REQUIRE (g[0] == 10.0_a);
        auto energy_old = energy;
        bool work = true;
        double deltaE;
        double rtol;
        do {
          lbfgsInterface.step (energy, x, g);

          using tr = BH::BHWWF::BHLBFGSBInterface::taskResult;
          switch (lbfgsInterface.giveCurrentTask ()) {
          case tr::FG: //"FG"
            energy = f (x, g);
            break;
          case tr::NEW_X: {                          //"NEW_X"
            deltaE = std::abs (energy - energy_old); // was en-
            rtol = 2.0 * (deltaE / (std::abs (energy) +
                                    std::abs (energy_old))); // was abs(en)
            energy_old = energy;
            if ((lbfgsInterface.nstep () >= 1000) && (deltaE > 1e-9)) {
              lbfgsInterface.setTaskToSTOP ();
            } else if ((rtol < 1e-10) && (deltaE > 1e-9)) {
              lbfgsInterface.setTaskToSTOP ();
            } else if ((lbfgsInterface.projectedGradientNorm () <
                        1e-10)) { // gradient is small
              lbfgsInterface.setTaskToSTOP ();
            }
            break;
          }
          case tr::CONV: //"CONV"
          case tr::STOP: //"STOP"
            work = false;
            break;
          case tr::ERROR: //"ERROR"
          case tr::ABNO:  //"ABNO"
          case tr::WARN:  //"WARN"
          case tr::unknown:
            lbfgsInterface.setTaskToSTOP ();
            break;
          }
        } while (work);
        CHECK (energy == Catch::Approx (0.0).margin (1e-9));
        CHECK (g[0] == Catch::Approx (0.0).margin (1e-9));
        CHECK (x[0] == Catch::Approx (0.0).margin (1e-9));
      }
    }
  }
}

template <unsigned N>
class sphereN : public BH::BHEnergyCalculator {
public:
  sphereN (const BH::BHCluster &c, const BH::BHMetalParameters &bhmp)
    : BH::BHEnergyCalculator (c, bhmp){};
  BHdouble Energy (BHdouble *x) override {
    return [x] {
      BHdouble tr = 0;
      for (unsigned i = 0; i < N; ++i) {
        tr += x[i] * x[i];
      }
      return tr;
    }();
  };
  BHdouble Energy_tol (BHdouble *x) override { return Energy (x); };
  BHdouble Gradient (BHdouble *x, BHdouble *g) override {
    return [x, g] {
      BHdouble tr = 0;
      for (unsigned i = 0; i < N; ++i) {
        tr += x[i] * x[i];
        g[i] = 2.0 * x[i];
      }
      return tr;
    }();
  };
  BHdouble Gradient_tol (BHdouble *x, BHdouble *g) override {
    return Gradient (x, g);
  };
  BHEnergyCalculator *clone () override { return new sphereN<N> (*this); }
};

TEMPLATE_TEST_CASE (
  "Minimization algorithm Benchmarks",
  "[minimizations],[.]",
  BH::BHMinimizationAlgorithm_LBFGSB,
  BH::BHMinimizationAlgorithm_FIRE) {
  BH::BHMetalParameters bhmp = getBHMPWithAgCu ();
  TestType minimizer;
  BH::BHCluster cl ({"seed0.in"});
  SECTION ("Sphere minimization") {
    BHdouble *x = new BHdouble[3];
    x[0] = 1;
    x[1] = 1;
    x[2] = 1;
    BH::BHEnergyCalculator *sphere = new sphereN<3> (cl, bhmp);

    CHECK (
      minimizer.Minimization (x, 1, sphere) ==
      Catch::Approx (0.0).margin (1e-9));
    delete sphere;
    delete[] x;
  }
  SECTION ("cluster minimization") {
    BH::BHEnergyCalculator_SMATB ec (cl, bhmp);
    std::vector<double> data (cl.getNofAtoms () * 3);
    cl.AssignToVector (data.data ());
    cl[10] += BH::BHVector (-0.1, 0.0, 0.0);
    REQUIRE (
      ec.Energy (data.data ()) ==
      Catch::Approx (minimizer.MinimizeCluster (cl, &ec)));
  }
  SECTION ("Benchmarks:") {
    BENCHMARK ("Cluster Minimization") {
      BH::BHEnergyCalculator_SMATB ec (cl, bhmp);
      cl[10] += BH::BHVector (-0.1, 0.0, 0.0);
      return minimizer.MinimizeCluster (cl, &ec);
    };
  }
}
