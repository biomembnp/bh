#!/bin/bash


#uncomment and update if needed
#export PATH=$PATH:/path/to/mover/
#export PATH=$PATH:/path/to/quantumEspressoExecutables/
#this is needed by QE
export LC_ALL=C

PWCommand=/path/to/pw.x
#PWCommand=mpirun -n 24 /path/to/pw.x
mc_max=10
startFile=start.in
#QE works with Rydbergs
EnType=Ry
tempfile=nextStep

#extract configuration from a minimization
extractLast(){
    filenames=${1}
    #NF is number of fields, $NF is last column value
    #This extracts the number of atoms
    NAT=$(awk '/number of atoms/ {  print $NF }' < "$1")
    echo "$NAT"
    echo "$2"
    if grep -q "End final coordinates" "${filenames}"; then
	grep "End final coordinates" "${filenames}" -B"$NAT"|head -"$NAT"
    else 
        grep "ATOMIC_POSITIONS (angstrom)" "${filenames}" -A "$NAT" |tail -"$NAT"
    fi
}

checkPresence(){
    error=0
    for testfile in "$@"; do
	echo -n -e "\t"
	if [ ! -f "$testfile" ]; then
	    echo -n "NOT "
	    error=$(( error+1 ))
	fi
	echo -n "present:"
	echo -e "\t ${testfile}"
    done
    return "$error"
}

checkAbsence(){
    error=0
    for testfile in "$@"; do
	echo -n -e "\t"
	if [ ! -f "$testfile" ]; then
	    echo -n "not present"
	else
	    error=$(( error+1 ))
	    echo -n "PRESENT "
	fi
	echo -e ":\t ${testfile}"
    done
    return "$error"
}

checkAbsence_silent(){
    error=0
    for testfile in "$@"; do
	if [ -f "$testfile" ]; then
	    error=$(( error+1 ))
	fi
    done
    return "$error"
}

extract_options_nospaces(){
    option=$(grep "$2" "$1")
    option=${option#*=}
    option=${option//[\', ]/}
    echo $option
}

QELaunch(){
    templateFile=nextStep.in
    outdir=$(extract_options_nospaces "$templateFile" outdir)

    mkdir -p OUTPUT
    mkdir -p "$outdir"
    #resume_type=from_scratch
    inputFile=templateFile.in
    outputFile=./OUTPUT/minimization.rx.out
    cp "$templateFile" "$inputFile"
    #insert here your pw.x correct input
    ${PWCommand} -in "$inputFile" > "$outputFile"
    qe_exit=$?
    #echo "QE exited with code $qe_exit"
    #from the sourcefile of qe 6.0:
    # ... * 0: completed successfully
    # ... * 1: an error has occurred (value returned by the errore() routine)
    # ... * 2-127: convergence error
    # ... * 2: scf convergence error
    # ... * 3: ion convergence error
    # ... * 128-255: code exited due to specific trigger
    #     * 255: exit due to user request, or signal trapped, or time > max_seconds
    energy=$(if grep "Final energy" "$outputFile" -q; then
		 grep "Final energy" "$outputFile" | awk '{print $4}'
	     else 
		 grep "!" "$outputFile" |grep "total energy" |tail -1 |awk '{print $5}'
	     fi
	  )
    extractLast "$outputFile" "$energy"
    
    return $qe_exit
}

checkPresence $startFile MetalParameters.in mover.in qeTemplate.in
error=$?
if [ "$error" -ge 1 ];then
    exit 1
fi
#if  it is the fisrt step creates the headers and set up the first minimization
if [[ ! -f resume.sh ]]; then
    echo "A new BH run will be started:"
    {
	echo "looking for old files"
	checkAbsence moves.out ener_best.out ener_all.out ener_accepted.out
	error=$?

	doExit=0
	if [ ! $error -eq 0 ];then
	    echo "some old files are present, to start a new plain run you should delete them first"
	    doExit=1
	fi
	
	checkAbsence_silent ./*.xyz
	error=$?
	if [ ! $error -eq 0 ];then
	    echo "some old *.xyz files are present, to start a new plain run you should delete them first"
	    doExit=1
	fi
	
	if [[ $doExit -ne 0 ]] ; then exit 1; fi
	
	echo "no old files are present"

	if [ -f "STOP" ]; then
	    echo "STOP is in the directory i will delete it"
	    rm STOP
	fi
    } >&2
    mover --start;
    cp start.in ${tempfile}.xyz
    mc=0
    minID=0
    #saving the number of atoms
    N=$(head -1 start.in |awk '{print $1}')
else
    echo "The BH run will be resumed:"
    cat ./resume.sh
    #gets the following variables: mc, minID, lastenergy, besten, N, oldstepfile
    source ./resume.sh
fi
{
    #please be sure that the number of atoms in start.in and in the qeTemplate are the same!!!!
    sed '/ATOMIC_POSITIONS/q' < qeTemplate.in
    tail -n +3 ${tempfile}.xyz
    echo ""
    echo K_POINTS gamma
} > nextStep.in

stepfile=step"$(printf "%04d" $mc)"

QELaunch > "${stepfile}.xyz"

newenergy=$(awk 'NR==2{print }' < "${stepfile}.xyz")

#if it is the first run i need to trick the mover
if [ ! -f "resume.sh" ];then
    besten=$newenergy
    lastenergy=$newenergy
    oldstepfile=$stepfile
    T_MC=300
elif [ -f moves.out ]; then
    T_MC=$(tail -1 moves.out | awk '{print $2}')
fi

{ 
    date
    moverCommand="mover ${oldstepfile}.xyz ${stepfile}.xyz ${tempfile}.xyz ${lastenergy} ${newenergy} ${T_MC} ${EnType}"
    echo "$moverCommand"
    $moverCommand
} >> mover_out.log

moverexit=$?

needtostop=0

case $moverexit in
    0)
	#move accepted: updating the new accepted move and the energy
	oldstepfile=$stepfile
	lastenergy=$newenergy
	energyEv=$(awk 'NR==2 {for (I=1;I<=NF;I++) if ($I == "#E=") {print $(I+1)};}' < "$stepfile".xyz)
	echo "Accepted move, with energy: ${energyEv} eV, and Montecarlo's temperature ${T_MC}"
	if [ "$(echo "$newenergy"'<'"$besten" | bc -l)" -eq 1 ] ;then #new minimum found
	    #advance the minimum ID
	    minID=$(( minID+1 ))
	    minfile=glo"$(printf "%03d" $minID)"
	    #update the best energy found
	    echo "Found a new #minimum: energy: $besten -> $newenergy, mc=$mc, min=$minID"
	    besten=$newenergy
	    #copy the actual file in the minfile
	    cp "$stepfile".xyz "$minfile".xyz
	fi

	;;
    1)
	#move refused, the stepfile is saved, but the next step is based on the last accepted configuration
	if [ ! -f "resume.sh" ];then
	    needtostop=1
	    error=1
	fi
	echo "Move refused: energy:  $energyEv eV, Montecarlo's temperature $T_MC"
	;;
    3)
	#exception
	echo "Thrown an exception, look at errors log"
	needtostop=1
	error=1
	;;
    10)
	#wrong input
	echo "Wrong $MOVER inputs"
	needtostop=1
	error=1
	;;
    *)
	#other exit status
	echo "Unknow exit status from $MOVER"
	needtostop=1
	error=1
	;;
esac

if [[ -f "STOP" ]]; then 
    needtostop=1
fi

#compile the resume file

mc=$(( mc+1 ))

if [ "$mc" -ge "$mc_max" ]; then 
    needtostop=1
fi

cat <<EOF >resume.sh
mc=$mc
minID=$minID
lastenergy=$lastenergy
besten=$besten
N=$N
oldstepfile=$oldstepfile
EOF

if [ $needtostop -eq 0 ]; then
    #the launcher must be done so that it will put this script in the queque
    ./BasinHoppingWithQE.sh
else
    if [ $error -ge 1 ]; then
	echo "I've stopped with a problem!"
    fi
    echo "The basin hopping with QE is done: best energy=$besten, found $minID minima"
fi

