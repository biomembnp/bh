/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
  @brief Declaration of move Exchange Mix
   @file BHmoveExchangeMix.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 15/4/2019
   @version 0.1
   creating the move
  */
#ifdef MOVE_PARSER
MoveParser (exchangeMix, BHMoveExchangeMix)
#else
#ifndef BHMOVEEXCHANGEMIX_H
#define BHMOVEEXCHANGEMIX_H

#include "BHmoveExchange.hpp"

namespace BH {
  /**
   * @brief The Move Exchange Mix aims at mixing together atoms of different
   *kinds
   *
   *This functions makes two lists of weights, aimed to mix the two kind
   *of atoms:
   *
   *-for lighter atoms gives the more weight to atoms that have more neighbours
   *of the same kind, and if an atom is on the surface tries to bring it inside,
   *by agumenting its weight.
   *
   *  -for heavier atoms gives the more weight to atoms that have more
   *neighbours of the same kin
   */
  class BHMoveExchangeMix : public BHMoveExchange {
  public:
    BHMoveExchangeMix ();
    ~BHMoveExchangeMix () override;
    static std::string DefaultString ();
    std::unique_ptr<BHMove> clone () override;

  protected:
    bool selectAtoms (
      BHCluster &out,
      BHClusterAnalyser &,
      const BHMetalParameters &,
      rndEngine &) override;
    std::string printSettingsSpecialized () const override;
    std::string printSettingsSpecializedForInput () const override;
    bool parseSpecialized (const std::string &) override;
    unsigned int fewNeighbours_{MinimumNeighbourForFCCBulk};
    BHdouble exponentialWeight_{2};
    bool smallInside_{true};
  };
  namespace BHParsers {
    namespace BHMV {
      enum class EXCHANGEMIXvariable {
        fewNeighbours_,
        exponentialWeight_,
        smallInside_
      };
      const std::array<std::string, 3> EXCHANGEMIX = {
        "fewneighbours", // int
        "weight",        // int
        "smallinside"    // int
      };
    } // namespace BHMV
  }   // namespace BHParsers
} // namespace BH
#endif // BHMOVEEXCHANGEMIX_H
#endif
