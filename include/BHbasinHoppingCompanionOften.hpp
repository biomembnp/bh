/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Here the declaration of the basin hopping algorithm

   @file BHbasinHoppingCompanionOften.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 12/6/2019
   @version 0.13
   mke this class hinerith from an abstract class

   @date 21/3/2017
   @version 0.9
*/
#ifndef BHBasinHoppingCompanionOften_H
#define BHBasinHoppingCompanionOften_H
#include "BHabstractBasinHopping.hpp"
// pof for interrupt
namespace BH {

  /// this class handles the basin hopping algorithm
  class BasinHoppingCompanionOften : public BHAbstractBasinHopping {
  public:
    const char *logo () override;
    static const char *staticLogo ();
    BasinHoppingCompanionOften (const BHSettings &settings);
    BasinHoppingCompanionOften (BasinHoppingCompanionOften &) = delete;
    BasinHoppingCompanionOften (BasinHoppingCompanionOften &&) = delete;
    BasinHoppingCompanionOften &
    operator= (BasinHoppingCompanionOften &) = delete;
    BasinHoppingCompanionOften &
    operator= (BasinHoppingCompanionOften &&) = delete;
    ~BasinHoppingCompanionOften () override;
    const std::string BHStyleName () override;

  protected:
    void PreOutputInitialization () override;
    void Initialization () override;
    void MoveAcceptedPostProduction () override;
  };
} // namespace BH
#endif // BHBASINHOPPINGCOMPANIONOFTEN_H
