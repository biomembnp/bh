/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declarationn of class BHChargedAtom, add the carge to Atom, for
   analisys purpose
   @file BHatom.hpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 19/6/2017
   @version 0.1
*/

#ifndef BHCHARGEDATOM_H
#define BHCHARGEDATOM_H
#include "BHatom.hpp"
#include <iosfwd> //foward declaration so that iostream is included only if needeed
namespace BH {

  /// Stores the charge along with informations of a standard BHAtom
  class BHChargedAtom : public BHAtom {
  public:
    BHChargedAtom ();
    BHChargedAtom (const BHChargedAtom &);
    BHChargedAtom (BHChargedAtom &&) noexcept;
    BHChargedAtom &operator= (const BHChargedAtom &);
    BHChargedAtom &operator= (BHChargedAtom &&) noexcept;
    ~BHChargedAtom () override;
    /// Create an atom in a given position
    BHChargedAtom (std::string, BHdouble, BHdouble, BHdouble, BHdouble);

    void Charge (const BHdouble &);
    BHdouble Charge () const;
    // setting members
  private:
    BHdouble charge_{0.0};
  };
} // namespace BH

std::istream &operator>> (std::istream &, BH::BHChargedAtom &);
std::ostream &operator<< (std::ostream &, const BH::BHChargedAtom &);
#endif // BHCHARGEDATOM_H
