/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief A Class that can be used to made analysis on a number of cluster

   @file BHanalysisSupport.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 22/02/2018
   @version 0.1
*/
#ifndef BHANALYSISUPPORT
#define BHANALYSISUPPORT
#include <map>
#include <tuple> //std::tuple
#include <unordered_map>
#include <vector>

#include "BHmyMath.hpp"
#include "BHorderParameter.hpp"

namespace BH {
  /// \brief BHAnalyzedClusterData stores the data of an analyzed cluster
  ///
  /// BHAnalyzedClusterData can be used to organize the clusters without the
  /// need of storing all the clusters coordinates after the analysis
  struct BHAnalyzedClusterData {
    explicit BHAnalyzedClusterData (
      std::string Name = "", BHdouble newE = 0, int N_OPs = 0);
    // BHAnalyzedClusterData(std::string &&name, std::vector<double> &&OParray ,
    // double
    // &&E );//move constructor
    bool operator== (const BHAnalyzedClusterData
                       &);    ///< Operator== is needed for searching reasons
    std::string Name_;        ///< Filename or ID of the cluster
    BHdouble E_;              ///< Energy of the cluster
    std::vector<double> OPs_; ///< List of OPs value of the cluster
    bool ready_{false};       ///< Utility boolean
  };

  ///
  /// \brief The BHAnalysisSupport class organizes the results of cluster
  /// analysis
  ///
  class BHAnalysisSupport {
  public:
    BHAnalysisSupport ();
    void addCluster (
      const std::string &Name = "", BHdouble newE = 0, unsigned N_OPs = 0);
    void clearData ();
    size_t getNofData () const;
    std::string &getClusterName (unsigned i);
    BHdouble &getClusterOP (unsigned dataID, unsigned OPID);
    void setClusterReadyForAnalysis (unsigned dataID, bool set = true);
    // BHAnalyzedClusterData getCluster(int i) const;
    /// Loads data from one file
    void loadDataFromFile (const std::string &energyFileName, bool reset);
    /// Loads data from two files
    void loadDataFromFiles (
      const std::string &energyFileName,
      const std::string &OPFileName,
      bool reset);
    void
    oneDimensionAnalysis (const unsigned OP_selected, const unsigned nbins);
    void oneDimensionAnalysis2OutFile (const std::string &);
    // updates the two arrays with the data and returns the number of points
    void oneDimensionAnalysis2OutData (
      std::vector<BHdouble> &OPvals, std::vector<BHdouble> &Energies);
    // functions for 2D anlisys
    void
    twoDimensionAnalysis (const unsigned OP_selected[], const unsigned nbins[]);
    void twoDimensionAnalysis2OutFile (const std::string &);
    void twoDimensionAnalysis2OutData (
      std::vector<BHdouble> &OP1vals,
      std::vector<BHdouble> &OP2vals,
      std::vector<BHdouble> &Energies);

  private:
    void OrderDataByOP (const unsigned int &selOP);
    std::vector<BHAnalyzedClusterData> data_;

    /// \brief The oneDData struct organizes the analisys results in a 1D order
    /// parameter space
    struct oneDData {
      using MINID = unsigned int;
      // map by the ID of the minima, order is important
      std::map<MINID, unsigned> bestMinima;
      BHdouble step{};
      unsigned int OPsel{};
    } oneDData_;

    /// \brief The twoDData struct organizes the analisys results in a 2D order
    /// parameters space
    struct twoDData {
      using MINID = unsigned int;
      // a vector of points int the BIN-OP space
      std::vector<std::tuple<MINID, MINID, unsigned>> bestMinima;
      BHdouble step[2]{};
      unsigned int OPsel[2]{};
    } twoDData_;
    bool ready1D_{false}, ready2D_{false};
  };
} // namespace BH
#endif // BHANALYSISUPPORT
