/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declaration of class BHEnergyCalculator_SMATB_GAUSS

   some variable are a double of the ones in BHMetalParameters, this class uses
   them in a way that wants to be efficient for calculation instead of
   classification
   @file BHenergyCalculator_SMATB_GAUSS.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 10/07/20018
   @version 1.0

   addint the new variables

*/

#ifndef BHENERGYCALCULATOR_SMATB_GAUSS_H
#define BHENERGYCALCULATOR_SMATB_GAUSS_H
#include "BHenergyCalculator.hpp"
namespace BH {
  /// Inherits from BHEnergyCalculator, calculates the energy with the SMATB
  /// potential
  class BHEnergyCalculator_SMATB_GAUSS final : public BHEnergyCalculator {
  public:
    BHEnergyCalculator_SMATB_GAUSS (const BHCluster &);
    virtual ~BHEnergyCalculator_SMATB_GAUSS ();
    /// calculates and returns the energy of the cluster
    /**
       coordinates must be given following the schematic:

       - x[3*i]=atom[i].X
       - x[3*i+1]=atom[i].Y
       - x[3*i+2]=atom[i].Z
    */
    BHdouble Energy (BHdouble *x);
    BHdouble Energy_tol (BHdouble *x);
    /// calculates and returns the energy of the cluster, assigns to g the value
    /// of the gradient
    /**
       coordinates must be given following the schematic:

       - x[3*i]=atom[i].X
       - x[3*i+1]=atom[i].Y
       - x[3*i+2]=atom[i].Z
    */
    BHdouble Gradient (BHdouble *x, BHdouble *g);
    BHdouble Gradient_tol (BHdouble *x, BHdouble *g);

  private:
    // calculation aid:
    BHdouble *sigma2_, *twosigma2_;
    bool *useGauss_;
    BHdouble *den_,
      **Fb_; // maybe this coud evolve in a different matrix
    // energy parameters
    // BHdouble  *_;
    BHdouble *G_, *gauss_dist_;
    BHdouble *p_, *q_, *a_, *xi_, *NNd_, *cs_, *ce_, *ce2_, *P5_, *P4_, *P3_,
      *Q5_, *Q4_, *Q3_;
  };
} // namespace BH
#endif // BHENERGYCALCULATOR_SMATB_GAUSS_H
