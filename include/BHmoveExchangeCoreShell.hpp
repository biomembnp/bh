/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
  @brief Declaration of move Exchange CoreShell
   @file BHmoveExchangeCoreShell.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 12/4/2019
   @version 0.1
   creating the move
  */
#ifdef MOVE_PARSER
MoveParser (exchangeCoreShell, BHMoveExchangeCoreShell)
#else
#ifndef BHMOVEEXCHANGECORESHELL_H
#define BHMOVEEXCHANGECORESHELL_H
#include "BHmoveExchange.hpp"

namespace BH {

  /// \brief The move Core-Shell eschamge atoms from core to the surface
  /**
   * This movemakes a list of light atoms that are on the surface
   * and a list of heavy atoms that are in the core of the system.
   * it can me set to do the opposite
   */
  class BHMoveExchangeCoreShell : public BHMoveExchange {
  public:
    BHMoveExchangeCoreShell ();
    ~BHMoveExchangeCoreShell () override;
    static std::string DefaultString ();
    std::unique_ptr<BHMove> clone () override;

  protected:
    bool selectAtoms (
      BHCluster &out,
      BHClusterAnalyser &,
      const BHMetalParameters &,
      rndEngine &) override;
    std::string printSettingsSpecialized () const override;
    bool parseSpecialized (const std::string &) override;
    std::string printSettingsSpecializedForInput () const override;
    unsigned int fewNeighbours_{11}, NNforHeavy_{1}, NNforLight_{1};
  };
  namespace BHParsers {
    namespace BHMV {
      enum class EXCHANGECORESHELLvariable {
        fewNeighbours_,
        NNforHeavy_,
        NNforLight_
      };
      const std::array<std::string, 3> EXCHANGECORESHELL = {
        "fewneighbours", // int
        "nnforheavy",    // int
        "nnforlight"     // int
      };
    } // namespace BHMV
  }   // namespace BHParsers
} // namespace BH
#endif // BHMOVEEXCHANGECORESHELL_HPP
#endif
