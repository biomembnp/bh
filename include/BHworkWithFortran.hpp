/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Contains the definitions of the Fortran's subroutines to be used in
   C++
   @file BHworkWithFortran.hpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 7/11/2017
   @version 1.0

   Added typedef for compatibility issues

   @date 21/3/2017
   @version 0.9
*/

#ifndef WORKWITHFORTRAN_H
#define WORKWITHFORTRAN_H
#include "BHcluster.hpp"
#include "BHmetalParameters.hpp"
#include "BHmyMath.hpp"

namespace BH {
  /// This namespace protect the fortran interface
  /**Here all the function thata are included in the fortran part of the project
     are interfaced with C++.

     So you can find the functions and the subroutines from bhmodule and the
     minimization interfaces with lbfgsb.

     Moreover here some type are defined in order to simplify the transition
     between fortran and c++
*/
  namespace BHWWF {
    /// sends clusterinfo and metalparameters to fortran
    void sendClusterToFortran (
      BHClusterAtoms &cluster, const BHMetalParameters &bhmp);

    using integer = int;
    using real = float;
    using real8 = double;
    using logical = int;
    using character = char;

    extern "C" {
    void BHforces (double x[], double forces[]);
    double BHGradient (double x[], double forces[]);
    void BHSetNeigh_tol (double x[]);
    double BHGradient_tol (double x[], double forces[]);
    double BHminimization (double x[]);
    double BHminimization_tol (double x[]);
    void BHInterface_SetCluster (
      int &N, int types[]); // setting only nat3d and the type of atoms
    void BHInterface_SetPrivatePars (
      int &Ntypes,
      double Coh[],
      double Radius[],
      double Mass[],
      int interactions[]);
    void BHInterface_SetCouplePars (
      int &Ninteractions,
      double p[],
      double q[],
      double a[],
      double qsi[],
      double A5[],
      double A4[],
      double A3[],
      double X5[],
      double X4[],
      double X3[],
      double dist[],
      double cutOff_start[],
      double cutOff_end[]);
    void BHInterface_CleanMemory ();
    double BHenergy (double x[]);
    double BHenergy_noCutOff (double x[]);

    void setulb_ (
      integer *n,
      integer *m,
      real8 x[],
      real8 l[],
      real8 u[],
      integer nbd[],
      real8 *f,
      real8 g[],
      real8 *factr,
      real8 *pgtol,
      real8 wa[],
      integer iwa[],
      char task[],
      integer *iprinteger,
      character csave[],
      logical lsave[],
      integer isave[],
      real8 dsave[]);
    }

    /// this class is an interface to the lbfgs
    class BHLBFGSBInterface final {
    public:
      enum class taskResult {
        FG,
        NEW_X,
        ERROR,
        ABNO,
        WARN,
        CONV,
        STOP,
        unknown
      };
      BHLBFGSBInterface (unsigned N, unsigned M = 7);
      ~BHLBFGSBInterface ();
      void step (BHWWF::real8 inputEnergy, BHWWF::real8 *x, BHWWF::real8 *g);
      void setTaskToSTART ();
      void setTaskToSTOP ();
      void setVerbosity (int v);
      void setFunctionTolerance (BHdouble ftol);
      void setGradientTolerance (BHdouble pgtol);
      void setLowerBoundFor (unsigned i, BHdouble Lower);
      void
      setUpperAndLowerBoundsFor (unsigned i, BHdouble Upper, BHdouble Lower);
      void setUpperBoundFor (unsigned i, BHdouble Upper);
      void setNoBoundsFor (unsigned i);
      int nstep () const;
      int nFunctionEvaluationTotal () const;
      int nFunctionIterationCurrentStep () const;
      BHdouble projectedGradientNorm () const;
      BHdouble fValuePreviousStep () const;
      taskResult giveCurrentTask () const;
      // these getters return a readoly access to the internal memory
      const char *getTask () const;
      const BHWWF::logical *getLsave () const;
      const BHWWF::integer *getIsave () const;
      const BHWWF::real8 *getDsave () const;

    private:
      /*
       #################################################
       ##NB: made for work with 2.1 l-bfgs-b algorithm##
       #################################################
       nc = not change
       n variables
       m corrections  between 3 and 20 nc
       x[n] array double
       l[n] array lowerbounds nc nd if not used
       u[n] array upperboounds nc nd if
       nbd[n] integer: dimension n (defines the bounds) 0= un bounded
       f is the function value, user set  when task[0:1]=="FG"
       g[n] is the gradient, userset when task[0:1]=="FG"
       factr is a DOUBLE PRECISION variable that must be set by the user.
       *  It is a tolerance in the termination test for the algorithm.
       *  The iteration will stop when
       *  (f^k - f^{k+1})/max{|f^k|,|f^{k+1}|,1} <= factr*epsmch
       *  factr=1.d+12 for low accuracy;
       *        1.d+7  for moderate accuracy;
       *        1.d+1  for extremely high accuracy.
       *  The user can suppress this termination test by setting factr=0.
       pgtol >= 0 userchoiche: iteration stops when the projectec gradient
       rurpass this wa[(2*mmax + 4)*nmax + 12*mmax*mmax + 12*mmax] workspace,
       user do not work with this iwa[3nmax] workspace task[60] charaters
       comunication with user iprint set the verbosity: <0 for zero verbosity
       csave[60] char
       lsave[4] bool task==NEW_X gives infos
       isave[44] int  informations:
       *    isave(30) = the current iteration number;
       *    isave(34) = the total number of function and gradient evaluations;
       *    isave(36) = the number of function value or gradient evaluations in
       the current iteration;
       *    isave(38) = the number of free variables in the current iteration;
       *    isave(39) = the number of active constraints at the current
       iteration; dsave[29] double workarray
       *     On exit with task = 'NEW_X', it contains information that the user
       may want to access:
       *     dsave(2)  = the value of f at the previous iteration;
       *     dsave(5)  = the machine precision epsmch generated by the code;
       *     dsave(13) = the infinity norm of the projected gradient;
       */
      const unsigned n;
      const unsigned m;

      real8 *lowerBounds_{nullptr};
      // BHWWF::real8 *lowerBounds_{new BHWWF::real8[n]};
      real8 *upperBounds_{nullptr};
      integer *boundsInfo_{nullptr};
      integer *intWorkArray_{nullptr};

      real8 factr_{1.0};
      real8 pgtol_{0.0};
      real8 *workArray_{nullptr};
      character *task_{new BHWWF::character[60]};
      character *csave_{new BHWWF::character[60]};
      integer verbosityFlag_{-1};
      logical *lsave_{new BHWWF::logical[4]};
      integer *integerInfos_{new BHWWF::integer[44]};
      real8 *doubleInfos_{new BHWWF::real8[29]};
    };

  } // namespace BHWWF
} // namespace BH
#endif // WORKWITHFORTRAN_H
