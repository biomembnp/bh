/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declaration of the class BHConfigurationOperator

   @file BHconfigurationOperator.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 19/11/2019
   @version 0.1

   This class should be a pathc to simplify how the walker class works.
   And to render the walker class easier to decline with different types of
   approach for energy calculation Accumulating data from the walker here
*/
#ifndef BHWALKER_H
#define BHWALKER_H
#include "BHclusterAtoms.hpp"
#include "BHclusterData.hpp"
#include "BHminimizationAlgorithms.hpp"
#include "BHmove.hpp"
#include "BHoperationsInOPSpace.hpp"
#include "BHwalkerAlgorithm.hpp"
#include "BHwalkerSupport.hpp"
#include <vector>

namespace BH {
  class BHWalker {
  public:
    using BHWalkerAlgorithm = BHWalkerAlgorithms::BHWalkerAlgorithm;
    BHWalker ();
    BHWalker (
      const BHWalkerSettings &walkersettings,
      const BHClusterAtoms &seed,
      const BHMetalParameters &bhmp,
      BHWalkerAlgorithm algorithm,
      const bool standaloneAlgorithm = false,
      const bool skipStep = false);
    BHWalker (const BHWalker &);
    BHWalker (BHWalker &&) noexcept;
    BHWalker &operator= (const BHWalker &) = delete;
    BHWalker &operator= (BHWalker &&) noexcept = delete;
    virtual ~BHWalker ();
    /// plots the legend of the clusterdata ouput
    static std::string lastMoveInfo_legenda ();
    std::string lastMoveInfo () const;
    bool IsLastMoveEschange () const;
    void movesCount (std::ostream &) const;
    int getTotalMovesDone () const;

    // These setters should not be used in a normal BH workflow: are useful for
    // debugging o for forcing parameters in specific situations, so I made them
    // clunky to be used
    static void
    forceLastMoveOPs (BHWalker &walker, std::vector<BHdouble> &newOPs);
    static void
    forceLastMoveOPs (BHWalker &walker, BHdouble newOP0, BHdouble newOP1);
    static void forceLastMoveEnergy (BHWalker &walker, BHdouble newEnergy);
    static void
    forceLastAcceptedOPs (BHWalker &walker, std::vector<BHdouble> &newOPs);
    static void
    forceLastAcceptedOPs (BHWalker &walker, BHdouble newOP0, BHdouble newOP1);
    static void forceLastAcceptedEnergy (BHWalker &walker, BHdouble newEnergy);

    void
    CollapseIslandsOnWorkingConf (const BHMetalParameters &bhmp, rndEngine &);
    unsigned CountIslandsOnWorkingConf (const BHMetalParameters &bhmp);
    // OPspace getters
    // BHdouble &lastAcceptedEnergy();
    BHdouble lastAcceptedEnergy () const;
    BHdouble lastAcceptedOP (const unsigned &) const;
    BHClusterData lastAcceptedClusterData () const;
    const BHCluster &lastAcceptedConf () const;

    BHdouble lastMoveEnergy () const;
    BHdouble lastMoveOP (const unsigned &) const;
    BHClusterData lastMoveClusterData () const;
    const BHCluster &lastMoveConf () const;

    // not owning pointers
    BHdouble
    minimizeWorkingConf (BHMinimizationAlgorithm *, BHEnergyCalculator *);
    BHdouble
    minimizeTolWorkingConf (BHMinimizationAlgorithm *, BHEnergyCalculator *);

    // The side effect of these function are the update of the NNlabels in the
    // walkers
    std::vector<BHdouble>
    AnalyzeWorkingConf (BHClusterAnalyser &, const BHMetalParameters &bhmp);
    std::vector<BHdouble> AnalyzeLastAcceptedConf (
      BHClusterAnalyser &, const BHMetalParameters &bhmp);

    std::vector<BHdouble> ForceAnalyzeWorkingConf (
      BHClusterAnalyser &analysis, const BHMetalParameters &bhmp);

    /// confront if the walkers are neighbour in the OP space
    bool isNeighbour (const BHWalker &) const;
    bool isInForbiddenRegion () const;
    const std::vector<unsigned> &interactWith () const;
    BHdouble getWalkerRepulsion () const;

    bool ignoreStep () const;
    bool hasStandaloneAlgorithm () const;

    /**@brief calls the algorithm and then calls verifyStep
     *
     * confirmStep does automatically the step
     *Algorithm->verifyStep->analysis->acceptance and return true if the move
     *has been accepted
     *
     *
     * The order of calling function should be:
     * -doMove
     * -external minimization of **WorkingConf_**
     * -Algorithm
     * -verifyStep
     * -eventually analysis of the WorkingConf_ and moveAccepted
     * -post processing
     * or
     * -doMove
     * -external minimization of **WorkingConf_**
     * -confirmStep
     * -post processing
     **/
    virtual bool confirmStep (
      std::vector<BHWalker> &walkers,
      const BHMetalParameters &bhmp,
      BHWalkerSupport::BHWalkerAlgorithmSupport &support,
      rndEngine &rng);
    /// does a move chosen randomly on the LastAcceptedConf_, the result is
    /// stored in WorkingConf_
    virtual BHdouble doMove (
      BHClusterAnalyser &Analyzer,
      const BHMetalParameters &bhmp,
      rndEngine &rng);

    /// \brief Algorithm is the Strategy function: must be defined in the
    /// children classes
    /// \param walkers is a list of pointer to the walkers that runs in the
    /// minimization
    /// \param rng is the reference to the minimization's random number engine
    /// if the algorithm would need it
    /// \return true if the algorithm has accepted the step
    BHdouble Algorithm (
      std::vector<BHWalker> &walkers,
      const BHMetalParameters &bhmp,
      BHWalkerSupport::BHWalkerAlgorithmSupport &parameters,
      rndEngine &rng);
    /// Montecarlo verifications happens here
    virtual bool verifyStep (rndEngine &rng, BHdouble EnergyModifier = 0.0);
    void moveAccepted ();

    void times (std::ostream &stream);
    bool isWorkingConfAnalysed () const;
    void reorderAndInitializeLabels (const BHMetalParameters &bhmp);
    void copyAllConfigurations (const BHWalker &);
    void copyAcceptedConfiguration (const BHWalker &);

    BHwalkerAlgorithmType myAlgorithm () const;

    void printSettings ();
    void setDepth (unsigned);
    /// standard walker has a depth of 0
    /// companion walkers have a depth of 1 and so on
    unsigned depth_{0};
    // some ids to be used during FHL minimzations
    unsigned flyingID_{};
    unsigned landingID_{};
    unsigned hikingID_{};
    std::function<void ()> postAcceptedActions_{[] {}};
    std::string identificator{""};

  protected:
    BHCluster WorkingConf_{}; ///< Stores the configuration on wich make moves,
                              ///< analysis and minimization
    BHCluster LastAcceptedConf_{}; ///< Stores the last accepted configuration

    BHClusterData lastAcceptedData_{0.0, {0.0, 0.0}};
    // BHdouble lastAcceptedEnergy_{};
    // std::vector<BHdouble> lastAcceptedOPs_{0.0, 0.0};

    BHClusterData lastMoveData_{0.0, {0.0, 0.0}};
    // BHdouble lastMoveEnergy_{};
    // std::vector<BHdouble> LastMoveOPs_{0.0, 0.0};

    const BHOperationsInOPSpace walkerSettings_{};
    void copyMovesFrom (const BHWalker &other);
    BHWalkerAlgorithm Algorithm_;
    BHRealRND Metropolis_{0.0, 1.0};
    std::vector<std::unique_ptr<BHMove>> moves_{};
    BHdouble TAccept_{};
    BHmoveSelector rnd_move{1.0};
    unsigned int chosenMove_{}; ///< The last chosen move
    // times
    tp start{}, ///< timepoint for stroring initial time in calculations
      stop{};   ///< timepoint for stroring end time in calculations
    ms move_time{std::chrono::steady_clock::duration::zero ()};
    int n_moves{0};
    /// tells if the current workingConf_ is already been analized
    bool workingConfAnalysed_{false};

    /// if true this walker will be skipped during the step advancing
    bool ignoreSteps_{false};
    /// if true means that the walker's algorithm does not need other
    /// walkers to work
    bool isStandalone_{false};
    bool isReorderedAndInitialized_{false};
  };
} // namespace BH
#endif // BHWALKER_H
