/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
 * @brief The only purpose of this file is to have all of the moves in one place
 * @file BHmoves.hpp
 */
#include "BHmoveShake.hpp"

#include "BHmoveBonds.hpp"

#include "BHmoveBall.hpp"

#include "BHmoveShell.hpp"

#include "BHmoveBrownian.hpp"

#include "BHmoveBrownianSurface.hpp"

#include "BHmoveExchangeAll.hpp"

#include "BHmoveExchangeMix.hpp"

#include "BHmoveExchangeInBulk.hpp"

#include "BHmoveExchangeOnSurface.hpp"

#include "BHmoveExchangeSeparate.hpp"

#include "BHmoveExchangeCoreShell.hpp"

#include "BHmoveExchangeInterface.hpp"
