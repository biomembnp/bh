/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Here the declaration of the basin hopping spawner algorithm

   @file BHbasinHopping.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 12/6/2019
   @version 0.13
   mke this class hinerith from an abstract class

   @date 21/3/2017
   @version 0.9
*/
#ifndef BHBASINHOPPINGSPAWNER_H
#define BHBASINHOPPINGSPAWNER_H
#include "BHabstractBasinHopping.hpp"

namespace BH {

  /// @brief this class handles the basin hopping spawneralgorithm
  ///
  /// The Spawner algorithm spawn a new walker each time that a new global
  /// minimum is found
  class BasinHoppingSpawner : public BHAbstractBasinHopping {
  public:
    BasinHoppingSpawner (const BHSettings &settings);
    BasinHoppingSpawner (BasinHoppingSpawner &) = delete;
    BasinHoppingSpawner (BasinHoppingSpawner &&) = delete;
    BasinHoppingSpawner &operator= (BasinHoppingSpawner &) = delete;
    BasinHoppingSpawner &operator= (BasinHoppingSpawner &&) = delete;
    ~BasinHoppingSpawner () override;
    const std::string BHStyleName () override;

  protected:
    void Initialization () override;
    void MoveAcceptedPostProduction () override;
  };
} // namespace BH
#endif // BHBASINHOPPINGSPAWNER_H
