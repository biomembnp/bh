/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declaration of class BHMinimizationAlgorithm_LBFGSB_FortranInterface

   This class cointain the

   @file BHminimizationAlgorithm_LBFGSB_FortranInterface.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 12/11/2019
   @version 1.0

   Declaration is complete
*/
#ifdef MINALGO_PARSER
MinAlgorithmParser (
  LBFGSBfortran, BHMinimizationAlgorithm_LBFGSB_FortranInterface)
#else
#ifndef BHMINIMIZATIONALGORITHM_LBFGSB_FORTRANINTERFACE_HPP
#define BHMINIMIZATIONALGORITHM_LBFGSB_FORTRANINTERFACE_HPP
#include "BHminimizationAlgorithm.hpp"

namespace BH {
  /// Wraps the LBFGSB fortran algoritm
  class BHMinimizationAlgorithm_LBFGSB_FortranInterface
    : public BHMinimizationAlgorithm {
  public:
    BHMinimizationAlgorithm_LBFGSB_FortranInterface ();
    ~BHMinimizationAlgorithm_LBFGSB_FortranInterface () override;
    BHdouble
    Minimization (BHdouble *x, const unsigned &, BHEnergyCalculator *) override;
    BHdouble Minimization_tol (
      BHdouble *x, const unsigned &, BHEnergyCalculator *) override;
    BHdouble MinimizeCluster (BHClusterAtoms &, BHEnergyCalculator *) override;
    BHdouble
    MinimizeCluster_tol (BHClusterAtoms &, BHEnergyCalculator *) override;
  };
} // namespace BH
#endif // BHMINIMIZATIONALGORITHM_LBFGSB_FORTRANINTERFACE_HPP
#endif
