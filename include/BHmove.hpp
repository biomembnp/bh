/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/** @brief contain the declaration ofthe cabstract class BHMove
  @file BHmove.hpp

    @date 3/4/2019
    @version 0.1
    class defined
  */
#ifndef BHMOVE_H
#define BHMOVE_H
#include "BHcluster.hpp"
#include "BHclusterAnalyser.hpp"
#include "BHmetalParameters.hpp"
#include "BHmyMath.hpp"
#include "BHrandom.hpp"
#include <array>
#include <memory>

namespace BH {
  /// THe abstract class for the moves
  class BHMove {
  public:
    BHMove (std::string name, const bool isExchange = false);
    BHMove () = delete;
    BHMove (const BHMove &);
    BHMove (BHMove &&) = delete;
    BHMove &operator= (const BHMove &) = delete;
    BHMove &operator= (BHMove &&) = delete;

    virtual std::unique_ptr<BHMove> clone () = 0;
    BHdouble getAcceptanceTemperature ();
    BHdouble getProbability ();
    unsigned int getMoveCount ();
    unsigned int getAcceptedCount ();
    bool isExchange ();
    /// increments the accepted_ counter, moves get accepted outside of this
    /// class
    void hasBeenAccepted ();
    virtual ~BHMove ();
    BHdouble doMove (
      BHCluster &in,
      BHCluster &out,
      BHClusterAnalyser &,
      const BHMetalParameters &,
      rndEngine &);
    bool parse (const std::string &);
    virtual std::string name () const;
    const std::string &typeName () const;
    const std::string &descriptor () const;
    static std::string printSettingslegend ();
    virtual std::string printSettings () const;
    virtual std::string printSettingsForInput () const;
    /// This function need to implement some specialization in various moves
    virtual void Specialize (const BHCluster &in, const BHMetalParameters &);
    virtual void writeStatistics (std::ostream &stream) const;
    static std::string DefaultString ();

  protected:
    virtual bool doMoveAlgorithm (
      BHCluster &in,
      BHCluster &out,
      BHClusterAnalyser &,
      const BHMetalParameters &,
      rndEngine &) = 0;
    /// The member that can be overloaded for specialized Moves, called in parse
    virtual bool parseSpecialized (const std::string &);
    /// This function contain the actions to do afer the parsing procedure, it
    /// can be specialized in the derived class
    virtual bool parseAfter ();
    /// This member but offers the parser for probability and accept temperature
    /// as it is, called in parse
    virtual bool parseProbAndTAcc (const std::string &);
    /// add a line with the settings to printsettings()
    virtual std::string printSettingsSpecialized () const;
    virtual std::string printSettingsSpecializedForInput () const;

  private:
    /// the name of the move
    const std::string name_{};
    /// the raw option string
    std::string rawOptions_{};
    /// the modifier of the name of the move, in case one wants to use more tha
    /// one move of the same kind
    std::string descriptor_{};
    /// the probability of the move
    BHdouble probability_{0.};
    /// the Temperature to accept the move
    BHdouble acceptTemperature_{0.0};
    unsigned count_{0};
    unsigned accepted_{0};
    // some options for the move
    const bool isExchange_{false};
  };

  const std::array<std::string, 6> _BHMVParser_StringsForBASE = {
    "p",          "prob", // double //0,1
    "t",                  // 2
    "acctemp",            // double //3
    "descriptor",         // 4
    "aka"                 // 5 string
  };
} // namespace BH
#endif // BHMOVE_H
