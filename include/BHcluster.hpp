/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declaration of class BHCluster that unifies BHNeighboursInfo and
   BHClusterAtoms
   @file BHcluster.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 6/3/2017
   @version 0.9.1

   added BHIsland namespace for analysis purpose

   @date 21/3/2017
   @version 0.9
*/
#ifndef BHCLUSTER_H
#define BHCLUSTER_H
#include <vector>

#include "BHclusterAtoms.hpp"

namespace BH {
  using vectorAtomID = std::vector<unsigned int>;
  /// Stores a cluster of atoms and the data relative to them
  class BHCluster : public BHClusterAtoms {
  public:
    BHCluster ();
    BHCluster (const BHClusterAtoms &);
    BHCluster (BHClusterAtoms &&) noexcept;
    BHCluster (const BHCluster &);
    BHCluster (BHCluster &&) noexcept;
    BHCluster (const unsigned int NAtoms);
    ~BHCluster () override;
    void setNofAtoms (const unsigned) override;
    BHCluster &operator= (const BHCluster &);
    BHCluster &operator= (BHCluster &&) noexcept;
    /*BHCluster& operator<< (const BHCluster&);*/
    void setINTlab (const unsigned i, const unsigned j, const unsigned INT);
    unsigned INTlab (const unsigned i, const unsigned j) const;
    void setSAMElab (const unsigned i, const unsigned j, const unsigned SAME);
    unsigned SAMElab (const unsigned i, const unsigned j) const;
    void copyInteractionLabelsFrom (const BHCluster &);
    void clear_NN ();
    void shrink_NN_to_fit ();
#ifdef COMPILEBHWITHSINGLENNDEFINTIONS
    void addNN (const unsigned atomID, const unsigned neighID);
    void addNN_same (const unsigned atomID, const unsigned neighID);
    void addNN_diff (const unsigned atomID, const unsigned neighID);
#endif
    // these function ensure that the NN list are ordered
    void addNNcouple (const unsigned firstID, const unsigned secondID);
    void addNNcouple_same (const unsigned firstID, const unsigned secondID);
    void addNNcouple_diff (const unsigned firstID, const unsigned secondID);
    void addINT (const unsigned atomID, const unsigned neighID);
    const vectorAtomID &getNNs (const unsigned) const;
    const vectorAtomID &getNNs_same (const unsigned) const;
    const vectorAtomID &getNNs_diff (const unsigned) const;
    const vectorAtomID &getInteracting (const unsigned) const;

  protected:
    /// Stores the interaction index between the atoms
    unsigned **InteractionLabels{nullptr};
    /// Stores 1 when atoms are different, useful in analysis
    unsigned **notOfSameKind{nullptr};
    /// list of the indexes of the nearest neighbours
    vectorAtomID *NNlist{nullptr};
    /// list of the indexes of the nearest neighbours of the same type
    vectorAtomID *NNlist_same{nullptr};
    /// list of the indexes of the nearest neighbours not of the same type
    vectorAtomID *NNlist_diff{nullptr};
    /// list of the indexes of the atoms that are nearer than the cutoff
    /// distance
    vectorAtomID *InteractingList{nullptr};
  };

  // IO
  // std::istream& operator>> (std::istream&, BHCluster&);
  // std::ostream& operator<< (std::ostream&, const BHCluster&);
} // namespace BH
#endif // BHCLUSTER_H
