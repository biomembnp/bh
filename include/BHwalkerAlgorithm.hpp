/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#ifndef BHWALKERALGORITHM_HPP
#define BHWALKERALGORITHM_HPP
#include "BHwalkerSupport.hpp"
#include <functional>

namespace BH {
  // forward declarations
  class BHWalker;
  // namespace  BHWalkerSupport{class BHWalkerAlgorithmSupport;}
  /// @brief This namespace contains the functions that define the algorithm
  /// that can act on each walker
  namespace BHWalkerAlgorithms {
    /// Usage:
    /// @param workon is the BHWalker on which the algorithm is
    /// currently working
    ///@param  worklist is the list of the BHWalker that are used
    /// in the run,
    /// @param bhmp contains the atom parameters that will be used by the
    /// algorithm
    ///@param supportObj is a BHWalkerSupport::BHWalkerAlgorithmSupport, a
    /// struct that contains the things that an algorithm can use
    /// @param rng is the random number engine
    /// @return  the energy modification that will modify the  probability of
    /// acceptance in the montecarlo move
    using BHWalkerAlgorithm = std::function<BHdouble (
      BHWalker &workon,
      std::vector<BHWalker> &worklist,
      const BHMetalParameters &bhmp,
      BHWalkerSupport::BHWalkerAlgorithmSupport &supportObj,
      rndEngine &rng)>;
  } // namespace BHWalkerAlgorithms
} // namespace BH
#endif // BHWALKERALGORITHM_HPP
