/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Contains the typedef of various random utilities
   @file BHrandom.hpp
   @author Daniele Rapetti (iximie@gmail.com)

   @date 7/03/2018
   @version 1.0

   Creation of the file, moving definition from BHWalker.hpp to here

*/
#ifndef BHRANDOM_H
#define BHRANDOM_H
#include <random>

#include "BHmyMath.hpp"
#include "BHversion.hpp"

/// This namespace includes and protects all function, classes, variables and
/// definitions for this suite
namespace BH {
  using BHRealRND = std::uniform_real_distribution<BHdouble>;
  using BHIntRND = std::uniform_int_distribution<int>;
  using BHUIntRND = std::uniform_int_distribution<unsigned int>;
  using BHULongRND = std::uniform_int_distribution<unsigned long>;
  using BHmoveSelector =
    std::discrete_distribution<unsigned>; // this take the weights and forms a
                                          // discrete distribution
  using rndEngine = std::mt19937_64;      // mersenne twister 64 bit
} // namespace BH
#endif // BHRANDOM_H
