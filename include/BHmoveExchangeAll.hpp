/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
  @brief Declaration of move Exchange All
   @file BHmoveExchangeAll.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 11/4/2019
   @version 0.1
   creating the move
  */
#ifdef MOVE_PARSER
MoveParser (exchangeAll, BHMoveExchangeAll)
#else
#ifndef BHMOVEEXCHANGEALL_H
#define BHMOVEEXCHANGEALL_H
#include "BHmoveExchange.hpp"
#include <vector>

namespace BH {
  /// \brief The exchange all move exchange two rendom atoms of different kind
  class BHMoveExchangeAll : public BHMoveExchange {
  public:
    BHMoveExchangeAll ();
    ~BHMoveExchangeAll () override;
    static std::string DefaultString ();
    std::unique_ptr<BHMove> clone () override;

  protected:
    bool selectAtoms (
      BHCluster &out,
      BHClusterAnalyser &,
      const BHMetalParameters &,
      rndEngine &) override;
    void SpecializeExchange (const BHCluster &in) override;
    std::vector<BHULongRND> AtomSelectors_;
  };
} // namespace BH
#endif // BHMOVEEXCHANGEALL_H
#endif
