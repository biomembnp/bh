/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
    @brief Declaration of abstract class  BHMoveExchange  that works as a base
   for all exchange moves
     @file BHmoveExchangeAll.hpp
     @author Daniele Rapetti (iximiel@gmail.com)

     @date 11/4/2019
     @version 0.1
     creating the move
    */
#ifndef BHMOVEEXCHANGE_H
#define BHMOVEEXCHANGE_H
#include "BHmove.hpp"
namespace BH {
  ///@todo add the possibility to make theexchange move work only for determined
  /// \brief This abstract class acts as a base for the meal moves
  class BHMoveExchange : public BHMove {
  public:
    BHMoveExchange (std::string name);
    ~BHMoveExchange () override;
    /// This function will verify the ordering of the cluster and call
    /// SpecializeExchange
    void Specialize (const BHCluster &in, const BHMetalParameters &) override;

  protected:
    /// Selects the two kinds of atoms to exchange and calls selectAtoms to
    /// decide which atoms to be swapped
    bool doMoveAlgorithm (
      BHCluster &in,
      BHCluster &out,
      BHClusterAnalyser &,
      const BHMetalParameters &,
      rndEngine &) override;
    virtual bool selectAtoms (
      BHCluster &out,
      BHClusterAnalyser &,
      const BHMetalParameters &,
      rndEngine &) = 0;
    virtual void SpecializeExchange (const BHCluster &in);
    std::vector<unsigned int> firstAtomperSpecie_{}, lastAtomperSpecie_{};
    size_t kindToExchange1_{0}, kindToExchange2_{1};
    size_t NofSpecies_{0};
    unsigned int chosenAtom1_{0}, chosenAtom2_{0};
    BHULongRND rndSpecies_{0, 0};
  };
} // namespace BH
#endif // BHMOVEEXCHANGE_HPP
