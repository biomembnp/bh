/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declaration of BHCouple

   @file BHcouple.hpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 20/4/2017
   @version 1.0
*/
#ifndef BHCOUPLE_H
#define BHCOUPLE_H
#include "BHdebug.hpp"
#include <vector>
namespace BH {
  /// This is a support struct that has to be used with CNA analisys
  struct BHCouple {
    size_t first, second; // the ids of the couple
    // r = common NN
    // s = NN bound between the r
    // t = lenght of the longest  chain of common NN
    unsigned char r, s, t; // the number of the signature (using signed char
                           // because is >=0 and i don't need more tha 8bit
    std::vector<unsigned> commonNeighs; // the ids of the common neighbours
#ifndef __MINGW32__
    BHCouple (unsigned i, unsigned j);
    BHCouple (int i, int j);
#endif //__MINGW32__
    BHCouple (size_t i, size_t j);

    friend std::ostream &operator<< (std::ostream &stream, const BHCouple &cp);
  };
} // namespace BH
#endif // BHCOUPLE_H
