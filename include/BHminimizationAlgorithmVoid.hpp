/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
#ifdef MINALGO_PARSER
MinAlgorithmParser (NOMIN, BHMinimizationAlgorithmVoid)
#else
#ifndef BHMINIMIZATIONALGORITHMVOID_HPP
#define BHMINIMIZATIONALGORITHMVOID_HPP

#include "BHminimizationAlgorithm.hpp"

namespace BH {
  /// Returns the energy without any minimization
  class BHMinimizationAlgorithmVoid : public BHMinimizationAlgorithm {
  public:
    BHMinimizationAlgorithmVoid ();
    BHdouble Minimization (
      BHdouble *x, const unsigned &nat, BHEnergyCalculator *) override;
    BHdouble Minimization_tol (
      BHdouble *x, const unsigned &nat, BHEnergyCalculator *) override;
  };
} // namespace BH
#endif // BHMINIMIZATIONALGORITHMVOID_HPP
#endif
