/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Contains the typedef of BHdouble and some constants, along enums and
   some utilities
   @file BHmyMath.hpp
   @author Daniele Rapetti (iximie@gmail.com)

   @date 8/02/2018
   @version 1.1

   Moving here the declaration of Factorial and RepetionioComb

   @date 21/3/2017
   @version 1.0
*/
#ifndef BHMYMATH_H
#define BHMYMATH_H
#include "BHversion.hpp"

/// This namespace includes and protects all function, classes, variables and
/// definitions for this suite
namespace BH {
  /// This is defined here so that it is possible
  using BHdouble = double;
  /// This namespace protects the constants
  namespace BHConst {
    /// Boltzmann's constant [eVk^-1]
    constexpr BHdouble kB = 8.6173303e-5;
    /// 1Ry in eV [eV]
    constexpr BHdouble Ry = 13.605693009;
    /// 1Ht in eV [eV]
    constexpr BHdouble Ht = 2.0 * Ry;
  } // namespace BHConst
  /// This namespace protects various enums
  namespace BHEnums {
    enum energyType {
      eV, ///< Electron Volt
      Ht, ///< Hartree
      Ry  ///< Rydberg
    };
  }
  int Factorial (int x);
  int RepetitionComb (int elements, int tuples);
  unsigned Factorial (unsigned x);
  unsigned RepetitionComb (unsigned elements, unsigned tuples);
  BHdouble
  convertEnergy2eV (BHdouble newEne, BHEnums::energyType units = BHEnums::eV);
  /// Function to get the value of the x^5 parameter for the swithching function
  BHdouble getSwitchingPolyA5 (
    const BHdouble cs,
    const BHdouble ce,
    const BHdouble Fval,
    const BHdouble dFval,
    const BHdouble d2Fval);
  /// Function to get the value of the x^4 parameter for the swithching function
  BHdouble getSwitchingPolyA4 (
    const BHdouble cs,
    const BHdouble ce,
    const BHdouble Fval,
    const BHdouble dFval,
    const BHdouble d2Fval);
  /// Function to get the value of the x^3 parameter for the swithching function
  BHdouble getSwitchingPolyA3 (
    const BHdouble cs,
    const BHdouble ce,
    const BHdouble Fval,
    const BHdouble dFval,
    const BHdouble d2Fval);

  constexpr BHdouble MINIMUM_OP_DIFFERENCE = 5e-4;
  constexpr BHdouble MINIMUN_ENERGY_DIFFERENCE = 5e-4;
  // this is the minimum number of neigbours for considering an atom in the bulk
  // (for FCC metals)
  constexpr unsigned int MinimumNeighbourForFCCBulk = 11;
  namespace BHparsers {}
} // namespace BH
#endif // BHMYMATH_H

#ifdef ALLDEBUG
#define METALPARDEBUG
#define WALKERSETTINGDEBUG
#define SETTINGSDEBUG
#define WALKERDEBUG
#endif // ALLDEBUG
