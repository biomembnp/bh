/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Contains the declaration of the BHmover class

   @file BHmover.hpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 20/12/2017
   @version 1.0

   class definitiom moved from mover.cxx
*/
#include "BHwalker.hpp"

namespace BH {
  namespace BHMoverSupport {
    ///\brief BHMontecarloStatus stores the status of the montecarlo algorithm,
    /// so it can be plotted
    struct BHMontecarloStatus {
      bool accepted;
      BHdouble deltaE, expAcc;
    };
  } // namespace BHMoverSupport

  /// Overload of BHWalker defined in order to make a move without internal
  /// minimization
  class BHMover : public BHWalker {
  public:
    BHMover (
      const BHClusterAtoms &workingConf,
      const BHClusterAtoms &lastAcceptedMove,
      const BHWalkerSettings &walkersettings,
      const BHMetalParameters &bhmp);
    ///
    /// \brief FinishPreviousStep: does the Metropolis for the previous step
    /// This function also sets up the right lastAcceptedEnergy_ and
    /// LastAcceptedConf_ returns a collection of data useful for monitoring the
    /// Montecarlo
    BHMoverSupport::BHMontecarloStatus
    FinishPreviousStep (const BHdouble &MonteCarloT, rndEngine &rng);
    void Energy (BHdouble newEne, BHEnums::energyType units = BHEnums::eV);
  };
} // namespace BH
//**************************************************
// mover.cxx funtions
/// Reorders and updates the cluster in file fname
void Reorder (const std::string &fname, BH::BHMetalParameters &bhmp);
/// Converts energy in eV
/**This function takes a xyz file, then assign to it (writing in the coment
 * line) the energy and two simple OP (that are bound mix and boun of the )*/
void EnergyConverter (
  /// Name of the file that contains
  const std::string &fname,
  /// Energy to be converted
  BH::BHdouble Energy,
  const BH::BHMetalParameters &bhmp,
  /// Units of the energy, the disponible one are in BH::BHEnums::energyType
  BH::BHEnums::energyType units = BH::BHEnums::eV);

/// Does the Montecarlo Basin Hopping algorithm
/** This function simply execute a single step for the basin hopping algorithm,
   except the minimization:

    - Does the Metropolis algorithm
    - Creates a new configuration based on the last accepted step
    - Readies the new configuration for the minimization
 */
int MonteCarlo (
  /// Name of the (input) file with the last accepted configuration
  const std::string &last_acp_fname,
  /// Name of the (input) file with the last minimized configuration
  const std::string &new_move_fname,
  /// Name  of the (output) file that will contai the next configuration that
  /// will be minimized
  const std::string &output_fname,
  /// the metal parameters
  BH::BHMetalParameters &bhmp,
  /// The value of the last accepted energy in non converted units
  BH::BHdouble latestEnergy,
  /// The value of the new energy in non converted units
  BH::BHdouble newEnergy,
  /// The Monte Carlo temperature in K
  BH::BHdouble TMC,
  /// The units used by the energy
  BH::BHEnums::energyType units);
