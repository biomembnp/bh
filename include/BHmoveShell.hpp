/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
  @brief Declaration of move Shell
   @file BHmoveShell.hpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 9/4/2019
   @version 0.1
   creating the move
  */
#ifdef MOVE_PARSER
MoveParser (shell, BHMoveShell)
#else
#ifndef BHMOVESHELL_H
#define BHMOVESHELL_H
#include "BHmove.hpp"
namespace BH {
  /**
   * @brief Displaces an atom on the surface
   *
   * *This move consists in:
   * -# randomly choosing an atom (iatom) of the cluster, among the less
   * coordinated (number of nn < 9)
   * -# locating the maximum distance between atoms and cdm (maxdist)
   * -# moving iatom in a randomly chosen position within a shell of fixed
   * thickness (shell_thickness=1.5 A) centered in cdm and of maximum
   * radius=maxdist
   * -# if iatom has thus been displaced of a quantity bigger then displamin,
   * the program returns.
   * -# If the displacement is smaller, a move is attempted to another point
   * within the shell
   */
  class BHMoveShell : public BHMove {
  public:
    BHMoveShell ();
    ~BHMoveShell () override;
    static std::string DefaultString ();
    std::unique_ptr<BHMove> clone () override;

  protected:
    bool doMoveAlgorithm (
      BHCluster &in,
      BHCluster &out,
      BHClusterAnalyser &,
      const BHMetalParameters &,
      rndEngine &) override;
    std::string printSettingsSpecialized () const override;
    std::string printSettingsSpecializedForInput () const override;
    bool parseSpecialized (const std::string &) override;
    bool parseAfter () override;
    /// The widht of the spherical crown
    BHdouble crown_{1.5};
    BHdouble MinimumMoveDisplacement_{3.0};
    BHdouble MinimumMoveDisplacementSQ_;
  };
  namespace BHParsers {
    namespace BHMV {
      enum class SHELLvariable { crown_, MinimumMoveDisplacement_ };
      const std::array<std::string, 2> SHELL = {
        "shellthickness",     // double //0
        "minimumdisplacement" // double //1
      };
    } // namespace BHMV
  }   // namespace BHParsers
} // namespace BH
#endif // BHMOVESHELL_H
#endif // MOVE_PARSER
