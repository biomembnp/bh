/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
  @brief Declaration of move Bonds
   @file BHmoveBonds.hpp
   @author Daniele Rapetti (iximiel@gmail.com)

   @date 9/4/2019
   @version 0.1
   creating the move
  */
#ifdef MOVE_PARSER
MoveParser (bonds, BHMoveBonds)
#else
#ifndef BHMOVEBONDS_H
#define BHMOVEBONDS_H
#include "BHmove.hpp"
namespace BH {
  /**
   * @brief Moves a low coordinated atome near another low coordinated atom
   *
   * This move consists in:
   *
   * -# evaluating the minimum number of first neighbours in the cluster
   * (_npvicmin_)
   * -# making a list of atoms with _nvic=npvicmin_ with _numvicmin_ members
   * -# making a list of atoms with _nvic=npvicmin+1_ with _numvicminp1_ members
   * [actually the second least]
   * -# choose an atom randomly in the first list
   * -# then:
   *  - **if _numvicmin_>1** choose the second atom from the first list
   *  - **if _numvicmin_=1** choose the second atom from the second list
   * -# move the first atom in the neighborhood of the second
   */
  class BHMoveBonds : public BHMove {
  public:
    BHMoveBonds ();
    ~BHMoveBonds () override;
    static std::string DefaultString ();
    std::unique_ptr<BHMove> clone () override;

  protected:
    bool doMoveAlgorithm (
      BHCluster &in,
      BHCluster &out,
      BHClusterAnalyser &,
      const BHMetalParameters &,
      rndEngine &) override;
    std::string printSettingsSpecialized () const override;
    std::string printSettingsSpecializedForInput () const override;
    bool parseSpecialized (const std::string &) override;
    bool parseAfter () override;
    BHdouble Rmin_{0.0}, Rmax_{0.0};
  };

  namespace BHParsers {
    namespace BHMV {
      enum class BONDSvariable { Rmin_, Rmax_ };

      const std::array<std::string, 2> BONDS = {
        "rmin", // double //0
        "rmax"  // double //1
      };
    } // namespace BHMV

  } // namespace BHParsers
} // namespace BH
#endif // BHMOVEBONDS_H
#endif // MOVE_PARSER
