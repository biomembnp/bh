/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/** @brief contain the declaration of the parsers
  @file BHparsers.hpp

    @date 3/4/2019
    @version 1.0
  */
#ifndef BHPARSERS_HPP
#define BHPARSERS_HPP

#include "BHmyMath.hpp"

#include <string>

namespace BH {
  namespace BHParsers {
    /// parse a double value and assign it to variable, \returns true on a
    /// success
    bool parse (std::string tofind, std::string where, BHdouble &variable);
    /// parse an integer value and assign it to variable, \returns true on a
    /// success
    bool parse (std::string tofind, std::string where, int &variable);
    /// parse a size_t value and assign it to variable, \returns true on a
    /// success
    bool parse (std::string tofind, std::string where, size_t &variable);
    /// parse an integer value and assign it to variable, \returns true on
    /// asuccess
    bool parse (std::string tofind, std::string where, unsigned int &variable);
    /// parse a boolena value and assign it to variable, \returns true on a
    /// success
    bool parse (std::string tofind, std::string where, bool &variable);
    /// parse a string, removes trailing and leading spaces and assign it to
    /// variable, \returns true on asuccess
    bool parse (std::string tofind, std::string where, std::string &variable);
  } // namespace BHParsers
} // namespace BH

#endif // BHPARSERS_HPP
