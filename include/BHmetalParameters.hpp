/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Declaration of class BHMetalParameters, container for Gupta parameters
   of potential
   @file BHmetalParameters.hpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 20/4/2017
   @version 1.1

*/
#ifndef BHMETALPARAMETERS_HN
#define BHMETALPARAMETERS_HN
#include "BHcluster.hpp"
#include "BHclusterData.hpp"
#include <memory>
#include <string>
#include <unordered_map> //is faster than map for access by key

//#define BHMPSINGLETON
namespace BH {
  struct BHAtomParameters {
    BHdouble Coh, ///< Cohesive energy
      Radius,     ///< Atomic radius
      Mass;
  };

  /// Inherited by
  struct BHInteraction {
    BHdouble NNdist{-1.0};   ///< near neighbours distance in Angstroms
    BHdouble NNdist_tolSQ{}; ///< near neighbours distance in Angstroms, squared
    BHdouble cutOff_start{}; ///< Start of the cut off
    BHdouble cutOff_end{};   ///< End of the cut off
    BHdouble cutOff_endSQ{};
    // void updateNNdistSAME (BHdouble radius);
    // void updateNNdistNOTSAME (BHdouble radius1, BHdouble radius2);
  };

  void updateNNdistSAME (BHInteraction &, BHdouble radius);
  void
  updateNNdistNOTSAME (BHInteraction &, BHdouble radius1, BHdouble radius2);

  struct BHSMATBInteraction final : public BHInteraction {
    BHdouble p{};   ///<\f$p_{ij}\f$
    BHdouble q{};   ///<\f$q_{ij}\f$
    BHdouble a{};   ///<\f$A_{ij}\f$
    BHdouble qsi{}; ///<\f$\xi_{ij}\f$
    // BHdouble Arete{};
    // The two switching functions parameters
    // The various parameters of the polynom for
    // \f$e^{-p_{ij}\left(\frac{r_{ij}}{r_{ij0}}-1\right)}\f$
    // (each combination has it's own)
    BHdouble P_par5{};
    BHdouble P_par4{};
    BHdouble P_par3{};

    BHdouble Q_par5{};
    BHdouble Q_par4{};
    BHdouble Q_par3{};
    // BHSMATBInteraction ();
    // rule of Big4 and a half
    // BHSMATBInteraction (const BHSMATBInteraction &);
    // BHSMATBInteraction (BHSMATBInteraction &&) noexcept;
    // BHSMATBInteraction &operator= (BHSMATBInteraction);
    // void swap (BHSMATBInteraction &);
  };

  BHdouble Qexp (const BHSMATBInteraction &, const BHdouble x);
  BHdouble Pexp (const BHSMATBInteraction &, const BHdouble x);
  BHdouble Qpoly (const BHSMATBInteraction &, const BHdouble x);
  BHdouble Ppoly (const BHSMATBInteraction &, const BHdouble x);

  /// Calc Poly parameters in angstroms
  void calcPar (BHSMATBInteraction &);

  struct BHLJInteraction final : public BHInteraction {
    BHdouble epsilon{}; ///<\f$p_{ij}\f$
    BHdouble sigma{};

    // BHLJInteraction ();
    // BHLJInteraction (const BHLJInteraction &x);
    // BHLJInteraction (BHLJInteraction &&x) noexcept;
    // BHLJInteraction &operator= (BHLJInteraction x);
    // void swap (BHLJInteraction &);
  };

  /// A singleton class that handles all the metal parameters
  /**
   *To tell the energy of each atom in the cluster we have to calculate the
   seguent formula:
   *\f[E_i = \sum_{j,r_{ij}\leq r_{c}} A_{ij}
   e^{-p_{ij}\left(\frac{r_{ij}}{r_{ij0}}-1\right)} - \sqrt{\sum_{j,r_{ij}\leq
   r_{c}} \xi^2_{ij} e^{-2q_{ij}\left(\frac{r_{ij}}{r_{ij0}}-1\right)}}\f]

  To use this class you must declare ONLY once:
      ~~~
      BHMetalParameters::BHMP_filename="filename_metalfiles";
  ~~~

    Then to call a public member of BHMetalParameters you only need to write:
      ~~~
      BHMetalParameters::getBHMP().method();
  ~~~

      BHMetalParameters will initialize itself the first time that getBHMP() is
          called.

              */
  class BHMetalParameters final {
  private:
    enum class Interactionkind { SMATB, LJ, DEFAULTSMATB };

    /// This class serves to store the interaction parameters regardless the
    /// types
    class BHInteractionStorer {
    private:
      Interactionkind kind{Interactionkind::SMATB};
      std::unique_ptr<BHInteraction> intData_{nullptr};

    public:
      BHInteractionStorer ();
      BHInteractionStorer (const BHInteractionStorer &);
      BHInteractionStorer (BHInteractionStorer &&) noexcept;
      BHInteractionStorer (const BHLJInteraction &);
      BHInteractionStorer (const BHSMATBInteraction &);
      BHInteractionStorer &operator= (BHInteractionStorer);
      void swap (BHInteractionStorer &);
      BHInteraction &getIntData ();
      BHSMATBInteraction &getSMATBData ();
      BHLJInteraction &getLJData ();
      const BHInteraction &getIntData () const;
      const BHSMATBInteraction &getSMATBData () const;
      const BHLJInteraction &getLJData () const;
    };

    Interactionkind intKind (std::string input);
    using BHAtomsPars = std::unordered_map<std::string, BHAtomParameters>;
    using BHInteractionPars =
      std::unordered_map<std::string, BHInteractionStorer>;
    using BHMetalNameToLabels = std::unordered_map<std::string, unsigned>;
    using BHMetalLabelToNames = std::unordered_map<unsigned, std::string>;

  public:
    BHMetalParameters ();
    BHMetalParameters (std::string, bool verbose = true);
    BHMetalParameters (const BHMetalParameters &);
    BHMetalParameters (BHMetalParameters &&) noexcept;
    BHMetalParameters &operator= (const BHMetalParameters &) = delete;
    BHMetalParameters &operator= (BHMetalParameters &&) = delete;
    ~BHMetalParameters ();

    void loadFromFile (std::string);
    void loadFromStream (std::istream &);
    void printData (std::ostream &);

    /// Returns the number of metals that are loaded in memory
    unsigned getNofMetals () const;
    unsigned getNofInteractions () const;
    /// look if an atom is in the Slabels
    bool IknowAbout (const std::string &) const;
    /// Return the mass given the name
    BHdouble getMass (const std::string &) const;
    /// Return the radius given the name
    BHdouble getRadius (const std::string &) const;
    /// Return the name of a single metal given the index
    std::string getAtomName (const unsigned) const;
    /// Return the name of a pair of metals given the index of the interaction
    std::string getPairName (const unsigned) const;
    /// Returns pair label given the index of the atom types
    unsigned getPlabels (const unsigned, const unsigned) const;
    /// Returns pair label given a string  with the atom types like AgPt
    unsigned getPlabels (const std::string &) const;
    /// Return single label given the id
    unsigned getSlabels (const unsigned) const;
    /// Return single label given a label name
    unsigned getSlabels (const std::string &) const;
    BHdouble getNNdist (const unsigned) const;
    BHdouble getNNdist (const std::string &) const;
    BHdouble getD2tol (const unsigned) const;
    BHdouble getD2tol (const std::string &) const;
    BHdouble getCutOffEnd (const unsigned codePair) const;
    BHdouble getCutOffEnd (const std::string &Atoms_pair) const;
    // void plotData ();
    /// gives all Cohesion energy, Radius and Mass for an asked index (C-like)
    void giveCohRadiusMass (
      const unsigned index,
      BHdouble &gCoh,
      BHdouble &gRadius,
      BHdouble &gMass) const;
    /// gives all the MP for an asked index (C-like)
    void giveMetalPars (
      const unsigned index,
      BHdouble &gp,
      BHdouble &gq,
      BHdouble &ga,
      BHdouble &gqsi,
      BHdouble &gNN,
      BHdouble &gcs,
      BHdouble &gce) const;
    /// gives all the polynomies parameters for an asked index (C-like)
    void givePolyPars (
      const unsigned,
      BHdouble &gP5,
      BHdouble &gP4,
      BHdouble &gP3,
      BHdouble &gQ5,
      BHdouble &gQ4,
      BHdouble &gQ3) const;
    /// gives all Cohesion energy, Radius and Mass for an asked index (C-like)
    void giveCohRadiusMass (
      const std::string &index,
      BHdouble &gCoh,
      BHdouble &gRadius,
      BHdouble &gMass) const;
    /// gives all the MP for an asked index (C-like)
    void giveMetalPars (
      const std::string &index,
      BHdouble &gp,
      BHdouble &gq,
      BHdouble &ga,
      BHdouble &gqsi,
      BHdouble &gNN,
      BHdouble &gcs,
      BHdouble &gce) const;
    /// gives all the polynomies parameters for an asked index (C-like)
    void givePolyPars (
      const std::string &,
      BHdouble &gP5,
      BHdouble &gP4,
      BHdouble &gP3,
      BHdouble &gQ5,
      BHdouble &gQ4,
      BHdouble &gQ3) const;

    const BHSMATBInteraction &
    giveSMATBInteractionParameters (const unsigned index) const;

    const BHSMATBInteraction &
    giveSMATBInteractionParameters (const std::string &index) const;

    const BHLJInteraction &
    giveLJInteractionParameters (const unsigned index) const;

    const BHLJInteraction &
    giveLJInteractionParameters (const std::string &index) const;

    BHSMATBInteraction &giveSMATBInteractionParameters (const unsigned index);

    BHSMATBInteraction &
    giveSMATBInteractionParameters (const std::string &index);

    BHLJInteraction &giveLJInteractionParameters (const unsigned index);

    BHLJInteraction &giveLJInteractionParameters (const std::string &index);
    // These function will be legacy functions: I want a new class to
    // elaborate them
    void setInteractionLabels (BHCluster &) const;
    void calcNeighbourhood (BHCluster &) const;
    void calcNeighINT (BHCluster &) const;
    void calcNeighbourhood_tol (BHCluster &) const;

  private:
    /// Number of metals for generating the index
    unsigned Nmetals{0};
    /// Number of interactions for generating the index
    unsigned Ninteractions{0};
    /// Atoms labels
    BHMetalNameToLabels Slabels;
    /// Pair labels
    BHMetalNameToLabels Plabels;
    /// single name from ID
    BHMetalLabelToNames Sname;
    /// pair name from ID
    BHMetalLabelToNames Pname;
    //"pair" parameters
    // BHSMATBInteractionPars SMATBinteractions_;
    BHInteractionPars InteractionParameters_;
    //"private" parameters
    BHAtomsPars AtomParameters_;
    /// tells if the arete datas are initializated
    bool ready{false};
  };
} // namespace BH
#endif // BHMETALPARAMETERS_H
