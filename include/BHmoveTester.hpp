/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Contains the declaration of moveTester  a tool used to tes a single
   move
   @file BHmoveTester.hpp
   @author Daniele Rapetti (iximiel@gmail.com)
   @date 21/3/2017
   @version 1.0
*/
#ifndef BHMOVETESTER_H
#define BHMOVETESTER_H
#include "BHwalker.hpp"
// overloaded walker for testing purpose
namespace BH {
  class moveTester final : public BHWalker {
  public: // same constructors as parent
    moveTester (
      const BHWalkerSettings &walkersettings,
      const BHClusterAtoms &seed,
      const BHMetalParameters &bhmp);

    BHdouble doMove (
      BHClusterAnalyser &Analyzer,
      const BHMetalParameters &bhmp,
      rndEngine &rng) override;
    void ExportResultAndMinimized (
      BHMinimizationAlgorithm *minimizator,
      BHEnergyCalculator *ec,
      std::string optional = "");
  };
} // namespace BH
#endif // BHMOVETESTER_H
