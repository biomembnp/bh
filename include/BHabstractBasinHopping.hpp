/*-----------------------------------------------------------------------
    Copyright (C) 2020-2022 by Daniele Rapetti

    This file is part of BH++.

    BH++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BH++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BH++.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/
/**
   @brief Here the declaration of the abstract class for the basin hopping
   algorithm

@file BHabstractBasinHopping.hpp
  @author Daniele Rapetti (iximiel@gmail.com)

  @date 12/6/2019
  @version 0.1

  */
#ifndef BHABSTRACTBASINHOPPING_H
#define BHABSTRACTBASINHOPPING_H
#include "BHclusterAnalyser.hpp"
#include "BHhisto.hpp"
#include "BHmetalParameters.hpp"
#include "BHminimizationAlgorithm.hpp"
#include "BHsettings.hpp"
#include "BHwalker.hpp"

#include <iosfwd>
#include <map> //I'm  using map while i don't find something more efficient

namespace BH {
  using minimaMap = std::map<int, BHClusterData>;
  /// @brief this class handles the basin hopping algorithm
  class BHAbstractBasinHopping {
  public:
    BHAbstractBasinHopping (const BHSettings &);
    BHAbstractBasinHopping (BHAbstractBasinHopping &) = delete;
    BHAbstractBasinHopping (BHAbstractBasinHopping &&) = delete;
    BHAbstractBasinHopping &operator= (BHAbstractBasinHopping &) = delete;
    BHAbstractBasinHopping &operator= (BHAbstractBasinHopping &&) = delete;
    static bool run; // used to handle signals
    virtual const char *logo ();
    static const char *staticLogo ();
    static void interruptRun (int) { run = false; }
    virtual ~BHAbstractBasinHopping ();
    /// do the algorithm
    virtual void Processing ();
    ///@brief Returns the name of the style of the basinhopping
    virtual const std::string BHStyleName () = 0;

  protected:
    BHSettings settings_;
    BHMetalParameters metalParameters_;
    /// \brief The BHMoveStatistic struct is a sugar to pass only one argument
    /// in the logs
    struct BHMoveStatistic {
      unsigned int AcceptedMoves{0}, RealMoves{0};
    };
    struct BHGloInfo {
      BHdouble energy;
      unsigned GloID;
      unsigned mcStep;
      void recodNewMinimum (const BHdouble &, const unsigned &);
    };
    /// initializes the outputDifferentiator variable
    virtual void PreOutputInitialization ();
    virtual void Initialization ();
    virtual void initializeLandingWalkers (const unsigned wID);
    virtual void initializeHikingWalkers (const unsigned wID);
    virtual void initializeBestWalker ();
    virtual void OutputInitialization ();
    virtual void MoveAccepted ();
    virtual void MoveAcceptedPostProduction ();
    virtual void OutputAfterStep ();
    virtual void ChangeWalker ();
    virtual void ScreenLogging ();

    /// \brief Updates all the files with a new global minimum
    /** \param wID walker ID of with the new minimum
     * \param mc_step the montecarlo step
     * \param gloID the ID of the minimum
     * \param ener_best the ofstram with the file with the energy data
     * \param prefix a prefix to prepend to the xyz filename
     */
    virtual void updateNewGlobalMinimum (
      const unsigned int &wID,
      const unsigned &mcStep,
      const unsigned &gloID,
      std::ofstream &ener_best,
      const std::string &prefix = "");
    virtual void updateNewGlobalMinimum (
      const unsigned int &wID,
      const BHGloInfo &,
      std::ofstream &ener_best,
      const std::string &prefix = "");

    ////Tests if the latest accepted configuration could be a new minimum for
    /// the given interval of Oreder Parameters
    virtual void updateMinima (const size_t &wID);

    virtual void
    writeStatus (std::ostream &, const BHMoveStatistic &, const size_t &mcStep);
    virtual void writeMovesStatus (
      std::ostream &, const BHMoveStatistic &, const size_t &mcStep);
    virtual void writeMovesStatusByType (std::ostream &);

    /// minimizes and sets the interaction label compatible with the analyzer
    BHdouble initializeClusters (const unsigned &wID);
    BHdouble minimizeAndCountTime (const unsigned wID);
    BHdouble minimizeLBFGSAndCountTime (const unsigned wID);
    std::vector<BHWalker> walkers_{};
    ///@todo: this should be in the walker?
    std::vector<BHGloInfo> GloInfoPerWalker_{};
    ///@todo: this should be in the walker?
    std::vector<BHMoveStatistic> acceptedMovesInfo_{};
    ///@todo: this should be in the walker?
    ///@todo make unique
    BHMinimizationAlgorithm *minimizator_;
    BHEnergyCalculator *energyCalculator_;

    BHWalkerSupport::BHWalkerAlgorithmSupport algorithmSupport_;
    // initializing the ener_all and ener_wNN files
    std::vector<std::ofstream> enerAll_{};
    std::vector<std::ofstream> enerBest_{};
    // differentiate the walkers in the glofiles
    // two files for understand how the simulation is going
    std::ofstream movesOut_;
    std::ofstream movesStatistics_; ///< a more verbose output
    std::vector<BHdouble> EnergyAtDive_{};
    BHGloInfo GloInfoGeneral_{0.0, 0, 0};
    size_t wIDSaved_{};
    unsigned int runnigWalker_{}, mcStep_{1}, lastLogOnStep_{};
    BHMoveStatistic totalMoves_{0, 0};
    rndEngine rndEngine_; // mt19937 is a standard mersenne_twister_engine
    // name aliased in BHrandom
    std::vector<unsigned> minimaListSelection_{};
    std::vector<minimaMap> minimaList_{
      BHSettings::NumberOfOP};    // the list of the minima
    bool foundNewMinimum_{false}; // true if i found a new minimum
    bool initialized_{false};
    ms minimizationTime_{std::chrono::steady_clock::duration::zero ()};
    int nMinimizations_{0};
    BHRealRND LandingRestart_{0, 1.0};
  };
} // namespace BH
#endif // BHABSTRACTBASINHOPPING_H
