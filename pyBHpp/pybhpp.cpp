#include <pybind11/operators.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <sstream>

#include "BHbasinHopping.hpp"
#include "BHclusterUtilities.hpp"
#include "BHmetalParameters.hpp"

namespace py = pybind11;

using BH::BasinHopping;
using BH::BHAtom;
using BH::BHCluster;
using BH::BHClusterAtoms;
using BH::BHdouble;
using BH::BHEnergyCalculator;
using BH::BHMetalParameters;
using BH::BHSettings;
using BH::BHVector;

// PYBIND11_MAKE_OPAQUE(std::vector<BH::BHOrderParameter>);
// trampoline class for BHMinimizationAlgorithm inheritance
template <class BHMinimizationBase>
class BHpyMinimization : public BHMinimizationBase {
public:
  using BHMinimizationBase::BHMinimizationBase; // Inherit constructors
  BHdouble Minimization (
    BHdouble *x, const unsigned &nat, BHEnergyCalculator *ec) override {
    PYBIND11_OVERLOAD_PURE (
      BHdouble,           // Return type
      BHMinimizationBase, // Parent class
      Minimization,       // Name of function in C++ (must match Python name)
      x, nat, ec          // Argument(s)
    );
  }
  BHdouble Minimization_tol (
    BHdouble *x, const unsigned &nat, BHEnergyCalculator *ec) override {
    PYBIND11_OVERLOAD_PURE (
      BHdouble, BHMinimizationBase, Minimization_tol, x, nat, ec);
  }
  BHdouble
  MinimizeCluster (BHClusterAtoms &cluster, BHEnergyCalculator *ec) override {
    PYBIND11_OVERLOAD (
      BHdouble, BHMinimizationBase, MinimizeCluster, cluster, ec);
  }
  BHdouble MinimizeCluster_tol (
    BHClusterAtoms &cluster, BHEnergyCalculator *ec) override {
    PYBIND11_OVERLOAD (
      BHdouble, BHMinimizationBase, MinimizeCluster_tol, cluster, ec);
  }
};

template <class energyCalculator>
class BHpyEnergyCalculator : public energyCalculator {
public:
  using energyCalculator::energyCalculator;
  BHdouble Energy (BHdouble *x) override {
    PYBIND11_OVERLOAD_PURE (BHdouble, energyCalculator, Energy, x);
  }
  BHdouble Energy_tol (BHdouble *x) override {
    PYBIND11_OVERLOAD_PURE (BHdouble, energyCalculator, Energy_tol, x);
  }
  BHdouble Gradient (BHdouble *x, BHdouble *g) override {
    PYBIND11_OVERLOAD_PURE (BHdouble, energyCalculator, Gradient, x, g);
  }
  BHdouble Pressure (BHdouble *x, BHdouble *p) override {
    PYBIND11_OVERLOAD (BHdouble, energyCalculator, Pressure, x, p);
  }
  void Neighbours_for_tol (BHdouble *x) override {
    PYBIND11_OVERLOAD (void, energyCalculator, Neighbours_for_tol, x);
  }
  BHdouble Gradient_tol (BHdouble *x, BHdouble *g) override {
    PYBIND11_OVERLOAD_PURE (BHdouble, energyCalculator, Gradient_tol, x, g);
  }
};

PYBIND11_MODULE (pyBHpp, BHpp) {
  BHpp.doc () = "pyBHpp BH++ python bindings";

  auto vector = py::class_<BHVector> (BHpp, "BHVector");
  vector.def (py::init<> ());
  vector.def (py::init<const BHdouble, const BHdouble, const BHdouble> ());
  vector.def (py::init<const BHVector &> ());
  vector.def_property (
    "X", static_cast<BHdouble (BHVector::*) () const> (&BHVector::X),
    static_cast<void (BHVector::*) (BHdouble)> (&BHVector::X));
  vector.def_property (
    "Y", static_cast<BHdouble (BHVector::*) () const> (&BHVector::Y),
    static_cast<void (BHVector::*) (BHdouble)> (&BHVector::Y));
  vector.def_property (
    "Z", static_cast<BHdouble (BHVector::*) () const> (&BHVector::Z),
    static_cast<void (BHVector::*) (BHdouble)> (&BHVector::Z));
  vector.def (
    "dist", static_cast<BHdouble (BHVector::*) (const BHVector &) const> (
              &BHVector::dist));
  vector.def (
    "distSquared",
    static_cast<BHdouble (BHVector::*) (const BHVector &) const> (
      &BHVector::dist2));
  vector.def (
    "setXYZ",
    static_cast<void (BHVector::*) (
      const BHdouble, const BHdouble, const BHdouble)> (&BHVector::setXYZ));
  vector.def_property_readonly ("R", &BHVector::R);
  vector.def_property_readonly ("Theta", &BHVector::Theta);
  vector.def_property_readonly ("Phi", &BHVector::Phi);
  vector.def ("setToZero", &BHVector::zero);
  vector.def (py::self + py::self);
  vector.def (py::self - py::self);
  vector.def (py::self -= py::self);
  vector.def (py::self += py::self);
  vector.def (py::self *= BHdouble ());
  vector.def ("__abs__", &BHVector::R);
  vector.def (BHdouble () * py::self);
  vector.def (py::self * BHdouble ());
  vector.def ("__str__", [] (const BHVector &x) {
    std::stringstream ss;
    ss << x;
    return ss.str ();
  });
  vector.def (
    "assign", [] (BHVector &self, const BHVector &other) { self = other; });

  auto atom = py::class_<BHAtom, BHVector> (BHpp, "BHAtom");
  atom.def (py::init<> ());
  atom.def (py::init<const BHAtom &> ());
  atom.def (
    py::init<std::string, const BHdouble, const BHdouble, const BHdouble> ());
  atom.def (py::init<std::string, const BHVector &> ());
  atom.def (py::self -= BHVector ());
  atom.def (py::self += BHVector ());
  atom.def (py::self < py::self);
  atom.def_property ("Type", &BHAtom::Type, &BHAtom::setType);

  auto metalParameters =
    py::class_<BHMetalParameters> (BHpp, "BHMetalParameters");
  metalParameters.def (py::init<> ());
  metalParameters.def (
    py::init<std::string, bool> (), py::arg ("FileName"),
    py::arg ("verbosity") = false);
  metalParameters.def (py::init<const BHMetalParameters &> ());
  metalParameters.def ("loadFromFile", &BHMetalParameters::loadFromFile);
  metalParameters.def (
    "loadFromString", [] (BHMetalParameters &bhmp, const std::string &data) {
      std::stringstream ss (data);
      std::cout << "the data is here:\n****************\n"
                << data << "****************\nend Data" << std::endl;
      bhmp.loadFromStream (ss);
    });
  metalParameters.def (
    "getDataString",
    [] (BHMetalParameters &bhmp, std::string data) -> std::string {
      std::stringstream ss;
      bhmp.printData (ss);
      return ss.str ();
    });

  metalParameters.def ("getNofMetals", &BHMetalParameters::getNofMetals);
  metalParameters.def (
    "getNofInteractions", &BHMetalParameters::getNofInteractions);
  metalParameters.def ("IknowAbout", &BHMetalParameters::IknowAbout);
  metalParameters.def ("getMass", &BHMetalParameters::getMass);
  metalParameters.def ("getRadius", &BHMetalParameters::getRadius);
  metalParameters.def ("getAtomName", &BHMetalParameters::getAtomName);
  metalParameters.def ("getPairName", &BHMetalParameters::getPairName);
  metalParameters.def (
    "getPlabels",
    static_cast<unsigned (BHMetalParameters::*) (const unsigned, const unsigned)
                  const> (&BHMetalParameters::getPlabels));
  metalParameters.def (
    "getPlabels",
    static_cast<unsigned (BHMetalParameters::*) (const std::string &) const> (
      &BHMetalParameters::getPlabels));
  metalParameters.def (
    "getSlabels",
    static_cast<unsigned (BHMetalParameters::*) (const unsigned) const> (
      &BHMetalParameters::getSlabels));
  metalParameters.def (
    "getSlabels",
    static_cast<unsigned (BHMetalParameters::*) (const std::string &) const> (
      &BHMetalParameters::getSlabels));
  metalParameters.def (
    "getNNdist",
    static_cast<BHdouble (BHMetalParameters::*) (const unsigned) const> (
      &BHMetalParameters::getNNdist));
  metalParameters.def (
    "getNNdist",
    static_cast<BHdouble (BHMetalParameters::*) (const std::string &) const> (
      &BHMetalParameters::getNNdist));
  metalParameters.def (
    "getD2tol",
    static_cast<BHdouble (BHMetalParameters::*) (const unsigned) const> (
      &BHMetalParameters::getD2tol));
  metalParameters.def (
    "getD2tol",
    static_cast<BHdouble (BHMetalParameters::*) (const std::string &) const> (
      &BHMetalParameters::getD2tol));
  metalParameters.def (
    "getCutOffEnd",
    static_cast<BHdouble (BHMetalParameters::*) (const unsigned) const> (
      &BHMetalParameters::getCutOffEnd));
  metalParameters.def (
    "getCutOffEnd",
    static_cast<BHdouble (BHMetalParameters::*) (const std::string &) const> (
      &BHMetalParameters::getCutOffEnd));
  metalParameters.def (
    "setInteractionLabels", &BHMetalParameters::setInteractionLabels);
  metalParameters.def (
    "calcNeighbourhood", &BHMetalParameters::calcNeighbourhood);
  metalParameters.def ("calcNeighINT", &BHMetalParameters::calcNeighINT);
  metalParameters.def (
    "calcNeighbourhood_tol", &BHMetalParameters::calcNeighbourhood_tol);
  metalParameters.def (
    "giveSMATBInteractionParameters",
    static_cast<const BH::BHSMATBInteraction &(
      BHMetalParameters::*)(const unsigned) const> (
      &BHMetalParameters::giveSMATBInteractionParameters));
  metalParameters.def (
    "giveSMATBInteractionParameters",
    static_cast<const BH::BHSMATBInteraction &(
      BHMetalParameters::*)(const std::string &) const> (
      &BHMetalParameters::giveSMATBInteractionParameters));
  metalParameters.def (
    "giveLJInteractionParameters",
    static_cast<const BH::BHLJInteraction &(
      BHMetalParameters::*)(const unsigned) const> (
      &BHMetalParameters::giveLJInteractionParameters));
  metalParameters.def (
    "giveLJInteractionParameters",
    static_cast<const BH::BHLJInteraction &(
      BHMetalParameters::*)(const std::string &) const> (
      &BHMetalParameters::giveLJInteractionParameters));

  BHpp.def (
    "AlphabeticalReorder", &BH::BHClusterUtilities::AlphabeticalReorder);
  BHpp.def ("MassesReorder", &BH::BHClusterUtilities::MassesReorder);

  auto clusterAtoms = py::class_<BHClusterAtoms> (BHpp, "BHClusterAtoms");
  clusterAtoms.def (py::init<> ());
  clusterAtoms.def (py::init<unsigned> ());
  clusterAtoms.def (py::init<std::vector<BHAtom>> ());
  clusterAtoms.def_property (
    "NofAtoms", &BHClusterAtoms::getNofAtoms, &BHClusterAtoms::setNofAtoms);
  clusterAtoms.def_property_readonly (
    "NofSpecies", &BHClusterAtoms::NofSpecies);
  clusterAtoms.def_property (
    "AdditionalInfos", &BHClusterAtoms::getAdditionalInfos,
    &BHClusterAtoms::setAdditionalInfos);
  /*clusterAtoms.def (
      "Composition",
      static_cast<std::string (BHClusterAtoms::*) (const unsigned) const> (
          &BHClusterAtoms::Composition));*/
  clusterAtoms.def_property_readonly (
    "Composition", static_cast<std::string (BHClusterAtoms::*) () const> (
                     &BHClusterAtoms::Composition));
  clusterAtoms.def ("updateComposition", &BHClusterAtoms::updateComposition);
  clusterAtoms.def (
    "Atom",
    static_cast<const BHAtom &(BHClusterAtoms::*)(const unsigned) const> (
      &BHClusterAtoms::operator[] ));
  clusterAtoms.def (
    "setAtom",
    [] (BHClusterAtoms &cluster, const unsigned i, const BHAtom &atom) {
      cluster[i] = atom;
    });
  clusterAtoms.def ("getGeometricCenter", &BHClusterAtoms::getGeometricCenter);
  clusterAtoms.def (
    "getGeometricSphereRadius", &BHClusterAtoms::getGeometricSphereRadius);
  clusterAtoms.def ("__str__", [] (const BHClusterAtoms &cluster) {
    std::stringstream ss;
    ss << cluster;
    return ss.str ();
  });
  clusterAtoms.def ("__repr__", [] (const BHClusterAtoms &cluster) {
    std::stringstream ss;
    ss << "pyBHpp.BHClusterAtoms(" << cluster.getNofAtoms () << ")";
    return ss.str ();
  });

  BHpp.def ("getScaledCluster", &BHClusterAtoms::getScaledCluster);
  BHpp.def (
    "getCenteredCluster", &BHClusterAtoms::getCenteredCluster,
    py::arg ("cluster"), py::arg ("center") = BHVector{0.0, 0.0, 0.0});

  auto cluster = py::class_<BHCluster, BHClusterAtoms> (BHpp, "BHCluster");
  cluster.def (py::init<> ());
  cluster.def (py::init<unsigned> ());
  cluster.def (py::init<BHClusterAtoms> ());
  cluster.def (py::init<BHCluster> ());
  cluster.def (py::init<std::vector<BHAtom>> ());
  cluster.def_property (
    "NofAtoms", &BHCluster::getNofAtoms, &BHCluster::setNofAtoms);
  cluster.def (
    "addNNcouple", &BHCluster::addNNcouple, py::arg ("i"), py::arg ("j"),
    "Manually sets atoms i and j as Nearest Neighbours");
  cluster.def (
    "addNNcouple_same", &BHCluster::addNNcouple_same, py::arg ("i"),
    py::arg ("j"),
    "Manually sets atoms i and j as Nearest Neighbours of same kind");
  cluster.def (
    "addNNcouple_diff", &BHCluster::addNNcouple_diff, py::arg ("i"),
    py::arg ("j"),
    "Manually sets atoms i and j as Nearest Neighbours of different kind");
  cluster.def (
    "getNNs", &BHCluster::getNNs, py::arg ("i"),
    "Get the array of Nearest Neighbours of atom i");
  cluster.def (
    "getNNs_same", &BHCluster::getNNs_same, py::arg ("i"),
    "Get the array of Nearest Neighbours of same kind of atom i");
  cluster.def (
    "getNNs_diff", &BHCluster::getNNs_diff, py::arg ("i"),
    "Get the array of Nearest Neighbours of different kind of atom i");
  cluster.def ("clearStoredNN", &BHCluster::clear_NN);
  cluster.def ("getInteractionLabel", &BHCluster::INTlab);
  cluster.def (
    "copyInteractionLabelsFrom", &BHCluster::copyInteractionLabelsFrom);

  auto walkerRepulsionZone =
    py::class_<BH::BHwalkerRepulsionZone> (BHpp, "BHwalkerRepulsionZone");
  walkerRepulsionZone.def (py::init<> ());
  walkerRepulsionZone.def_readwrite ("low", &BH::BHwalkerRepulsionZone::low);
  walkerRepulsionZone.def_readwrite ("high", &BH::BHwalkerRepulsionZone::high);

  auto walkerAlgorithmType =
    py::enum_<BH::BHwalkerAlgorithmType> (BHpp, "BHwalkerAlgorithmType");

#define WALKER_ALGORITHM_PARSER
#define WalkerAlgorithmParser(key, Class, standalone, skipSteps)               \
  walkerAlgorithmType.value (#key, BH::BHwalkerAlgorithmType::key);

#include "BHwalkerAlgorithms.hpp"

#undef WalkerAlgorithmParser
#undef WALKER_ALGORITHM_PARSER

  auto walkerSettings =
    py::class_<BH::BHWalkerSettings> (BHpp, "BHWalkerSettings");
  walkerSettings.def (py::init<> ());
  /* these options were in the old fortran code
  walkerSettings.def_readwrite ("VariableTempHighTemp",
                                &BH::BHWalkerSettings::VariableTempHighTemp_);
  walkerSettings.def_readwrite ("VariableTempDeltaTemp",
                                &BH::BHWalkerSettings::VariableTempDeltaTemp_);
  walkerSettings.def_readwrite ("VariableTempPeriodTemp",
                                &BH::BHWalkerSettings::VariableTempPeriodTemp_);
walkerSettings.def_readwrite ("RestartEnergy",
                                &BH::BHWalkerSettings::RestartEnergy_);
  walkerSettings.def_readwrite ("OPT_VariableTemp",
                                &BH::BHWalkerSettings::OPT_VariableTemp_);
  walkerSettings.def_readwrite ("OPT_Restart",
                                &BH::BHWalkerSettings::OPT_Restart_);
  */
  walkerSettings.def_readwrite (
    "WalkerRepulsion", &BH::BHWalkerSettings::WalkerRepulsion_);
  walkerSettings.def_readwrite (
    "OPspaceSettings", &BH::BHWalkerSettings::OPspaceSettings_); // vector
  walkerSettings.def_readwrite (
    "ChosenAlgorithm", &BH::BHWalkerSettings::ChosenAlgorithm_);
  walkerSettings.def_readwrite (
    "interactWith", &BH::BHWalkerSettings::interactWith_);
  walkerSettings.def_readwrite ("seedFname", &BH::BHWalkerSettings::seedFname_);
  walkerSettings.def_readwrite (
    "MovesStrings", &BH::BHWalkerSettings::MovesStrings_);
  walkerSettings.def (
    "AddMove",
    [] (
      BH::BHWalkerSettings &ws, std::string movename, std::string moveOptions) {
      ws.MovesStrings_.push_back ({movename, moveOptions});
    },
    py::arg ("moveName"), py::arg ("moveOptions"));
  walkerSettings.def ("AddMove", [] (BH::BHWalkerSettings &ws) {
    ws.MovesStrings_.push_back ({});
  });
  walkerSettings.def (
    "AddMove",
    [] (
      BH::BHWalkerSettings &ws,
      BH::BHWalkerSettings::MoveNameAndString moveData) {
      ws.MovesStrings_.push_back ({moveData});
    },
    py::arg ("moveData"));
  walkerSettings.def (
    "AddInteraction",
    [] (BH::BHWalkerSettings &ws, unsigned ID) {
      ws.interactWith_.push_back (ID);
    },
    py::arg ("otherWalkerID"));
  walkerSettings.def ("clearInteractions", [] (BH::BHWalkerSettings &ws) {
    ws.interactWith_.clear ();
  });
  walkerSettings.def_readwrite ("seeded", &BH::BHWalkerSettings::seeded_);

  auto moveNameAndString = py::class_<BH::BHWalkerSettings::MoveNameAndString> (
    walkerSettings, "BHmoveNameAndString");
  moveNameAndString.def (py::init<> ());
  moveNameAndString.def_readwrite (
    "name", &BH::BHWalkerSettings::MoveNameAndString::name);
  moveNameAndString.def_readwrite (
    "option", &BH::BHWalkerSettings::MoveNameAndString::settingsString);

  auto interactionInOPSpace =
    py::class_<BH::BHWalkerSettings::interactionInOPSpace> (
      walkerSettings, "BHinteractionInOPSpace");
  interactionInOPSpace.def (py::init<> ());
  interactionInOPSpace.def_readwrite (
    "WalkerWidth", &BH::BHWalkerSettings::interactionInOPSpace::WalkerWidth_);
  interactionInOPSpace.def_readwrite (
    "RepulsionZone",
    &BH::BHWalkerSettings::interactionInOPSpace::RepulsionZone_);

  auto histoPars = py::class_<BH::BHHisto::BHHistoPars> (BHpp, "BHHistoPars");
  histoPars.def (py::init<> ());
  histoPars.def_property (
    "start", &BH::BHHisto::BHHistoPars::getStart,
    &BH::BHHisto::BHHistoPars::setStart);
  histoPars.def_property (
    "end", &BH::BHHisto::BHHistoPars::getEnd,
    &BH::BHHisto::BHHistoPars::setEnd);
  histoPars.def_property (
    "nbins", &BH::BHHisto::BHHistoPars::getNbins,
    &BH::BHHisto::BHHistoPars::setNbins);
  histoPars.def_property_readonly ("step", &BH::BHHisto::BHHistoPars::getStep);

  auto OrderParameter =
    py::class_<BH::BHOrderParameter> (BHpp, "BHOrderParameter");
  OrderParameter.def (py::init<> ());
  OrderParameter.def (py::init<const BH::BHOrderParameter &> ());
  OrderParameter.def (py::init<const std::string> ());
  OrderParameter.def ("initialize", &BH::BHOrderParameter::initialize);
  OrderParameter.def ("__str__", &BH::OparName);
  OrderParameter.def ("__repr__", [] (BH::BHOrderParameter &x) {
    return "BH::BHOrderParameter(" + BH::OparName (x) + ")";
  });

  auto OPsettings = py::class_<BHSettings::OPSettings> (BHpp, "BHOPSettings");
  OPsettings.def_readwrite ("OP", &BHSettings::OPSettings::OP_);
  OPsettings.def_readwrite (
    "parametersForOutput", &BHSettings::OPSettings::parametersForOutput_);
  OPsettings.def_readwrite ("histoPars", &BHSettings::OPSettings::histoPars_);

  auto settings = py::class_<BHSettings> (BHpp, "BHSettings");
  settings.def (py::init<> ());
  settings.def_readwrite ("RngSeed", &BHSettings::RngSeed_);
  settings.def_readwrite (
    "minimizationAlgorithm", &BHSettings::minimizationAlgorithm_);
  settings.def_readwrite ("namePotpar", &BHSettings::namePotpar_);
  settings.def_readwrite ("monteCarloSteps", &BHSettings::nMC_);
  settings.def_readwrite ("WalkersFiles", &BHSettings::walkerFilenames_);
  settings.def (
    "addWalker", [] (BHSettings &x) { x.walkerFilenames_.emplace_back (); });
  settings.def (
    "numWalkers", [] (BHSettings &x) { return x.walkerFilenames_.size (); });
  settings.def (
    "OP", [] (BHSettings &x, int i) { return x.OPSettings_[i].OP_; });
  settings.def ("OP", [] (BHSettings &x, int i, BH::BHOrderParameter op) {
    return x.OPSettings_[i].OP_ = op;
  });
  settings.def ("addOP", [] (BHSettings &x, BH::BHOrderParameter op) {
    x.OPSettings_.push_back (BHSettings::OPSettings{
      op, BHSettings::parameterSettingsForOutput (),
      BH::BHHisto::BHHistoPars ()});
  });
  settings.def ("numOP", [] (BHSettings &x) { return x.OPSettings_.size (); });
  settings.def_readwrite ("renewHisto", &BHSettings::renewHisto_);
  settings.def_readwrite ("histoWeight", &BHSettings::histoWeight_);
  settings.def_readwrite ("landingAlpha", &BHSettings::landingAlpha_);
  settings.def_readwrite ("HistogramOutFile", &BHSettings::HistogramOutFile_);
  settings.def_readwrite (
    "separateWalkerOutput", &BHSettings::separateWalkerOutput_);
  settings.def_readwrite ("plotMinima", &BHSettings::plotMinima_);
  settings.def_readwrite (
    "analyzeAtEveryStep", &BHSettings::analyzeAtEveryStep_);
  settings.def_readwrite ("saveHistogramData", &BHSettings::saveHistogramData_);
  /*settings.def ("__str__", [] (const BHSettings &x) {
    std::stringstream ss;
    printData (ss, x);
    return ss.str ();
  });*/

  auto walkerFiles =
    py::class_<BH::BHSettings::walkerFiles> (settings, "BHwalkerFiles");
  walkerFiles.def_readwrite (
    "flyingWalker", &BH::BHSettings::walkerFiles::flyingWalker);
  walkerFiles.def_readwrite (
    "landingWalker", &BH::BHSettings::walkerFiles::landingWalker);
  walkerFiles.def_readwrite (
    "hikingWalker", &BH::BHSettings::walkerFiles::hikingWalker);

  auto outputParameterSettings =
    py::class_<BHSettings::parameterSettingsForOutput> (
      settings, "BHOutputParametersSettings");
  outputParameterSettings.def (py::init<> ());
  outputParameterSettings.def_readwrite (
    "paramMin", &BHSettings::parameterSettingsForOutput::paramMin);
  outputParameterSettings.def_readwrite (
    "paramMax", &BHSettings::parameterSettingsForOutput::paramMax);
  outputParameterSettings.def_readwrite (
    "interval", &BHSettings::parameterSettingsForOutput::interval);
  outputParameterSettings.def_readwrite (
    "partitioning", &BHSettings::parameterSettingsForOutput::partitioning);
  outputParameterSettings.def_readwrite (
    "active", &BHSettings::parameterSettingsForOutput::active);

  auto clusterAnalyser =
    py::class_<BH::BHClusterAnalyser> (BHpp, "BHClusterAnalyser");
  clusterAnalyser.def (
    py::init<
      const std::vector<BH::BHOrderParameter> &, const BHMetalParameters &> ());
  clusterAnalyser.def (
    "parameterAnalysis", &BH::BHClusterAnalyser::ParameterAnalysis);

  auto basinHopping = py::class_<BasinHopping> (BHpp, "BHBasinHoppingExecutor");
  basinHopping.def (py::init<const BHSettings &> ());
  basinHopping.def ("executeBH", [] (BasinHopping &x) {
    try {
      x.Processing ();
    } catch (const char *problem) {
      cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m" << endl;
    } catch (const std::string &problem) {
      cerr << "\033[1;31mERROR: \033[0m\033[1m" << problem << "\033[0m" << endl;
    }
  });
  /*
  .def("executeBH",[](BasinHopping& x){try{x.Processing();} catch (const
char *problem) { PyErr_SetString(PyExc_RuntimeError, problem); } catch
(const std::string &problem) { PyErr_SetString(PyExc_RuntimeError,
problem.c_str());
}})*/

  // minimization algorithms
  auto baseMinimizator = py::class_<
    BH::BHMinimizationAlgorithm,
    BHpyMinimization<BH::BHMinimizationAlgorithm>> (BHpp, "BHMinimizator");
  auto MiminimizatorLBFGS = py::class_<
    BH::BHMinimizationAlgorithm_LBFGSB, BH::BHMinimizationAlgorithm,
    BHpyMinimization<BH::BHMinimizationAlgorithm_LBFGSB>> (
    BHpp, "BHLBFGSBMinimizator");
  MiminimizatorLBFGS.def (py::init<> ());
  auto MinimizatorFIRE = py::class_<
    BH::BHMinimizationAlgorithm_FIRE, BH::BHMinimizationAlgorithm,
    BHpyMinimization<BH::BHMinimizationAlgorithm_FIRE>> (
    BHpp, "BHFIREMinimizator");
  MinimizatorFIRE.def (py::init<> ());

  auto lbfgsInterface =
    py::class_<BH::BHWWF::BHLBFGSBInterface> (BHpp, "BHLBFGSInterface");
  lbfgsInterface.def (
    py::init<unsigned, unsigned> (), py::arg ("N"), py::arg ("M") = 7);
  lbfgsInterface.def (
    "step", &BH::BHWWF::BHLBFGSBInterface::step, py::arg ("inputenergy"),
    py::arg ("positions"), py::arg ("gradient"));
  lbfgsInterface.def (
    "setTaskToSTART", &BH::BHWWF::BHLBFGSBInterface::setTaskToSTART);
  lbfgsInterface.def (
    "setTaskToSTOP", &BH::BHWWF::BHLBFGSBInterface::setTaskToSTOP);
  lbfgsInterface.def (
    "setVerbosity", &BH::BHWWF::BHLBFGSBInterface::setVerbosity);
  lbfgsInterface.def (
    "setFunctionTolerance",
    &BH::BHWWF::BHLBFGSBInterface::setFunctionTolerance);
  lbfgsInterface.def (
    "setGradientTolerance",
    &BH::BHWWF::BHLBFGSBInterface::setGradientTolerance);
  lbfgsInterface.def (
    "setLowerBoundFor", &BH::BHWWF::BHLBFGSBInterface::setLowerBoundFor);
  lbfgsInterface.def (
    "setUpperAndLowerBoundsFor",
    &BH::BHWWF::BHLBFGSBInterface::setUpperAndLowerBoundsFor);
  lbfgsInterface.def (
    "setUpperBoundFor", &BH::BHWWF::BHLBFGSBInterface::setUpperBoundFor);
  lbfgsInterface.def (
    "setNoBoundsFor", &BH::BHWWF::BHLBFGSBInterface::setNoBoundsFor);
  lbfgsInterface.def ("nstep", &BH::BHWWF::BHLBFGSBInterface::nstep);
  lbfgsInterface.def (
    "nFunctionEvaluationTotal",
    &BH::BHWWF::BHLBFGSBInterface::nFunctionEvaluationTotal);
  lbfgsInterface.def (
    "nFunctionIterationCurrentStep",
    &BH::BHWWF::BHLBFGSBInterface::nFunctionIterationCurrentStep);
  lbfgsInterface.def (
    "projectedGradientNorm",
    &BH::BHWWF::BHLBFGSBInterface::projectedGradientNorm);
  lbfgsInterface.def (
    "fValuePreviousStep", &BH::BHWWF::BHLBFGSBInterface::fValuePreviousStep);
  lbfgsInterface.def (
    "giveCurrentTask", &BH::BHWWF::BHLBFGSBInterface::giveCurrentTask);
  lbfgsInterface.def ("getTask", &BH::BHWWF::BHLBFGSBInterface::getTask);
  lbfgsInterface.def ("getLsave", &BH::BHWWF::BHLBFGSBInterface::getLsave);
  lbfgsInterface.def ("getIsave", &BH::BHWWF::BHLBFGSBInterface::getIsave);
  lbfgsInterface.def ("getDsave", &BH::BHWWF::BHLBFGSBInterface::getDsave);

  auto LBFGSBTR = py::enum_<BH::BHWWF::BHLBFGSBInterface::taskResult> (
    lbfgsInterface, "BHLBFGSBTaskResult");
  LBFGSBTR.value ("FG", BH::BHWWF::BHLBFGSBInterface::taskResult::FG);
  LBFGSBTR.value ("NEW_X", BH::BHWWF::BHLBFGSBInterface::taskResult::NEW_X);
  LBFGSBTR.value ("ERROR", BH::BHWWF::BHLBFGSBInterface::taskResult::ERROR);
  LBFGSBTR.value ("ABNO", BH::BHWWF::BHLBFGSBInterface::taskResult::ABNO);
  LBFGSBTR.value ("WARN", BH::BHWWF::BHLBFGSBInterface::taskResult::WARN);
  LBFGSBTR.value ("CONV", BH::BHWWF::BHLBFGSBInterface::taskResult::CONV);
  LBFGSBTR.value ("STOP", BH::BHWWF::BHLBFGSBInterface::taskResult::STOP);

  // definition to make pip find the lbfgsb.so
  BHpp.def ("setulb", &BH::BHWWF::setulb_);
}
