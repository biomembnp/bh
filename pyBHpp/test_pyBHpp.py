import pyBHpp
from pyBHpp import BHAtom, BHVector
import unittest
from math import sqrt, pi

def give555Signature():
    return pyBHpp.BHCluster([
        BHAtom("Cu", -0.364776857, -1.16683214, 0.215925143),
        BHAtom("Cu", 0.424477143, 1.35708786, -0.251014857),
        BHAtom("Ag", -1.93382586, 0.304076857, -1.39194086),
        BHAtom("Ag", -1.79985286, 0.776672857, 1.38829614),
        BHAtom("Ag", 2.28071814, -0.753118143, 0.0178561429),
        BHAtom("Ag", 0.587981143, -0.641160143, -2.23879286),
        BHAtom("Ag", 0.805279143, 0.123272857, 2.25967114)])

def giveA422Signature():
    return pyBHpp.BHCluster([
        BHAtom("Ag",    1.44500,    0.83427,    2.35968),
        BHAtom("Ag",    0.00000,    0.00000,    4.71935),
        BHAtom("Ag",    1.44500,    0.83427,    7.07903),
        BHAtom("Ag",    1.44500,    2.50281,    4.71935),
        BHAtom("Ag",    2.89000,    0.00000,    4.71935),
        BHAtom("Ag",    4.33500,    2.50281,    4.71935)])

def giveA421Signature():
    return pyBHpp.BHCluster([
        BHAtom("Ag",    1.44500,    0.83427,   2.35968),
        BHAtom("Ag",    2.89000,   -1.66854,   2.35968),
        BHAtom("Ag",    4.33500,    0.83427,   2.35968),
        BHAtom("Ag",    2.89000,    0.00000,   4.71935),
        BHAtom("Ag",    4.33500,    2.50281,   4.71935),
        BHAtom("Ag",    5.78000,    0.00000,   4.71935)])

def giveA444Signature():
    return pyBHpp.BHCluster([
        BHAtom("Ag", 1.66854, 1.66854, 1.66854),
        BHAtom("Ag", 5.00563, 1.66854, 1.66854),
        BHAtom("Ag", 3.33708, 0.00000, 0.00000),
        BHAtom("Ag", 3.33708, 3.33708, 0.00000),
        BHAtom("Ag", 3.33708, 0.00000, 3.33708),
        BHAtom("Ag", 3.33708, 3.33708, 3.33708)])

def giveA666Signature():
    return pyBHpp.BHCluster([
        BHAtom("Ag", 1.66854, 1.66854, 1.66854),
        BHAtom("Ag", 0.00000, 3.33708, 3.33708),
        BHAtom("Ag", -1.66854, 1.66854, 1.66854),
        BHAtom("Ag", 0.00000, 3.33708, 0.00000),
        BHAtom("Ag", 0.00000, 0.00000, 3.33708),
        BHAtom("Ag", 1.66854, 5.00563, 1.66854),
        BHAtom("Ag", 1.66854, 1.66854, 5.00563),
        BHAtom("Ag", 3.33708, 3.33708, 3.33708)])

def checkArraysAlmostEqual(checker,a,b):
    checker.assertEqual(len(a),len(b))
    for i in range(len(a)) :
        checker.assertAlmostEqual(a[i],b[i])

class BHVectorTest(unittest.TestCase):
    def testVector(self):
        """
        Initializations
        """
        a=pyBHpp.BHVector()

        #self.assertEqual(a,b)
        self.assertEqual(a.R,0.0)

        a.X=1.0
        a.Y=2.0
        a.Z=3.0
        self.assertEqual(a.R,sqrt(14.0))
        self.assertEqual(a.X,1.0)
        self.assertEqual(a.Y,2.0)
        self.assertEqual(a.Z,3.0)
        a.setXYZ(2,3,4)
        self.assertEqual(a.R,sqrt(29.0))
        self.assertEqual(a.X,2.0)
        self.assertEqual(a.Y,3.0)
        self.assertEqual(a.Z,4.0)

        b=pyBHpp.BHVector(1.0,1.0,1.0)
        self.assertEqual(b.X,1.0)
        self.assertEqual(b.Y,1.0)
        self.assertEqual(b.Z,1.0)
        c=pyBHpp.BHVector()
        c.assign(b)
        self.assertEqual(c.X,1.0)
        c.X=2
        self.assertEqual(b.X,1.0)
        self.assertEqual(c.X,2.0)

    def testVectorSpherical(self):
        vX=pyBHpp.BHVector(1.0,0,0)
        self.assertEqual(vX.Theta,pi/2)
        self.assertEqual(vX.Phi,0.0)

        vY=pyBHpp.BHVector(0,1.0,0)
        self.assertEqual(vY.Theta,pi/2)
        self.assertEqual(vY.Phi,pi/2)

        vZ=pyBHpp.BHVector(0,0,1.0)
        self.assertEqual(vZ.Theta,0)
        self.assertEqual(vZ.Phi,0)

    def testVectorOperators(self):
        #Given the axes
        vX=pyBHpp.BHVector(1.0,0,0)
        vY=pyBHpp.BHVector(0,1.0,0)
        vZ=pyBHpp.BHVector(0,0,1.0)
        vres=vX+vY
        self.assertEqual(vres.X,1.0)
        self.assertEqual(vres.Y,1.0)
        vres=vX*2.0
        self.assertEqual(vres.X,2.0)
        vres=3.0*vX
        self.assertEqual(vres.X,3.0)
        self.assertEqual(vX.dist(vY),sqrt(2))
        self.assertEqual(vX.distSquared(vY),2)

    def testAtom(self):
        #test if atom inheriths correctly
        atm=pyBHpp.BHAtom()
        atm.setXYZ(1.0,2.0,3.0)
        atm.Type="Ag"
        self.assertEqual(atm.R,sqrt(14.0))
        self.assertEqual(atm.X,1.0)
        self.assertEqual(atm.Y,2.0)
        self.assertEqual(atm.Z,3.0)
        self.assertEqual(atm.Type,"Ag")

class BHSettingsTest(unittest.TestCase):
    def testWorks(self):
        #given a BHsettings
        a=pyBHpp.BHSettings()
        #let's see if the default variables works
        self.assertEqual(a.RngSeed,12345678)
        self.assertEqual(a.numOP(),2)
        self.assertEqual(a.numWalkers(),1)
        self.assertEqual(a.monteCarloSteps,0)

        self.assertEqual(a.WalkersFiles[0].flyingWalker,"defaultWalker.in")
        self.assertEqual(a.WalkersFiles[0].landingWalker,"")
        self.assertEqual(a.WalkersFiles[0].hikingWalker,"")

        a.WalkersFiles[0].flyingWalker="nW.in"
        a.WalkersFiles[0].landingWalker="nWF.in"
        a.WalkersFiles[0].hikingWalker="nWH.in"
        self.assertEqual(a.WalkersFiles[0].flyingWalker,"nW.in")
        self.assertEqual(a.WalkersFiles[0].landingWalker,"nWF.in")
        self.assertEqual(a.WalkersFiles[0].hikingWalker,"nWH.in")

        a.addWalker()
        self.assertEqual(a.numWalkers(),2)

        self.assertTrue(a.plotMinima)
        a.plotMinima=False
        self.assertFalse(a.plotMinima)

        #set create a new OP
        g=pyBHpp.BHOrderParameter("543");
        self.assertEqual(str(g),"sign_543")
        self.assertEqual(repr(g),"BH::BHOrderParameter(sign_543)")
        #and add it to the settings
        a.addOP(g)
        self.assertEqual(a.numOP(),3)
        self.assertEqual(str(a.OP(2)),"sign_543")

class BHHistoTest(unittest.TestCase):
    def testWorksSettings(self):
        #default values
        hp = pyBHpp.BHHistoPars()
        self.assertEqual(hp.start,0.0)
        self.assertEqual(hp.end,1.0)
        self.assertEqual(hp.step,1.0)
        self.assertEqual(hp.nbins,1)
        #changing start should change also only the step
        hp.start=0.3
        self.assertEqual(hp.start,0.3)
        self.assertEqual(hp.step,0.7)
        self.assertEqual(hp.end,1.0)
        self.assertEqual(hp.nbins,1)
        #changing end should change also only the step
        hp.end=0.5
        self.assertEqual(hp.start,0.3)
        self.assertEqual(hp.step,0.2)
        self.assertEqual(hp.end,0.5)
        self.assertEqual(hp.nbins,1)
        #changing the number of bins should change also only the step
        hp.nbins=2
        self.assertEqual(hp.start,0.3)
        self.assertEqual(hp.step,0.1)
        self.assertEqual(hp.end,0.5)
        self.assertEqual(hp.nbins,2)

class BHBasinHoppingTest(unittest.TestCase):
    def testWalkerSettings(self):
        #default
        ws=pyBHpp.BHWalkerSettings()
        self.assertEqual(ws.ChosenAlgorithm,pyBHpp.BHwalkerAlgorithmType.stnd)
        self.assertEqual(ws.WalkerRepulsion,0.7)
        self.assertEqual(ws.OPspaceSettings[0].RepulsionZone.high,1.0)
        self.assertEqual(ws.OPspaceSettings[0].RepulsionZone.low,0.0)
        self.assertEqual(ws.OPspaceSettings[0].WalkerWidth,0.01)
        self.assertEqual(ws.OPspaceSettings[1].RepulsionZone.high,1.0)
        self.assertEqual(ws.OPspaceSettings[1].RepulsionZone.low,0.0)
        self.assertEqual(ws.OPspaceSettings[1].WalkerWidth,1.0)
        self.assertEqual(len(ws.interactWith),0)
        self.assertEqual(len(ws.MovesStrings),0)

    def testWorksBH(self):
        bhs=pyBHpp.BHSettings()
        bhs.monteCarloSteps=1
        bhs.WalkersFiles[0].flyingWalker="inputWalkerTEST.in"
        bhs.namePotpar="AgCu.in"
        bh = pyBHpp.BHBasinHoppingExecutor(bhs)
        try:
            #bh.executeBH()
            bh
        except Exception as e:
            print( "Error: %s" % str(e) )

class BHClusterTest(unittest.TestCase):
    def testClusterAtoms(self):
        cl=pyBHpp.BHClusterAtoms()
        self.assertEqual(cl.NofAtoms,0)
        cl=pyBHpp.BHClusterAtoms(10)
        self.assertEqual(cl.NofAtoms,10)
        cl.NofAtoms=5
        self.assertEqual(cl.NofAtoms,5)
        atomArray=[]
        for i in range(6):
            atomArray.append(pyBHpp.BHAtom("Ag",i,i,i))
        cl=pyBHpp.BHClusterAtoms(atomArray)
        self.assertEqual(cl.NofAtoms,len(atomArray))
        for i in range(len(atomArray)):
            self.assertEqual(cl.Atom(i).Type,atomArray[i].Type)
        cl.setAtom(3,BHAtom("Cu",0.2,0.2,0.2))
        self.assertEqual(cl.Atom(3).Type,"Cu")
        #print(repr(cl))
        cl.updateComposition()
        print(cl.Composition)
        self.assertEqual(cl.Composition,"Ag Cu")
        tl=pyBHpp.getCenteredCluster(cl)
        #print(str(cl))
        #print(str(tl))

    def testClusterAtoms(self):
        agcu="""#NofMetals:
2
#Metal parameters:
#Cohesion energy[eV]	Atomic radius[A]	Mass[amu]
Ag     2.949710351788	    1.445	108.
Cu     3.4997844019608	    1.28	64.
#Interaction Parameters
#kind	     met1	     met2	p	q	a qsi	cutOff_Start	cutOff_End	interaction_NNdist[facultative]
SMATB	     Ag	     Ag		10.85	3.18	0.1031	1.1895	4.08707719	5.0056268338740553
SMATB	     Ag	     Cu		10.70	2.805	0.0977	1.2275	4.08707719	4.4340500673763259
SMATB	     Cu	     Cu		10.55	2.43	0.0894	1.2799	3.62038672	4.4340500673763259
#comments terminate the reading process
"""

        bhmp=pyBHpp.BHMetalParameters()
        bhmp.loadFromString(agcu)
        
        cl=pyBHpp.BHCluster()
        self.assertEqual(cl.NofAtoms,0)
        cl.NofAtoms=4
        cl.setAtom(0,BHAtom("Ag",0.0,0.0,0.0))
        cl.setAtom(1,BHAtom("Ag",2.88,0.0,0.0))
        cl.setAtom(2,BHAtom("Cu",0.0,0.0,2.6))
        cl.setAtom(3,BHAtom("Ag",-2.88,0.0,0.0))
        bhmp.setInteractionLabels(cl)
        bhmp.calcNeighbourhood(cl)
        self.assertEqual(len(cl.getNNs(0)),3)
        self.assertEqual(len(cl.getNNs(1)),1)
        self.assertEqual(len(cl.getNNs(2)),1)
        self.assertEqual(len(cl.getNNs(3)),1)
        self.assertEqual(cl.getInteractionLabel(0,2),1)
        self.assertEqual(cl.getInteractionLabel(0,1),0)

    def testAnalysis(self):
        OPArray=[]
        OPArray.append(pyBHpp.BHOrderParameter("666"))
        OPArray.append(pyBHpp.BHOrderParameter("555"))
        OPArray.append(pyBHpp.BHOrderParameter("444"))
        OPArray.append(pyBHpp.BHOrderParameter("422"))
        OPArray.append(pyBHpp.BHOrderParameter("421"))
        agcu="""#NofMetals:
2
#Metal parameters:
#Cohesion energy[eV]	Atomic radius[A]	Mass[amu]
Ag     2.949710351788	    1.445	108.
Cu     3.4997844019608	    1.28	64.
#Interaction Parameters
#kind	     met1	     met2	p	q	a qsi	cutOff_Start	cutOff_End	interaction_NNdist[facultative]
SMATB	     Ag	     Ag		10.85	3.18	0.1031	1.1895	4.08707719	5.0056268338740553
SMATB	     Ag	     Cu		10.70	2.805	0.0977	1.2275	4.08707719	4.4340500673763259
SMATB	     Cu	     Cu		10.55	2.43	0.0894	1.2799	3.62038672	4.4340500673763259
#comments terminate the reading process
"""
        
        bhmp=pyBHpp.BHMetalParameters()
        bhmp.loadFromString(agcu)
        
        #given a BHClusterAnalyser initalizated with an array of OPs
        ca=pyBHpp.BHClusterAnalyser(OPArray,bhmp)
        cl555=give555Signature()
        bhmp.setInteractionLabels(cl555)
        OPs=ca.parameterAnalysis(cl555,bhmp)

        checkArraysAlmostEqual(self,OPs,[0.0,0.0625,0.0,0.0,0.0])

        cl422=giveA422Signature()
        bhmp.setInteractionLabels(cl422)
        OPs=ca.parameterAnalysis(cl422,bhmp)
        checkArraysAlmostEqual(self,OPs,[0.0,0.0,0.0,0.0909090909,0.0])

        cl421=giveA421Signature()
        bhmp.setInteractionLabels(cl421)
        OPs=ca.parameterAnalysis(cl421,bhmp)
        checkArraysAlmostEqual(self,OPs,[0.0,0.0,0.0,0.0,0.0909090909])

        cl444=giveA444Signature()
        bhmp.setInteractionLabels(cl444)
        OPs=ca.parameterAnalysis(cl444,bhmp)
        checkArraysAlmostEqual(self,OPs,[0.0,0.0,0.0769230769,0.0,0.0])

        cl666=giveA666Signature()
        bhmp.setInteractionLabels(cl666)
        OPs=ca.parameterAnalysis(cl666,bhmp)
        checkArraysAlmostEqual(self,OPs,[0.0526315789,0.0,0.0,0.0,0.0])



if __name__ == "__main__":
     unittest.main()
