#ifndef BHENUMTEST_HPP
#define BHENUMTEST_HPP
#define WALKER_ALGORITHM_PARSER
#define WalkerAlgorithmParser(key, Class, standalone, skipSteps) key,
namespace BH {
enum class BHalgorithm {
#include "BHwalkerAlgorithms.hpp"
  NofKnownAlgoriythms
};
}
#undef WalkerAlgorithmParser
#undef WALKER_ALGORITHM_PARSER

#endif // BHENUMTEST_HPP
