# Changelog

## Changes since v0.20

- Bugfix: Now the analysis is perfomed on accepted moves, as intended
- Bugfix: Now the parser for input files can ignore spaces and tabs before string variables
- Bugfix: Various bugs in the cluster creator
- New feature: Now the cluster creator can output generic atoms with radius 1 Å
- Bugfix: Q4 and Q6 parameters now are correctly computed
