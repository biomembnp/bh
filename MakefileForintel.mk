#PATH		:=

CCINC		= -iquoteinclude -iquote$(BUILDDIR)
CC		= icc

#if you are using a obsolete version of gcc add the flag -std=c++11
CPPOPTIMIZATION	= -O0 -g
CFLAGS		= $(INCLUDE) -Wall -Wextra $(CPPOPTIMIZATION)
CMAINFLAGS	= $(CFLAGS)


#quando non mi serve piu` una build di debug aggiungere -O3 a CFLAGS e -s alle Mainflags e togliere -g
F90		= ifort
F90OPTIMIZZATIONS = -O0 -g -check all
F90FLAGS	= -cpp $(F90OPTIMIZZATIONS)

FC		= ifort
FOPTIMIZZATIONS = -O3
FFLAGS		= -cpp $(FOPTIMIZZATIONS) -malign-double -pedantic -Wall -fbounds-check -Wno-uninitialized 


#F90MOD_OUT	= -J$(BUILDDIR)
F90MOD_DIR	= -I$(BUILDDIR)

LINKER		= $(F90)
LINKERFLAGS 	:= -nofor_main -fexceptions #-s
LINKERLIBS 	:= -cxxlib
