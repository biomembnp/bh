MetalParameters file {#parserMetalParameters}
=================
@brief How to write a MetalParameters.in file

The file should be divided in two parts: the first will be occuped by the declarration of the metals, and the second will be occuped by the delaration of the interaction parameters for the SMATB approximation.

Here an exaple for a file with silver and copper, NB: you can also have more than two metal in the same file, the routine in @ref BH::BHEnergyCalculator and @ref BH::BHWWF will select only the necessary kind of atoms.
~~~
#NofMetals:
2
#Metal parameters:
#Cohesion energy[eV]	Atomic radius[A]	Mass[amu]
Ag     2.949710351788	    1.445	108.
Cu     3.4997844019608	    1.28	64.
#Interaction Parameters
#met1	     met2	p	q	a	qsi	cutOff_Start	cutOff_End
Ag	     Ag		10.85	3.18	0.1031	1.1895	4.08707719	5.0056268338740553
Cu	     Cu		10.55	2.43	0.0894	1.2799	3.62038672	4.4340500673763259
Ag	     Cu		10.70	2.805	0.0977	1.2275	4.08707719	4.4340500673763259
#comments terminate the reading process
various comments
~~~
