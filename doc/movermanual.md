How to start mover {#movermanual}
=================
@brief This page will show how to set up the files for a Basing Hopping run with `mover` and an external tool for minimization.

`mover` does the "Monte Carlo Thing" and the move  needed by the Basin Hopping but does not do the local minimization part of the algorithm.

`mover` can also assign to a .xzy file an energy in eV along with some basic order parameters given an energy in Rydberg or Hartree.

For all uses of the program yhou need to have in the working directory a file named _MetalParameters.in_ with the data for the desired metals.

### Use as Basing Hopping algorithm ###

For doing a basing hopping run along an external program two files are mandatory:

- MetalParameter.in where metal parameters are written
- mover.in where the settings for the walker are stored, see [this page](@ref parserBHWalkerSettings) to see how this file is organized

#### Start the algorithm
The commad to use for generating the headers for the files  `ener_accepted.out`, `ener_all.out` and `moves.out` is:

`mover --start`

#### Run the algorithm
The commad to use for generating the next BH step to be minimized is:

`mover last_accepted.xyz new_move.xyz output.xyz lastEnergy newEnergy TMC Ht/Ry`

where the various symbosl corresponds to:

 - `last_accepted.xyz`	file with the last accepted move, uses this as a base for the next move if last move is **rejeted**
 - `new_move.xyz`		file with the last minimized move, uses this as a base for the next move if last move is **accepted**
 - `output.xyz`		file in which the configuration of next move will be stored
 - `lastEnergy`		the last accepted energy
 - `newEnergy`		the energy of the new move
 - `TMC`			the Monte Carlo's temperature
 - `Ht/Ry`			write "Ht" or "Ry" to chose in which unity you are giving the energy to the routine

#### Exit status
The exit status of `mover` will be `0` if the last move has been accepted or `1` if has been rejected

### Use as a mover
 `mover` can be used for doing a move on a configuration, two files are mandatory:

- MetalParameter.in where metal parameters are written
- mover.in where the settings for the walker are stored, see [this page](@ref parserBHWalkerSettings) to see how this file is organized

With the command `mover original.xyz moved.xyz` a move will be made on the first file (following the settings in mover.in) and the result will be written on the second file

### Assigning energy in eV to a cluster

by the command `mover --Ry input_file.xyz EnergyInRy` or`mover --Ht input_file.xyz EnergyInHt` one could update a .xyz file with the energy in eV in the comment line, given the energy in Ry or Ht.
