Settings file for BH::BHSettings {#parserBHSettings}
================
@brief In this page will be explained how the parser for the run settings works.

The parser for @ref BH::BHSettings
===
There is an alternate implementation for writing the desired settings for the program.

You can write a lighter version of input_bh++.in.

The new versions can contain comments. You can start a comment line with `*#!`.
To tell the parser that you are giving a parsed input you need the first line containing the word `parsed`, lowercase.

The parser works with cards to divide the parameters in separated arguments. To enter in a specific card simply add a comment block with the name of the card inside in UPPERCASE letters.

The settings must be written in lowercase letters and followed by an equal sign (`=`) and then the data.
To separate the settings you can write them on a new line or separate them with a comma (`,`).

The cards are:

- **GENERAL**
	+ *rng_seed* **integer**, the seed used by the simulation, the default value is `12345678`
- **POTENTIAL**
	+ *name_potpar* **string**, the name of the file with the potential parameters, the default value is `MetalParameters.in`
	+ *cut_off* **not used**
- **RUN**
	+ *n_mc* **integer**, the number of montecarlo steps, default value `0`
- **GOA**
 	+ *histoWeight* **double**, weight for histogram if using the HISTO algorithm, default value `0.0`
	+ *OP1*	**string**, the name of the first Order Parameter
	+ *OP2* **string**, the name of the second Order Parameter
	+ *histo#* where *#* is 1 or 2 for assign setting to the histogram relative to the OP1 or OP2. The settings are:
	  + *.nbins* **int**, the number of bins of the histogram, default value `1`.
	  + *.start* **double**, the starting point for the histogram, default value `0.0`.
	  + *.end* **double**, the end point for the histogram, default value `1.0`.
	  + *.width* **double**, the width if  the histogram, default value `1.0`.
	  + *.step* **double**, the width of the steps of the histogram, default value `1.0`.
	+ *renew_histo* **int**, number of steps after which reset the histogram, default value `MAX_OF_INT`
	+ *freq_walker_exchange* **int**, **not used**
- **OUTPUT**, secondary minima plotting
 	+ *separateWalkerOutput* **bool**, default value `false`, if true adds a new tree o files for each different walker (glo, ener_all and ener_best) 
	+ *plotMinima* **bool**, **not used**, default value `true`, it is always used `true`.
	+ *analyzeAllMinima* **bool**, if set to true analysis is done for each mimimum found, could slow down the calculation, default value `false`
	+ *param_min* **double**, the lower bound for secondary minima storage , default value `0`
	+ *param_max* **double**, the upper bound for secondary minima storage, default value `1`
	+ *parameter_partitioning* **int**, the number of intervals used to plot secondary minima, defaut value `1`
- **WALKERS**, a list of the names of the walker files
	+ this is a special card. After `WALKERS` you must write the number of walker desidered
	+ the parser will read the number of lines indicated and store them as name of file for the walkers
	+ writing more filenames than the number you have indicated does not create errors

###Some examples:###

~~~
#parsed
#GENERAL
rng_seed = 369 #seed for the random number generator
#POTENTIAL
name_potpar = 'MetalParameters.in'
#RUN
n_mc = 10
#GOA
histo1.nbins = 10
histo2.nbins = 10
OP1 = signature 555 #you can also only write the 3 numbers of the signature
OP2 = Cu Ag#looks at the % of bonds of this type: you cna separate the two atoms with spaces or simbols like ,.-_
#OUTPUT
param_min = 0
param_max = 1
parameter_partitioning = 50
#WALKERS 1
input_walker0.in
~~~

~~~
#parsed
#GENERAL
rng_seed = 87654321
#POTENTIAL
name_potpar = "MetalParametersAgCu.in"
#RUN
n_mc = 100
#GOA
histo1.nbins = 20
histo1.end = 0.4
histo2.nbins = 20
histo2.end = 0.4
OP1=555#1
OP2=422#4
#WALKERS 1
input_walker0.in
input_walker0.in
input_walker1.in
#OUTPUT
parameter_partitioning = 500
param_max = 0.4
~~~
