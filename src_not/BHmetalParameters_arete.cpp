#include "BHmetalParameters.hpp"

void BHMetalParameters::selectArete(const std::string& Selected){
  //from the base element construct all the data needed
  int BaseEl=Plabels[(Selected+Selected)];//from "Ag" to "AgAg", i need it for recalling later
  arete = AllArete[BaseEl];
  //first of all create/update the dist list
  //and the cutoffs in arete units
  for( auto it = Plabels.begin(); it != Plabels.end(); ++it){
    ANNdist[it->second] = NNdist[it->second] / arete;
    AcutOff_start[it->second] = cutOff_start[it->second] / arete;
    AcutOff_end[it->second] = cutOff_end[it->second] / arete;
  }
  /*
    !dist are the nearest neighbours distances in arete(1) units
    dist(1)=1.d0/dsqrt(2.d0)
    dist(2)=nn(2)/arete(1)
    dist(3)=nn(3)/arete(1)
  */
#ifdef METALPARDEBUG
  cout << "m1m2\tdist\t\tAcutOff_start\tAcutOff_end" << endl;
  int op= cout.precision();
  cout << std::setprecision(9);
  for( auto it = Plabels.begin(); it != Plabels.end(); ++it )
    cout << it->first << "\t" << ANNdist[it->second] << "\t" <<AcutOff_start[it->second]
	 <<"\t" <<AcutOff_end[it->second] <<endl;
  cout << std::setprecision(op);
#endif //METALPARDEBUG
  
  //then create/update the variables for the polynoms
  for( auto it = Plabels.begin(); it != Plabels.end(); ++it ){
    BHdouble dik0 = ANNdist[it->second];//equilibrium distance
    int i = it->second;
    BHdouble es =  AcutOff_end[i] - AcutOff_start[i];//non capisco perche` e` end - start e non start-end dato che nel pdf del gupta il link e` a x-ce
    BHdouble es2 = es*es;
    BHdouble es3 = es2*es;
    //variables for poly for p and A
    BHdouble ap, bp, cp;
    BHdouble expp = a[i] * exp(p[i]*(1.-AcutOff_start[i]/dik0));
    ap = -1./es3;
    bp = p[i]/(dik0*es2);
    cp = -(p[i]*p[i])/(es*dik0*dik0);
    //a5 in BH
    AP_par5[i]= expp * (12.*ap + 6.*bp + cp)/(2.*es2);
    //a4 in BH
    AP_par4[i]= expp * (15.*ap + 7.*bp + cp)/es;
    //a3 in BH
    AP_par3[i]= expp * (20.*ap + 8.*bp + cp)/2.;
    //variables for poly for q and qsi
    BHdouble aq, bq, cq;
    BHdouble expq = qsi[i]*exp(q[i]*(1.-AcutOff_start[i]/dik0));
    aq = -1/es3;
    bq = q[i]/(es2*dik0);
    cq = -(q[i]*q[i])/(es*dik0*dik0);
    //x5 in BH
    AQ_par5[i]= expq * (12.*aq + 6.*bq + cq)/(2.*es2);
    //x4 in BH
    AQ_par4[i]= expq * (15.*aq + 7.*bq + cq)/es;
    //x3 in BH
    AQ_par3[i]= expq * (20.*aq + 8.*bq + cq)/2.;
#ifdef METALPARDEBUG
    cout << i<<": "<<it->first<<endl
	 <<"p:\t" <<expp <<"?="<< -es3*(AP_par3[i] - es*(AP_par4[i] - es*AP_par5[i]))<<endl
	 <<"q:\t" <<expq <<"?="<< -es3*(AQ_par3[i] - es*(AQ_par4[i] - es*AQ_par5[i]))<<endl;
#endif//METALPARDEBUG
  }
#ifdef METALPARDEBUG
  cout << "m1m2\ta5\ta4\ta3" << endl;
  for( auto it = Plabels.begin(); it != Plabels.end(); ++it ){
    int i = it->second;
    cout << it->first << "\t" << AP_par5[i] << "\t" << AP_par4[i] << "\t" << AP_par3[i] << endl;
  }
  cout << "m1m2\tx5\tx4\tx3" << endl;
  for( auto it = Plabels.begin(); it != Plabels.end(); ++it ){
    int i = it->second;
    cout << it->first << "\t" << AQ_par5[i] << "\t" << AQ_par4[i] << "\t"<< AQ_par3[i] << endl;
  }
#endif//METALPARDEBUG
    
#ifdef CUTOFFDEBUG
  BHdouble minimum = 1e99, maximum = 0;
  for( auto it = Plabels.begin(); it != Plabels.end(); ++it ){
    if(AcutOff_start[it->second] < minimum)
      minimum = AcutOff_start[it->second];
    if(AcutOff_end[it->second] > maximum)
      maximum = AcutOff_end[it->second];
  }
  BHdouble del = maximum-minimum;
  minimum-=del/10.;
  del = maximum - minimum;
  std::ofstream qfile("aqcut.out"), pfile("apcut.out");
  qfile << "x\t";
  pfile << "x\t";
  for( auto it = Plabels.begin(); it != Plabels.end(); ++it ){
    qfile << it->first << "\t";
    pfile << it->first << "\t";
  }
  qfile << std::endl;
  pfile << std::endl;  
  for(int i=0;i<1000;i++){
    BHdouble myx = minimum + del*(i/1000.);
    qfile << myx << "\t";
    pfile << myx << "\t";
    for( auto it = Plabels.begin(); it != Plabels.end(); ++it ){
      int i = it->second;
      if(myx<AcutOff_start[i]){
	BHdouble espo = 1-myx/ANNdist[i];
	qfile << qsi[i]*exp(q[i]*espo) << "\t";
	pfile << a[i]*exp(p[i]*espo) << "\t";
      }else if(myx<AcutOff_end[i]){
	BHdouble Tmyx = myx - AcutOff_end[i];
	BHdouble myx2, myx3;
	myx2 = Tmyx*Tmyx;
	myx3 = myx2*Tmyx;
	BHdouble Qpoly =  myx3*(AQ_par3[i] + Tmyx*(AQ_par4[i]+Tmyx*AQ_par5[i]));
	BHdouble Ppoly =  myx3*(AP_par3[i] + Tmyx*(AP_par4[i]+Tmyx*AP_par5[i]));
	qfile << Qpoly <<"\t";
	pfile << Ppoly <<"\t";
      }else{
	qfile << 0 <<"\t";
	pfile << 0 <<"\t";
      }
    }
    qfile << std::endl;
    pfile << std::endl;
  }
#endif //CUTOFFDEBUG
  ready = true;
}

BHdouble BHMetalParameters::calcMMEnergy_arete(const BHClusterAtoms &cluster, const BHNeighboursInfo& neighbourhood,int**larray, BHVector *F){
  int N = cluster.NofAtoms();//sugar for not call everywhere cluster.NofAtoms()
  BHdouble *eb2 = new BHdouble[N];//bound energy contribution, squared
  BHdouble **fb = new BHdouble*[N];//bound force contribution, to be processed
  BHdouble ene = 0;//total energy of the cluster
  for(int i=0;i<N;i++){//initializing the arrays
    eb2[i] = 0;//bound energy contribution from i, squared
    fb[i] = new BHdouble[N];
    F[i] *= 0;//this make me sure that forces start from 0
  }
  for(int i=0;i<N;i++){
    /*where energies are calculated and the force that 
     *comes from the repulsive part of the energy is computed
     */
    BHdouble eri = 0;//repulsive energy contribution from i and k
    for(const int& k:neighbourhood.getINT(i)){//if 0 neighbours, this cycle is not performed
      if(k>i){//if k<i the calculation between k and i are already done
	//defining some sugars for simplify the algo
	int indexTypes = larray[i][k];
	BHdouble dik0 = ANNdist[indexTypes];//equilibrium distance d_{ik0} (in arete units)
	BHdouble dik = cluster[i].dist(cluster[k])/arete;//distance in arete units
	BHdouble fr; //module of the repulsive force acting between the two atoms
	BHdouble aexpp, qsiexpq;//declared here to be less verbose in the if
	if(dik<cutOff_start[indexTypes]){
	  BHdouble expo = 1.-dik/dik0;// -q(d/d0-1) = q(1-d/d0)
	  aexpp = a[indexTypes]*exp(expo*p[indexTypes]);
	  fr = 2.*(p[indexTypes]/dik0)*aexpp;//-1*-1=+1
	  eri += 2.*aexpp;
	  qsiexpq = qsi[indexTypes]*qsi[indexTypes]*exp(2.*expo*q[indexTypes]);
	  fb[i][k] = (q[indexTypes]/dik0)*qsiexpq;
	}else{
	  dik-=cutOff_end[indexTypes];
	  BHdouble myx2,myx3,myx4,myx5;
	  myx2 = dik*dik;
	  myx3 = myx2*dik;
	  myx4 = myx3*dik;
	  myx5 = myx4*dik;
	  eri += 2*((myx3*P_par3[indexTypes]) + (myx4*P_par4[indexTypes]) + (myx5*P_par5[indexTypes]));
	  qsiexpq = (myx3*Q_par3[indexTypes]) + (myx4*Q_par4[indexTypes]) + (myx5*Q_par5[indexTypes]);
	  fr = -2*(myx2*(3*P_par3[indexTypes]) + myx3*(4*P_par4[indexTypes]) + myx4*(5*P_par5[indexTypes]));
	  fb[i][k] = -(myx2*(3*Q_par3[indexTypes]) + myx3*(4*Q_par4[indexTypes]) + myx4*(5*Q_par5[indexTypes]))*qsiexpq;
#ifdef METALPARDEBUG
	  if(dik>0){//tells if something is being calculated further tha cut off end
	    cout << "\t"<<i << "," << k << " " <<indexTypes <<endl;
	    cout << cluster[i]<<endl;
	    cout << cluster[k]<<endl;
	    cout << dik0 << " " << dik+AcutOff_end[indexTypes] <<endl;
	    cout <<"coff: "<<AcutOff_start[indexTypes]<<" -> "<<AcutOff_end[indexTypes]<<endl;
	    cout <<dik<<"\n";
	    cout << aexpp<< ", "<< qsiexpq <<endl;
	    cout <<eri<<" " << eb2[i]<<endl;
	  }
#endif //METALPARDEBUG
	  qsiexpq*=qsiexpq;//ebi is a squareroot of a sum of squares
	}
	//eri += 2*aexpp;//sums to the energy the contribute from the interaction ki and ik, because i wont redo the calc if k<i
	eb2[i] += qsiexpq;
	eb2[k] += qsiexpq;
	/*cout << i+1<<" " << k+1 << " " <<force;
	  cout <<" "<< force/(cluster[i].dist(cluster[k])/arete)<<endl;
	  cout << "\t" << force * BHVector::directionVersorFT(cluster[i],cluster[k])<<endl;*/
	F[i] += fr * BHVector::directionVersorFT(cluster[k],cluster[i]);
	F[k] += fr * BHVector::directionVersorFT(cluster[i],cluster[k]);
	//cout << i<<"\t"<<k<<":: "<<fb[i][k]<<endl;
      }
    }
    BHdouble ebi = sqrt(eb2[i]);
    ene += eri-ebi;
    eb2[i] = 1/ebi;//this will be used only in the next loop
  }

  for(int i=0;i<N;i++){
    //where force that comes from the bound part of the energy is computed
    for(const int& k:neighbourhood.getINT(i)){//if 0 neighbours, this cycle is not performed
      if(k>i){//if k<i the calculation between k and i are already done
	//cout << i+1<<" " << k+1 << " " <<f;
	fb[i][k]*=eb2[i]+eb2[k];
	//	cout << i<<"\t"<<k<<" "<<fb[i][k]<<endl;
	F[i] += fb[i][k] * BHVector::directionVersorFT(cluster[i],cluster[k]);
	F[k] += fb[i][k] * BHVector::directionVersorFT(cluster[k],cluster[i]);
      }
    }
  }
  //freeing memory
  delete[] eb2;
  return ene;
}
