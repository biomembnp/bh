MODULE BH_MODULE
  use iso_c_binding!in realta` e` uno statement per fortran 2003 ma se compila non mi lamento
  USE PARAMETERS
  Implicit None

  SAVE
  !From module BH_mod
  SAVE
  Integer(C_INT) :: nat3d    ! number of atoms in the cluster  
  Integer,        allocatable :: nvois(:), ivois(:,:)!n of neighbours per atom, neighbours list of atom  i
  Integer                     :: nwithvois!number of atoms with neighbours
  Integer,        allocatable :: hasvois(:)!indexes of atoms with neighbours
  Integer(C_INT), allocatable :: itype(:)!type of the atoms

  !From Module Parameters
  Integer(C_INT) :: Ntypes,Nint !different types of atoms and interactions
  !private parameters
  Real(C_DOUBLE),allocatable :: ecoh(:),rat(:),mass(:)
  !interaction parameter
  Real(C_DOUBLE), allocatable :: p(:), q(:), a(:), qsi(:), arete(:)
  Real(C_DOUBLE), allocatable :: a5(:), a4(:), a3(:), x5(:), x4(:), x3(:)
  Real(C_DOUBLE), allocatable :: dist(:)
  Real(C_DOUBLE), allocatable :: cutoff_start(:),&!<the cut off of the exponential, where it became a polinomy
       & cutoff_end(:)!<where the polinom goes to zero
  Real(C_DOUBLE), allocatable :: cutoff_end2(:), cutoff_end_tol2(:)!cutoff_end2 is here for bigvoi
CONTAINS
  !>For each atom a list of neighbours is calculated with some tolerance and is also calculated a list of atoms WITH neighbours, to do smaller loops.
  !!Neighbours are considered only if they have a index greater than the protagonist atom's one.
  !!It is intended to be used with #bhtolgradient().
  SUBROUTINE BHtolvoi(x)
!!$    USE BH_MOD, Only: Nat3D
!!$    USE FUNCTION_PAR, Only: nvmax0,nvois,ivois,&
!!$         hasvois, itype,cutoff_end_tol2,nwithvois,hasvois
    use iso_c_binding!in realta` e` uno statement per fortran 2003 ma se compila non mi lamento
    implicit none
    Real(C_DOUBLE),intent(in)::x(3*Nat3D)
    integer*4 ::i,j,ikjk
    Real(C_DOUBLE) :: dij2,xij,yij,zij

    !fortran does array operation by itself
    nvois = 0
    !hasvois = 0
    !set the number of atoms with neighbours
    nwithvois = 1
#ifdef _OPENMP
    Do i=1,Nat3D
       do j=1,Nat3D
          if (i.eq.j) then
             CYCLE
          endif
          ikjk=3
          if((itype(i).eq.1).and.(itype(j).eq.1)) then
             ikjk=1
          else if ((itype(i).eq.2).and.(itype(j).eq.2)) then
             ikjk=2
          endif
          xij=x(3*j-2)-x(3*i-2)
          yij=x(3*j-1)-x(3*i-1)
          zij=x(3*j)  -x(3*i)

          dij2=xij*xij+yij*yij+zij*zij

          if (dij2.lt.cutoff_end_tol2(ikjk)) then!there are atoms in the tolerance list
             nvois(nwithvois) = nvois(nwithvois)+1
             ivois(nvois(nwithvois),nwithvois) = j
          endif
       enddo
       IF (nvois(nwithvois).gt.0)THEN
          hasvois(nwithvois) = i
          nwithvois = nwithvois+1
       ENDIF
    endDo
#else
    Do i=1,Nat3D-1
       do j=i+1,Nat3D
          ikjk=3
          if((itype(i).eq.1).and.(itype(j).eq.1)) then
             ikjk=1
          else if ((itype(i).eq.2).and.(itype(j).eq.2)) then
             ikjk=2
          endif
          xij=x(3*j-2)-x(3*i-2)
          yij=x(3*j-1)-x(3*i-1)
          zij=x(3*j)  -x(3*i)

          dij2=xij*xij+yij*yij+zij*zij

          if (dij2.lt.cutoff_end_tol2(ikjk)) then!there are atoms in the tolerance list
             nvois(nwithvois) = nvois(nwithvois)+1
             ivois(nvois(nwithvois),nwithvois) = j
          endif
       enddo
       IF (nvois(nwithvois).gt.0)THEN
          hasvois(nwithvois) = i
          nwithvois = nwithvois+1
       ENDIF
    endDo
#endif
    nwithvois = nwithvois-1
!!$    do i=1, 10
!!$       write(*,*)"atomo:",i,hasvois(i),nvois(i)
!!$       write(*,*)ivois(1:nvois(i),i)
!!$    enddo
!!$stop
  END SUBROUTINE BHtolvoi
  !>Gradient of the SMATB energy calculated by using the data precalculated in #bhtolvoi().
  SUBROUTINE BHTolGradient(myx,frx,energy)
!!!!!!!
!!!!!!!NB: forces are returned with inverted sign (gradient)
!!!!!!!
    use iso_c_binding!in realta` e` uno statement per fortran 2003 ma se compila non mi lamento
!!$    USE PARAMETERS
!!$    USE BH_MOD, Only: Nat3D
!!$    USE FUNCTION_PAR, Only:p,q,a,qsi,a5,a4,a3,x5,x4,x3,&
!!$         dist,cutoff_start,cutoff_end,&
!!$         itype,ivois,nvois,cutoff_end2,nwithvois,hasvois
    implicit none
    !inout
    Real(C_DOUBLE),intent(in) ::myx(3*Nat3D)
    Real(C_DOUBLE),intent(out)::frx(3*Nat3D)
    Real(C_DOUBLE),intent(out)::energy
    ! Local variables
    Real(C_DOUBLE) :: den(Nat3D)
    Real(C_DOUBLE) :: ebi,eri,for,forsudik,denik
    !Real(C_DOUBLE) :: fb(Nat3D-1,Nat3D-1)
    !i don't expect that an atom will have more neighbours than the number of atoms with neighbours
    Real(C_DOUBLE) :: fb(nwithvois,nwithvois)!give errors on runtime
    Real(C_DOUBLE) :: xik,yik,zik,dik,dik2, x_i,y_i,z_i
    Real(C_DOUBLE) :: dik0, espo, qsiexpq, aexpp
    Real(C_DOUBLE) :: dikm,dikm2,dikm3,dikm4,dikm5
    Integer :: n_i,n_k,itypik
    Integer :: i,k!indexes of the atoms
    energy=0.d0
    !fortran can do array operation by itself
    den=0.d0
    frx=0.d0
    fb(:,:) = 0

    n_i=1
    !$OMP PARALLEL DO default(shared) PRIVATE(i,k,n_i,n_k,itypik,x_i,y_i,z_i,eri,dik,dik2,dik0, espo, qsiexpq, aexpp,&
    !$OMP& dikm,dikm2,dikm3,dikm4,dikm5,for,forsudik,denik,xik,yik,zik, ebi) reduction(+:energy)
    do 10 i=1,nat3D
       eri=0.d0
#ifndef _OPENMP
       if(i.eq.hasvois(n_i)) then 
#else
          n_i=i
#endif

          x_i = myx(3*i-2)
          y_i = myx(3*i-1)
          z_i = myx(3*i)
          do 20 n_k= 1, nvois(n_i)
             k = ivois(n_k,n_i)
             if((itype(i).eq.1).and.(itype(k).eq.1)) then
                itypik=1   ! stesso metallo A
             else if((itype(i).eq.2).and.(itype(k).eq.2)) then
                itypik=2   ! stesso metallo B
             else
                itypik=3  ! interazione A-B
             endif

             xik=myx(3*k-2)-x_i
             yik=myx(3*k-1)-y_i
             zik=myx(3*k)  -z_i
             dik2=xik*xik+yik*yik+zik*zik
             if (dik2.lt.cutoff_end2(itypik)) then
                dik=dsqrt(dik2)
                if (dik.lt.cutoff_start(itypik)) then
                   dik0=dist(itypik)
                   espo=1.d0-dik/dik0
                   aexpp=dexp(p(itypik)*espo)*a(itypik)
                   for=(2.d0*p(itypik)/dik0)*aexpp
#ifndef _OPENMP
                   eri=eri+2.d0*aexpp
#else
                   eri=eri+aexpp
#endif
                   qsiexpq = (qsi(itypik)*qsi(itypik)) * dexp(2.d0*q(itypik)*espo)
                   fb(n_k,n_i) = (q(itypik)/dik0)*qsiexpq;
                else
                   dikm=dik-cutoff_end(itypik)
                   dikm2=dikm*dikm
                   dikm3=dikm2*dikm
                   dikm4=dikm3*dikm
                   dikm5=dikm4*dikm
                   qsiexpq =x5(itypik)*dikm5+x4(itypik)*dikm4+x3(itypik)*dikm3
                   for=-2.d0*(5.d0*a5(itypik)*dikm4+4.d0*a4(itypik)*dikm3+3.d0*a3(itypik)*dikm2)
#ifndef _OPENMP
                   eri=eri+2*(a5(itypik)*dikm5+a4(itypik)*dikm4+a3(itypik)*dikm3)
#else
                   eri=eri+(a5(itypik)*dikm5+a4(itypik)*dikm4+a3(itypik)*dikm3)
#endif
                   fb(n_k,n_i) = -((5.d0*x5(itypik)*dikm4+4.d0*x4(itypik)*dikm3+3.d0*x3(itypik)*dikm2))*qsiexpq;
                   qsiexpq = qsiexpq**2
                endif
                den(i)=qsiexpq+den(i)
#ifndef _OPENMP
                den(k)=qsiexpq+den(k)
#endif
                forsudik=for/dik

                frx(3*i-2)=frx(3*i-2)+forsudik*xik
                frx(3*i-1)=frx(3*i-1)+forsudik*yik
                frx(3*i)  =frx(3*i)  +forsudik*zik

#ifndef _OPENMP
                frx(3*k-2)=frx(3*k-2)-forsudik*xik
                frx(3*k-1)=frx(3*k-1)-forsudik*yik
                frx(3*k)  =frx(3*k)  -forsudik*zik
#endif
             endif
20        enddo
#ifndef _OPENMP
          n_i=n_i+1
       endif
#endif
       ebi=dsqrt(den(i))
       den(i)=1.d0/ebi
       energy=energy+(eri-ebi)
10  enddo
    !$END OMP PARALLEL DO

    !$OMP PARALLEL DO default (none) private(n_i,i,x_i,y_i,z_i,k,xik,yik,zik,denik,dik)&
    !$OMP shared(den,frx,fb,nvois,ivois,hasvois,myx)
    do 30 n_i=1,nwithvois!i'm pretty sure that won't extist an atom with a greather index than Nat3D
       i = hasvois(n_i)
       x_i = myx(3*i-2)
       y_i = myx(3*i-1)
       z_i = myx(3*i)
       do 40 n_k=1,nvois(n_i)
          if(fb(n_k,n_i).ne.0.d0)then
             k=ivois(n_k,n_i)
             xik=myx(3*k-2)-x_i
             yik=myx(3*k-1)-y_i
             zik=myx(3*k)  -z_i

             dik=dsqrt(xik*xik+yik*yik+zik*zik)
             !fb(n_k,n_i) so this won't run on a "distant" address
             denik=fb(n_k,n_i)*(den(i)+den(k))/dik

             frx(3*i-2)=frx(3*i-2)-denik*xik
             frx(3*i-1)=frx(3*i-1)-denik*yik
             frx(3*i)  =frx(3*i)  -denik*zik
#ifndef _OPENMP
             frx(3*k-2)=frx(3*k-2)+denik*xik
             frx(3*k-1)=frx(3*k-1)+denik*yik
             frx(3*k)  =frx(3*k)  +denik*zik
#endif
          endif
40     enddo
30  enddo
    !$END OMP PARALLEL DO

  END SUBROUTINE BHTolGradient
  !>Create a list of neighbours for each atoms
  SUBROUTINE BHbigvoi(x)
    !ho eliminato tutto cio` che proviene da met-oxi
!!$  USE BH_MOD, Only: Nat3D
!!$  USE FUNCTION_PAR, Only: nvmax0,nvois,ivois,&
!!$       itype,cutoff_end2
!!$  
    implicit none
    Real(8),intent(in)::x(3*Nat3D)
    integer*4 ::i,j,ikjk
    Real(8) :: dij2,xij,yij,zij

    !cutz(1,1)=(1./1.41421356237309504880_8+0.18)**2

    !fortran does array operation by itself
    nvois = 0

    do i=1,Nat3D-1
       do j=i+1,Nat3D
          ikjk=3
          if((itype(i).eq.1).and.(itype(j).eq.1)) then
             ikjk=1
          else if ((itype(i).eq.2).and.(itype(j).eq.2)) then
             ikjk=2
          endif
          xij=x(3*j-2)-x(3*i-2)
          yij=x(3*j-1)-x(3*i-1)
          zij=x(3*j)  -x(3*i)

          dij2=xij*xij+yij*yij+zij*zij

          if (dij2.lt.cutoff_end2(ikjk)) then
             nvois(i) = nvois(i)+1
             ivois(nvois(i),i) = j
             !in order to not add an if(k.gt.i) in BHgradient_rgl i won't count the neighs with a lower index
             !nvois(j) = nvois(j)+1
             !ivois(nvois(j),j) = i
          endif
       enddo

       if (nvois(i).ge.nvmax0) then
          write (*,530) i,nvois(i)
          stop
       endif


    enddo
    ! stop
530 format (' troppi vicini in bigvoi  ',2i5)

    return
  END SUBROUTINE BHbigvoi
  !>Calculate the gradient of the energy, and update the neighbours list for each atoms
  SUBROUTINE BHgradient(myx,frx,energy)
!!!!!!!
!!!!!!!NB: forces are returned with inverted sign (gradient)
!!!!!!!
    use iso_c_binding!in realta` e` uno statement per fortran 2003 ma se compila non mi lamento
    !USE PARAMETERS
!!$  USE BH_MOD, Only: Nat3D
!!$  USE FUNCTION_PAR, Only:p,q,a,qsi,a5,a4,a3,x5,x4,x3,&
!!$       dist,cutoff_start,cutoff_end,&
!!$       itype,ivois,nvois,cutoff_end2
    implicit none
    !inout
    Real(C_DOUBLE),intent(in) ::myx(3*Nat3D)
    Real(C_DOUBLE),intent(out)::frx(3*Nat3D)
    Real(C_DOUBLE),intent(out)::energy
    ! Local variables
    Real(C_DOUBLE) :: den(Nat3D)
    Real(C_DOUBLE) :: ebi,eri,for,forsudik,denik
#ifndef _OPENMP
    Real(C_DOUBLE) :: fb(Nat3D-1,Nat3D-1)
#else
    Real(C_DOUBLE) :: fb(Nat3D-1,Nat3D)
#endif
    !Real(C_DOUBLE) :: xik(Nat3D,Nat3D),yik(Nat3D,Nat3D),zik(Nat3D,Nat3D),d(Nat3D,Nat3D)
    Real(C_DOUBLE) :: xik,yik,zik,dik,dik2,x_i,y_i,z_i
    Real(C_DOUBLE) :: dik0, espo, qsiexpq, aexpp
    Real(C_DOUBLE) :: dikm,dikm2,dikm3,dikm4,dikm5
    Integer :: i,j,k,itypik

    !f(dabs(zat(1))>3275786477704)write(*,*)'0000000000 zat(1)=',zat(1)
!!$  write(*,*)'gradient_rgl'
!!$  write(*,*)'cutoff_start ',cutoff_start
!!$  write(*,*)'cutoff_end ',cutoff_end
!!$  write(*,*)'dist ',dist
!!$
!!$  write(*,*)'a3:',a3
!!$  write(*,*)'a4:',a4
!!$  write(*,*)'a5:',a5
!!$  write(*,*)'x3:',x3
!!$  write(*,*)'x4:',x4
!!$  write(*,*)'x5:',x5
!!$  write(*,*)Nat3D
!!$  stop
    !     ener_debug = energy
    energy=0.d0
    !fortran can do array operation by itself
    den=0.d0
    frx=0.d0
!!$  do  i=1,Nat3D
!!$     write(*,*)i,",",nvois(i),":",ivois(1:nvois(i),i)
!!$  enddo
!!$  stop
    nvois = 0

    !$OMP PARALLEL DO default(shared) PRIVATE(ebi,eri,for,forsudik,denik,xik,yik,zik,dik,dik2,x_i,y_i,z_i,&
    !$OMP &dik0,espo,qsiexpq,aexpp,dikm,dikm2,dikm3,dikm4,dikm5,i,j,k,itypik) reduction(+:energy)
    do 10 i=1,Nat3D
       eri=0.d0
       x_i = myx(3*i-2)
       y_i = myx(3*i-1)
       z_i = myx(3*i)
#ifdef _OPENMP
       do 20 k= 1, Nat3D
          IF(k.ne.i) THEN!!!ompdifference
             if((itype(i).eq.1).and.(itype(k).eq.1)) then
                itypik=1   ! stesso metallo A
             else if((itype(i).eq.2).and.(itype(k).eq.2)) then
                itypik=2   ! stesso metallo B
             else
                itypik=3  ! interazione A-B
             endif

             xik=myx(3*k-2)-x_i
             yik=myx(3*k-1)-y_i
             zik=myx(3*k)  -z_i

             dik2=xik*xik+yik*yik+zik*zik
             If (dik2.lt.cutoff_end2(itypik)) Then
                nvois(i) = nvois(i)+1
                ivois(nvois(i),i) = k
                dik=dsqrt(dik2)
                if (dik.lt.cutoff_start(itypik)) then
                   dik0=dist(itypik)
                   espo=1.d0-dik/dik0
                   aexpp=dexp(p(itypik)*espo)*a(itypik)
                   for=(2.d0*p(itypik)/dik0)*aexpp
                   eri=eri+aexpp!!!ompdifference
                   qsiexpq = (qsi(itypik)*qsi(itypik)) * dexp(2.d0*q(itypik)*espo)
                   !fb(j,i) so this won't run on a "distant" address
                   fb(nvois(i),i) = (q(itypik)/dik0)*qsiexpq;
                else
                   dikm=dik-cutoff_end(itypik)
                   dikm2=dikm*dikm
                   dikm3=dikm2*dikm
                   dikm4=dikm3*dikm
                   dikm5=dikm4*dikm
                   qsiexpq =x5(itypik)*dikm5+x4(itypik)*dikm4+x3(itypik)*dikm3
                   for=-2.d0*(5.d0*a5(itypik)*dikm4+4.d0*a4(itypik)*dikm3+3.d0*a3(itypik)*dikm2)
                   eri=eri+(a5(itypik)*dikm5+a4(itypik)*dikm4+a3(itypik)*dikm3)!!!ompdifference
                   fb(nvois(i),i) = -((5.d0*x5(itypik)*dikm4+4.d0*x4(itypik)*dikm3+3.d0*x3(itypik)*dikm2))*qsiexpq;
                   qsiexpq = qsiexpq**2
                endif
                den(i)=qsiexpq+den(i)
                forsudik=for/dik

                frx(3*i-2)=frx(3*i-2)+forsudik*xik
                frx(3*i-1)=frx(3*i-1)+forsudik*yik
                frx(3*i)  =frx(3*i)  +forsudik*zik
             EndIf
          ENDIF

20     enddo
#else
       do 21 k= i+1, Nat3D
          if((itype(i).eq.1).and.(itype(k).eq.1)) then
             itypik=1   ! stesso metallo A
          else if((itype(i).eq.2).and.(itype(k).eq.2)) then
             itypik=2   ! stesso metallo B
          else
             itypik=3  ! interazione A-B
          endif

          xik=myx(3*k-2)-x_i
          yik=myx(3*k-1)-y_i
          zik=myx(3*k)  -z_i

          dik2=xik*xik+yik*yik+zik*zik
          If (dik2.lt.cutoff_end2(itypik)) Then
             nvois(i) = nvois(i)+1
             ivois(nvois(i),i) = k
             dik=dsqrt(dik2)
             if (dik.lt.cutoff_start(itypik)) then
                dik0=dist(itypik)
                espo=1.d0-dik/dik0
                aexpp=dexp(p(itypik)*espo)*a(itypik)
                for=(2.d0*p(itypik)/dik0)*aexpp
                eri=eri+2.d0*aexpp!!!ompdifference
                qsiexpq = (qsi(itypik)*qsi(itypik)) * dexp(2.d0*q(itypik)*espo)
                !fb(j,i) so this won't run on a "distant" address
                fb(nvois(i),i) = (q(itypik)/dik0)*qsiexpq;
             else
                dikm=dik-cutoff_end(itypik)
                dikm2=dikm*dikm
                dikm3=dikm2*dikm
                dikm4=dikm3*dikm
                dikm5=dikm4*dikm
                qsiexpq =x5(itypik)*dikm5+x4(itypik)*dikm4+x3(itypik)*dikm3
                for=-2.d0*(5.d0*a5(itypik)*dikm4+4.d0*a4(itypik)*dikm3+3.d0*a3(itypik)*dikm2)
                eri=eri+2*(a5(itypik)*dikm5+a4(itypik)*dikm4+a3(itypik)*dikm3)!!!ompdifference
                fb(nvois(i),i) = -((5.d0*x5(itypik)*dikm4+4.d0*x4(itypik)*dikm3+3.d0*x3(itypik)*dikm2))*qsiexpq;
                qsiexpq = qsiexpq**2
             endif
             den(i)=qsiexpq+den(i)
             den(k)=qsiexpq+den(k)!!!ompdifference
             forsudik=for/dik

             frx(3*i-2)=frx(3*i-2)+forsudik*xik
             frx(3*i-1)=frx(3*i-1)+forsudik*yik
             frx(3*i)  =frx(3*i)  +forsudik*zik

             frx(3*k-2)=frx(3*k-2)-forsudik*xik!!!ompdifference
             frx(3*k-1)=frx(3*k-1)-forsudik*yik!!!ompdifference
             frx(3*k)  =frx(3*k)  -forsudik*zik!!!ompdifference

          EndIf

21     enddo
#endif
       ebi=dsqrt(den(i))
       den(i)=1.d0/ebi
       !eneri=eri-ebi
       !ener_atom(i)=eneri
       energy=energy+(eri-ebi)
       !write(*,*)i,nvois(i)
10  enddo
!$OMP end parallel do

#ifndef _OPENMP
    do 31 i=1,Nat3D-1!i'm pretty sure that won't exist an atom with a greater index than Nat3D
       x_i = myx(3*i-2)
       y_i = myx(3*i-1)
       z_i = myx(3*i)
       IF(nvois(i) .gt. 0) THEN !exist neighbours
          do 41 j=1,nvois(i)
             k=ivois(j,i)

             xik=myx(3*k-2)-x_i
             yik=myx(3*k-1)-y_i
             zik=myx(3*k)  -z_i

             dik=dsqrt(xik*xik+yik*yik+zik*zik)
             !fb(j,i) so this won't run on a "distant" address
             denik=fb(j,i)*(den(i)+den(k))/dik

             frx(3*i-2)=frx(3*i-2)-denik*xik
             frx(3*i-1)=frx(3*i-1)-denik*yik
             frx(3*i)  =frx(3*i)  -denik*zik

             frx(3*k-2)=frx(3*k-2)+denik*xik
             frx(3*k-1)=frx(3*k-1)+denik*yik
             frx(3*k)  =frx(3*k)  +denik*zik

41        enddo
       ENDIF ! no neighbours
31  enddo
#else
    !$OMP parallel do default(shared) private(x_i,y_i,z_i,xik,yik,zik,dik,denik,i,k,j)
    do 30 i=1,Nat3D
       x_i = myx(3*i-2)
       y_i = myx(3*i-1)
       z_i = myx(3*i)
       IF(nvois(i) .gt. 0) THEN !exist neighbours
          do 40 j=1,nvois(i)
             k=ivois(j,i)

             xik=myx(3*k-2)-x_i
             yik=myx(3*k-1)-y_i
             zik=myx(3*k)  -z_i

             dik=dsqrt(xik*xik+yik*yik+zik*zik)
             !fb(j,i) so this won't run on a "distant" address
             denik=fb(j,i)*(den(i)+den(k))/dik

             frx(3*i-2)=frx(3*i-2)-denik*xik
             frx(3*i-1)=frx(3*i-1)-denik*yik
             frx(3*i)  =frx(3*i)  -denik*zik
40        enddo
       ENDIF ! no neighbours
30  enddo
    !$OMP end parallel do

#endif

  END SUBROUTINE BHgradient

  Real(C_DOUBLE) FUNCTION BHenergy(myx)BIND(C,name="BHenergy")

!!!!!!!
!!!!!!!NB: forces are returned with inverted sign (gradient)
!!!!!!!
    use iso_c_binding!in realta` e` uno statement per fortran 2003 ma se compila non mi lamento
    !USE PARAMETERS
!!$  USE BH_MOD, Only: Nat3D
!!$  USE FUNCTION_PAR, Only:p,q,a,qsi,a5,a4,a3,x5,x4,x3,&
!!$       dist,cutoff_start,cutoff_end,&
!!$       itype,ivois,nvois,cutoff_end2
    implicit none
    !inout

    Real(C_DOUBLE),intent(in) ::myx(3*Nat3D)
    Real(C_DOUBLE) :: energy
    ! Local variables
    Real(C_DOUBLE) :: den(Nat3D)
    Real(C_DOUBLE) :: ebi,eri
    !Real(C_DOUBLE) :: xik(Nat3D,Nat3D),yik(Nat3D,Nat3D),zik(Nat3D,Nat3D),d(Nat3D,Nat3D)
    Real(C_DOUBLE) :: xik,yik,zik,dik,dik2,x_i,y_i,z_i
    Real(C_DOUBLE) :: dik0, espo, qsiexpq, aexpp
    Real(C_DOUBLE) :: dikm,dikm2,dikm3,dikm4,dikm5
    Integer :: i,k,itypik

    !f(dabs(zat(1))>3275786477704)write(*,*)'0000000000 zat(1)=',zat(1)
!!$  write(*,*)'gradient_rgl'
!!$  write(*,*)'cutoff_start ',cutoff_start
!!$  write(*,*)'cutoff_end ',cutoff_end
!!$  write(*,*)'dist ',dist
!!$
!!$  write(*,*)'a3:',a3
!!$  write(*,*)'a4:',a4
!!$  write(*,*)'a5:',a5
!!$  write(*,*)'x3:',x3
!!$  write(*,*)'x4:',x4
!!$  write(*,*)'x5:',x5
!!$  write(*,*)Nat3D
!!$  stop
    !     ener_debug = energy
    energy=0.d0
    !fortran can do array operation by itself
    den=0.d0
!!$  do  i=1,Nat3D
!!$     write(*,*)i,",",nvois(i),":",ivois(1:nvois(i),i)
!!$  enddo
!!$  stop
    nvois = 0

    !$OMP PARALLEL DO default(shared) PRIVATE(ebi,eri,for,forsudik,denik,xik,yik,zik,dik,dik2,x_i,y_i,z_i,&
    !$OMP &dik0,espo,qsiexpq,aexpp,dikm,dikm2,dikm3,dikm4,dikm5,i,k,itypik) reduction(+:energy)
    do 10 i=1,Nat3D
       eri=0.d0
       x_i = myx(3*i-2)
       y_i = myx(3*i-1)
       z_i = myx(3*i)
#ifdef _OPENMP
       do 20 k= 1, Nat3D
          IF(k.ne.i) THEN!!!ompdifference
             if((itype(i).eq.1).and.(itype(k).eq.1)) then
                itypik=1   ! stesso metallo A
             else if((itype(i).eq.2).and.(itype(k).eq.2)) then
                itypik=2   ! stesso metallo B
             else
                itypik=3  ! interazione A-B
             endif

             xik=myx(3*k-2)-x_i
             yik=myx(3*k-1)-y_i
             zik=myx(3*k)  -z_i

             dik2=xik*xik+yik*yik+zik*zik
             If (dik2.lt.cutoff_end2(itypik)) Then
                nvois(i) = nvois(i)+1
                ivois(nvois(i),i) = k
                dik=dsqrt(dik2)
                if (dik.lt.cutoff_start(itypik)) then
                   dik0=dist(itypik)
                   espo=1.d0-dik/dik0
                   aexpp=dexp(p(itypik)*espo)*a(itypik)
                   eri=eri+aexpp!!!ompdifference
                   qsiexpq = (qsi(itypik)*qsi(itypik)) * dexp(2.d0*q(itypik)*espo)
                else
                   dikm=dik-cutoff_end(itypik)
                   dikm2=dikm*dikm
                   dikm3=dikm2*dikm
                   dikm4=dikm3*dikm
                   dikm5=dikm4*dikm
                   qsiexpq =x5(itypik)*dikm5+x4(itypik)*dikm4+x3(itypik)*dikm3
                   eri=eri+(a5(itypik)*dikm5+a4(itypik)*dikm4+a3(itypik)*dikm3)!!!ompdifference
                   qsiexpq = qsiexpq**2
                endif
                den(i)=qsiexpq+den(i)
             EndIf
          ENDIF

20     enddo
#else
       do 21 k= i+1, Nat3D
          if((itype(i).eq.1).and.(itype(k).eq.1)) then
             itypik=1   ! stesso metallo A
          else if((itype(i).eq.2).and.(itype(k).eq.2)) then
             itypik=2   ! stesso metallo B
          else
             itypik=3  ! interazione A-B
          endif

          xik=myx(3*k-2)-x_i
          yik=myx(3*k-1)-y_i
          zik=myx(3*k)  -z_i

          dik2=xik*xik+yik*yik+zik*zik
          If (dik2.lt.cutoff_end2(itypik)) Then
             nvois(i) = nvois(i)+1
             ivois(nvois(i),i) = k
             dik=dsqrt(dik2)
             if (dik.lt.cutoff_start(itypik)) then
                dik0=dist(itypik)
                espo=1.d0-dik/dik0
                aexpp=dexp(p(itypik)*espo)*a(itypik)
                eri=eri+2.d0*aexpp!!!ompdifference
                qsiexpq = (qsi(itypik)*qsi(itypik)) * dexp(2.d0*q(itypik)*espo)
             else
                dikm=dik-cutoff_end(itypik)
                dikm2=dikm*dikm
                dikm3=dikm2*dikm
                dikm4=dikm3*dikm
                dikm5=dikm4*dikm
                qsiexpq =x5(itypik)*dikm5+x4(itypik)*dikm4+x3(itypik)*dikm3
                eri=eri+2*(a5(itypik)*dikm5+a4(itypik)*dikm4+a3(itypik)*dikm3)!!!ompdifference
                qsiexpq = qsiexpq**2
             endif
             den(i)=qsiexpq+den(i)
             den(k)=qsiexpq+den(k)!!!ompdifference
          EndIf

21     enddo
#endif
       ebi=dsqrt(den(i))
       den(i)=1.d0/ebi
       !eneri=eri-ebi
       !ener_atom(i)=eneri
       energy=energy+(eri-ebi)
       !write(*,*)i,nvois(i)
10  enddo
    BHenergy=energy
  END FUNCTION BHenergy

  !!returns the forces
  SUBROUTINE BHforces(myx,frx)bind(C,name="BHforces")
    use iso_c_binding!in realta` e` uno statement per fortran 2003 ma se compila non mi lamento
    implicit none
    !inout
    Real(C_DOUBLE),intent(in) ::myx(3*Nat3D)
    Real(C_DOUBLE),intent(out)::frx(3*Nat3D)
    ! Local variables
    Real(C_DOUBLE) :: den(Nat3D)
    Real(C_DOUBLE) :: ebi,for,forsudik,denik
#ifndef _OPENMP
    Real(C_DOUBLE) :: fb(Nat3D-1,Nat3D-1)
#else
    Real(C_DOUBLE) :: fb(Nat3D-1,Nat3D)
#endif
    Real(C_DOUBLE) :: xik,yik,zik,dik,dik2,x_i,y_i,z_i
    Real(C_DOUBLE) :: dik0, espo, qsiexpq, aexpp
    Real(C_DOUBLE) :: dikm,dikm2,dikm3,dikm4,dikm5
    Integer :: i,j,k,itypik
    !fortran can do array operation by itself
    den=0.d0
    frx=0.d0
    nvois = 0

    !$OMP PARALLEL DO default(shared) PRIVATE(ebi,for,forsudik,denik,xik,yik,zik,dik,dik2,x_i,y_i,z_i,&
    !$OMP &dik0,espo,qsiexpq,aexpp,dikm,dikm2,dikm3,dikm4,dikm5,i,j,k,itypik)
    do 10 i=1,Nat3D
       x_i = myx(3*i-2)
       y_i = myx(3*i-1)
       z_i = myx(3*i)
#ifdef _OPENMP
       do 20 k= 1, Nat3D
          IF(k.ne.i) THEN!!!ompdifference
             if((itype(i).eq.1).and.(itype(k).eq.1)) then
                itypik=1   ! stesso metallo A
             else if((itype(i).eq.2).and.(itype(k).eq.2)) then
                itypik=2   ! stesso metallo B
             else
                itypik=3  ! interazione A-B
             endif

             xik=myx(3*k-2)-x_i
             yik=myx(3*k-1)-y_i
             zik=myx(3*k)  -z_i

             dik2=xik*xik+yik*yik+zik*zik
             If (dik2.lt.cutoff_end2(itypik)) Then
                nvois(i) = nvois(i)+1
                ivois(nvois(i),i) = k
                dik=dsqrt(dik2)
                if (dik.lt.cutoff_start(itypik)) then
                   dik0=dist(itypik)
                   espo=1.d0-dik/dik0
                   aexpp=dexp(p(itypik)*espo)*a(itypik)
                   for=(2.d0*p(itypik)/dik0)*aexpp
                   qsiexpq = (qsi(itypik)*qsi(itypik)) * dexp(2.d0*q(itypik)*espo)
                   fb(nvois(i),i) = (q(itypik)/dik0)*qsiexpq;
                else
                   dikm=dik-cutoff_end(itypik)
                   dikm2=dikm*dikm
                   dikm3=dikm2*dikm
                   dikm4=dikm3*dikm
                   dikm5=dikm4*dikm
                   qsiexpq =x5(itypik)*dikm5+x4(itypik)*dikm4+x3(itypik)*dikm3
                   for=-2.d0*(5.d0*a5(itypik)*dikm4+4.d0*a4(itypik)*dikm3+3.d0*a3(itypik)*dikm2)
                   fb(nvois(i),i) = -((5.d0*x5(itypik)*dikm4+4.d0*x4(itypik)*dikm3+3.d0*x3(itypik)*dikm2))*qsiexpq;
                   qsiexpq = qsiexpq**2
                endif
                den(i)=qsiexpq+den(i)
                forsudik=for/dik

                frx(3*i-2)=frx(3*i-2)-forsudik*xik
                frx(3*i-1)=frx(3*i-1)-forsudik*yik
                frx(3*i)  =frx(3*i)  -forsudik*zik
             EndIf
          ENDIF

20     enddo
#else
       do 21 k= i+1, Nat3D
          if((itype(i).eq.1).and.(itype(k).eq.1)) then
             itypik=1   ! stesso metallo A
          else if((itype(i).eq.2).and.(itype(k).eq.2)) then
             itypik=2   ! stesso metallo B
          else
             itypik=3  ! interazione A-B
          endif

          xik=myx(3*k-2)-x_i
          yik=myx(3*k-1)-y_i
          zik=myx(3*k)  -z_i

          dik2=xik*xik+yik*yik+zik*zik
          if (dik2.lt.cutoff_end2(itypik)) then
             nvois(i) = nvois(i)+1
             ivois(nvois(i),i) = k
             dik=dsqrt(dik2)
             if (dik.lt.cutoff_start(itypik)) then
                dik0=dist(itypik)
                espo=1.d0-dik/dik0
                aexpp=dexp(p(itypik)*espo)*a(itypik)
                for=(2.d0*p(itypik)/dik0)*aexpp
                qsiexpq = (qsi(itypik)*qsi(itypik)) * dexp(2.d0*q(itypik)*espo)
                fb(nvois(i),i) = (q(itypik)/dik0)*qsiexpq;
             else
                dikm=dik-cutoff_end(itypik)
                dikm2=dikm*dikm
                dikm3=dikm2*dikm
                dikm4=dikm3*dikm
                dikm5=dikm4*dikm
                qsiexpq =x5(itypik)*dikm5+x4(itypik)*dikm4+x3(itypik)*dikm3
                for=-2.d0*(5.d0*a5(itypik)*dikm4+4.d0*a4(itypik)*dikm3+3.d0*a3(itypik)*dikm2)
                fb(nvois(i),i) = -((5.d0*x5(itypik)*dikm4+4.d0*x4(itypik)*dikm3+3.d0*x3(itypik)*dikm2))*qsiexpq;
                qsiexpq = qsiexpq**2
             endif
             den(i)=qsiexpq+den(i)
             den(k)=qsiexpq+den(k)

             forsudik=for/dik

             frx(3*i-2)=frx(3*i-2)-forsudik*xik
             frx(3*i-1)=frx(3*i-1)-forsudik*yik
             frx(3*i)  =frx(3*i)  -forsudik*zik

             frx(3*k-2)=frx(3*k-2)+forsudik*xik
             frx(3*k-1)=frx(3*k-1)+forsudik*yik
             frx(3*k)  =frx(3*k)  +forsudik*zik
          endif
21     enddo
#endif
       ebi=dsqrt(den(i))
       den(i)=1.d0/ebi
10  enddo

#ifndef _OPENMP
    do 31 i=1,Nat3D-1!i'm pretty sure that won't extist an atom with a greather index than Nat3D
       x_i = myx(3*i-2)
       y_i = myx(3*i-1)
       z_i = myx(3*i)
       IF(nvois(i) .gt. 0) THEN !exist neighbours
          do 41 j=1,nvois(i)
             k=ivois(j,i)

             xik=myx(3*k-2)-x_i
             yik=myx(3*k-1)-y_i
             zik=myx(3*k)  -z_i

             dik=dsqrt(xik*xik+yik*yik+zik*zik)

             denik=fb(j,i)*(den(i)+den(k))/dik

             frx(3*i-2)=frx(3*i-2)+denik*xik
             frx(3*i-1)=frx(3*i-1)+denik*yik
             frx(3*i)  =frx(3*i)  +denik*zik

             frx(3*k-2)=frx(3*k-2)-denik*xik
             frx(3*k-1)=frx(3*k-1)-denik*yik
             frx(3*k)  =frx(3*k)  -denik*zik
41        enddo
       ENDIF ! no neighbours
31  enddo
#else
    !$OMP parallel do default(shared) private(x_i,y_i,z_i,xik,yik,zik,dik,denik,i,k,j)
    do 30 i=1,Nat3D
       x_i = myx(3*i-2)
       y_i = myx(3*i-1)
       z_i = myx(3*i)
       IF(nvois(i) .gt. 0) THEN !exist neighbours
          do 40 j=1,nvois(i)
             k=ivois(j,i)

             xik=myx(3*k-2)-x_i
             yik=myx(3*k-1)-y_i
             zik=myx(3*k)  -z_i

             dik=dsqrt(xik*xik+yik*yik+zik*zik)

             denik=fb(j,i)*(den(i)+den(k))/dik

             frx(3*i-2)=frx(3*i-2)+denik*xik
             frx(3*i-1)=frx(3*i-1)+denik*yik
             frx(3*i)  =frx(3*i)  +denik*zik
40        enddo
       ENDIF ! no neighbours
30  enddo
    !$OMP end parallel do
#endif
  END SUBROUTINE BHforces

END MODULE BH_MODULE
