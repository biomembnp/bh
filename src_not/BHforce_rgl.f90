Real(C_DOUBLE) FUNCTION BHenergy(N,myx,myy,myz,frx,fry,frz) BIND(C,name="BHenergy")
!!!!!!!NB: forces are returned with inverted sign
  use iso_c_binding!in realta` e` uno statement per fortran 2003 ma se compila non mi lamento
  !USE PARAMETERS
  USE FUNCTION_PAR, Only:p,q,a,qsi,a5,a4,a3,x5,x4,x3,&
       dist,cutoff_start,cutoff_end,&
       itype,ivois,nvois!,&
       !ener_atom,emetal,ener,&
       !dispx,dispy,dispz
  !USE BH_MOD
  implicit none
  !inout
  Integer(C_INT),intent(in) ::N
  Real(C_DOUBLE),intent(in) ::myx(N),myy(N),myz(N)
  Real(C_DOUBLE),intent(out)::frx(N),fry(N),frz(N)
  ! Local variables
  Real(8) :: ener
  Real(8) :: den(N)
  Real(8) :: ebi,eri,eneri,for,forsudik,denik
  Real(8) :: fb(N,N)
  Real(8) :: xik(N,N),yik(N,N),zik(N,N),d(N,N)
  Real(8) :: dik0, espo, qsiexpq, aexpp
  Real(8) :: dikm,dikm2,dikm3,dikm4,dikm5
  Integer :: i,j,k,itypik

  !f(dabs(zat(1))>3275786477704)write(*,*)'0000000000 zat(1)=',zat(1)
!!$  write(*,*)'force_rgl'
!!$  write(*,*)'cutoff_start ',cutoff_start
!!$  write(*,*)'cutoff_end ',cutoff_end
!!$  write(*,*)'dist ',dist
!!$
!!$  write(*,*)a3
!!$  write(*,*)a4
!!$  write(*,*)a5
!!$  write(*,*)x3
!!$  write(*,*)x4
!!$  write(*,*)x5
!!$  write(*,*)N
!!$write(*,*)myx,myy,myz
  !     ener_debug = ener
  ener=0.d0

  do i=1,N
!!$     dfmyx(i)=fx(i)
!!$     dfy(i)=fy(i)
!!$     dfz(i)=fz(i)
     den(i)=0.d0
     frx(i)=0.d0
     fry(i)=0.d0
     frz(i)=0.d0
  enddo

!!$  do  i=1,N
!!$     write(*,*)i,",",nvois(i),":",ivois(1:nvois(i),i)
!!$  enddo
!!$  stop
  do 10 i=1,N

     ebi=0.d0
     eri=0.d0
     eneri=0.d0
     !write(*,*)i,nvois(i)
     IF(nvois(i).gt.0) THEN
        do 20 j=1,nvois(i)
           k=ivois(j,i)
           if(k.gt.i) then
              if((itype(i).eq.1).and.(itype(k).eq.1)) then
                 itypik=1   ! stesso metallo A
              else if((itype(i).eq.2).and.(itype(k).eq.2)) then
                 itypik=2   ! stesso metallo B
              else
                 itypik=3  ! interazione A-B
              endif
              dik0=dist(itypik)

              xik(k,i)=myx(k)-myx(i)
              yik(k,i)=myy(k)-myy(i)
              zik(k,i)=myz(k)-myz(i)
              !dispx(j,i)=xik(k,i)
              !dispy(j,i)=yik(k,i)
              !dispz(j,i)=zik(k,i)
              d(k,i)=dsqrt(xik(k,i)*xik(k,i)+yik(k,i)*yik(k,i)+zik(k,i)*zik(k,i))
              if (d(k,i).lt.cutoff_start(itypik)) then
                 espo=1.d0-d(k,i)/dik0
                 aexpp=dexp(p(itypik)*espo)*a(itypik)
                 for=(2.d0*p(itypik)/dik0)*aexpp
                 eri=eri+2.d0*aexpp
                 qsiexpq = (qsi(itypik)*qsi(itypik)) * dexp(2.d0*q(itypik)*espo)
                 fb(k,i) = (q(itypik)/dik0)*qsiexpq;
              else
                 dikm=d(k,i)-cutoff_end(itypik)
                 dikm2=dikm*dikm
                 dikm3=dikm2*dikm
                 dikm4=dikm3*dikm
                 dikm5=dikm4*dikm
                 qsiexpq =x5(itypik)*dikm5+x4(itypik)*dikm4+x3(itypik)*dikm3
                 for=-2.d0*(5.d0*a5(itypik)*dikm4+4.d0*a4(itypik)*dikm3+3.d0*a3(itypik)*dikm2)
                 eri=eri+2*(a5(itypik)*dikm5+a4(itypik)*dikm4+a3(itypik)*dikm3)
                 fb(k,i) = -((5.d0*x5(itypik)*dikm4+4.d0*x4(itypik)*dikm3+3.d0*x3(itypik)*dikm2))*qsiexpq;
                 qsiexpq = qsiexpq**2
              endif !k>i
              den(i)=qsiexpq+den(i)
              den(k)=qsiexpq+den(k)

              forsudik=for/d(k,i)

              frx(i)=frx(i)+forsudik*xik(k,i)
              fry(i)=fry(i)+forsudik*yik(k,i)
              frz(i)=frz(i)+forsudik*zik(k,i)

              frx(k)=frx(k)-forsudik*xik(k,i)
              fry(k)=fry(k)-forsudik*yik(k,i)
              frz(k)=frz(k)-forsudik*zik(k,i)
           endif!k>i
20      enddo
        ebi=dsqrt(den(i))
        den(i)=1.d0/ebi
        eneri=eri-ebi
        !ener_atom(i)=eneri
        ener=ener+eneri
!        write(*,*)i,eneri,ebi,eri
     ENDIF !exist neighbours	
     
10 enddo
 ! write(*,*)ener
  BHenergy = ener
  !emetal=ener
  do 30 i=1,N
     IF(nvois(i) .gt. 0) THEN !exist neighbours
        do 40 j=1,nvois(i)
           k=ivois(j,i)
           if(k.gt.i)then
              denik=fb(k,i)*(den(i)+den(k))/d(k,i)

              frx(i)=frx(i)-denik*xik(k,i)
              fry(i)=fry(i)-denik*yik(k,i)
              frz(i)=frz(i)-denik*zik(k,i)

              frx(k)=frx(k)+denik*xik(k,i)
              fry(k)=fry(k)+denik*yik(k,i)
              frz(k)=frz(k)+denik*zik(k,i)
           endif!k>i
40      enddo
     ENDIF ! no neighbours
!!$     fx(i)=frx(i)
!!$     fy(i)=fry(i)
!!$     fz(i)=frz(i)
30 enddo
!!$  if(substrate == 'si') then
!!$     !   call force_met_mgo
!!$     e_met_mgo=0
!!$  else
!!$     e_met_mgo=0
!!$     is_nan = .false.
!!$  endif
  !write(*,*)ener,emetal
!!$  write(*,*)'force_rgl, ener= ',ener
!!$  stop
RETURN

END FUNCTION BHenergy
